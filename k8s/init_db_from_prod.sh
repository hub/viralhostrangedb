#!/bin/bash

echo ${STORAGE_SUFFIX}
echo $NAMESPACE
pwd && ls -lah

export need_init=0

kubectl --namespace=$NAMESPACE get pvc -l branch=branch${STORAGE_SUFFIX},role=front --output jsonpath='{.items[0]}' || export need_init=1

echo $need_init

if [ "$need_init" == "1" ]; then
  # start the pvc
  #envsubst < k8s/kubernetes-storage.yaml | kubectl --namespace=$NAMESPACE apply --wait=true -f - || exit 3

  # start the db
  #envsubst < k8s/manifest-postgres.yaml | kubectl --namespace=$NAMESPACE apply --wait=true -f - || exit 4

  # wait after db fully started
  until kubectl --namespace=$NAMESPACE wait --for=condition=ready pod -l branch=branch${STORAGE_SUFFIX},app=postgres-app --timeout=1s
  do
    date
    sleep 1
    kubectl --namespace=$NAMESPACE get po -l branch=branch${STORAGE_SUFFIX}
  done;

  sleep 30 # bitnami postgres start, say hi, shutdown and restart, so yes it is ugly but we have to

  until kubectl --namespace=$NAMESPACE wait --for=condition=ready pod -l branch=branch${STORAGE_SUFFIX},app=postgres-app --timeout=1s
  do
    date
    sleep 1
    kubectl --namespace=$NAMESPACE get po -l branch=branch${STORAGE_SUFFIX}
  done;

  dev_db=$(kubectl --namespace=$NAMESPACE get po -l branch=branch${STORAGE_SUFFIX},app=postgres-app --output jsonpath='{.items[0].metadata.name}' || echo "")

  echo "Starting to import"

  # Import production db dump into current db
  echo "kubectl --namespace=$NAMESPACE exec -i $dev_db -- psql -U postgres < prod.sql"
  du -sh prod.sql
  # -i, --stdin=false: Pass stdin to the container
  kubectl --namespace=$NAMESPACE exec -i $dev_db -- bash -c "PGPASSWORD=$POSTGRES_PASSWORD psql -U postgres viralhostrangedb" < prod.sql || exit 6

  echo "Import done"
fi
