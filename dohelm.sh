#!/usr/bin/env bash

touch tokens.sh
source ./tokens.sh # put `export SECRET_KEY="..."` in this file

NAMESPACE="viralhostrange-dev"
#VALUES_OVERRIDE_FILENAME="values.prod.yaml" # uncomment for prod
CI_PROJECT_NAMESPACE="hub"
CI_PROJECT_NAME="viralhostrangedb"
CI_REGISTRY="registry-gitlab.pasteur.fr"
CI_REGISTRY_IMAGE="${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/"
CI_COMMIT_SHA=$(git log --format="%H" -n 1)
#CI_COMMIT_SHA="95dc275b2b161231d6630be8860c2bcf0eeabf92"
CI_COMMIT_REF_SLUG=$(git branch --show | cut -c1-63 | sed 's/-*$//g')
REL_NAME=$CI_COMMIT_REF_SLUG
REL_NAME=$([[ $REL_NAME == [0-9]* ]] && echo "b${REL_NAME}" || echo $REL_NAME)
REL_NAME=$(echo $REL_NAME | cut -c1-53)
CHART_LOCATION="chart"
DEBUG="True"
PUBLICLY_OPEN="False"
IS_PROD="False"

export ACTION="upgrade --install"
#export ACTION="template --debug"

helm ${ACTION} --namespace=${NAMESPACE} \
    --set commitSha=${CI_COMMIT_SHA} \
    --set registry=${CI_REGISTRY_IMAGE}${CI_COMMIT_REF_SLUG} \
    --set biopython.ncbi.email=${NCBI_EMAIL} \
    --set biopython.ncbi.apiKey=${NCBI_API_KEY} \
    --values ./chart/values.yaml \
    --values ./chart/${VALUES_OVERRIDE_FILENAME:-values.yaml} \
    ${REL_NAME} ./${CHART_LOCATION}/
