.. Viral Host Range database documentation master file, created by
   sphinx-quickstart on Mon Oct 14 09:01:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

VHRdb documentation
===================

You found a bug ? Please submit an issue in the `issue tracking system <https://gitlab.pasteur.fr/hub/viralhostrangedb/issues>`_

You have a question ? Please send us an email at viralhostrangedb at pasteur.fr.

A good way to start using the VRHdb is to look at existing data through the :ref:`Explore interface<explore-interface>`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   explore
   data_import
   search_tool
   compatible_file
   virus_host_identifiers
   permissions
   history
   api
   admin
   glossary
