.. _api:

API access to data
===========================================

The explore page uses the API to gather all the information that is rendered. You also can access to the data using the API, all that you can see. Per data sources :term:`responses <response>` are available, so does aggregated responses. Note that presented responses are the responses within the :term:`global scheme <global scheme>`, not the :term:`raw responses <raw response>`. The responses provided within the API can be interpreted by querying the api to list all the possible values.

Global scheme responses values
------------------------------------------

URL: https://viralhostrangedb.pasteur.cloud/api/global-response/?format=json

.. code-block:: json
   :caption: Values responses can have

   [
      {
         "name": "No infection",
         "description": "",
         "value": 0,
         "color": "#FF0000"
      },
      {
         "name": "Intermediate",
         "description": "",
         "value": 1,
         "color": "#ff8000"
      },
      {
         "name": "Infection",
         "description": "",
         "value": 2,
         "color": "#00FF00"
      },
      ...
   ]

Aggregated responses
------------------------------------------

URL: https://viralhostrangedb.pasteur.cloud/api/aggregated-responses/?allow_overflow=true&format=json

.. code-block:: json
   :caption: Complet responses


    {
      "254": {
        "1216": {
          "val": 2,
          "diff": 1
        },
        ...
      }
      ...
    }

The format of the data is virus_id>host_id>value and count. First entry should be read as "1 response is documented for the infection of host 1216 by virus 254, and have average value is 2"

Per data source responses
------------------------------------------

URL: https://viralhostrangedb.pasteur.cloud/api/responses/?allow_overflow=true&format=json

.. code-block:: json
   :caption: Complet responses


    {
      "254": {
        "1216": {
          "153": 2
          ...
        },
        ...
      }
      ...
    }

The format of the data is virus_id>host_id>datasource_id>value. First entry should be read as "in data source 153, the response of host 1216 to virus 254 is 2.

Virus
------------------------------------------

You can get virus one by one or all at once:

* One by one: https://viralhostrangedb.pasteur.cloud/api/virus/913/?format=json
* All: https://viralhostrangedb.pasteur.cloud/api/virus/?format=json


Host
------------------------------------------

You can get hosts one by one or all at once:

* One by one: https://viralhostrangedb.pasteur.cloud/api/host/3592/?format=json
* All: https://viralhostrangedb.pasteur.cloud/api/host/?format=json

Data source
------------------------------------------

You can get data sources one by one or all at once:

* One by one: https://viralhostrangedb.pasteur.cloud/api/data-source/150/?format=json
* All: https://viralhostrangedb.pasteur.cloud/api/data-source/?format=json

Accessing private data
------------------------------------------

To access to your private data, go to the website and get the cookies named ``csrftoken`` and ``sessionid``, then provide them as header/cookie to a curl GET request:

.. code-block:: bash
   :caption: cURL call to access its private data

   CSRF_TOKEN=not-so-secret-token
   SESSION_ID=not-so-secret-session-id

   curl -H "X-CSRFToken: ${CSRF_TOKEN}" \
        --cookie "csrftoken=${CSRF_TOKEN};sessionid=${SESSION_ID}" \
        https://viralhostrangedb.pasteur.cloud/api/data-source/1/?format=json
