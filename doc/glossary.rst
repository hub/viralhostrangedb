Glossary
=============================================

.. Glossary::

   data source
      A **data source** regroups the results of interaction tests between :term:`viruses <virus>` and :term:`hosts <host>`. Data sources have associated meta data such as the owner, the life domain for the hosts, and the description of the data.

   virus
      Any **virus** should be named according the literature. When the genome of a virus is available in the NCBI nucleotide databank, the corresponding :term:`identifier` should be specified. Example: virus ``T4`` and its NCBI identifier:``T4 (NC_000866.4)``. You can also provide its :ref:`HER Number <virus-host-identifiers-label>`: ``T4 (NC_000866.4; HER:27)``.

   host
      Any **host** should be named according to the literature. When the genome of a host is available in the NCBI nucleotide databank, the corresponding :term:`identifier` should be specified. Example: ``E. coli MG1655`` and its NCBI identifier: ``E. coli MG1655 (NC_000913.3)``.

   response
      A **response** corresponds to the result of the interaction test between one :term:`virus` and one :term:`host`. In the VHRdb, responses are digits that correspond to the :term:`global scheme`.

   raw response
      Each user/lab has its own way to record the output of host range tests, from basic two-state (infection/no infection) to more detailed information such as numerical values corresponding to efficiency of plating calculations. A **raw response** is the value that was in the file when it was imported.

   global scheme
      The :term:`responses <response>` in the VHRdb can only take 3 values corresponding to the VHRdb's **Global scheme**. This scheme classifies virus-host interactions in 3 states : "No infection", corresponding to value "0", "Intermediate", corresponding to value "1" and "Infection", corresponding to value "2".
    
   identifier
      An **identifier** is an additional information related to a :term:`virus` or a :term:`host`. Identifiers can currently be NCBI identifiers, HER identifiers, or custom identifiers. Identifiers are optional, but recommended when available. More information in :ref:`Identifying viruses and hosts<virus-host-identifiers-label>`.