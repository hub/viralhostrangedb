.. _explore-interface:

Explore
================

In the exploration page you can explore available data by selecting :term:`data sources<data source>`,  :term:`viruses <virus>` or :term:`hosts <host>`.
When narrowing your exploration, a key notion to understand is `What is a relevant host/virus/data source`_.
Use the :ref:`advanced options<advanced-options>` tabs to adjust :ref:`filtering criteria<data-filtering>`, :ref:`rendering options<rendering>` and to apply :ref:`analysis tools<analysis-tools>`.


.. figure:: _static/explore-ui.png
  :width: 100%
  :alt: Main interface to explore the responses
  :figclass: align-center

  Main interface to explore the responses

.. _selecting-comparing:

Selecting and comparing data sources, viruses and hosts
------------------------------------------------------------------

The Explore interface has 3 main tabs : "Data source", "Virus" and "Host". Users can click on any of these tabs in order to browse the content of each section. A search interface is integrated in each tab for quick browsing.
To visualize data, users must click on the element they want to visualize (this element can be a :term:`data source`, a :term:`virus` or a :term:`host`).
Users can then click to add additional elements to visualize simultaneously, or click on already selected elements to remove them from the current visualization. This way, it is possible to compare multiple data sources or restrict your visualization to specific hosts or viruses.

By default, when selecting one element, all data relevant to this element will be visualized (see section :ref:`What is a relevant host/virus/data source<what-is-a-relevant-host-virus-data-source>`). When clicking on a tab of the "relevant elements" (e.g. "Virus" if an "Host" was selected), only the list of relevant elements will appear. They will appear unselected even though they are printed on screen. This is to allow users to click on them in order to restrict the visualization to these elements. Conversely, if users want remove a few relevant elements from the current view, they can either :

- click on "All", and then de-select each unwanted element.
- click on them (i.e select unwanted element), and then click "Inverse" to invert the selection, therefore selecting all the elements but these. Note that this approach is the less CPU consuming way to do it.

.. _what-is-a-relevant-host-virus-data-source:

What is a relevant host/virus/data source
-----------------------------------------

A :term:`host`/:term:`virus`/:term:`data source` is relevant if it has at least one :term:`response` associated with at least one of the selected elements. If you selected a data source, the relevant :term:`viruses <virus>` and :term:`hosts <host>` are only those present in this :term:`data source`. If you select a :term:`virus`, a relevant :term:`host` is a :term:`host` that has this :term:`host` has been tested for the selected :term:`virus` in at least one of the public :term:`data source` available in the VHRdb. Any :term:`data source` where this :term:`response` can be found is also relevant.


Example
~~~~~~~

- :term:`data source` ``datasource_1``     documents the :term:`response` of :term:`host` ``E. coli``     to :term:`virus` ``T4``     and :term:`virus` ``T7``
- :term:`data source` ``datasource_2``     documents the :term:`response` of :term:`host` ``E. coli``     to :term:`virus` ``P1``    .

+--------------------------------+--------------------------------------------------------------+
| Element(s) selected            | Element(s) relevant                                          |
+================================+==============================================================+
| ``datasource_1``               |     ``T4``,     ``T7``, ``E. coli``                          |
+--------------------------------+--------------------------------------------------------------+
| ``E. coli``                    | ``datasource_1``, ``datasource_2``, ``T4``, ``T7``, ``P1``   |
+--------------------------------+--------------------------------------------------------------+
| ``E. coli``     and ``T4``     | ``datasource_1``                                             |
+--------------------------------+--------------------------------------------------------------+
| ``T7`` and ``datasource_2``    | none                                                         |
+--------------------------------+--------------------------------------------------------------+

.. _advanced-options:

Advanced options
---------------------------------
There are three sets of advanced options: `Analysis Tools`_, `Data filtering`_ and `Rendering`_.


All selected options are stack-up under the selection area, as shown below, allowing users to easily remove them. These options will also be displayed when sending the web page using link sharing.

.. image:: _static/option-stacked.png
  :width: 100%
  :alt: option stacked


Quick tip
~~~~~~~~~~~~~~~~~~~~~


To keep the `Advanced options`_ panel opened, just click on the lock.



.. image:: _static/advanced-options-lock.png
  :alt: advanced-options-lock
  :align: center

.. _analysis-tools:

Analysis Tools
~~~~~~~~~~~~~~~~~~~~~
- | **Consider all positive responses as an infection**
  | Users can choose how "intermediate" :term:`responses <response>` should be considered for binary analysis. If this option is selected, "intermediate" :term:`responses <response>` (value = "1")  will be considered as an infection (value = "2"). By default, "intermediate" :term:`responses <response>` are considered as "no infection" (value = "0"). This is taken into account for the calculations in other options such as "show the infection ratio".

- | **Show the infection ratio for viruses**
  | Users can calculate the percentage of :term:`hosts <host>` infected by each selected :term:`virus`. Results are displayed next the name of each :term:`virus` and can be ranked from the lowest to the highest (or reverse) values.

- | **Show the infection ratio for hosts**
  | Users can calculate the percentage of :term:`viruses <virus>` infecting each selected :term:`host`. Results are displayed next the name of each :term:`host` and can be ranked from the lowest to the highest (or reverse) values.

- | **Hide  virus without any infection**
  | Users can hide :term:`viruses <virus>` that do not infect the selected :term:`hosts <host>`.

- | **Hide  host without any infection**
  | Users can hide :term:`hosts <host>` that are not infected by the selected :term:`viruses <virus>`.

- | **Consider that there is an infection only when all data sources documenting the interaction observed an infection**
  | Different :term:`data sources <data source>` can disagree on whether a given :term:`virus` can infect or not a given :term:`host`. Users can choose to display only congruent :term:`responses <response>` across all selected :term:`data sources <data source>`, i.e. only when they all agree on it. Selecting this option will affect the calculations in other options such as "show the infection ratio".

.. _data-filtering:

Data filtering
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

These options allows you to narrow the data displayed to published data (**Only published data**), or :term:`viruses <virus>` with NCBI :term:`identifiers <identifier>` (**Only identified viruses**), or :term:`hosts <host>` with NCBI :term:`identifiers <identifier>` (**Only identified hosts**). Users can also restrict data to only one **life domain**.

.. _rendering:

Rendering
~~~~~~~~~~~~~~~~~~~~~

.. _rendering-label:

- | **Display the full table, even if it does not fit on the screen**
  | Selecting this option allows the entire set of data to be displayed without the need to use scrollbars. This option can be useful to create screenshots.

- | **Write hosts horizontally**
  | In columns header write the :term:`hosts <host>` and their :term:`identifier` horizontally instead of vertically by default.

- | **Render table focusing on disagreements**
  | When two :term:`data sources <data source>` disagree (have conflicting values) on the :term:`response` associated to a pair of :term:`host` and :term:`virus`, the cell is bordered in black with the :ref:`default rendering<default_rendering>`. You can increase this highlighting by selecting this option which will fade other agreeing cells (see example below :ref:`focusing on disagreements<focus_rendering>`).

.. _default_rendering:

.. figure:: _static/normal.png
  :figclass: align-center

  Default visual rendering


.. _focus_rendering:

.. figure:: _static/focus.png
  :figclass: align-center

  Visual rendering focusing on disagreements

- | **Display the actual average response computed**
  | By default, when two or more :term:`data sources <data source>` have different :term:`responses <response>` for a given pair of exact same :term:`host` and :term:`virus`, the value "1-2" is indicated when the value of the average :term:`response` computed is between 1 and 2, or "0-1" if the value of the average :term:`response` computed is between 0 and 1. When selecting this option, the actual computed average will be displayed (e.g. "1.5" instead of "1-2" if one datasource has :term:`response` "1" and another :term:`response` "2"). 
  | Note that the background colour of each cell is always correlated to the actual average computed value, even if you select this option.

