.. _what-is-compatible-file-label:

What is a compatible file?
==========================

Short version
-------------
A compatible file is an Excel spreadsheet (.xlsx) in which :term:`hosts <host>` are indicated in columns and :term:`viruses <virus>` are indicated in rows. Each cell is filled with the :term:`response` (a digit) of the interaction between one :term:`host` and one :term:`virus` (see example below). 

+---------------------------+-------------------------------+------------------------------+
|                           | E. coli MG1655 (NC_000913.3)  | E. coli O157:H7 (AE005174.2) |
+---------------------------+-------------------------------+------------------------------+
| T4 (NC_000866.4; HER:27)  | 2                             | 0                            |
+---------------------------+-------------------------------+------------------------------+
| T7 (NC_001604.1)          | 1                             | 2                            |
+---------------------------+-------------------------------+------------------------------+
| MyVirus                   | 2                             | 1                            |
+---------------------------+-------------------------------+------------------------------+

.. warning::

    * Only data located in the first Excel sheet will be taken into account.
    * No duplication is allowed for :term:`host` and :term:`virus`.
    * At least three empty lines must remain between the main table and any other information you may want to write after, such as the legend in a generated template.

User can also download the xlsx version `here <https://gitlab.pasteur.fr/hub/viralhostrangedb/blob/master/src/viralhostrange/viralhostrangedb/static/media/example.xlsx>`_.

Detailed version
----------------

Values accepted as responses
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Each user has its own way to record the output of host range tests. From basic two-state (infection/no infection) to more detailed information such as numerical values corresponding to efficiency of plating calculations. The VHRdb will accept any :term:`response` that is a digit. It is highly recommended to allocate the lowest value to the no infection state and the highest value to the infection state. Therefore, optionals intermediates states should correspond to any value between the lowest and the highest. The range between the lowest and the highest values can be as large as you wish. For example, you can upload data source ranging from 0 to 100 or from 0.0001 to 1. The VHRdb mapping scheme allow users to freely modify the threshold values defining the possible states according to the :term:`global scheme` (No infection, Intermediate, Infection).

Providing the identifier of a virus or a host
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Users can add identifiers for :term:`viruses <virus>` and :term:`hosts <host>` in two different ways. When filling the spreadsheet users can add identifiers (between parentheses) in the same heading cell than the :term:`virus` or :term:`host` name (see example below). Identifiers can be NCBI :term:`identifier` and/or HER :term:`identifier`, and or any custom :term:`identifier`. Multiple :term:`identifiers <identifier>` must be separated by a semicolon ``;``. More documentation can be found :ref:`here<virus-host-identifiers-label>`.

The second way to enter :term:`identifiers <identifier>` is to edit the VHRdb data after the source table has been uploaded. This is particularly convenient for adding NCBI :term:`identifiers <identifier>` sometimes obtained after the uploading the source table.

Examples :
    * ``T4 (NC_000866.4)`` stands for a virus named ``T4`` for which the NCBI identifier is ``NC_000866.4``. The VHRdb will automatically generate a link to the NCBI https://www.ncbi.nlm.nih.gov/nuccore/NC_000866.4
    * ``T4 (NC_000866.4;HER:27)`` stands for a virus named ``T4`` for which the NCBI identifier is ``NC_000866.4`` and the HER number is ``27``. The VHRdb will automatically generate links to both NCBI and Felix d'Hérelle collection such as https://www.ncbi.nlm.nih.gov/nuccore/NC_000866.4 and https://www.phage.ulaval.ca/?pageDemandee=search_results&id=40&L=1&txtNoPhage=27.

How to compare data across several data sources
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Every :term:`data source` is processed using the same mapping procedure linked to a simplified three-state :term:`global scheme`. Therefore, all :term:`data sources <data source>` can be compared.


Can cells of the Excel source file be colored?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Cell colors in the source file are not taken into account. Therefore, if you ranked your responses by using a color scheme only, you must convert it to digits before submission to the VHRdb. If you are using colors in addition to digits, you don't need to remove the colors, they will not interfere with the uploading process. Note that colors are also used in exported file when possible to improve readability.


Variants in the file disposition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. The header of a row (or column) in the spreadsheet can be preceded by one or many columns (or rows). Only the last column (or row) will be taken into account. In the following example, only the cell in bold will be uploaded into the VHRdb.

+-------+-----------------------+------------------------+------------------------+
|       |                       | Obtained from John doe | Obtained from Jane doe |
+-------+-----------------------+------------------------+------------------------+
|       |                       | On 2009-02-21          | On 2012-03-21          |
+-------+-----------------------+------------------------+------------------------+
|       |                       | **E. coli MG1655**     | **E. coli O157:H7**    |
+-------+-----------------------+------------------------+------------------------+
| Virus | **T4 (NC_000866.4)**  | **2**                  | **0**                  |
+-------+-----------------------+------------------------+------------------------+
| Virus | **T7 (NC_001604.1)**  | **1**                  | **2**                  |
+-------+-----------------------+------------------------+------------------------+
| Virus | **MyVirus**           | **2**                  | **1**                  |
+-------+-----------------------+------------------------+------------------------+

2. The data must be on the first sheet of the file.

3. There can be additional rows after the :term:`responses <response>`, they will not be imported as long as there is nothing written where the :term:`virus` (or :term:`host`) name is expected. In the following example, the last row will not be imported.

+-----------------+-----------------------+--------------------+---------------------+
|                 |                       | **E. coli MG1655** | **E. coli O157:H7** |
+-----------------+-----------------------+--------------------+---------------------+
| Virus           | **T4 (NC_000866.4)**  | **2**              | **0**               |
+-----------------+-----------------------+--------------------+---------------------+
| Virus           | **T7 (NC_001604.1)**  | **1**              | **2**               |
+-----------------+-----------------------+--------------------+---------------------+
| Virus           | **MyVirus**           | **2**              | **1**               |
+-----------------+-----------------------+--------------------+---------------------+
| Infection Ratio |  *<Must be blank>*    | 100%               | 50%                 |
+-----------------+-----------------------+--------------------+---------------------+

4. There can be any additional rows after the :term:`responses <response>` as long as there is at least **three empty line after the responses**. In the following example, **Batch** will not be considered as a virus.

+-----------------------+--------------------+---------------------+
|                       | **E. coli MG1655** | **E. coli O157:H7** |
+-----------------------+--------------------+---------------------+
| **T4 (NC_000866.4)**  | **2**              | **0**               |
+-----------------------+--------------------+---------------------+
| **T7 (NC_001604.1)**  | **1**              | **2**               |
+-----------------------+--------------------+---------------------+
| **MyVirus**           | **2**              | **1**               |
+-----------------------+--------------------+---------------------+
|                       |                    |                     |
+-----------------------+--------------------+---------------------+
|                       |                    |                     |
+-----------------------+--------------------+---------------------+
|                       |                    |                     |
+-----------------------+--------------------+---------------------+
| Batch                 | 11603              | Jane Doe            |
+-----------------------+--------------------+---------------------+

Duplication of virus or host
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
It is not allowed to provide file with :term:`host` or :term:`virus` duplicated. If you do so the import will only keep one version of it. Whether the first or last occurrence is kept depends on implementation details and can be changed at any moment. When importing a file with duplication, a warning is rendered indicating which occurrence is kept.


Empty virus name or host name
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A virus, or a host, must have at least a name or an identifier, ideally both. You are not allowed to have empty name/identifier, and import will prevent such data to be imported.


Robustness of file import
~~~~~~~~~~~~~~~~~~~~~~~~~
The resilience of the import module to read and interpret the file is of a paramount importance, we generated multiple configurations in which a file could be written and how we should read it. At each change in the programme we test that each file is still read as expected. The file collection can be browsed at https://gitlab.pasteur.fr/hub/viralhostrangedb/tree/master/src/viralhostrange/test_data, where for an input file ``<filename>.xlsx`` the data we extract from it is ``<filename>.xlsx.json``.

If you tried to import a file which should work but did not, please to not hesitate to submit an issue at https://gitlab.pasteur.fr/hub/viralhostrangedb/issues with the file cleaned of its private data. You can also e-mail the file and steps to reproduce the bug at viralhostrangedb@pasteur.fr. We will correct the import module so that either it can process succesfully the file, or it returns a clear and understandable error message to users.
