.. _virus-host-identifiers-label:

Identifying viruses and hosts
=========================================

Nature of identifiers
------------------------------

Both :term:`viruses <virus>` and :term:`hosts <host>` can be identified with NCBI identifier in nuccore (https://www.ncbi.nlm.nih.gov/nuccore). In addition, for bacterial :term:`viruses <virus>` the :term:`identifier` (HER Number) from the Felix d’Hérelle collection (https://www.phage.ulaval.ca/en/home/) can also be added. Users can provide a custom :term:`identifier` as well (e.g. "MYCODE_631", "BOB1", ...). All :term:`identifiers <identifier>` must be entered between parentheses using a semicolon ``;`` as a separator if needed.

Example of identifiers:

+------------------------------------------------------------+-------+------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Kind of identifier                                         | Virus | Host | Example     | Outgoing link(s) ?                                                                                                                                             |
+============================================================+=======+======+=============+================================================================================================================================================================+
| `NCBI Identifier <https://www.ncbi.nlm.nih.gov/nuccore/>`_ | Yes   | Yes  | NC_000866.4 | Yes, `nuccore link <https://www.ncbi.nlm.nih.gov/nuccore/NC_000866.4>`_ and `tax link <https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=10665>`_    |
+------------------------------------------------------------+-------+------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------+
| `HER Number <https://www.phage.ulaval.ca/en/home/>`_       | Yes   | No   | 27          | Yes, `her link <https://www.phage.ulaval.ca/?pageDemandee=phage&noPhage=27&id=41&L=1>`_                                                                        |
+------------------------------------------------------------+-------+------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------+
| Custom identifier                                          | Yes   | Yes  | DE 123      | No                                                                                                                                                             |
+------------------------------------------------------------+-------+------+-------------+----------------------------------------------------------------------------------------------------------------------------------------------------------------+


How to add identifiers ?
----------------------------

Preferably, :term:`identifiers <identifier>` should be included in the source file. Adding NCBI :term:`identifiers <identifier>`, when available, is strongly encouraged. Users can also add :term:`identifiers <identifier>` after uploading their file by using the Edit menus available in the page of the datasource (section Hosts or Virus, in the lower part of the page).


Implementing a specific identifier in Viral Host Range database
-----------------------------------------------------------------------------------

.. _new-identifier-label:

Implementing a specific identifier in VHRdb with a direct link to another database is possible, and has been done for the Felix d’Hérelle collection and the Taxonomic ID. A specific ID can be motivated by the size of a collection, or its value to the community. Please |contact_us_link| if you consider that a specific identifier could benefit the community.

.. |contact_us_link| raw:: html

   <a href="https://viralhostrangedb.pasteur.cloud/about/#contact" target="_blank">contact us</a>

How to specify an identifier in a compatible file ?
-----------------------------------------------------------------------------------

:term:`Identifiers <identifier>` must be specified in the same cell as to the ID of any :term:`virus` or :term:`host` and delimited by parentheses. Entrez api is used to detect automatically if the :term:`identifier` corresponds to a referenced sequence (i.e, the identifier match in nuccore), and if so also fetch the NCBI TaxID associated. If there is a correspondence the VHRdb will add the prefix NCBI to this identifier and create links to this resource in nuccore and the Taxonomy Browser. If there is no correspondence, the VHRdb will considered the :term:`identifier` as "custom", unless it is preceded by a specific prefix. For example, the prefix HER: (e.g. "HER:27") corresponds to an :term:`identifier` linked to the Félix d’Hérelle collection of bacterial viruses.

.. |ncbi| image:: _static/ncbi-identifier.png
   :height: 25px

.. |her| image:: _static/her-identifier.png
   :height: 25px

.. |custom| image:: _static/custom-identifier.png
   :height: 25px

.. |both| image:: _static/ncbi-and-her-identifier.png
   :height: 25px

+-----------------------------------+-----------------------------------------------------------------------------------------------+
| ``E. coli MG1655 (NC_000913.3)``  +  |ncbi|                                                                                       +
+-----------------------------------+-----------------------------------------------------------------------------------------------+
| ``Ω8 (HER:315)``                  +  |her|                                                                                        +
+-----------------------------------+-----------------------------------------------------------------------------------------------+
| ``BEN 1045 (APEC O25)``           + |custom|                                                                                      +
+-----------------------------------+-----------------------------------------------------------------------------------------------+
| ``N4 (NC_008720; HER:119952179)`` +  |both|                                                                                       +
+-----------------------------------+-----------------------------------------------------------------------------------------------+
