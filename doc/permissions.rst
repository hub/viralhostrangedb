.. _permissions:

Permissions attached to a data source
================================================

A data source can not only be public, or private. It can also be shared with specific users, and even allow some to edit this data source. Here after are the roles a user can have regarding a data source, and what possibility come with each role.

Note that when a data source is changed, its owner is notified, according to his/her :ref:`settings<email-changes-label>`.

Roles
----------------------------------------

.. _owner-role-label:

Owner
~~~~~~~~

The owner of the :term:`data source` can see all information, and make all possible changes, such as:

 * Edit the list of :term:`viruses <virus>` and :term:`hosts <host>`
 * Edit the information of a :term:`data source`
 * Edit the list of viewer/editor
 * Edit the responses
 * Edit the :ref:`mapping <mapping-label>`
 * Upload a new version of the data source
 * Delete :term:`viruses <virus>` and :term:`hosts <host>`
 * Restore the :term:`data source` to a previous state (see :ref:`history`)
 * Delete the data source
 * Transfer ownership to an other user

.. _editor-role-label:

Editor
~~~~~~~~

An editor of the :term:`data source` can deeply changes the :term:`data source`, such as:

 * Edit the list of :term:`viruses <virus>` and :term:`hosts <host>`
 * Edit the information of a :term:`data source` (except the list of viewer/editor)
 * Edit the responses, and see them
 * Edit the :ref:`mapping <mapping-label>`
 * Upload a new version of the data source
 * Delete :term:`viruses <virus>` and :term:`hosts <host>`
 * Restore the :term:`data source` to a previous state (see :ref:`history`)

Although, an editor cannot:

 * See the list of viewer/editor
 * Delete the data source
 * Transfer ownership to an other user

Viewer
~~~~~~~~

A viewer of the :term:`data source` **can not** edit it. A viewer can:

 * See the list of  :term:`viruses <virus>` and :term:`hosts <host>`.
 * See the information of a :term:`data source` (except the the list of viewer/editor)
 * See the responses
 * See the :ref:`mapping <mapping-label>`

A viewer cannot:

 * Edit anything
 * See the list of viewer/editor

When a data source is public, any users are considered as viewers.

.. _curator-role-label:

Curator
~~~~~~~~

A curator can perform changes on **public** data sources even if he/she is not an editor of the data source. The mission
of a curator is to keep naming between viruses, hosts coherent, but also edit data source details when needed.

 * Edit the :term:`viruses <virus>` and :term:`hosts <host>`
 * Edit the information of a :term:`data source` (except the the list of viewer/editor)
 * Edit the mapping
 * Switch a :term:`data source` back to private (but not the opposite)

Although, an curator cannot:

 * Edit the responses
 * Delete :term:`viruses <virus>` or :term:`hosts <host>`
 * See the list of viewer/editor, and thus cannot edit it either
 * Restore the data source to a previous version
 * Download a previous version
 * Delete the data source
 * Transfer ownership to an other user


Permissions with viruses, hosts, and responses
-----------------------------------------------

Permissions of a user regarding a host/virus/response come from the permissions this user have with the data source associated.

Let us consider a virus, if a user can see it, it means that this virus is associated with a data source which can be seen by this user. So does for hosts, and/or edition.

If a user can see a response, it obviously is because this response is in a data source visible to the user.



Consequences of someone editing a virus/host
--------------------------------------------

Let us consider a virus ``MyT4`` present in multiple data source (``ds_1``, ``ds_2``, ..., ``ds_n``). If an owner/curator/editor of one of this data source (``ds_1``) decide to rename this virus ``MyT4`` into ``T4``, all responses formerly associated to  ``MyT4`` in  ``d_1`` are now associated to  ``T4`` in  ``d_1``, but *only in* ``d_1``. It means that ``MyT4`` is still associated to data source ``ds_2``, ..., ``ds_n``.


.. graphviz::
   :align: center
   :caption: Before

    digraph G {
        dsp [label="Public data source"]
        ds_1 [ color=blue ];
        ds_2
        ds_3 [label="..."]
        ds_n
        ds_1 -> MyT4;
        ds_2 -> MyT4;
        ds_3 -> MyT4;
        ds_n -> MyT4;
        dsp -> T4;
    }

.. graphviz::
   :align: center
   :caption: After

    digraph G {
        dsp [label="Public data source"]
        ds_1 [ color=blue ];
        ds_2
        ds_3 [label="..."]
        ds_n
        ds_1 -> T4 [ label=" Only edited link"; color=red ];
        ds_2 -> MyT4;
        ds_3 -> MyT4;
        ds_n -> MyT4;
        dsp -> T4;
    }

