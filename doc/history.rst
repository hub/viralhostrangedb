.. _history:

History of data sources, and backup policy
===========================================

Each time a data source is changed, we log the person who did it, and what this person did (editing the mapping, removing hosts, ...). When such changes concern the viruses, hosts, or responses, we also do a backup, so you can rollback the changes (see :ref:`backup-scope-label`). To see the logged changes of a data source, you have to be an :ref:`editor<editor-role-label>` of this data source, or its :ref:`owner<owner-role-label>`. If so, an *History* button is available in the *Information panel* of the :term:`data source` page.


.. figure:: _static/history-button.png
  :width: 50%
  :figclass: align-center


.. _backup-scope-label:

What is backed up ?
------------------------------------------

The backup focus on the core information of a data source, it contains :

* :term:`Viruses <virus>`
* :term:`Hosts <host>`
* :term:`Raw responses <raw response>` associated to each pair of virus/host in the data source

Note that this list can be extended in the future, but for now neither the mapping nor name and description are backed up.


What happens when you restore a backup ?
----------------------------------------

:term:`Viruses <virus>`, :term:`hosts <host>` and :term:`raw responses <raw response>` currently in the data source are replace with the ones in the backup. It means that all changes (add, edit, delete) that occurred after the creation of this backup to viruses/hosts/raw responses are canceled.


What happens when I delete my data source ?
-------------------------------------------

Your data source is completely removed form the data base, backups remain available a little longer in order to avoid data loss if the deletion was a mistake. If you mistakenly deleted a data source, you need to contact the VHRdb administrators (viralhostrangedb@pasteur.fr) in order to restore this data source.


Step-by-step procedure to restore a backup
------------------------------------------

* Go to the :term:`data source` page
* Click on the History button in the Information panel
* Browse the table of backup to find the version you want to restore
* Click on *Restore data at this point*
* You can download the backup and open it in order to be sure of what you are about to restore
* Check the checkbox to indicate that you understand what you are about to do
* Click on *Restore the backup*

That's all ! Note that a new backup has also be done as, by restoring a backup, you just changed the data source.


Life cycle of a backup
------------------------------------------

A backup is kept if at least one of the following statement is true :

* The backup is one of the 20 latest backups for this :term:`data source` (and the data source is still in the data base)
* The backup have been created in the past 18 months


.. _email-changes-label:

Periodical summary of changes
-------------------------------------------

Every day changes are reviewed, and owners get a brief summary by email of the changes applied to their data sources since the previous day. To avoid inconvenience, we do not send summary during the week-end.

According to his/her account settings (see below), a data source owner will receive an email notification after changes made by :ref:`curators<curator-role-label>` and :ref:`editors<editor-role-label>`, or just :ref:`curators<curator-role-label>`.



.. figure:: _static/app-settings.png
  :width: 100%
  :figclass: align-center