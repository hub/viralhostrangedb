.. _data-import-label:

Importing data into Viral Host Range database
=============================================

Users will find below the information for guidance to import data into the VHRdb. 
The label of the sections correspond to those displayed during the import steps.

.. _general-information:

General information
------------------------------------------

*Step: "Provide a new data source: General information"*

Users need to provide a name for their :term:`data source`, and indicate the corresponding life domain (Bacteria, Archaea or Eukaryote). To make the imported data publicly visible, users should tick the corresponding box. This option allows all VHRdb users to search/explore the :term:`data source`. Note that all data that have been published should be public. Users choosing to keep their data private will nevertheless have the possibility to share their :term:`data source` with collaborators (see :ref:`visibility-label`). The collaborators need to have a VHRdb account in order to get access to the :term:`data source`. This option can for instance be useful for sharing work in progress within lab members.


.. _visibility-label:

Visibility of your data source
------------------------------------------

*Step: "Provide a new data source: Adjusting its visibility"*

When a :term:`data source` is not public, users who own this :term:`data source` can share it with collaborators (registered VHRdb users) by providing their first and last name, or their email used in the application. See example below:


.. image:: _static/visibility-example.png
  :width: 100%
  :alt: Sharing a data source


Description of the data source
------------------------------------------

*Step: "Provide a new data source: Description"*

This step is crucial for the quality of the information that will be available in the VHRdb. Users should indicate as many experimental details as possible including, but not limited to: origin of :term:`viruses <virus>`, origin of :term:`hosts <host>`, method used for the tests, who performed the experiment, where the experiment was performed... This description is mandatory for public :term:`data sources <data source>`, recommended otherwise. It is strongly recommended that data published or related to a publication should be indicated here with a link to this publication. This will allows users to properly cite the :term:`data source`.

How to provide the data
------------------------------------------

*Step: "Provide a new data source: Way to provide it"*

Users can either choose to download a :ref:`compatible file<what-is-compatible-file-label>` or to :ref:`generate a template<template-label>` to be filled offline and then uploaded.

.. _upload-file-label:

Upload a compatible file
------------------------------------------

*Step: "Provide a new data source: Upload a file"*

Select the :ref:`compatible file<what-is-compatible-file-label>` and upload it.


.. _template-label:

Generating a template to share your data
------------------------------------------

*Step: "Provide a new data source: Preparing the template"*

Users that have a limited dataset can directly copy/paste the list of :term:`viruses <virus>` and :term:`hosts <host>` in the respective boxes following the examples shown. When available users are encouraged to indicate the NCBI :term:`identifiers <identifier>` using parentheses. Note that this can also be done after the :term:`data source` has been uploaded as well as any time after :term:`identifiers <identifier>` have been assigned to new sequences of :term:`viruses <virus>` or :term:`hosts <host>`. More information in :ref:`Identifying viruses and hosts<virus-host-identifiers-label>`.

Hereafter are examples of viruses and hosts using various accepted formats:

Example 1, with viruses:
~~~~~~~~~~~~~~~~~~~~~~~~~~
Copy/pasted from a spreadsheet

+---------+-------------+----+
| T4      | NC_000866.4 | 27 |
+---------+-------------+----+
| T7      | NC_001604.1 |    |
+---------+-------------+----+
| MyVirus |             |    |
+---------+-------------+----+

Examples 2, with hosts:
~~~~~~~~~~~~~~~~~~~~~~~~
One entry per line

+------------------------------+
| E. coli MG1655 (NC_000913.3) |
+------------------------------+
| E. coli O157:H7 (AE005174.2) |
+------------------------------+

Examples 3, with hosts:
~~~~~~~~~~~~~~~~~~~~~~~~
Entries separated with a semi colon

``E. coli MG1655 (NC_000913.3)``;  ``E. coli O157:H7 (AE005174.2)``

.. _fill-and-upload-label:

Filling and uploading the template
------------------------------------------

*Step: "Provide a new data source: Uploading the template"*

Users can download the template file, which is an Excel spreadsheet (xlsx) where :term:`hosts <host>` are indicated in columns, and :term:`viruses <virus>`, in rows (see example below). :term:`Responses <response>`, which correspond to the result of interaction tests, can be entered in many numerical format as the VHRdb allows users to later map their data to the global three-state scheme (see :ref:`Mapping responses to the global scheme<mapping-label>`). Any Excel file (preferably with the extension .xlsx) that is organized as suggested is a compatible file.

Below are shown examples of the template file before and after entering the :term:`responses <response>`:

+-------------------------------+-------------------------------+------------------------------+
|                               | E. coli MG1655 (NC_000913.3)  | E. coli O157:H7 (AE005174.2) |
+-------------------------------+-------------------------------+------------------------------+
| T4 (NC_000866.4;  HER:27)     | **<response>**                | **<response>**               |
+-------------------------------+-------------------------------+------------------------------+
| T7 (NC_001604.1)              | **<response>**                | **<response>**               |
+-------------------------------+-------------------------------+------------------------------+
| MyVirus                       | **<response>**                | **<response>**               |
+-------------------------------+-------------------------------+------------------------------+

+-------------------------------+-------------------------------+------------------------------+
|                               | E. coli MG1655 (NC_000913.3)  | E. coli O157:H7 (AE005174.2) |
+-------------------------------+-------------------------------+------------------------------+
| T4 (NC_000866.4;  HER:27)     | **2**                         | **0**                        |
+-------------------------------+-------------------------------+------------------------------+
| T7 (NC_001604.1)              | **1**                         | **2**                        |
+-------------------------------+-------------------------------+------------------------------+
| MyVirus                       | **2**                         | **1**                        |
+-------------------------------+-------------------------------+------------------------------+

A complete description of the properties of *compatible files* is available :ref:`here<what-is-compatible-file-label>`.

.. _mapping-label:

Mapping responses to the global scheme.
------------------------------------------

*Step: "Defining the mapping of data source ..."*

One of the main goals of VHRdb is to enable the comparisation of independent :term:`data sources <data source>`. We defined a three-state :term:`global scheme` to recapitulate virus-host interactions with "No infection" and "Infection" states, which are self-explanatory. We also included an "Intermediate" state to reflect situations where virus-host interactions are clearly different from the two other states. Note that users can submit data using one, two or three states.

The result of virus-host interactions, hereafter called the :term:`response`, must be a digit. This is the only requirement. In order to easily accept :term:`data sources <data source>` from different users, any digit can be uploaded. For example users can submit data with a two-state scheme (0/1), or a continuous scale from 0 to 4, or from 1 to 0.0001. Any scheme can be uploaded. The next step allows users to map the original scheme of the :term:`data source` to the :term:`global scheme`.

In the example shown below, original :term:`responses <response>` range from 0 to 4. To distribute the original responses, users can adjust the thresholds between the :term:`global scheme`. In the example shown below, any value below 0.58 will correspond to "No infection" state, any value between 0.58 and 2.3 will correspond to "Intermediate" state and any value higher than 2.3 will correspond to "Infection" state.

Example
~~~~~~~


.. image:: _static/mapping-float.png
  :width: 100%
  :alt: Mapping example with float responses


Computing details about the mapping thresholds
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To compute the thresholds, a clustering algorithm is run on the original :term:`responses <response>` of the :term:`data source` with three clusters :math:`c_1`, :math:`c_2` and :math:`c_3`, then the threshold between :math:`c_k` and :math:`c_{k+1}` is the mean of the centroid of the these two clusters. The source code corresponding to this algorithm can be found in the `RangeMappingForm implementation <https://gitlab.pasteur.fr/hub/viralhostrangedb/blob/master/src/viralhostrange/viralhostrangedb/forms.py#L344-356>`_.



.. Citation example
.. ----------------
.. See :cite:`Strunk1979` for an introduction to stylish blah, blah...
.. .. bibliography:: references.bib

