from django import forms

from live_settings import models


class LiveSettingsForm(forms.ModelForm):
    next = forms.CharField(widget=forms.HiddenInput(), required=True)

    class Meta:
        model = models.LiveSettings
        fields = ("value",)
        widgets = {"value": forms.HiddenInput()}
