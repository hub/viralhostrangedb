from django.core.cache import cache
from django.db import OperationalError, ProgrammingError


class LiveSettings(object):
    def __setattr__(self, key, value, *args, **kwargs):
        try:
            from live_settings.models import LiveSettings

            LiveSettings.objects.update_or_create(key=key, defaults=dict(value=value))
        except (ProgrammingError, OperationalError):
            return super().__setattr__(key, value)

    def __getattribute__(self, key):
        live_settings_dict = cache.get("live_settings_dict")
        if live_settings_dict is None:
            try:
                from live_settings.models import LiveSettings

                live_settings_dict = dict(
                    LiveSettings.objects.values_list("key", "value")
                )
                cache.set("live_settings_dict", live_settings_dict)
            except (ProgrammingError, OperationalError):
                return super().__getattribute__(key)
        return live_settings_dict.get(key, None)


live_settings = LiveSettings()
