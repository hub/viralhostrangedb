from django.contrib.auth.decorators import permission_required
from django.http import HttpResponseForbidden, HttpResponseBadRequest
from django.shortcuts import redirect

from live_settings import forms
from live_settings import live_settings


@permission_required("live_settings.change_livesettings")
def change_value(request, slug):
    if request.method != "POST":
        return HttpResponseForbidden()

    form = forms.LiveSettingsForm(data=request.POST)
    if not form.is_valid():
        return HttpResponseBadRequest()

    setattr(live_settings, slug, form.cleaned_data["value"])

    return redirect(form.cleaned_data["next"])
