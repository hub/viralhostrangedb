from django.urls import re_path

from live_settings import views

app_name = "live_settings"
urlpatterns = [
    re_path(r"^update/(?P<slug>[a-zA-Z][\w]*)/$", views.change_value, name="update")
]
