# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_permission_codename
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


def migration_code(apps, schema_editor):
    # inspired by django.contrib.auth.management.create_permissions
    group, _ = Group.objects.get_or_create(name="LiveSettingEditor")

    for klass in [
        apps.get_model("live_settings", "LiveSettings"),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            group.permissions.add(perm)


def reverse_code(apps, schema_editor):
    Group.objects.filter(name="LiveSettingEditor").delete()


class Migration(migrations.Migration):
    dependencies = [
        ('live_settings', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=reverse_code),
    ]
