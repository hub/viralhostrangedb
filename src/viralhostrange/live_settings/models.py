from __future__ import unicode_literals

from django.core.cache import cache
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from django.utils import timezone
from django.utils.safestring import mark_safe


class LiveSettings(models.Model):
    class Meta:
        verbose_name = "Live setting"
        verbose_name_plural = "Live settings"

    regex = "^[a-zA-Z][\\w]*$"
    key = models.CharField(
        max_length=256,
        unique=True,
        validators=(
            RegexValidator(
                regex=regex,
                message=mark_safe(
                    "LiveSettings key must must be Alphanumeric and start with a letter: "
                    "<code>%s</code>." % regex
                ),
                code="invalid_live_settings_key",
            ),
        ),
    )
    value = models.TextField(blank=True, null=True, )
    last_edition_date = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.last_edition_date = timezone.now()
        super().save(*args, **kwargs)


@receiver(post_save, sender=LiveSettings)
@receiver(post_delete, sender=LiveSettings)
def flush_live_settings_in_cache(*args, **kwargs):
    cache.delete("live_settings_dict")
