import logging
from unittest import TestCase

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.core import management
from django.core.cache import cache
from django.test import TestCase as DjangoTestCase
from django.urls import reverse

from live_settings import live_settings, models

logger = logging.getLogger(__name__)


class LiveSettingsTestCase(DjangoTestCase):

    def setUp(self) -> None:
        super().setUp()
        ################################################################################
        self.user = get_user_model().objects.create(
            username="root",
        )

    def test_get_value(self):
        self.assertIsNone(live_settings.toto)
        models.LiveSettings.objects.create(key="toto", value="tata")
        self.assertIsNotNone(live_settings.toto)
        self.assertEqual(live_settings.toto, "tata")

    def test_get_set(self):
        live_settings.tralala = 1
        self.assertEqual(str(live_settings.tralala), "1")

    def test_view_works(self):
        form_data = dict(value="titi", next="/")
        url = reverse('live_settings:update', args=["toto"])

        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(live_settings.toto, form_data["value"])

        self.client.force_login(self.user)

        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        self.assertNotEqual(live_settings.toto, form_data["value"])

        change = Permission.objects.get(
            content_type=ContentType.objects.get_for_model(models.LiveSettings),
            codename__startswith="change")
        self.user.user_permissions.add(change)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertNotEqual(live_settings.toto, form_data["value"])

        response = self.client.post(url, dict(value=form_data["value"]))
        self.assertEqual(response.status_code, 400)
        self.assertNotEqual(live_settings.toto, form_data["value"])

        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(live_settings.toto, form_data["value"])


class LiveSettingsNoDBTestCase(TestCase):

    def test_get_value(self):
        management.call_command("migrate", "live_settings", "zero", no_input=True, skip_checks=True)
        cache.delete("live_settings_dict")
        try:
            ex = live_settings.tralala
        except Exception as e:
            ex = e
        assert isinstance(ex, AttributeError)
        live_settings.tralala = 1
