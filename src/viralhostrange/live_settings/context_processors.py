from live_settings import live_settings


def processors(request):
    return dict(live_settings=live_settings)
