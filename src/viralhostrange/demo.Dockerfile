ARG CI_REGISTRY_IMAGE
ARG CI_COMMIT_SHA
FROM $CI_REGISTRY_IMAGE/main:$CI_COMMIT_SHA
#FROM test

ENV CRON_FETCH_ID True
ENV USE_SQLITE_AS_DB True
ENV SECRET_KEY not_so_secret_but_Ahcalee0oipahchee4aigahta9haex5E
ENV ALLOWED_HOSTS 127.0.0.1
ENV DEBUG False
ENV STATIC_URL /static
ENV MEDIA_URL /media
ENV PROJECT_NAME viralhostrange

RUN python manage.py migrate && \
    python manage.py setup_demo && \
    chmod -R 777 persistent_volume && \
    export WHEN="$(date +'%Y-%m-%d %H:%M %Z')"; envsubst < ./viralhostrangedb/templates/viralhostrangedb/pre_content_page_title.demo.html > ./viralhostrangedb/templates/viralhostrangedb/pre_content_page_title.html;