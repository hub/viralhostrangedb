from django.urls import path, re_path
from django.views.i18n import JavaScriptCatalog

from viralhostrangedb import views

data_source_wizard = views.DataSourceWizard.as_view(url_name='viralhostrangedb:data-source-create-step')

app_name = 'viralhostrangedb'
urlpatterns = [
    path('', views.index, name='home'),
    path('import/data-source/', views.file_import, name='file-import-view'),
    # re_path(r'^data_source/(?P<pk>\d+)/edit/$', views.file_import, name='data-source-edit'),
    re_path(
        r'^data-source/pending-mapping/$',
        views.DataSourceMappingPendingListView.as_view(),
        name='data-source-pending-mapping-list',
    ),
    path('explore/', views.browse, name='browse'),
    re_path(r'^explore/css/(?P<slug>[^/]*)$', views.custom_css, name='custom_css'),
    path('download/', views.download_responses, name='download_responses'),
    path('data-source/', views.DataSourceListView.as_view(), name='data-source-list'),
    re_path(r'^data-source/(?P<pk>\d+)/download/$', views.data_source_download, name='data-source-download'),
    re_path(r'^data-source/(?P<pk>\d+)/update/$', views.data_source_data_update, name='data-source-data-update'),
    re_path(r'^data-source/(?P<pk>\d+)/edit/$', views.DataSourceUpdateView.as_view(), name='data-source-update'),
    re_path(r'^data-source/(?P<pk>\d+)/virus/update/$', views.data_source_virus_update, name='data-source-virus-update'),
    re_path(r'^data-source/(?P<pk>\d+)/host/update/$', views.data_source_host_update, name='data-source-host-update'),
    re_path(r'^data-source/(?P<pk>\d+)/virus/delete/$', views.data_source_virus_delete, name='data-source-virus-delete'),
    re_path(r'^data-source/(?P<pk>\d+)/host/delete/$', views.data_source_host_delete, name='data-source-host-delete'),
    re_path(r'^data-source/(?P<pk>\d+)/delete/$', views.DataSourceDeleteView.as_view(), name='data-source-delete'),
    re_path(r'^data-source/(?P<pk>\d+)/$', views.DataSourceDetailView.as_view(), name='data-source-detail'),
    re_path(r'^data-source/(?P<pk>\d+)/explore/$', views.SelectDataFromDataSourceView.as_view(), name='data-source-browse'),
    re_path(r'^data-source/(?P<pk>\d+)/explore_by_virus_and_host/$', views.SelectVirusAndHostFromDataSourceView.as_view(),
        name='data-source-browse-by-virus-and-host'),
    re_path(r'^data-source/(?P<pk>\d+)/mapping/edit-as-label/$', views.data_source_mapping_edit,
        name='data-source-mapping-label-edit'),
    re_path(r'^data-source/(?P<pk>\d+)/mapping/edit/$', views.data_source_mapping_range_edit,
        name='data-source-mapping-edit'),
    re_path(r'^data-source-contribution/(?P<step>.+)/$', data_source_wizard, name='data-source-create-step'),
    path('data-source-contribution/', data_source_wizard, name='data-source-create'),
    re_path(r'^data-source/(?P<pk>\d+)/contact/$', views.contact_data_source_owner, name='data-source-contact'),
    re_path(r'^data-source/(?P<pk>\d+)/history/$', views.data_source_history_list, name='data-source-history'),
    re_path(r'^data-source/(?P<pk>\d+)/history/(?P<log_pk>\d+)/download/$',
        views.data_source_history_download,
        name='data-source-history-download'),
    re_path(r'^data-source/(?P<pk>\d+)/history/(?P<log_pk>\d+)/restoration/$',
        views.data_source_history_restoration,
        name='data-source-history-restoration'),

    re_path(r'^data-source/(?P<pk>\d+)/transfer/$', views.data_source_initiate_ownership_transfer,
        name='data-source-initiate-transfer'),
    re_path(r'^data-source/transfer/(?P<datab64>[0-9A-Za-z_\-]+)/(?P<ts>[0-9A-Za-z_\-]+)/(?P<sig>[0-9A-Za-z_\-]+)/$',
        views.data_source_verified_transfer_ownership,
        name='data-source-verified-transfer'),

    path('virus/', views.VirusListView.as_view(), name='virus-list'),
    re_path(r'^virus/(?P<pk>\d+)/$', views.VirusDetailView.as_view(), name='virus-detail'),
    re_path(r'^virus/(?P<pk>\d+)/download/$', views.virus_download, name='virus-download'),

    path('host/', views.HostListView.as_view(), name='host-list'),
    re_path(r'^host/(?P<pk>\d+)/$', views.HostDetailView.as_view(), name='host-detail'),
    re_path(r'^host/(?P<pk>\d+)/download/$', views.host_download, name='host-download'),

    re_path(r'^response/ds-(?P<ds_pk>\d+)-virus-(?P<virus_pk>\d+)-host-(?P<host_pk>\d+)/update/$',
        views.response_update, name='response-update'),
    re_path(r'^response/virus-(?P<virus_pk>\d+)-host-(?P<host_pk>\d+)/$',
        views.response_detail, name='response-detail'),
    path('search/', views.search, name='search'),

    path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),

    re_path(r'^host/(?P<pk>\d+)/$', views.HostDetailView.as_view(), name='host-detail'),

]
