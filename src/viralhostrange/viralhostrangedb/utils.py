def order_queryset_specifically(*, queryset, actual_order: dict):
    """
    From https://stackoverflow.com/a/51291817/2144569
    :param queryset: the queryset of objects to apply a specific order
    :param actual_order: a dict where key is pk, and value is the rank of the associated object
    :type dict
    :return: the queryset annotate such as the order will follow actual_order
    """
    from django.db.models import Case, When, Value, IntegerField
    if len(actual_order) == 0:
        return queryset
    return queryset.annotate(
        rank=Case(
            *[When(pk=pk, then=Value(actual_order[pk])) for pk in actual_order.keys()],
            default=Value(len(actual_order)),
            output_field=IntegerField(),
        ),
    ).order_by('rank')

# import functools
# import time
# from django.db import connection, reset_queries
# from django.template.response import TemplateResponse
#
#
# def query_debugger(func):
#     @functools.wraps(func)
#     def inner_func(*args, **kwargs):
#         reset_queries()
#
#         start_queries = len(connection.queries)
#
#         start = time.perf_counter()
#         result = func(*args, **kwargs)
#         end = time.perf_counter()
#
#         end_queries = len(connection.queries)
#
#         print(f"Function : {func.__name__}")
#         print(f"Number of Queries : {end_queries - start_queries}")
#         print(f"Finished in : {(end - start):.2f}s")
#         qd = dict()
#         for q in connection.queries[start_queries:end_queries]:
#             try:
#                 qd[q['sql']] += 1
#             except KeyError:
#                 qd[q['sql']] = 1
#         with open('/tmp/query_debugger.txt','w') as f:
#             for sql, count in qd.items():
#                 f.write(sql)
#                 f.write('\n')
#                 if count == 1:
#                     continue
#                 print(f'\t{count}\t{sql}')
#         return result
#
#     return inner_func
#
#
# class QueryDebugTemplateResponse(TemplateResponse):
#     @query_debugger
#     def render(self):
#         return super().render()