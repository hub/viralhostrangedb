import csv
import io
import os
import random
import re
import string

import pandas as pd
from basetheme_bootstrap.user_preferences_utils import get_user_preferences_for_user
from crispy_forms import helper as crispy_forms_helper
from crispy_forms import layout as crispy_forms_layout
from crispy_forms.helper import FormHelper
from crispy_forms.layout import HTML, Layout, Row, Column
from django import forms
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import FieldDoesNotExist, ValidationError
from django.db.models import Q, Min, Max
from django.forms import widgets
from django.utils import timezone
from django.utils.datastructures import MultiValueDictKeyError
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _, gettext

from viralhostrangedb import models, business_process, mixins
from viralhostrangedb.business_process import ImportationObserver, explicit_item
from viralhostrangedb.mixins import only_public_or_granted_or_owned_queryset_filter


class ByLastFirstNameModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.last_name.upper() + " " + obj.first_name.title()


class DateInput(forms.DateInput):
    input_type = 'date'


class ImportDataSourceForm(forms.ModelForm):
    file = forms.FileField(
        label=_('File'),
        help_text=_("The file to upload."),
        required=True,
    )
    make_upload_mandatory = True

    class Meta:
        model = models.DataSource
        fields = (
            'name',
            'description',
            'public',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.on_disk_file = None
        self.import_file_fcn = None

    def full_clean(self):
        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        if self.files is not None and len(self.files) == 1:
            file = list(self.files.values())[0]
            errors, self.import_file_fcn = business_process.import_file_later(file=file)
            for e in errors:
                self.add_error('file', e)
        # else:
        #     self.add_error('file', "No file provided")
        # if (self.files is None or len(self.files) == 0) and len(self.cleaned_data["url"]) == 0 or \
        #         (self.files is not None and len(self.files) > 0) and len(self.cleaned_data["url"]) > 0:
        #     self.add_error("url", _("You have to either provide a file or an URL, and not both."))
        #     self.add_error("file", _("You have to either provide a file or an URL, and not both."))

    def save(self, owner=None, commit=True, importation_observer: ImportationObserver = None):
        instance = super().save(commit=False)
        if owner is not None:
            instance.owner = owner
        instance.save()
        if (instance.raw_name or "") == "":
            instance.raw_name = list(self.files.values())[0].name
            instance.kind = "FILE"
        self.import_file_fcn(
            data_source=instance,
            importation_observer=importation_observer,
        )
        if commit:
            instance.save()
        return instance


class UploadToUpdateDataSourceForm(ImportDataSourceForm):
    class Meta:
        model = models.DataSource
        fields = ()


class DataSourceOwnershipTransferForm(forms.Form):
    ownership_transfer_recipient = forms.CharField(
        label=_("ownership_transfer_recipient__label"),
        help_text=_("ownership_transfer_recipient__help_text"),
        max_length=255,
    )
    recipient = forms.ModelChoiceField(
        queryset=get_user_model().objects.none(),
        required=False,
        widget=forms.HiddenInput,
    )

    def clean(self):
        f = super().clean()
        if "ownership_transfer_recipient" in self.cleaned_data:
            c = self.cleaned_data["ownership_transfer_recipient"].strip()
            self.cleaned_data["recipient"], rw = find_user(self, "ownership_transfer_recipient", c + ";;")
        return f


def find_user(form, field_name, user_str, sep=" "):
    if sep == " " and user_str.count(",") > 0:
        return find_user(form, field_name, user_str, ",")
    user_str = re.sub(r"[ ][ ]+", " ", user_str.replace('\t', ' ').strip())
    fields = user_str.split(';')
    assert len(fields) == 3
    user = fields[0]
    cs = user.strip().split(sep)
    if len(cs) == 1:
        users = get_user_model().objects.filter(email=cs[0].strip())
    elif len(cs) == 2:
        users = get_user_model().objects.filter(
            Q(first_name__iexact=cs[0].strip()) & Q(last_name__iexact=cs[1].strip())
            | Q(last_name__iexact=cs[0].strip()) & Q(first_name__iexact=cs[1].strip())
        )
    else:
        form.add_error(field_name,
                       _("Cannot determine first_name and last_name in \"%s\", "
                         "please separate them with a comma ','") % user)
        return None, False
    if fields[1] != '':
        users = users.filter(pk=fields[1])

    user_count = users.count()
    if user_count == 1:
        return users.first(), fields[2].upper() == "TRUE"
    elif user_count == 0:
        msg = _("Unknown user '%s'")
    elif len(cs) == 1:
        msg = _("Cannot determine of which user we are talking about, multiple user use this email, "
                "please provide first name and last name '%s'")
    else:  # if len(cs) == 2:
        msg = _("Cannot determine of which user we are talking about, "
                "please provide its email '%s'")
    form.add_error(field_name, msg % user)
    return None, False


class DataSourceUserCreateOrUpdateForm(forms.ModelForm):
    allowed_users_editor = forms.CharField(
        label=_("allowed_users_editor__label"),
        help_text=_("allowed_users_editor__help_text"),
        widget=widgets.Textarea(attrs={'rows': '4', 'class': 'allowed-users-editor-serialized'}),
        required=False,
    )

    class Meta:
        model = models.DataSource
        fields = (
            'name',
            # 'experiment_date',
            'publication_url',
            'life_domain',
            'description',
            'provider_first_name',
            'provider_last_name',
            'public',
            'allowed_users_editor',
        )
        widgets = dict(
            raw_name=widgets.TextInput(),
            # experiment_date=DateInput,
        )

    def __init__(self, owner=None, data=None, *args, **kwargs):
        self.owner = owner
        super().__init__(data=data, *args, **kwargs)
        if self.instance and data and "public" in data and data["public"]:
            self.fields["description"].required = True
            # self.fields["experiment_date"].required = True

        if self.instance and self.instance.pk is not None:
            if not mixins.only_owned_queryset_filter(
                    self=None,
                    request=None,
                    queryset=models.DataSource.objects.filter(pk=self.instance.pk),
                    user=owner,
            ).exists():
                try:
                    self.fields.pop("allowed_users_editor")
                except KeyError:
                    pass
            else:
                gus = []
                for gu in self.instance.granteduser_set.order_by("user__last_name", "user__first_name"):
                    gus.append("%(last_name)s %(first_name)s;%(pk)i;%(can_write)s" % dict(
                        last_name=gu.user.last_name,
                        first_name=gu.user.first_name,
                        can_write=gu.can_write,
                        pk=gu.user.pk,
                    ))
                self.fields["allowed_users_editor"].initial = '\n'.join(gus)
        try:
            self.fields["description"].widget.attrs["placeholder"] = _("description_placeholder_in_form")
        except KeyError:
            pass
        # try:
        #     self.fields["public"].widget.attrs['data-onchange-js'] = 'update_allowed_user_visibility'
        # except KeyError:
        #     pass

    def clean(self):
        f = super().clean()
        if "name" in self.cleaned_data:
            name = self.cleaned_data["name"]
            homonyms = only_public_or_granted_or_owned_queryset_filter(
                None,
                request=None,
                queryset=self._meta.model.objects.filter(name=name),
                user=self.owner
            )
            if self.instance:
                homonyms = homonyms.filter(~Q(pk=self.instance.pk))
            if homonyms.exists():
                self.data = self.data.copy()
                try:
                    key = self.data['data_source_wizard-current_step'] + "-name"
                except MultiValueDictKeyError:
                    key = "name"
                suffix_numbered = ' (%i)'
                suffix_named = None
                if homonyms.filter(owner=self.owner):
                    pass
                else:
                    suffix_named = ' - %s %s' % (self.owner.last_name, self.owner.first_name)
                    suffix_numbered = suffix_named + suffix_numbered

                if suffix_named and not self._meta.model.objects.filter(name=name + suffix_named).exists():
                    suffix = suffix_named
                else:
                    i = 1
                    while self._meta.model.objects.filter(name=name + (suffix_numbered % i)).exists():
                        i += 1
                    suffix = suffix_numbered % i
                self.data[key] += suffix
                self.add_error("name", mark_safe(_("homomys detected, suffixed by \"%s\"") % suffix))

        try:
            if self.cleaned_data.get("public", False) and "experiment_date" not in self.cleaned_data:
                self.add_error("experiment_date", _("experiment_date_required_when_public"))
        except ValueError:
            pass
        try:
            if self.cleaned_data.get("public", False) and len(self.cleaned_data.get("description", "")) < 10:
                self.add_error("description", _("description_required_when_public%i") % 10)
        except ValueError:
            pass
        if "allowed_users_editor" in self.cleaned_data:
            users = []
            self.cleaned_data["allowed_users_with_permission"] = users
            for c in self.cleaned_data["allowed_users_editor"].replace("\r", "\n").replace("\n\n", "\n").split("\n"):
                if c == "":
                    continue
                users.append(find_user(self, "allowed_users_editor", c))
        return f

    def save(self, commit=True):
        instance = super().save(commit=False)
        try:
            # Do not allow to edit the owner with this form
            getattr(instance, 'owner')
        except models.DataSource.owner.RelatedObjectDoesNotExist as e:
            instance.owner = self.owner
        if commit:
            instance.save()
            self.save_m2m()
        if "allowed_users_editor" in self.cleaned_data:
            to_keep = list()
            for user, can_write in self.cleaned_data["allowed_users_with_permission"]:
                if user is None:
                    continue
                gu, created = models.GrantedUser.objects.update_or_create(
                    data_source=instance,
                    user=user,
                    defaults=dict(
                        can_write=can_write,
                    )
                )
                to_keep.append(gu.pk)
            models.GrantedUser.objects.filter(data_source=instance).exclude(pk__in=to_keep).delete()

        if commit:
            instance.save()
        return instance


class MappingForm(forms.Form):
    raw_response = forms.FloatField(
    )
    mapping = forms.ModelChoiceField(
        queryset=models.GlobalViralHostResponseValue.objects_mappable().order_by("value"),
        required=True,
    )


MappingFormSet = forms.formset_factory(
    form=MappingForm,
    extra=0,
    can_delete=False,
)


class RangeMappingForm(forms.Form):
    def __init__(self, data_source=None, data=None, *args, **kwargs):
        super().__init__(data=data, *args, **kwargs)
        self.data_source = data_source
        use_default = not models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=data_source) \
            .filter(~Q(response__pk=models.GlobalViralHostResponseValue.get_not_mapped_yet_pk())) \
            .exists()
        min_maxs = []  # min_maxs will contains min and max for each GlobalViralHostResponseValue
        if use_default:
            try:
                from sklearn.cluster import KMeans
                import numpy as np
                responses = models.ViralHostResponseValueInDataSource.objects \
                    .filter(data_source=data_source) \
                    .values_list("raw_response", flat=True)
                clusters = KMeans(
                    n_clusters=models.GlobalViralHostResponseValue.objects_mappable().count(),
                    random_state=0,
                ).fit(np.array(responses).reshape(-1, 1)).cluster_centers_
                centroids = sorted([c[0] for c in clusters])
                # put in min_maxs the centroid as min and max for each GlobalViralHostResponseValue as we do not known
                # which element have been associated to each cluster (or don't want to compute it)
                min_maxs = [[x, x] for x in centroids]
            except ModuleNotFoundError:
                # Here when we do not have numpy
                pass
            except ValueError:
                # Here when there is no data to work on
                pass

        # if we do not use KMeans, of if it failed
        if not min_maxs:
            for m in models.GlobalViralHostResponseValue.objects_mappable().order_by("value"):
                min_max = models.ViralHostResponseValueInDataSource.objects \
                    .filter(data_source=data_source) \
                    .filter(response=m) \
                    .aggregate(Min('raw_response'), Max('raw_response'))
                min_maxs.append([min_max["raw_response__min"], min_max["raw_response__max"]])
            prev = None
            # Replace the None with an upper value
            for i in range(len(min_maxs) - 1, -1, -1):
                # Go backward from higher to lower GlobalViralHostResponseValue
                for j in range(1, -1, -1):
                    # Go backward from max (j=1) to min (j=0)
                    if min_maxs[i][j] is None:
                        # if the value is None replace it with the last seen values, and thus greater or equal of what
                        # we will see in the future
                        if prev is None:
                            # if prev is None, we find the overall maximum value, and increment it of one
                            prev = max(*[x for mm in min_maxs for x in mm if x is not None], *[-1000, -1000]) + 1
                        min_maxs[i][j] = prev
                    else:
                        # we see a value, so keeping it in memory for later None value to replace
                        prev = min_maxs[i][j]

        for pos, m in enumerate(models.GlobalViralHostResponseValue.objects_mappable().order_by("value")[1:], 1):
            key = 'mapping_for_%i_starts_with' % m.pk
            # val is the average between the maximum of the previous response and the minimum of the current response
            val = (min_maxs[pos][0] + min_maxs[pos - 1][1]) / 2
            self.fields[key] = forms.FloatField(
                initial=val,
                required=True,
            )
            self.fields[key].obj = m

    def clean(self):
        f = super().clean()
        previous_key = None
        for m in models.GlobalViralHostResponseValue.objects_mappable().order_by("value")[1:]:
            key = 'mapping_for_%i_starts_with' % m.pk
            if previous_key is None:
                previous_key = key
                continue
            try:
                val = self.cleaned_data[key]
                try:
                    if val < self.cleaned_data[previous_key]:
                        self.add_error(previous_key, _('Value should not be higher than the next one'))
                except KeyError:
                    self.add_error(previous_key, _('Field is required'))
            except KeyError:
                self.add_error(key, _('Field is required'))
        return f

    @property
    def get_current_range_start(self):
        return {f.obj.pk: f.initial for f in self.fields.values()}

    def save(self):
        ceiling = None
        for m in models.GlobalViralHostResponseValue.objects_mappable().order_by("-value"):
            key = 'mapping_for_%i_starts_with' % m.pk
            try:
                floor = self.cleaned_data[key]
            except KeyError:
                floor = None
            qs = models.ViralHostResponseValueInDataSource.objects.filter(data_source=self.data_source)
            if floor is not None:
                qs = qs.filter(raw_response__gte=floor)
            if ceiling is not None:
                qs = qs.filter(raw_response__lt=ceiling)
            qs.update(response=m)

            ceiling = floor


class AutoMergeSplitUpdateFormSet(forms.BaseModelFormSet):
    def __init__(self, data_source=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__data_source = data_source
        try:
            self.model._meta.get_field("her_identifier")
            self.has_her_identifier = True
        except FieldDoesNotExist:
            self.has_her_identifier = False

    def save(self, commit=True):
        assert (self.__data_source is not None)
        assert commit, "It has to be actually saved in DB"
        objects = super().save(commit=False)
        indexed_objects = dict([(o.pk, o) for o in objects])

        for obj, reason in self.changed_objects:
            # get an instance unchanged
            old_o = obj.__class__.objects.get(pk=obj.pk)
            # Find an object which have same identifier and names as what we are trying to save
            alter_egos = obj.__class__.objects \
                .filter(identifier=obj.identifier) \
                .filter(name=obj.name) \
                .filter(~Q(pk=obj.pk))
            if self.has_her_identifier:
                alter_egos = alter_egos.filter(her_identifier=obj.her_identifier)
            # alter_ego is the object to which we have to rename the obj
            alter_ego = alter_egos.first()
            # If there is no alter_ego we may have to build one new
            if alter_ego is None:
                # If we can get the is_ncbi_identifier_value elsewhere then we re-use it
                if 'identifier' in reason:
                    identifier_alter_ego = obj.__class__.objects.filter(identifier=obj.identifier).first()
                    if identifier_alter_ego is None:
                        obj.is_ncbi_identifier_value = None
                    else:
                        obj.is_ncbi_identifier_value = identifier_alter_ego.is_ncbi_identifier_value
                # If the object is not used by someone else then we just change it
                if obj.data_source.count() == 1:
                    obj.save()
                    continue
                # obj is used, we clone it
                obj.pk = None
                obj.save()
                # And use this copy as its alter_ego
                alter_ego = obj
            elif alter_ego.data_source.filter(pk=self.__data_source.pk).exists():
                raise ValidationError(
                    mark_safe(gettext("Duplicated entry <b>%s</b> have been found but is not allowed.")
                              % alter_ego.explicit_name_html))

            # remove the old instance from the current data_source
            old_o.data_source.remove(self.__data_source)
            old_o.save()

            # in any case, replace the instance that was about to be returned by the alter ego/newly created one
            indexed_objects[old_o.pk] = alter_ego
            # obj is now a new db instance, while old_o is still pointing to the original db instance

            # add the new instance to the data source
            alter_ego.data_source.add(self.__data_source)

            # get all the response associated to the original instance and the data source
            for r in old_o.responseindatasource.filter(data_source=self.__data_source):
                # and set the new instance in place of the old
                setattr(r, alter_ego.__class__.__name__.lower(), alter_ego)
                # don't forget to save !
                r.save()

            # If the old instance is not linked to anything anymore, we delete it
            if old_o.data_source.count() == 0:
                old_o.delete()
        # return all the instance changed
        return indexed_objects.values()


UpdateVirusFormSet = forms.modelformset_factory(
    model=models.Virus,
    form=forms.modelform_factory(
        model=models.Virus,
        fields=("name", "identifier", "her_identifier", "id",),
    ),
    formset=AutoMergeSplitUpdateFormSet,
    extra=0,
    can_delete=False,

)

UpdateHostFormSet = forms.modelformset_factory(
    model=models.Host,
    form=forms.modelform_factory(
        model=models.Host,
        fields=("name", "identifier", "id",),
    ),
    formset=AutoMergeSplitUpdateFormSet,
    extra=0,
    can_delete=False,
)

DeleteVirusFormSet = forms.modelformset_factory(
    model=models.Virus,
    form=forms.modelform_factory(
        model=models.Virus,
        fields=("name", "identifier", "id",),
        widgets={
            "name": forms.TextInput(attrs={"readonly": True}),
            "identifier": forms.TextInput(attrs={"readonly": True}),
        }
    ),
    extra=0,
    can_delete=True,
)

DeleteHostFormSet = forms.modelformset_factory(
    model=models.Host,
    form=forms.modelform_factory(
        model=models.Host,
        fields=("name", "identifier", "id",),
        widgets={
            "name": forms.TextInput(attrs={"readonly": True}),
            "identifier": forms.TextInput(attrs={"readonly": True}),
        }
    ),
    extra=0,
    can_delete=True,
)

VirusForm = forms.modelform_factory(
    model=models.Virus,
    fields=(
        "name",
        "identifier",
    ),
)

HostForm = forms.modelform_factory(
    model=models.Host,
    fields=(
        "name",
        "identifier",
    ),
)


class BoostrapSelectMultiple(forms.SelectMultiple):
    def __init__(
            self,
            allSelectedText,
            nonSelectedText,
            SelectedText,
            onChangeFunctionName=None,
            attrs=None,
            *args,
            **kwargs,
    ):
        if attrs is not None:
            attrs = attrs.copy()
        else:
            attrs = {}
        if onChangeFunctionName is not None:
            attrs["data-on-change-fcn-name"] = onChangeFunctionName
        attrs.update({
            "data-all-selected-text": allSelectedText,
            "data-non-selected-text": nonSelectedText,
            "data-n-selected-text": SelectedText,
            "class": "bootstrap-multiselect-applicant",
            "style": "display:none;",
        })
        super().__init__(attrs=attrs, *args, **kwargs)


class ByNameModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class ByExplicitNameModelMultipleChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return obj.explicit_name


class CommonSearchPrefForm(forms.Form):
    class Meta:
        abstract = True

    only_published_data = forms.BooleanField(
        label=_("Only published data"),
        help_text=_("only_published_data.help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-published',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    only_host_ncbi_id = forms.BooleanField(
        label=_("only_host_ncbi_id.label"),
        help_text=_("only_host_ncbi_id.help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-nbci-id',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    only_virus_ncbi_id = forms.BooleanField(
        label=_("only_virus_ncbi_id.label"),
        help_text=_("only_virus_ncbi_id.help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-nbci-id',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    life_domain = forms.ChoiceField(
        label=_("preference.life_domain.label"),
        help_text=_("preference.life_domain.help_text"),
        choices=(('', _('All')),) + models.DataSource._meta.get_field("life_domain").choices,
        widget=widgets.RadioSelect(
            attrs={
                'data-toggle-css-class': 'only-nbci-id',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 0,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        initial='',
        required=False,
    )
    helper = FormHelper()


class CommonPrefBrowseForm(CommonSearchPrefForm):
    class Meta:
        abstract = True

    advanced_option_families = {
        "2": _("Analysis Tools"),
        "0": _("Data filtering options"),
        "1": _("Rendering options"),
    }
    display_all_at_once = forms.BooleanField(
        # label=_("Fit table to screen, scroll when needed"),
        # help_text=_("Otherwise display all the table at once but may not fit on screen"),
        label=_("Display all the table at once, even if it does not fit on screen"),
        help_text=_("Otherwise we fit the table to the screen and use scroll bar when needed."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'freeze-col-header-v3 freeze-row-header-v3',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
            },
        ),
        required=False,
        initial=False,
    )
    horizontal_column_header = forms.BooleanField(
        label=_("horizontal_column_header_label"),
        help_text=_("horizontal_column_header_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
                'data-toggle-css-class': 'horizontal-col-header',
            },
        ),
        required=False,
        initial=False,
    )
    weak_infection = forms.BooleanField(
        label=_("Consider all positive response as a infection"),
        help_text=_("By default we consider that there is an infection only when the highest response is observed."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-4 order-7',
            },
        ),
        required=False,
        initial=False,
    )


class BrowseForm(CommonPrefBrowseForm):
    ds = ByNameModelMultipleChoiceField(
        label=_("Data source"),
        queryset=models.DataSource.objects.none(),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("All data sources selected"),
            nonSelectedText=_("All relevant data sources") + "*",
            SelectedText=_(" data source selected"),
            onChangeFunctionName="filter_changed",
            attrs={
                "data-select-all-inv-nop": "nop",
            },
        ),
        required=False,
    )
    virus = ByExplicitNameModelMultipleChoiceField(
        label=_("Virus"),
        queryset=models.Virus.objects.none(),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("All viruses selected"),
            nonSelectedText=_("All relevant viruses") + "*",
            SelectedText=_(" viruses selected"),
            onChangeFunctionName="filter_changed",
            attrs={
                "data-select-all-inv-nop": True,
            },
        ),
        required=False,
    )
    host = ByExplicitNameModelMultipleChoiceField(
        label=_("Host"),
        queryset=models.Host.objects.none(),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("All hosts selected"),
            nonSelectedText=_("All relevant hosts") + "*",
            SelectedText=_(" hosts selected"),
            onChangeFunctionName="filter_changed",
            attrs={
                "data-select-all-inv-nop": True,
            },
        ),
        required=False,
    )
    focus_disagreements_toggle = forms.BooleanField(
        label=_("Render table focusing on disagreements"),
        help_text=_("Responses where data sources disagree are highlighted, others are faded."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'only-disagree',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
            },
        ),
        required=False,
        initial=False,
    )
    render_actual_aggregated_responses = forms.BooleanField(
        label=_("render_actual_aggregated_responses_label"),
        help_text=_("render_actual_aggregated_responses_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
        required=False,
        initial=False,
    )
    full_length_vh_name_toggle = forms.BooleanField(
        label=_("Show names at full length"),
        help_text=_("To improve rendering, viruses and hosts names are capped."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-toggle-css-class': 'full-length-vh-name',
                'data-advanced-option': 'true',
                'data-advanced-option-family': 1,
            },
        ),
        required=False,
        initial=False,
    )
    virus_infection_ratio = forms.BooleanField(
        label=_("Show the infection ratio for the viruses"),
        help_text=_("Note that it can deteriorate the overall response time."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_virus_infection_ratio',
                'data-container-additional-class': 'order-xl-2 order-3',
            },
        ),
        required=False,
        initial=False,
    )
    host_infection_ratio = forms.BooleanField(
        label=_("Show the infection ratio for the hosts"),
        help_text=_("Note that it can deteriorate the overall response time."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_host_infection_ratio',
                'data-container-additional-class': 'order-xl-3 order-5',
            },
        ),
        required=False,
        initial=False,
    )
    hide_rows_with_no_infection = forms.BooleanField(
        label=_("hide_virus_with_no_infection_label"),
        help_text=_("hide_rows_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-rows-with-no-infection',
                # 'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-5 order-4',
            },
        ),
        required=False,
        initial=False,
    )
    hide_cols_with_no_infection = forms.BooleanField(
        label=_("hide_host_with_no_infection_label"),
        help_text=_("hide_cols_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-cols-with-no-infection',
                # 'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-6 order-6',
            },
        ),
        required=False,
        initial=False,
    )
    agreed_infection = forms.BooleanField(
        label=_(
            "Consider that there is an infection only when <i>all data sources</i> "
            "documenting the interaction observed an infection"),
        help_text=_("By default we consider that there is an infection when in <i>at least one</i> "
                    "data source an infection was observed."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-xl-8 order-8',
            },
        ),
        required=False,
        initial=False,
    )
    sort_rows = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )
    sort_cols = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )

    def __init__(self, data=None, user=None, initial=None, *args, **kwargs):
        use_pref = True
        initial = initial or dict()
        if data is not None:
            data = data.copy()
            initial.setdefault('host', [])
            initial.setdefault('virus', [])
            initial.setdefault("ds", [])
            for k, v in data.items():
                initial[k] = v
            for k in ['host', 'ds', 'virus']:
                list_for_k = data.getlist(k)
                if len(list_for_k) > 1:
                    initial[k] = list_for_k
                elif len(list_for_k) == 1:
                    initial[k] = list_for_k[0].split(',')
            use_pref = False
        if use_pref or data and data.get('use_pref', 'False').lower() == 'true':
            pref = get_user_preferences_for_user(user)
            for group, fields in models.UserPreferences.preferences_groups.items():
                for f in fields:
                    initial[f] = getattr(pref, f)
                    if data:
                        data[f] = getattr(pref, f)
        super().__init__(initial=initial, data=data if data and len(data) > 0 else None, *args, **kwargs)
        if user is None:
            return
        self.fields["virus"].queryset = only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=user,
            queryset=models.Virus.objects,
            path_to_data_source="data_source__",
        )
        self.fields["host"].queryset = only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=user,
            queryset=models.Host.objects,
            path_to_data_source="data_source__",
        )
        self.fields["ds"].queryset = only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=user,
            queryset=models.DataSource.objects,
            path_to_data_source="",
        )
        self.fields["horizontal_column_header"].label = _("horizontal_host_header_label")


class VirusHostDetailViewForm(CommonPrefBrowseForm):
    infection_ratio = forms.BooleanField(
        label=_("Show the infection ratio"),
        help_text=_("Note that it can deteriorate the overall response time."),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-onchange-js': 'refresh_all_infection_ratio',
                'data-container-additional-class': 'order-0',
            },
        ),
        required=False,
        initial=False,
    )
    hide_rows_with_no_infection = forms.BooleanField(
        label=_("hide_rows_with_no_infection_label"),
        help_text=_("hide_rows_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-rows-with-no-infection',
                'data-container-additional-class': 'order-3',
            },
        ),
        required=False,
        initial=False,
    )
    hide_cols_with_no_infection = forms.BooleanField(
        label=_("hide_ds_with_no_infection_label"),
        help_text=_("hide_cols_with_no_infection_help_text"),
        widget=widgets.CheckboxInput(
            attrs={
                'data-advanced-option': 'true',
                'data-advanced-option-family': 2,
                'data-toggle-css-class': 'hide-cols-with-no-infection',
                'data-container-additional-class': 'order-4',
            },
        ),
        required=False,
        initial=False,
    )
    sort_rows = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )
    sort_cols = forms.ChoiceField(
        widget=widgets.HiddenInput(),
        choices=(('asc', 'asc'), ('desc', 'desc'),),
        required=False,
    )
    row_order = forms.CharField(
        widget=widgets.HiddenInput(),
        required=False,
    )
    col_order = forms.CharField(
        widget=widgets.HiddenInput(),
        required=False,
    )

    def __init__(self, data=None, user=None, pref=None, initial=None, *args, **kwargs):
        initial = initial or dict()
        pref = pref or get_user_preferences_for_user(user)
        initial['infection_ratio'] = pref.virus_infection_ratio or pref.host_infection_ratio
        initial['horizontal_column_header'] = True
        for f in [
            'focus_disagreements_toggle',
            'display_all_at_once',
            'weak_infection',
            'hide_rows_with_no_infection',
            'hide_cols_with_no_infection',
            'only_virus_ncbi_id',
            'only_host_ncbi_id',
            'only_published_data',
            'life_domain',
        ]:
            initial[f] = getattr(pref, f)
        super().__init__(initial=initial, data=data if data and len(data) > 0 else None, *args, **kwargs)
        self.fields["horizontal_column_header"].label = _("horizontal_ds_header_label")


class EmptyForm(forms.Form):
    helper = FormHelper()
    helper.form_tag = False
    agree = forms.BooleanField(
        initial=False,
        required=True,
        label="",
    )

    def __init__(self, agree_label=None, agree_help_text=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if agree_label is None:
            del self.fields["agree"]
        else:
            self.fields["agree"].label = agree_label
            self.fields["agree"].help_text = agree_help_text


class DataSourceNameModelForm(DataSourceUserCreateOrUpdateForm):
    class Meta:
        model = models.DataSource
        fields = (
            'name',
            'public',
            'life_domain',
            'provider_first_name',
            'provider_last_name',
        )

    helper = FormHelper()
    helper.form_tag = False

    def __init__(self, owner=None, *args, **kwargs):
        super().__init__(owner=owner, *args, **kwargs)
        self.fields.pop("allowed_users_editor")
        if not business_process.is_curator(owner):
            del self.fields["provider_last_name"]
            del self.fields["provider_first_name"]
        else:
            self.helper = crispy_forms_helper.FormHelper()
            self.helper.layout = crispy_forms_layout.Layout(
                'name',
                'public',
                'life_domain',
                crispy_forms_layout.Row(
                    crispy_forms_layout.Column(
                        HTML(
                            "<hr/>"
                        ),
                        HTML(
                            _("provider explanation when editing")
                        ),
                        css_class='col-12 mb-0',
                    ),
                    crispy_forms_layout.Column(
                        # 'experiment_date',
                        'provider_first_name',
                        css_class='col-12 col-md-6 mb-0',
                    ),
                    crispy_forms_layout.Column(
                        'provider_last_name',
                        css_class='col-12 col-md-6 mb-0',
                    ),
                    css_class='form-row',
                ),
            )
            self.helper.form_tag = False


class DataSourceDescriptionModelForm(forms.ModelForm):
    class Meta:
        model = models.DataSource
        fields = (
            # 'experiment_date',
            'description',
            'publication_url',
        )
        widgets = dict(
            experiment_date=DateInput,
        )

    def __init__(self, required, msg=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["description"].required = required
        # self.fields["experiment_date"].required = required
        # Translators: The text displayed in the form field when it is empty.
        self.fields["description"].widget.attrs["placeholder"] = _("description_placeholder_in_form")
        self.helper = crispy_forms_helper.FormHelper()
        if msg != None:
            self.helper.layout = crispy_forms_layout.Layout(
                crispy_forms_layout.Row(
                    crispy_forms_layout.Column(
                        HTML(msg),
                        css_class='col-md-6 mb-0',
                    ),
                    crispy_forms_layout.Column(
                        # 'experiment_date',
                        'description',
                        'publication_url',
                        css_class='col-md-6 mb-0',
                    ),
                    css_class='form-row',
                ),
            )
        self.helper.form_tag = False

    def clean(self):
        f = super().clean()
        if self.fields["description"].required and len(self.cleaned_data.get("description", "")) < 10:
            self.add_error("description", _("description_required_when_public%i") % 10)
        return f


class DataSourceVisibilityModelForm(DataSourceUserCreateOrUpdateForm):
    class Meta:
        model = models.DataSource
        fields = (
        )

    helper = FormHelper()
    helper.form_tag = False


class UploadOrLiveInput(forms.Form):
    upload_or_live_input = forms.ChoiceField(
        label=_("upload_or_live_input__label"),
        choices=(
            ("upload", _("UploadOrLiveInput__upload__choice")),
            # ("template", _("UploadOrLiveInput__template__choice")),
            ("live", _("UploadOrLiveInput__live_input__choice")),
        ),
        required=True,
        initial=None,
        widget=forms.RadioSelect,
    )
    helper = FormHelper()
    helper.form_tag = False


class UploadDataSourceForm(ImportDataSourceForm):
    make_upload_mandatory = False

    class Meta:
        model = models.DataSource
        fields = (
        )

    helper = FormHelper()
    helper.form_tag = False


DELIMITER_CHOICES = [
    # ('\n', _("delimiter__line_jump")),
    (None, mark_safe(gettext("delimiter__automatic"))),
    ('\t', mark_safe(gettext("delimiter__tabulation"))),
    (',', mark_safe(gettext("delimiter__comma"))),
    (';', mark_safe(gettext("delimiter__semicolon"))),
    ('two_col\t', mark_safe(gettext("delimiter__two_col_tabulation_format"))),
    ('two_col ', mark_safe(gettext("delimiter__two_col_space_format"))),
    ('two_col,', mark_safe(gettext("delimiter__two_col_comma_format"))),
    ('two_col;', mark_safe(gettext("delimiter__two_col_semicolon_format"))),
    # (' ', mark_safe(gettext("delimiter__space"))),
]
DELIMITER_CHOICES_VIRUS = DELIMITER_CHOICES + [
    ('tri_col\t', mark_safe(gettext("delimiter__tri_col_tabulation_format"))),
    ('tri_col ', mark_safe(gettext("delimiter__tri_col_space_format"))),
    ('tri_col,', mark_safe(gettext("delimiter__tri_col_comma_format"))),
    ('tri_col;', mark_safe(gettext("delimiter__tri_col_semicolon_format"))),
]


class LiveInputVirusHostForm(forms.Form):
    virus = forms.CharField(
        label=_("Virus"),
        help_text=_("LiveVirusHostInput__Virus__help_text"),
        required=True,
        widget=forms.widgets.Textarea({'rows': '8'})
    )
    virus_delimiter = forms.ChoiceField(
        label=_("LiveVirusHostInput__virus_delimiter__label"),
        help_text=_("LiveVirusHostInput__virus_delimiter__help_text"),
        choices=DELIMITER_CHOICES_VIRUS,
        required=False,
        initial=None,
        widget=forms.Select,
    )
    host = forms.CharField(
        label=_("Host"),
        help_text=_("LiveVirusHostInput__Host__help_text"),
        required=True,
        widget=forms.widgets.Textarea({'rows': '8'})
    )
    host_delimiter = forms.ChoiceField(
        label=_("LiveVirusHostInput__host_delimiter__label"),
        help_text=_("LiveVirusHostInput__host_delimiter__help_text"),
        choices=DELIMITER_CHOICES,
        required=False,
        initial=None,
        widget=forms.Select,
    )

    def __init__(self, data=None, user=None, *args, **kwargs):
        self.user = user
        super().__init__(data=data, *args, **kwargs)
        self.fields["virus"].widget.attrs["placeholder"] = mark_safe(_("virus_placeholder_in_form"))
        self.fields["host"].widget.attrs["placeholder"] = _("host_placeholder_in_form")
        self.helper = crispy_forms_helper.FormHelper()
        self.helper.layout = crispy_forms_layout.Layout(
            crispy_forms_layout.Row(
                crispy_forms_layout.Column(
                    'virus',
                    'virus_delimiter',
                    css_class='col-md-6 mb-0',
                ),
                crispy_forms_layout.Column(
                    'host',
                    'host_delimiter',
                    css_class='col-md-6 mb-0',
                ),
                css_class='form-row',
            ),
        )
        self.helper.form_tag = False
        if not data:
            self.fields["host_delimiter"].widget = forms.HiddenInput()
            self.fields["virus_delimiter"].widget = forms.HiddenInput()

    def get_as_list(self, key):
        f = io.StringIO(self.cleaned_data[key])
        delimiter = self.cleaned_data["%s_delimiter" % key]
        if delimiter.startswith('two_col'):
            reader = csv.reader(f, delimiter=delimiter[7:])
            for row in reader:
                ids = business_process.default_identifiers()
                if len(row) > 1:
                    ids[business_process.NCBI_IDENTIFIER] = row[1].strip()
                yield row[0].strip(), ids
        elif delimiter.startswith('tri_col'):
            reader = csv.reader(f, delimiter=delimiter[7:])
            for row in reader:
                ids = business_process.default_identifiers()
                ids[business_process.NCBI_IDENTIFIER] = row[1].strip()
                if len(row) > 2:
                    ids[business_process.HER_IDENTIFIER] = int(row[2].strip())
                yield row[0].strip(), ids
        else:
            reader = csv.reader(f, delimiter=delimiter)
            for row in reader:
                for cell in row:
                    cell = cell.replace('\r', '').replace('\n', '').strip()
                    if len(cell) > 0:
                        yield business_process.extract_name_and_identifiers(cell)

    def get_guessed_delimiter(self, key):
        text = self.cleaned_data[key]
        guessing = []
        for d, _ in DELIMITER_CHOICES:
            if d is None:
                continue
            count = text.count(d)
            if count == 0:
                continue
            guessing.append((d, count))
        if len(guessing) == 0:
            return '\t'
        if len(guessing) == 1:
            if not text.endswith('\n'):
                text += '\n'
            line_jump = text.count('\n')
            if line_jump > 1 and line_jump == guessing[0][1]:
                return "two_col" + guessing[0][0]
            if line_jump > 1 and line_jump * 2 == guessing[0][1]:
                return "tri_col" + guessing[0][0]
            return guessing[0][0]
        return None

    def full_clean(self):
        super().full_clean()
        if not self.is_bound:  # Stop further processing.
            return
        for key in ["virus", "host"]:
            delimiter_key = "%s_delimiter" % key
            if '' == (self.cleaned_data[delimiter_key] or ''):
                self.cleaned_data[delimiter_key] = self.get_guessed_delimiter(key)
                if self.cleaned_data[delimiter_key] is None:
                    self.add_error(key, _("delimiter_not_found_please_check_input"))
                    self.add_error(delimiter_key, _("delimiter_not_found_please_specify_it"))
                # else:
                #     self.add_error(key, "eee")

    def get_template_files(self):
        media_root = business_process.get_user_media_root(self.user)

        col_header = [explicit_item(n, **ids) for n, ids in self.get_as_list('host')]
        row_header = [explicit_item(n, **ids) for n, ids in self.get_as_list('virus')]

        # Prepare the legend
        mapping = models.GlobalViralHostResponseValue.objects_mappable().order_by('value')
        legend_name = []
        legend_value = []
        legend = []
        for m in models.GlobalViralHostResponseValue.objects_mappable().order_by('value'):
            legend_name.append([m.name])
            legend_value.append(m.value)
            legend.append([m.name, m.value])
        df_legend = pd.DataFrame(legend_name, columns=[str(_("Recommended responses scheme"))], index=legend_value)

        filename = 'VHRdb template generated on %(date)s at %(time)s.%(seed)s' % dict(
            date=timezone.now().strftime('%Y-%m-%d'),
            time=timezone.now().strftime('%Hh%Mm%Ss'),
            seed="".join([random.choice(string.ascii_letters) for i in range(4)])
        )
        file_path = os.path.join(media_root, filename + ".xlsx")
        with pd.ExcelWriter(file_path) as writer:
            df_data = pd.DataFrame(
                data=[["<response>"] * len(col_header)] * len(row_header),
                columns=col_header,
                index=row_header,
            )
            df_data.to_excel(writer)
            df_legend.to_excel(writer, startcol=1, startrow=len(row_header) + 4)
        os.chmod(file_path, 0o777)

        if len(col_header) > 4:
            col_header = col_header[:4] + ['...', ]
            data = ["<response>"] * 4 + ['...', ]
        else:
            data = ["<response>"] * len(col_header)
        if len(row_header) > 4:
            row_header = row_header[:4] + ['...', ]
            data = [data] * 4 + [['...'] * len(data), ]
        else:
            data = [data, ] * len(row_header)
        file_path = os.path.join(media_root, filename + ".sample.xlsx")
        with pd.ExcelWriter(file_path) as writer:
            df_data = pd.DataFrame(
                data=data,
                columns=col_header,
                index=row_header,
            )
            df_data.to_excel(writer)
        os.chmod(file_path, 0o777)
        return dict(
            file_url=settings.MEDIA_URL + filename + ".xlsx",
            sample_path=os.path.join(media_root, filename + ".sample.xlsx"),
        )


class ResponseUpdateForm(forms.ModelForm):
    # virus = forms.HiddenInput()
    # host = forms.HiddenInput()
    # data_source = forms.HiddenInput()

    class Meta:
        model = models.ViralHostResponseValueInDataSource
        fields = (
            'raw_response',
        )

    def __init__(self,
                 instance=None,
                 virus_pk=None,
                 host_pk=None,
                 ds_pk=None,
                 *args, **kwargs
                 ):
        if instance is not None and (
                virus_pk is not None or
                host_pk is not None or
                ds_pk is not None
        ):
            raise NotImplementedError()
        super().__init__(
            instance=models.ViralHostResponseValueInDataSource.objects.get(
                virus__pk=virus_pk,
                data_source__pk=ds_pk,
                host__pk=host_pk),
            *args, **kwargs)

    def save(self, commit=True):
        instance = super().save(commit=False)
        mapping = dict(instance.data_source.get_mapping())
        try:
            instance.response = mapping[instance.raw_response]
        except KeyError:
            instance.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
        if commit:
            instance.save()
            instance.data_source.save()
            self.save_m2m()
        return instance


class ContactOwnerForm(forms.Form):
    recipient = forms.CharField(
        max_length=256,
        required=False,
        label=_("ContactOwnerForm__recipient"),
    )
    data_source = forms.CharField(
        max_length=256,
        required=False,
        label=models.DataSource._meta.verbose_name.title(),
    )
    subject = forms.CharField(
        max_length=256,
        required=True,
        label=_("ContactOwnerForm__email_subject"),
    )
    body = forms.CharField(
        required=True,
        label=_("ContactOwnerForm__email_body"),
        widget=forms.widgets.Textarea(),
    )

    def __init__(self, data_source=None, recipient=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if recipient:
            self.fields["recipient"].initial = recipient.last_name.upper() + " " + recipient.first_name.title()
        self.fields["recipient"].widget.attrs["disabled"] = True
        self.fields["data_source"].initial = str(data_source)
        self.fields["data_source"].widget.attrs["disabled"] = True


class SearchForm(CommonSearchPrefForm):
    search = forms.CharField(
        label=_("Term to search for"),
        help_text=_("Example: T4, NC_000866.4, spotting method"),
        required=True,
        min_length=1,
        widget=forms.HiddenInput(),
    )
    str_max_length = forms.IntegerField(
        required=False,
        initial=-1,
        widget=forms.HiddenInput(),
    )  # long string in results should be reduced to at most str_max_length
    sample_size = forms.IntegerField(
        required=False,
        initial=0,
        widget=forms.HiddenInput(),
    )
    sorting = forms.ChoiceField(
        required=False,
        initial="TFIDFnCOV",
        label="Sorting [only in debug]",
        widget=forms.RadioSelect(
            attrs={
                'data-onchange-js': 'load_data_wrapper',
            },
        ) if settings.DEBUG else forms.HiddenInput(),
        choices=(  # should be sync with views_api.search
            ('PK', 'None'),
            ('TF', 'TF'),
            ('TFIDF', 'TFIDF'),
            ('TFIDFnCOV', 'TFIDF + coverage'),
        ),
    )
    kind = forms.ChoiceField(
        choices=(
            ('all', _('All')),
            ('virus', _('Virus')),
            ('host', _('Host')),
            ('data_source', _('Data source')),
        ),
        initial='all',
        required=False,
        label=_("Search in"),
        widget=forms.RadioSelect(
            attrs={
                'data-onchange-js': 'load_data_wrapper',
            },
        ),
    )
    ui_help = forms.BooleanField(
        initial=False,
        required=False,
        widget=forms.HiddenInput(),
    )
    owner = ByLastFirstNameModelMultipleChoiceField(
        queryset=get_user_model().objects.none(),
        label="",
        # label=_("SearchForm.owner.label"),
        help_text=_("SearchForm.owner.help_text"),
        widget=BoostrapSelectMultiple(
            allSelectedText=_("From all owner"),
            nonSelectedText=_("From all owner"),
            SelectedText=_(" owners selected"),
            onChangeFunctionName="load_data_wrapper",
            attrs={
                "data-select-all-inv-nop": False,
            },
        ),
        required=False,
    )
    helper = FormHelper()
    helper.layout = Layout(
        Row(
            Column(
                'search',
                'sample_size',
                'sorting',
                'ui_help',
                'str_max_length',
                'kind',
                css_class="col"
            ),
        ),
        Row(
            Column('life_domain', css_class="col"),
        ),
        Row(
            Column('only_published_data', css_class="col"),
        ),
        Row(
            Column('only_virus_ncbi_id', css_class="col"),
        ),
        Row(
            Column('only_host_ncbi_id', css_class="col"),
        ),
        Row(
            Column('owner', css_class="col"),
        ),
    )

    def __init__(self, user=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if user is None:
            return
        self.fields["owner"].queryset = get_user_model().objects.filter(
            datasource__in=only_public_or_granted_or_owned_queryset_filter(
                None,
                request=None,
                user=user,
                queryset=models.DataSource.objects.all(),
            )
        ).distinct()

    def clean(self):
        cleaned_data = super().clean()
        for key, value in cleaned_data.items():
            if value is None or value == "":
                initial = self.initial.get(key, self.fields[key].initial)
                cleaned_data[key] = initial
        return cleaned_data
