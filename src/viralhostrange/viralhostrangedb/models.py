import colorsys
import re
import traceback

from basetheme_bootstrap import user_preferences
from colorfield.fields import ColorField
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import mail_admins
from django.core.validators import MaxLengthValidator
from django.db import models
# Create your models here.
from django.db.models import Exists, OuterRef, Q
from django.db.models.functions import Upper
from django.urls import reverse
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _, gettext

from viralhostrangedb import business_process


class GlobalViralHostResponseValue(models.Model):
    class Meta:
        verbose_name = _("Global Viral-Host response values")
        verbose_name_plural = _("Global Viral-Host response values")

        index_together = [
            ["id", "name"],
            ["id", "value"],
            ["id", "name", "value"],
        ]

        permissions = (
            ("change_globalviralhostresponsevalue_color", "Can change Global Viral-Host response color"),
        )

    name = models.CharField(
        verbose_name=_("response__name__verbose_name"),
        help_text=_("response__name__help_text"),
        max_length=32,
        validators=[MaxLengthValidator(32)] if settings.DEBUG else [],
        unique=True,
    )
    description = models.TextField(
        verbose_name=_("response__description__verbose_name"),
        help_text=_("response__description__help_text"),
        blank=True,
        default="",
    )
    value = models.FloatField(
        verbose_name=_("response__value__verbose_name"),
        help_text=_("response__value__help_text"),
        default=-1000000,
    )
    color = ColorField(
        verbose_name=_("response__color__verbose_name"),
        help_text=_("response__color__help_text"),
    )

    def __str__(self):
        return "%s (value=%s)" % (self.name, '{0:g}'.format(self.value))

    def to_hsv(self):
        h, s, v = colorsys.rgb_to_hsv(
            int("0x" + self.color[1:3], 0) / 255,
            int("0x" + self.color[3:5], 0) / 255,
            int("0x" + self.color[5:7], 0) / 255,
        )
        return h * 360, s * 100, v * 50

    @classmethod
    def get_hsv_color_for(cls, value):
        lower = cls.objects_mappable().filter(value__lte=value).order_by("-value").first()
        upper = cls.objects_mappable().filter(value__gte=value).order_by("value").first()
        lower_hsv = lower.to_hsv()
        upper_hsv = upper.to_hsv()
        if lower.value == upper.value:
            percentage = 0
        else:
            percentage = (value - lower.value) / (upper.value - lower.value)

        hue = lower_hsv[0] * percentage + upper_hsv[0] * (1 - percentage)
        sat = lower_hsv[1] * percentage + upper_hsv[1] * (1 - percentage)
        lgt = lower_hsv[2] * percentage + upper_hsv[2] * (1 - percentage)
        return hue, sat, lgt

    @classmethod
    def get_rgb_color_for(cls, value):
        hue, sat, lgt = cls.get_hsv_color_for(value=value)
        r, g, b = colorsys.hsv_to_rgb(hue / 360, sat / 100, lgt / 50)
        return int(r * 255), int(g * 255), int(b * 255)

    @classmethod
    def get_html_color_for(cls, value):
        r, g, b = cls.get_rgb_color_for(value)
        return '#%02x%02x%02x' % (r, g, b)

    @staticmethod
    def get_not_mapped_yet_keyword():
        return 'NOT MAPPED YET'

    @staticmethod
    def get_not_mapped_yet():
        return GlobalViralHostResponseValue.objects.get(
            name=GlobalViralHostResponseValue.get_not_mapped_yet_keyword())

    @staticmethod
    def get_not_mapped_yet_pk():
        return GlobalViralHostResponseValue.objects.filter(
            name=GlobalViralHostResponseValue.get_not_mapped_yet_keyword()).values_list('pk', flat=True)[0]

    @classmethod
    def objects_mappable(cls):
        return cls.objects.filter(~Q(name=cls.get_not_mapped_yet_keyword()))


class DataSource(models.Model):
    class Meta:
        verbose_name = _("Data Source")
        verbose_name_plural = _("Data Sources")
        ordering = [Upper('name'), ]

    @classmethod
    def get_objects_annotated_with_pending(cls, qs=None):
        if qs is None:
            qs = DataSource.objects.all()
        pending = DataSource.objects.all().filter(
            responseindatasource__response=GlobalViralHostResponseValue.get_not_mapped_yet()
        ).filter(pk=OuterRef('pk'))
        return qs.annotate(
            mapping_done=~Exists(pending)
        )

    owner = models.ForeignKey(
        to=get_user_model(),
        on_delete=models.PROTECT,
    )
    provider_last_name = models.CharField(
        verbose_name=_("data_source__provider_last_name__verbose_name"),
        help_text=_("data_source__provider_last_name__help_text"),
        max_length=64,
        validators=[MaxLengthValidator(64)] if settings.DEBUG else [],
        blank=True,
        null=True,
    )
    provider_first_name = models.CharField(
        verbose_name=_("data_source__provider_first_name__verbose_name"),
        help_text=_("data_source__provider_first_name__help_text"),
        max_length=64,
        validators=[MaxLengthValidator(64)] if settings.DEBUG else [],
        blank=True,
        null=True,
    )
    public = models.BooleanField(
        verbose_name=_("public__verbose_name"),
        help_text=_("public__help_text"),
        default=False,
    )
    name = models.CharField(
        verbose_name=_("data_source__name__verbose_name"),
        help_text=_("data_source__name__help_text"),
        max_length=128,
        validators=[MaxLengthValidator(128)] if settings.DEBUG else []
    )
    description = models.TextField(
        verbose_name=_("data_source__description__verbose_name"),
        help_text=_("data_source__description__help_text"),
        blank=True,
        default="",
    )
    raw_name = models.TextField(
        verbose_name=_("raw_name__verbose_name"),
        help_text=_("raw_name__help_text"),
    )
    kind = models.CharField(
        verbose_name=_("kind__verbose_name"),
        help_text=_("kind__help_text"),
        max_length=4,
        choices=(
            ("FILE", _("From a file")),
            ("URL", _("From an URL")),
            # ("LIVE", _("Live input")),
        )
    )
    allowed_users = models.ManyToManyField(
        to=get_user_model(),
        verbose_name=_("allowed_users__verbose_name"),
        help_text=_("allowed_users__help_text"),
        related_name='datasource_granted_set',
        blank=True,
        through="GrantedUser",
    )
    experiment_date = models.DateField(
        verbose_name=_("experiment_date__verbose_name"),
        help_text=_("experiment_date__help_text"),
        blank=True,
        null=True,
    )
    creation_date = models.DateTimeField(
        verbose_name=_("creation_date__verbose_name"),
        help_text=_("creation_date__help_text"),
    )
    last_edition_date = models.DateTimeField(
        verbose_name=_("last_edition_date__verbose_name"),
        help_text=_("last_edition_date__help_text"),
    )
    publication_url = models.URLField(
        verbose_name=_("publication_url__verbose_name"),
        help_text=_("publication_url__help_text"),
        null=True,
        blank=True,
    )
    life_domain = models.CharField(
        choices=(
            ("archaea", _("Archaea")),
            ("bacteria", _("Bacteria")),
            ("eukaryote", _("Eukaryote")),
        ),
        verbose_name=_("life_domain.label"),
        help_text=_("life_domain.help_text"),
        null=False,
        blank=False,
        default="bacteria",
        max_length=32,
    )

    def __str__(self):
        return "%s (created on %s)" % (
            self.name,
            "???" if self.creation_date is None else self.creation_date.strftime('%Y-%m-%d %H:%M:%S %Z'),
        )

    def get_absolute_url(self):
        return reverse("viralhostrangedb:data-source-detail", args=[self.pk])

    def get_ordered_mapping(self, only_pk=False):
        return self.get_mapping(ordered=True)

    def get_mapping(self, only_pk=False, ordered=False):
        if not only_pk:
            used_responses = dict()
            for r in GlobalViralHostResponseValue.objects.filter(
                    pk__in=self.responseindatasource.values_list('response__pk').distinct()):
                used_responses[r.pk] = r
        q = self.responseindatasource.values_list('raw_response', 'response__pk')
        if ordered:
            q = q.order_by("raw_response")
        for raw_response, response_mapped in q.distinct():
            yield raw_response, response_mapped if only_pk else used_responses[response_mapped]

    @property
    def is_mapping_done(self):
        return not ViralHostResponseValueInDataSource.objects \
            .filter(data_source__pk=self.pk, response__name=GlobalViralHostResponseValue.get_not_mapped_yet_keyword()) \
            .exists()

    @property
    def is_mapping_absent(self):
        return not GlobalViralHostResponseValue.objects \
            .filter(responseindatasource__data_source__pk=self.pk) \
            .filter(~Q(name=GlobalViralHostResponseValue.get_not_mapped_yet_keyword())) \
            .exists()

    @property
    def has_provider(self):
        return self.provider_last_name is not None \
               and self.provider_first_name is not None \
               and len(self.provider_last_name) > 0 \
               and len(self.provider_first_name) > 0

    @classmethod
    def get_not_mapped_data_sources(cls, *, owner):
        return cls.objects.filter(pk__in=cls.get_not_mapped_data_sources_pk(owner=owner))

    @classmethod
    def get_not_mapped_data_sources_pk(cls, *, owner):
        if owner.pk is None:
            return []
        return cls._get_not_mapped_data_sources(owner=owner) \
            .values_list("responseindatasource__data_source__pk", flat=True).distinct()

    @classmethod
    def has_not_mapped_data_sources(cls, *, owner):
        if owner.pk is None:
            return False
        return cls._get_not_mapped_data_sources(owner=owner).exists()

    @classmethod
    def _get_not_mapped_data_sources(cls, *, owner):
        return GlobalViralHostResponseValue.objects.filter(
            name=GlobalViralHostResponseValue.get_not_mapped_yet_keyword(),
            responseindatasource__data_source__owner__pk=owner.pk)

    @property
    def raw_filename(self):
        if self.raw_name is None or len(str(self.raw_name).strip()) == 0:
            raw_name = self.name
        else:
            raw_name = self.raw_name
        raw_name = str(raw_name)
        if len(raw_name) > 0 and raw_name[-1] == '/':
            raw_name = raw_name[:-1]
        try:
            raw_name = raw_name[raw_name.rindex("/") + 1:]
        except ValueError:
            pass
        raw_name = re.sub(r'([^.a-zA-Z0-9])', '_', raw_name)
        raw_name = re.sub(r'(_[_]+)', '_', raw_name)
        return raw_name

    def save(self, *args, **kwargs):
        if self.pk is None:
            self.creation_date = timezone.now()
        if self.publication_url == '':
            self.publication_url = None
        self.last_edition_date = timezone.now()
        super().save(*args, **kwargs)


class GrantedUser(models.Model):
    class Meta:
        unique_together = (('user', 'data_source'),)

    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    data_source = models.ForeignKey(DataSource, on_delete=models.CASCADE)
    can_write = models.BooleanField(default=False)

    def __str__(self):
        return "%s for %s on DS %i" % ("RW" if self.can_write else "R ", self.user.__str__(), self.data_source.id)


class IsNCBIIdentifier(models.Model):
    class Meta:
        abstract = True

    tax_id_value = models.CharField(
        verbose_name=_("tax_id_value__verbose_name"),
        help_text=_("tax_id_value__help_text"),
        max_length=32,
        validators=[MaxLengthValidator(32)] if settings.DEBUG else [],
        blank=True,
        null=True,
    )

    @property
    def tax_id(self):
        if self.is_ncbi_identifier:
            return self.tax_id_value
        return None

    @property
    def is_ncbi_identifier(self):
        if self.is_ncbi_identifier_value is not None:
            return self.is_ncbi_identifier_value
        if self.identifier != '':
            try:
                nc_id, tax_id = business_process.get_ncbi_ids(self.identifier)
                self.is_ncbi_identifier_value = nc_id is not None
                self.tax_id_value = tax_id
            except Exception as e:
                mail_admins("Error in is_ncbi_identifier", message=traceback.format_exc(), fail_silently=True, )
        else:
            self.is_ncbi_identifier_value = False
            self.tax_id_value = None
        self.save()
        return self.is_ncbi_identifier_value

    def get_ncbi_link(self):
        return "https://www.ncbi.nlm.nih.gov/nuccore/" + str(self.identifier) if self.is_ncbi_identifier else None

    def get_tax_link(self):
        if self.is_ncbi_identifier:
            return "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=" + str(self.tax_id)
        return None


class Host(IsNCBIIdentifier):
    class Meta:
        verbose_name = _("Host")
        verbose_name_plural = _("Hosts")
        ordering = [Upper('name'), ]

    data_source = models.ManyToManyField(
        to=DataSource,
        verbose_name=_("data_source__verbose_name"),
        help_text=_("data_source__help_text"),
    )
    name = models.CharField(
        verbose_name=_("host__name__verbose_name"),
        help_text=_("host__name__help_text"),
        max_length=64,
        validators=[MaxLengthValidator(64)] if settings.DEBUG else []
    )
    identifier = models.CharField(
        verbose_name=_("host__identifier__verbose_name"),
        help_text=_("host__identifier__help_text"),
        max_length=128,
        validators=[MaxLengthValidator(128)] if settings.DEBUG else [],
        blank=True,
        default='',
    )
    is_ncbi_identifier_value = models.BooleanField(
        verbose_name=_("Is identifier an NCBI identifier"),
        help_text=_("This attribute is computed"),
        default=None,
        null=True,
        blank=True,
    )

    @property
    def explicit_name(self):
        return business_process.explicit_item(self.name, self.identifier, tax_id=self.tax_id_value)

    @property
    def explicit_name_html(self):
        return mark_safe(
            business_process.explicit_item(self.name, self.identifier, tax_id=self.tax_id_value, html=True))

    def __str__(self):
        return "%s from %s" % (self.explicit_name, ", ".join(str(s) for s in self.data_source.all()))

    def get_absolute_url(self):
        return reverse("viralhostrangedb:host-detail", args=[self.pk])


class Virus(IsNCBIIdentifier):
    class Meta:
        verbose_name = _("Virus")
        verbose_name_plural = _("Viruses")
        ordering = [Upper('name'), ]

    data_source = models.ManyToManyField(
        to=DataSource,
        verbose_name=_("data_source__verbose_name"),
        help_text=_("data_source__help_text"),
    )
    name = models.CharField(
        verbose_name=_("virus__name__verbose_name"),
        help_text=_("virus__name__help_text"),
        max_length=64,
        validators=[MaxLengthValidator(64)] if settings.DEBUG else []
    )
    her_identifier = models.IntegerField(
        verbose_name=_("virus__her_identifier__verbose_name"),
        help_text=_("virus__her_identifier__help_text"),
        blank=True,
        null=True,
    )
    identifier = models.CharField(
        verbose_name=_("virus__identifier__verbose_name"),
        help_text=_("virus__identifier__help_text"),
        max_length=128,
        validators=[MaxLengthValidator(128)] if settings.DEBUG else [],
        blank=True,
        default='',
    )
    is_ncbi_identifier_value = models.BooleanField(
        verbose_name=_("Is identifier an NCBI identifier"),
        help_text=_("This attribute is computed"),
        default=None,
        null=True,
        blank=True,
    )

    def get_herelle_link(self):
        return "https://www.phage.ulaval.ca/?pageDemandee=phage&noPhage=%i&id=41&L=1" % self.her_identifier if self.her_identifier else None

    @property
    def explicit_name(self):
        return business_process.explicit_item(self.name, self.identifier, self.her_identifier, tax_id=self.tax_id_value)

    @property
    def explicit_name_html(self):
        return mark_safe(
            business_process.explicit_item(self.name, self.identifier, self.her_identifier, tax_id=self.tax_id_value,
                                           html=True))

    def __str__(self):
        return "%s from %s" % (self.explicit_name, ", ".join(str(s) for s in self.data_source.all()))

    def get_absolute_url(self):
        return reverse("viralhostrangedb:virus-detail", args=[self.pk])


class ViralHostResponseValueInDataSource(models.Model):
    class Meta:
        verbose_name = _("Viral-Host response value in data sources")
        verbose_name_plural = _("Viral-Host responses value in data sources")

    data_source = models.ForeignKey(
        to=DataSource,
        verbose_name=_("data_source__verbose_name"),
        help_text=_("data_source__help_text"),
        on_delete=models.CASCADE,
        related_name="responseindatasource"
    )
    virus = models.ForeignKey(
        to=Virus,
        verbose_name=_("virus__verbose_name"),
        help_text=_("virus__help_text"),
        on_delete=models.PROTECT,
        related_name="responseindatasource"
    )
    host = models.ForeignKey(
        to=Host,
        verbose_name=_("host__verbose_name"),
        help_text=_("host__help_text"),
        on_delete=models.PROTECT,
        related_name="responseindatasource"
    )
    raw_response = models.FloatField(
        verbose_name=_("raw_response__verbose_name"),
        help_text=_("raw_response__help_text"),
    )
    response = models.ForeignKey(
        to=GlobalViralHostResponseValue,
        verbose_name=_("response__verbose_name"),
        help_text=_("response__help_text"),
        on_delete=models.PROTECT,
        related_name="responseindatasource"
    )

    def __str__(self):
        return "(%s,%s)%s" % (self.virus.explicit_name, self.host.explicit_name, self.raw_response)


class UserPreferences(user_preferences.UserPreferencesAbstractModel, models.Model):
    class Meta:
        verbose_name = _("User Preferences")
        verbose_name_plural = _("Users Preferences")

    preferences_groups = {
        gettext("Analysis Tools"): [
            'virus_infection_ratio',
            'hide_rows_with_no_infection',
            'host_infection_ratio',
            'hide_cols_with_no_infection',
            'weak_infection',
            'agreed_infection',
        ],
        gettext("Data filtering options"): [
            'only_published_data',
            'only_host_ncbi_id',
            'only_virus_ncbi_id',
            'life_domain',
        ],
        gettext("Rendering options"): [
            'display_all_at_once',
            'horizontal_column_header',
            'focus_disagreements_toggle',
            'render_actual_aggregated_responses',
            'full_length_vh_name_toggle',
        ],
        gettext("Application settings"): [
            'edition_notification',
        ],
    }
    preferences_groups_descriptions = {
        gettext("Data filtering options"): _("Data filtering options preferences_groups_descriptions"),
        gettext("Rendering options"): _("Rendering options preferences_groups_descriptions"),
        gettext("Analysis Tools"): _("Analysis Tools preferences_groups_descriptions"),
    }

    focus_disagreements_toggle = models.BooleanField(
        verbose_name=_("Render table focusing on disagreements"),
        help_text=_("Responses where data sources disagree are highlighted, others are faded."),
        default=False,
    )
    display_all_at_once = models.BooleanField(
        verbose_name=_("Display all the table at once, even if it does not fit on screen"),
        help_text=_("Otherwise we fit the table to the screen and use scroll bar when needed."),
        default=False,
    )
    virus_infection_ratio = models.BooleanField(
        verbose_name=_("Show the infection ratio for the viruses"),
        help_text=_("Note that it can deteriorate the overall response time."),
        default=False,
    )
    hide_rows_with_no_infection = models.BooleanField(
        verbose_name=_("hide_rows_with_no_infection_label"),
        help_text=_("hide_rows_with_no_infection_help_text"),
        default=False,
    )
    host_infection_ratio = models.BooleanField(
        verbose_name=_("Show the infection ratio for the hosts"),
        help_text=_("Note that it can deteriorate the overall response time."),
        default=False,
    )
    hide_cols_with_no_infection = models.BooleanField(
        verbose_name=_("hide_cols_with_no_infection_label"),
        help_text=_("hide_cols_with_no_infection_help_text"),
        default=False,
    )
    weak_infection = models.BooleanField(
        verbose_name=_("Consider all positive response as a infection"),
        help_text=_(
            "By default we consider that there is an infection only when the highest response is observed."),
        default=False,
    )
    agreed_infection = models.BooleanField(
        verbose_name=_("Consider that there is an infection only when <i>all data sources</i> "
                       "documenting the interaction observed an infection"),
        help_text=_(
            "By default we consider that there is an infection when "
            "in <i>at least one</i> data source an infection was observed."),
        default=False,
    )
    only_published_data = models.BooleanField(
        verbose_name=_("Only published data"),
        help_text=_("only_published_data.help_text"),
        default=False,
    )
    only_host_ncbi_id = models.BooleanField(
        verbose_name=_("only_host_ncbi_id.label"),
        help_text=_("only_host_ncbi_id.help_text"),
        default=False,
    )
    only_virus_ncbi_id = models.BooleanField(
        verbose_name=_("only_virus_ncbi_id.label"),
        help_text=_("only_virus_ncbi_id.help_text"),
        default=False,
    )
    life_domain = models.CharField(
        verbose_name=_("preference.life_domain.label"),
        help_text=_("preference.life_domain.help_text"),
        choices=(('', _('All')),) + DataSource._meta.get_field("life_domain").choices,
        default='',
        max_length=32,
        blank=True,
    )
    render_actual_aggregated_responses = models.BooleanField(
        verbose_name=_("preference.render_actual_aggregated_responses.label"),
        help_text=_("preference.render_actual_aggregated_responses.help_text"),
        default=False,
    )
    full_length_vh_name_toggle = models.BooleanField(
        verbose_name=_("preference.full_length_vh_name_toggle.label"),
        help_text=_("preference.full_length_vh_name_toggle.help_text"),
        default=False,
    )
    horizontal_column_header = models.BooleanField(
        verbose_name=_("preference.horizontal_column_header.label"),
        help_text=_("preference.horizontal_column_header.help_text"),
        default=False,
    )
    NOTIFY_FOR_NO_ONE = 0
    NOTIFY_FOR_CURATOR = 1
    NOTIFY_FOR_EDITOR_AND_CURATOR = 2
    NOTIFY_EVEN_FOR_ME = 3
    edition_notification = models.IntegerField(
        verbose_name=_("preference.edition_notification.label"),
        help_text=_("preference.edition_notification.help_text"),
        choices=(
            # (NOTIFY_FOR_NO_ONE, _("preference.edition_notification.NOTIFY_FOR_NO_ONE")),
            (NOTIFY_FOR_CURATOR, _("preference.edition_notification.NOTIFY_FOR_CURATOR")),
            (NOTIFY_FOR_EDITOR_AND_CURATOR, _("preference.edition_notification.NOTIFY_FOR_EDITOR_AND_CURATOR")),
            (NOTIFY_EVEN_FOR_ME, _("preference.edition_notification.NOTIFY_EVEN_FOR_ME")),
        ),
        default=NOTIFY_FOR_CURATOR,
    )
