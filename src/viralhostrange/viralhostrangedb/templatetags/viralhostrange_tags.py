import json
import logging

from django import template
from django.conf import settings
from django.db.models import QuerySet
from django.utils.text import get_text_list
from django.utils.translation import gettext

from viralhostrangedb import models, mixins, business_process

register = template.Library()

logger = logging.getLogger(__name__)


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def has_mapping_pending(owner):
    return owner is not None and models.DataSource.has_not_mapped_data_sources(owner=owner)


@register.filter
def class_verbose_name(obj):
    return obj._meta.verbose_name.title()


# @register.filter
# def class_verbose_name_plural(obj):
#     return obj._meta.verbose_name_plural.title()


@register.filter
def field_verbose_name(obj, field_name):
    return obj._meta.get_field(field_name).verbose_name.title()


# @register.filter
# def field_verbose_class_name(obj, field_name):
#     return obj._meta.get_field(field_name).related_model._meta.verbose_name.title()


# @register.filter
# def field_verbose_class_name_plural(obj, field_name):
#     return obj._meta.get_field(field_name).related_model._meta.verbose_name_plural.title()


@register.filter
def can_edit(user, obj):
    # if user is a WSGIRequest, get the attr user
    user = getattr(user, "user", user)
    if not user.is_authenticated:
        return False
    try:
        return mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=type(obj).objects.filter(pk=obj.pk),
            user=user,
        ).exists()
    except Exception as e:
        if settings.DEBUG:
            raise e
    return False


@register.filter
def is_editor_or_owner_of_ds(user, obj_pk):
    # used to access datasource history, should follow get_log_entry_with_permission_check_or_404
    # if user is a WSGIRequest, get the attr user
    if hasattr(obj_pk,'pk'):
        obj_pk=obj_pk.pk
    user = getattr(user, "user", user)
    if not user.is_authenticated:
        return False
    try:
        return mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.filter(pk=obj_pk),
            user=user,
        ).exists()
    except Exception as e:
        if settings.DEBUG:
            raise e
    return False


@register.filter
def get_curators_list(_):
    return business_process.get_curators().order_by("last_name", "first_name")


@register.filter
def can_delete(user, obj):
    # if user is a WSGIRequest, get the attr user
    user = getattr(user, "user", user)
    try:
        return obj.owner.pk == user.pk  # or obj.granteduser_set.filter(user=user, can_delete=True).exists()
    except Exception as e:
        if settings.DEBUG:
            raise e
    return False


@register.filter
def can_see(user, obj):
    # if user is a WSGIRequest, get the attr user
    user = getattr(user, "user", user)
    try:
        if isinstance(obj, QuerySet):
            if obj.model == models.DataSource:
                qs = obj
                boolean_return = False
            else:
                raise NotImplementedError("tag can_see not implemented for QuerySet of " + obj.model.__name__)
        else:
            qs = type(obj).objects.filter(pk=obj.pk)
            boolean_return = True
        qs = mixins.only_public_or_granted_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=qs,
            user=user,
        )
        if boolean_return:
            return qs.exists()
        return qs
    except Exception as e:
        if settings.DEBUG:
            raise e
    return False


@register.filter
def is_advanced_option(obj):
    try:
        return obj.field.widget.attrs['data-advanced-option']
    except KeyError:
        return False


@register.filter
def get_in_widget(obj, attr):
    try:
        return str(obj.field.widget.attrs[attr])
    except KeyError as e:
        return ''


@register.filter
def has_backup_file(le):
    return business_process.get_backup_file_path(le, test_if_exists=True) is not None


@register.filter
def should_have_backup_file(le):
    try:
        change_message = json.loads(le.change_message)
        return change_message[0]["has_backup"]
    except IndexError:
        return False
    except KeyError:
        return False
    except json.JSONDecodeError:
        return False


@register.filter
def get_change_message_with_action(le):
    """
    If self.change_message is a JSON structure, interpret it as a change
    string, properly translated.
    """
    try:
        change_message = json.loads(le.change_message)
        messages = []
        for sub_message in change_message:

            if 'added' in sub_message:
                if 'action' in sub_message['added']:
                    messages.append(gettext(sub_message['added']['action']))
                elif 'fields' in sub_message['added'] and len(sub_message['added']['fields']) > 0:
                    sub_message['added']['fields'] = get_text_list(sub_message['added']['fields'], gettext('and'))
                    messages.append(gettext('Added {fields}.').format(fields=sub_message['added']['fields']))
                else:
                    messages.append(gettext('Data source creation.'))

            elif 'changed' in sub_message:
                fields = []
                for f in sub_message['changed'].setdefault('fields', []):
                    if f == "allowed_users_editor":
                        fields.append("granted users")
                    else:
                        fields.append(f)
                sub_message['changed']['fields'] = get_text_list(fields, gettext('and'))
                if 'action' in sub_message['changed']:
                    msg = gettext('Changed {fields} by {action}.')
                else:
                    msg = gettext('Changed {fields}.')
                messages.append(msg.format(**sub_message['changed']))

            elif 'deleted' in sub_message:
                current_sub_message = sub_message['deleted']
                current_sub_message.setdefault("object", "???")
                messages.append(gettext('Deleted {object}.').format(**current_sub_message))

        change_message = ' '.join(msg[0].upper() + msg[1:] for msg in messages)
        return change_message or gettext('No fields changed.')
    except json.JSONDecodeError:
        return le.get_change_message()
