import logging
import os

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.management import BaseCommand
from django.utils import timezone

from viralhostrangedb import business_process, models, forms

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = "a command to remove old backups when there are too many"

    def handle(self, *args, **options):
        # create accounts
        alice = get_user_model().objects.create_user(
            username="alice",
            email="alice@demo.vhrdb",
            password="alicealice",
            first_name="Alice",
            last_name="User",
        )
        bob = get_user_model().objects.create_user(
            username="bob",
            email="bob@demo.vhrdb",
            password="bobbob",
            first_name="Bob",
            last_name="User",
        )
        curator = get_user_model().objects.create_user(
            username="curator",
            email="curator@demo.vhrdb",
            password="curatorcurator",
            first_name="Charlie",
            last_name="Curator",
        )
        business_process.set_curator(curator, True)
        admin = get_user_model().objects.create_user(
            username="admin",
            email="admin@demo.vhrdb",
            password="adminadmin",
            first_name="admin",
            last_name="Admin",
            is_staff=True,
        )
        Group.objects.get(name="Admin").user_set.add(admin)
        root = get_user_model().objects.create_user(
            username="root",
            email="root@demo.vhrdb",
            password="rootroot",
            first_name="Root",
            last_name="Superuser",
            is_staff=True,
            is_superuser=True,
        )

        # load data source
        filename = os.path.join(".static", "media", "example.xlsx")
        alice_data = models.DataSource.objects.create(
            raw_name=filename,
            kind="FILE",
            owner=alice,
            name="alice data",
            description="a data source own by Alice that can be changed by Bob",
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
            public=True,
        )
        business_process.import_file(
            data_source=alice_data,
            file=filename,
        )

        # allow bob to edit it
        alice_data.allowed_users.add(bob, through_defaults=dict(can_write=True))

        # map it
        form = forms.RangeMappingForm(data_source=alice_data)
        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=alice_data, data=data)
        form.is_valid()
        form.save()

        # load data source
        filename = os.path.join("test_data", "demo-1.xlsx")
        bob_data = models.DataSource.objects.create(
            raw_name=filename,
            kind="FILE",
            owner=bob,
            name="bob data",
            description="a data source own by Bob",
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
            public=True,
        )
        business_process.import_file(
            data_source=bob_data,
            file=filename,
        )

        # map it
        form = forms.RangeMappingForm(data_source=bob_data)
        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=bob_data, data=data)
        form.is_valid()
        form.save()

        # load data source
        filename = os.path.join("test_data", "demo-2.xlsx")
        bob_data_private = models.DataSource.objects.create(
            raw_name=filename,
            kind="FILE",
            owner=bob,
            name="secret data of Bob",
            description="a private data source own by Bob, and invisible to anyone else.",
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
            public=False,
        )
        business_process.import_file(
            data_source=bob_data_private,
            file=filename,
        )

        # map it
        form = forms.RangeMappingForm(data_source=bob_data_private)
        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=bob_data_private, data=data)
        form.is_valid()
        form.save()
