from django.core.management import BaseCommand

from viralhostrangedb import models, business_process


class Command(BaseCommand):
    help = "a command to alter is_ncbi_identifier_value of Virus and Host models"

    def add_arguments(self, parser):
        parser.add_argument(
            '--virus',
            action='store_true',
            dest='virus',
            default=False,
            help='run on viruses',
        )
        parser.add_argument(
            '--host',
            action='store_true',
            dest='host',
            default=False,
            help='run on hosts',
        )
        parser.add_argument(
            '--all',
            action='store_true',
            dest='all',
            default=False,
            help='run on both viruses and hosts',
        )
        parser.add_argument(
            '--progress',
            action='store_true',
            dest='progress',
            default=False,
            help='show a progressbar',
        )
        parser.add_argument(
            '--reset',
            action='store_true',
            dest='reset',
            default=False,
            help='scoped identifier have to be re-evaluated to know if they are ncbi identifier or not',
        )

    def handle(self, *args, **options):
        classes_to_do = []
        if options["virus"] or options["all"]:
            classes_to_do.append(models.Virus)
        if options["host"] or options["all"]:
            classes_to_do.append(models.Host)

        for klass in classes_to_do:
            business_process.set_identifier_status(
                queryset=business_process.get_identifier_status_unknown(klass.objects).filter(identifier=''),
                status=False,
            )
            if options["reset"]:
                business_process.reset_identifier_status(klass.objects)
            else:
                business_process.fetch_identifier_status(
                    queryset=business_process.get_identifier_status_unknown(klass.objects),
                    progress=options["progress"],
                )
