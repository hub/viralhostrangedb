from django.contrib.admin import models as admin_models
from django.core.management import BaseCommand

from viralhostrangedb import models, business_process


class Command(BaseCommand):
    help = "a command to remove old backups when there are too many"

    def handle(self, *args, **options):
        business_process.remove_old_backup_files(queryset=models.DataSource.objects.all())
        business_process.remove_old_orphan_backup_files(queryset=admin_models.LogEntry.objects.all())
