from django.core.management import BaseCommand

from live_settings import live_settings
from viralhostrangedb import business_process


class Command(BaseCommand):
    help = "a command to send notification for all the new event that occurred since last execution"

    def add_arguments(self, parser):
        parser.add_argument(
            '--starting_with',
            dest='starting_with',
            type=int,
            default=live_settings.log_entry_up_to or 0,
            help='primary key of the oldest LogEntry to consider',
        )

    def handle(self, *args, **kwargs):
        business_process.send_latest_edition_notification(kwargs['starting_with'])
