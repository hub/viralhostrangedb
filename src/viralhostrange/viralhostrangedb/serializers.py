from django.conf import settings
from drf_dynamic_fields import DynamicFieldsMixin
from rest_framework import serializers

from viralhostrangedb import models


def get_short_name(obj):
    if len(obj.name) > 23:
        return (obj.name[0:10] + "..." + obj.name[-10:]).replace('....', '...').replace('....', '...')
    return None


class VirusSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Virus
        depth = 1
        fields = ('name', 'short_name', 'identifier', 'is_ncbi_identifier_value', 'her_identifier', 'tax_id', 'id')

    short_name = serializers.SerializerMethodField()

    @staticmethod
    def get_short_name(obj):
        return get_short_name(obj)


class HostSerializer(DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Host
        depth = 1
        fields = ('name', 'short_name', 'identifier', 'is_ncbi_identifier_value', 'tax_id', 'id',)

    short_name = serializers.SerializerMethodField()

    @staticmethod
    def get_short_name(obj):
        return get_short_name(obj)


class DataSourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataSource
        depth = 1
        fields = (
            'id',
            'name',
            'description',
            'public',
            'creation_date',
            'last_edition_date',
        )


class GlobalViralHostResponseValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GlobalViralHostResponseValue
        depth = 1
        fields = (
            'name',
            'description',
            'value',
            'color',
        )


class HitOrder:
    class Meta:
        abstract = True

    @property
    def fields(self):
        fields = super().fields
        fields["relevance"] = serializers.SerializerMethodField()
        fields["has_identifier"] = serializers.SerializerMethodField()
        return fields

    def get_relevance(self, obj):
        return getattr(obj, "relevance", -1)

    def get_has_identifier(self, obj):
        return getattr(obj, "has_identifier", -1)


class Nop:
    class Meta:
        abstract = True


class HitOrderInDebugOnly(HitOrder if settings.DEBUG else Nop):
    class Meta:
        abstract = True


class VirusSerializerWithURL(HitOrderInDebugOnly, DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Virus
        depth = 1
        fields = (
            'name',
            'identifier',
            'is_ncbi_identifier_value',
            'her_identifier',
            'tax_id',
            'id',
            'url',
        )

    url = serializers.URLField(read_only=True, source='get_absolute_url')


class HostSerializerWithURL(HitOrderInDebugOnly, DynamicFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = models.Host
        depth = 1
        fields = (
            'name',
            'identifier',
            'is_ncbi_identifier_value',
            'tax_id',
            'id',
            'url',
        )

    url = serializers.URLField(read_only=True, source='get_absolute_url')


class DataSourceSerializerForSearch(HitOrderInDebugOnly, DynamicFieldsMixin, serializers.ModelSerializer):
    url = serializers.URLField(read_only=True, source='get_absolute_url')
    last_edition_date = serializers.DateTimeField(format="%Y-%m-%d")
    creation_date = serializers.DateTimeField(format="%Y-%m-%d")
    description = serializers.SerializerMethodField()
    has_publication = serializers.SerializerMethodField()
    provider = serializers.SerializerMethodField()

    class Meta:
        model = models.DataSource
        depth = 1
        fields = (
            'id',
            'name',
            'description',
            'public',
            'provider',
            'url',
            'creation_date',
            'last_edition_date',
            'has_publication',
        )

    def __init__(self, str_max_length=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.str_max_length = str_max_length

    def get_description(self, obj):
        if self.str_max_length and self.str_max_length > 0:
            return obj.description[:self.str_max_length]
        return obj.description

    def get_provider(self, obj):
        provider = ((obj.provider_last_name or "").upper() + " " + (obj.provider_first_name or "").title()).strip()
        if len(provider) > 0:
            return provider
        return obj.owner.last_name.upper() + " " + obj.owner.first_name.title()

    def get_has_publication(self, obj):
        return obj.publication_url is not None
