//https://stackoverflow.com/a/41633001/2144569
function bench_start() {
  startTime = new Date();
};

//https://stackoverflow.com/a/41633001/2144569
function bench_end() {
  endTime = new Date();
  var timeDiff = endTime - startTime; //in ms
  // strip the ms
  timeDiff /= 1000;

  // get seconds
  var seconds = Math.round(timeDiff);
  console.log(seconds + " seconds");
}

//https://coderwall.com/p/nilaba/simple-pure-javascript-array-unique-method-with-5-lines-of-code
Array.prototype.unique = function() {
  return this.filter(function (value, index, self) {
    return self.indexOf(value) === index;
  });
}

function create_bootstrap_multiselect_from_applicant(source) {
    $(source).multiselect({
        allSelectedText: $(source).attr("data-all-selected-text"),
        nonSelectedText: $(source).attr("data-non-selected-text"),
        nSelectedText: $(source).attr("data-n-selected-text"),
        enableFiltering: true,
        enableCaseInsensitiveFiltering: true,
        buttonWidth:'100%',
        numberDisplayed: 4,
        maxHeight: 400,
        onChange:window[$(source).attr("data-on-change-fcn-name")],
        onInitialized: function(select, container) {
            let ain = $(source).attr("data-select-all-inv-nop");
            if (typeof ain == "undefined")
                return;
            if (ain == true || ain == ""){
                ain="all,inv,nop";
            }else{
                ain=ain.split(",");
            }
            let li = $(
            '<li class="multiselect-reset text-center"><div class="input-group btn-group btn-group-toggle" data-toggle="buttons">'+
                '<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1">'+gettext('Selection')+'</span></div>'+
                (ain.indexOf('all')!=-1?'<a class="btn btn-outline-secondary select-all">'+gettext('All')+'</a>':'')+
                (ain.indexOf('inv')!=-1?'<a class="btn btn-outline-secondary select-inv">'+gettext('Inverse')+'</a>':'')+
                (ain.indexOf('nop')!=-1?'<a class="btn btn-outline-secondary select-nop">'+gettext('None')+'</a>':'')+
            '</div></li>');
            li.on('click', function(event) {event.stopPropagation();})
            li.find(".select-all").on('click', function(e) {
                e.stopPropagation();
                $(select).multiselect('selectAll',true).multiselect('refresh');
                func_name=$(source).attr("data-on-change-fcn-name");
                if(typeof func_name != "undefined"){
                    window[func_name](null,null,select);
                }
            });
            li.find(".select-inv").on('click', function(e) {
                e.stopPropagation();
                selected = $(container).find("input:visible:checked");
                $(select).multiselect('selectAll',true).multiselect('refresh');
                selected.each(function(i,e){
                    $(select).multiselect('deselect',$(e).attr("value"));
                });
                func_name=$(source).attr("data-on-change-fcn-name");
                if(typeof func_name != "undefined"){
                    window[func_name](null,null,select);
                }
            });
            li.find(".select-nop").on('click', function(e) {
                e.stopPropagation();
                $(select).multiselect('deselectAll',false).multiselect('refresh');
                func_name=$(source).attr("data-on-change-fcn-name");
                if(typeof func_name != "undefined"){
                    window[func_name](null,null,select);
                }
            });
            container.find("ul").prepend(li);
        },
        templates:{
            button: '<button type="button" class="multiselect dropdown-toggle btn btn-secondary btn-default " data-toggle="dropdown"><span class="multiselect-selected-text"></span> <b class="caret"></b></button>',
            filterClearBtn: '<div class="input-group-append"><button class="btn btn-outline-secondary multiselect-clear-filter" type="button"><i class="fa fa-times-circle"></i></button></div>',
            filter: '<li class="multiselect-reset multiselect-item multiselect-filter"><div class="input-group"><div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="fa fa-search"></i></span></div><input class="form-control multiselect-search" type="text" /></div></li>',
        }
    }).show();
}

$(document).ready(function(){
    $("select[multiple].bootstrap-multiselect-applicant").each(function(i,e){
        create_bootstrap_multiselect_from_applicant(e);
    });
    $("[data-onchange-js]").change(function(e){
        window[$(e.target).attr("data-onchange-js")](e);
    });
    $("input[type=file]").change(function (e){$(this).next('.custom-file-label').text(e.target.files[0].name);});
});