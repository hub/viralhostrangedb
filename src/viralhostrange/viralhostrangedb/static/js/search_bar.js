$(document).ready(function(){
    var str_max_length=100;
    $('#search_bar').autocomplete({
        source : function(request, response_callback){
            $.ajax({
                url : $('#search_bar').closest("form").attr("data-action"),
                dataType : 'json',
                data : {
                    search : request["term"],
                    str_max_length : str_max_length + 1,
                },

                success : function(data){
                    responses=[];
                    has_no_match=true;
                    $.each(data, function(k,v){
                        if (k != "query" && v.count>0){
                            has_no_match=false;
                        }
                    });
                    $.each(data, function(k,v){
                        if (k != "query" && (has_no_match || v.count>0)){
                            v["kind"]=k;
                            v["searched_text"]=data["query"]["searched_text"];
                            v["alternative_searched_texts"]=data["query"]["alternative_searched_texts"];
                            v["mandatory_searched_texts"]=data["query"]["mandatory_searched_texts"];
                            responses.push(v);
                        }
                    });
                    response_callback(responses);
                }
            });
        },
        appendTo: '#search-bar-container'
    })
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        let html="",
            icon,
            verbose_name,
            verbose_has_more,
            srh=getSearchResultHighlighter(item["searched_text"], item["alternative_searched_texts"], item["mandatory_searched_texts"]).createHighlightedSearch;

        if (item["kind"]=="host"){
            icon='<span class="btf-host"></span> ';
            verbose_name=gettext("Hosts");
            verbose_has_more= gettext("See all %s matching hosts");
        }else if (item["kind"]=="virus"){
            icon='<span class="btf-virus"></span> ';
            verbose_name=gettext("Viruses");
            verbose_has_more= gettext("See all %s matching viruses");
        }else if (item["kind"]=="data_source"){
            icon='<i class="fa fa-database"></i> ';
            verbose_name=gettext("Data source");
            verbose_has_more= gettext("See all %s matching data sources");
        }
        html+='<li class="ui-state-disabled ui-menu-title" aria-disabled="true">'
            +'<div tabindex="-1" class="ui-menu-item-wrapper">'
            +icon+verbose_name
            +'</div>'
            +'</li>';

        if (item["count"]==0){
            html+='<li class="ui-state-disabled">'
                +'<div tabindex="-1" class="ui-menu-item-wrapper">'
                +'<i>'
                +gettext("No match")
                +'</i>'
                +'</div>'
                +'</li>';
        }
        $.map(item["sample"], function(o){
            let content;
            if ("identifier" in o){
                content = srh(o.name);
                if (o.identifier)
                    content+=' <span class="identifier '+(o.is_ncbi_identifier_value?'ncbi':'custom')+'">'+srh(o["identifier"])+'</span>';
                if (o.her_identifier)
                    content+=' <span class="identifier her">'+srh(o["her_identifier"])+'</span>';
                if (o.tax_id)
                    content+=' <span class="identifier tax">'+srh(o["tax_id"])+'</span>';
            }else{
                content = "<div>"
                            + srh(o.name) +' ('+gettext('From: ')+srh(o.provider)+')'
                            + "</div><small>"
                            + srh(o.description.substring(0,str_max_length)+(o.description.length>str_max_length?"...":''))
                            + "</small>";

            }
            html+='<li class="ui-menu-item">'
                +'<a tabindex="-1" class="ui-menu-item-wrapper" href="'+o.url+'">'
                +content
                +'</a>'
                +'</li>';
        });
        if (item["count"]>item["sample"].length){
            html+='<li class="ui-menu-item">'
                +'<a tabindex="-1" class="ui-menu-item-wrapper" href="'+item.subset_url+'">'
                +"<i>"
                +interpolate(verbose_has_more, [item.count, ])
                +"</i>"
                +'</a>'
                +'</li>';
        }
        html+='<li>-</li>';
        return $(html).appendTo( ul );
    }
    ;
});

function getSearchResultHighlighter(searchedText, alternative_searched_texts, mandatory_searched_texts){
    function my_normalize(s){return s.toUpperCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");}

    var searched_texts=[];
    alternative_searched_texts=alternative_searched_texts || [];
    mandatory_texts= [];
    mandatory_searched_texts=mandatory_searched_texts || [];

    searched_texts.push(my_normalize(searchedText));
    if(searched_texts[0].startsWith("NCBI:")){
        searched_texts[0]=searched_texts[0].substr(5);
    }else if(searched_texts[0].startsWith("HER:")){
        searched_texts[0]=searched_texts[0].substr(4);
    }else if(searched_texts[0].startsWith("ID:")){
        searched_texts[0]=searched_texts[0].substr(3);
    }

    for( let i_alt =0;i_alt<alternative_searched_texts.length;i_alt++){
        let alternative_searched_text=my_normalize(alternative_searched_texts[i_alt]);
        if( alternative_searched_text.startsWith("NCBI:") ||
            alternative_searched_text.startsWith("NAME:") ||
            alternative_searched_text.startsWith("DESC:") ||
            alternative_searched_text.startsWith("PROV:")){
            alternative_searched_text=alternative_searched_text.substr(5);
        }else if(alternative_searched_text.startsWith("OWNER:")){
            alternative_searched_text=alternative_searched_text.substr(6);
        }else if(alternative_searched_text.startsWith("PROVIDER:")){
            alternative_searched_text=alternative_searched_text.substr(9);
        }else if(alternative_searched_text.startsWith("HER:")){
            alternative_searched_text=alternative_searched_text.substr(4);
        }else if(alternative_searched_text.startsWith("ID:")){
            alternative_searched_text=alternative_searched_text.substr(3);
        }
        searched_texts.push(alternative_searched_text);
    }
    for( let i_alt =0;i_alt<mandatory_searched_texts.length;i_alt++){
        let mandatory_searched_text=my_normalize(mandatory_searched_texts[i_alt]);
        if( mandatory_searched_text.startsWith("NCBI:") ||
            mandatory_searched_text.startsWith("NAME:") ||
            mandatory_searched_text.startsWith("DESC:") ||
            mandatory_searched_text.startsWith("PROV:")){
            mandatory_searched_text=mandatory_searched_text.substr(5);
        }else if(mandatory_searched_text.startsWith("OWNER:")){
            mandatory_searched_text=mandatory_searched_text.substr(6);
        }else if(mandatory_searched_text.startsWith("PROVIDER:")){
            mandatory_searched_text=mandatory_searched_text.substr(9);
        }else if(mandatory_searched_text.startsWith("HER:")){
            mandatory_searched_text=mandatory_searched_text.substr(4);
        }else if(mandatory_searched_text.startsWith("ID:")){
            mandatory_searched_text=mandatory_searched_text.substr(3);
        }
        mandatory_texts.push(mandatory_searched_text);
    }
    // when searching for "f felix" we want to highlight felix instead of just f (we cannot highlight both). We thus
    // sort searched_texts by length to the bigger are highlighted first by the algorithm.
    searched_texts=searched_texts.sort(function(a, b){
      return b.length - a.length;
    });

    var srh = function(){}

    srh.createHighlightedSearch = function(result){
        var elt=[document.createElement('i')];
        srh.appendHighlightedSearchTo(result,elt);
        return elt[0].innerHTML;
    }

    srh.appendHighlightedSearchTo = function(result, elts){
        var result_raw=(result+""),
            result_raw_norm = my_normalize(result_raw),
            result_upper=[result_raw_norm],
            found_searched_text=[],
            frag_len,
            span;
        for (let i_searched_text=0; i_searched_text<searched_texts.length; i_searched_text++){
            for (let j=0;j<result_upper.length;j++){
                let slices=result_upper[j].split(searched_texts[i_searched_text]);
                result_upper.splice(j, 1, slices[0]);
                for(let k=slices.length-1;k>0;k--){
                    result_upper.splice(j+1, 0, slices[k]);
                    found_searched_text.splice(j, 0, searched_texts[i_searched_text]);
                }
            }
        }
        for (let i_elt=0;i_elt<elts.length;i_elt++){
            var elt=elts[i_elt],
                frag_start=0;
            elt.innerText="";
            for(let i_frag=0; i_frag<result_upper.length;i_frag++){
                if (i_frag>0){
                    match_fragment=result_raw.substr(frag_start,found_searched_text[i_frag-1].length);
                    span = document.createElement('span');
                    span.innerText=match_fragment;
                    span.classList.add("matched");
                    if(mandatory_texts.length>0 && mandatory_texts.includes(found_searched_text[i_frag-1])){
                        span.classList.add("mandatory");
                    }
                    elt.appendChild(span);
                    frag_start+=match_fragment.length;
                }
                let fragment = result_raw.substr(frag_start,result_upper[i_frag].length);
                if(fragment.length>0){
                    span = document.createElement('span');
                    span.innerText=fragment;
                    elt.appendChild(span);
                }
                frag_start+=fragment.length;
            }
        }
        return srh;
    }

    return srh;
}