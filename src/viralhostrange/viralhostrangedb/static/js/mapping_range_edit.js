$(document).ready(function(){
    $('.range-holder input').change(range_changed).mouseup(range_changed).first().change();
    $('button[type=submit]').mouseenter(range_changed);

    $('.range-holder button[data-action="next"]').click(function(){
        let mapping = $(this).parent().parent();
        let ceiling_input = $(mapping).nextAll(".range-holder .mapping_splitter").find("input:valid").first();
        let ceiling = ceiling_input.val();
        let new_ceiling=null;
        for (let i=0;i<raw_responses.length;i++){
            if ( raw_responses[i] < ceiling &&
                (new_ceiling == null || raw_responses[i] > new_ceiling)){
                new_ceiling=raw_responses[i];
            }
        }
        if (new_ceiling != null){
            $(ceiling_input).val(new_ceiling);
            range_changed({target:$(ceiling_input)});
        }
    });

    $('.range-holder button[data-action="prev"]').click(function(){
        let mapping = $(this).parent().parent();
        let floor_input = $(mapping).prevAll(".range-holder .mapping_splitter").first().find("input");
        let floor = floor_input.val();
        let new_floor=null;
        let on_the_floor=false;
        for (let i=0;i<raw_responses.length;i++){
            if (raw_responses[i] > floor &&
                (new_floor == null || raw_responses[i] < new_floor)){
                new_floor=raw_responses[i];
            } else if (new_floor == null && raw_responses[i] == floor) {
                on_the_floor=true;
            }
        }
        if (new_floor ==  null){
            new_floor=raw_responses[raw_responses.length-1]+1;
        }
        if (new_floor != null){
            $(floor_input).val(new_floor);
            range_changed({target:$(floor_input)});
        }
    });
});

function range_changed(event){
    prev_splitter = $(event.target).parent().parent().prevAll(".mapping_splitter").first();
    next_splitter = $(event.target).parent().parent().nextAll(".mapping_splitter").first();
    current_val = $(event.target).val();
    if( typeof prev_splitter != "undefined" ){
        let prev_val = $(prev_splitter).find("input").val();
        if(prev_val!="" && prev_val>parseFloat(current_val)){
            $(prev_splitter).find("input").val(current_val);
            range_changed({target:$(next_splitter).find("input")});
        }
    }
    if( typeof next_splitter != "undefined" ){
        let next_val = $(next_splitter).find("input").val();
        if( next_val!="" && next_val<parseFloat(current_val)){
            $(next_splitter).find("input").val(current_val);
            range_changed({target:$(next_splitter).find("input")});
        }
    }

    var width=0;
    var cpt=0;
    $(".range-holder .raw_responses_for_mapping").each(function(i,e){
        width+=$(this).parent().parent().width();
        cpt+=1;
    });
    width=width/cpt;

    $(".range-holder .raw_responses_for_mapping").each(function(i,e){
        let mapping = $(this).parent().parent();
        $(mapping).css("width",width+"px");
        let floor = $(mapping).prevAll(".range-holder .mapping_splitter").first().find("input").val();
        let ceiling = $(mapping).nextAll(".range-holder .mapping_splitter").find("input:valid").first().val();
        let text=null;
        for (let i=0;i<raw_responses.length;i++){
            if (  ( typeof floor == "undefined" || floor !="" && floor <= raw_responses[i] )
               && ( typeof ceiling == "undefined" ||  raw_responses[i] < ceiling ) ){
                if (text==null){
                    text="";
                }else{
                    text+=", ";
                }
                text+=raw_responses[i];
            }
        }
        if (text==null){
            $(this).html("<i>"+gettext("None")+"</i>");
        }else{
            $(this).text(text);
        }
    });
}