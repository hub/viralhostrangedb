$(document).ready(function(){
    $('input.searched_term').trigger("input");
    search_now();
    $("#big_search_bar").keyup(function(e){
        if (e.keyCode === 13) {
            window.history.pushState('Search', document.title, "?"+serialize_form_for_shearing());
            search_now();
        }
        window.history.replaceState('Search', document.title, "?"+serialize_form_for_shearing());
        delayed_search_now();
    });
    window.history.replaceState('Search', 'Search', "?"+serialize_form_for_shearing());
});

delayed_search_now = delayed_action(
        search_now,
        function (){
            $("#big_search_bar").siblings(".spinner").show();
        },
        800
    );

function search_now(){
    let $form=$('#search_deeper');
    let big_search_bar_value = $("#big_search_bar").val();
    document.title=big_search_bar_value.length>0 ? "Search \""+big_search_bar_value+"\"" : "Search";
    $.ajax({
        url : $form.attr("data-action"),
        dataType : 'json',
        data : $form.serialize(),
        success : function(data){
            let $detail=$('.results-detail');
            $detail.empty();
            $("#big_search_bar").siblings(".spinner").hide();
            if ("app_error_code" in data){
                console.log(data);
                $template = $($(
                    '[data-template="app_error_code"][data-template-kind='+data["app_error_code"]+']'
                ).html().replace("XXX",data["searched_text"]).replace("XXX",data["searched_text"]));
                $template.appendTo($detail);
                return;
            }
            let searchResultHighlighter=getSearchResultHighlighter(data["query"]["searched_text"], data["query"]["alternative_searched_texts"], data["query"]["mandatory_searched_texts"]);
            for(k in data["ui_help"]){
                let $see_all=null;
                count=data[k]["count"];

                $header_html = $($(
                    '[data-template*="'+k+'"][data-template-kind="header'+(count==0?'-no-match':'')+'"]'
                ).html());
                $header_html.find("[data-source=ui_help]").text(data["ui_help"][k]);
                $header_html.find("[data-source=subset_url]").attr("href",data[k]["subset_url"]);
                $header_html.appendTo($detail);

                $(data[k]["sample"]).each(function(i,entry){
                    $entry_html = $($('[data-template*="'+k+'"][data-template-kind="main"]').html());
                    for (entry_key in entry){
                        let elt = $entry_html.find('[data-source="'+entry_key+'"]');
                        if (elt.length == 0){
                            continue
                        }else if (typeof elt.attr("data-target") == "undefined"){
                            if (entry[entry_key] == null)
                                continue
                            searchResultHighlighter.appendHighlightedSearchTo(entry[entry_key],elt);
                        }else{
                            elt.attr(elt.attr("data-target"),entry[entry_key]);
                        }
                    }
                    $entry_html.appendTo($detail);
                });
                if (count>data["query"]["sample_size"] && data["query"]["sample_size"] > 0){
                    $see_all = $($('[data-template*="'+k+'"][data-template-kind="see-all-has-more"]').html());
                }else if (count>0){
                    $see_all = $($('[data-template*="'+k+'"][data-template-kind="see-all"]').html());
                }
                if ($see_all != null){
                    $see_all.find("[data-source=subset_url]").attr("href",data[k]["subset_url"]);
                    $see_all.appendTo($detail);
                }
            }
        }
    });
}

function load_data_wrapper(){
    $("input[name=sample_size]").val(0);
    window.history.pushState('Search', document.title, "?"+serialize_form_for_shearing());
    search_now();
}

function serialize_form_for_shearing(){
    let kvs = $("#search_deeper").serialize().split("&");
    let results=[];
    for(let i=0;i<kvs.length;i++){
        let ks = kvs[i].split("=");
        if (["search",
             "life_domain",
             "only_published_data",
             "only_virus_ncbi_id",
             "only_host_ncbi_id",
             "owner"].indexOf(ks[0])>=0 && ks[1].length>0){
            results.push(kvs[i]);
        }
        if ("kind"===ks[0] && ks[1]!=="all"){
            results.push(kvs[i]);
        }
    }
    return results.join("&");
}