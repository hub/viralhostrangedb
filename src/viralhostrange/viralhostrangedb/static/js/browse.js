$(document).ready(function(){
    var parts = location.search.substring(1).split('&');
    mem['virus']=[];
    mem['host']=[];
    mem['ds']=[];
    mem['data_source']={};
    params={}
    for (let i=0;i<parts.length;i++){
        parts[i]=parts[i].split("=");
        if (typeof parts[i][1] == "undefined")
            continue
        parts[i][1]=parts[i][1].split(",");
        params[parts[i][0]]=parts[i][1];
    }
    for (let i=0;i<parts.length;i++){
        if (typeof parts[i][1] == "undefined")
            continue
        for(let j=0;j<parts[i][1].length;j++){
            cb=$('select[name="'+parts[i][0]+'"]').parent().find('input[type="checkbox"][value="'+parts[i][1][j]+'"]');
            cb.prop('checked', true);
            if (parts[i][0]=="virus"){
                virus_changed(parts[i][1][j],true,false);
            }else if (parts[i][0]=="host"){
                host_changed(parts[i][1][j],true,false);
            }else{
                //cb.change();
            }
        }
    }
    load_data(null,null);
    refresh_advanced_option_counter();
    $("[data-advanced-option]").change(function(e){
        if(e.target.getAttribute("type")=="checkbox"){
            if(e.target.checked){
                add_badge(e.target.id);
            }else{
                document
                    .getElementById("badge-container")
                    .parentNode
                    .querySelectorAll('[for="'+e.target.id+'"]')
                    .forEach(e => e.parentNode.removeChild(e));
            }
        }else if(e.target.getAttribute("type")=="radio") {
            var radio_id = "id_" + e.target.getAttribute("name")+"_1";
            document
                .getElementById("badge-container")
                .parentNode
                .querySelectorAll('[for="'+radio_id+'"]')
                .forEach(e => e.parentNode.removeChild(e));
            if(e.target.value != ''){
                add_badge(radio_id, e.target.parentNode.querySelectorAll("label")[0].textContent);
            }
        }else{
            console.log("Unhandled option for "+e.target.id);
        }
        refresh_advanced_option_counter();
        update_get_parameter();
    });
    $("[data-toggle-css-class]").change(function(e){
        $(e.target).attr("data-toggle-css-class").split(" ").map(function(c){
            $("#grid_container").toggleClass(c);
        });
    });
    load_data_wrapper.disabled=true;
    refresh_all_infection_ratio.disabled=true;
    refresh_virus_infection_ratio.disabled=true;
    refresh_host_infection_ratio.disabled=true;
    $("[data-advanced-option]:checked").change();
    load_data_wrapper.disabled=false;
    refresh_all_infection_ratio.disabled=false;
    refresh_virus_infection_ratio.disabled=false;
    refresh_host_infection_ratio.disabled=false;
    $('#advanced_holder').mouseleave(function(){$('#advanced_holder:not(.pinned)').collapse('hide');});
});

function refresh_advanced_option_counter(){
    let l = $("[data-advanced-option]:checked:not([value=''])").length;
    l=(l==0?"":" ("+l+")");
    $('[href="#advanced_holder"] .counter').text(l);
    $("#advanced_holder [data-toggle]").map(function(i,o){
        let l = $($(o).attr("href")).find(":checked:not([value=''])").length;
        l=(l==0?"":" ("+l+")");
        $(o).find('.counter').text(l);
    });
}

function filter_changed(option, checked, select){
    if (option == null){
        load_data();
        return;
    }
    var select = $(option).closest("select");
    if (select.attr('name') == "host"){
        host_changed($(option).attr("value"),checked);
    }else if (select.attr('name') == "virus"){
        virus_changed($(option).attr("value"),checked);
    } else if (select.attr('name') == "ds"){
        load_data();
    }
}

function host_changed_multiple(pks, checked, do_load_data){
    $("#grid_host th[data-row] .ratio").removeClass("computed");
    if (checked){
        let cell_th_str=[]
        var data_init_order = 1 + Math.max(-1, ...$("#grid_host th[data-col] [data-init-order]").map(function(i,o){return parseInt(o.getAttribute("data-init-order"))}).get());
        //add new columns
        let sub_pk=[]
        for(let i=0;i<pks.length;i++){
            var pk =pks[i];
            sub_pk.push(pk);
            var data_col_attr = 'data-col="'+pk+'"';
            if ($('th['+data_col_attr+']').length==0){
                cell_th_str.push('<th onmouseover="cell_hover(this)" onmouseout="cell_hover(this)" '+data_col_attr+'>'+
                                '<a target="_blank" href="'+get_host_url.replace("000000",pk)+'">_</a>'+
                                '<span><span data-init-order="'+data_init_order+'" class="ratio"></span></span>'+
                                '</th>');
                data_init_order+=1;
                $('#grid_host tbody>tr[data-row]').map(function(i,o){
                    let virus=o.getAttribute("data-row");
                    let data_row_attr='data-row="'+virus+'"';
                    if($('td['+data_row_attr+']['+data_col_attr+']').length==0){
                        let cell=$('<td onclick="cell_click(this)" onmouseover="cell_hover(this)" onmouseout="cell_hover(this)" '+data_row_attr+' '+data_col_attr+'>&nbsp;</td>');
                        cell.appendTo($('tr[data-row="'+virus+'"]'));
                    }
                });
                mem["host"][pk]=true;
            }
        }
        $(cell_th_str.join('')).appendTo($("#grid_host").find("thead>tr"));
        for(let i=0;i<sub_pk.length;i++){
            load_host_header(sub_pk[i]);
        }
        if(do_load_data != false){
            load_data(null,pk);
        }
    }else{
        for(var pk in pks){
            //remove the column
            $("[data-col="+pk+"]").popover('hide').popover('dispose').remove();
            //mem["host"].remove(pk);
            delete mem["host"][pk];
        }
        if(do_load_data != false && $('select[name="host"]').parent().find('input[type="checkbox"]:checked').length==0){
            load_data();
        }else{
            update_get_parameter();
        }
    }
}

function host_changed(pk, checked, do_load_data){
    $("#grid_host th[data-row] .ratio").removeClass("computed");
    if (checked){
        //add a new column
        var data_col_attr = 'data-col="'+pk+'"';
        if ($('th['+data_col_attr+']').length==0){
            var data_init_order = 1 + Math.max(-1, ...$("#grid_host th[data-col] [data-init-order]").map(function(i,o){return parseInt(o.getAttribute("data-init-order"))}).get());
            let cell_th_str='<th onmouseover="cell_hover(this)" onmouseout="cell_hover(this)" '+data_col_attr+'>'+
                            '<a target="_blank" href="'+get_host_url.replace("000000",pk)+'">_</a>'+
                            '<span><span data-init-order="'+data_init_order+'" class="ratio"></span></span>'+
                            '</th>';
            let cell_th=$(cell_th_str);
            cell_th.appendTo($("#grid_host").find("thead>tr"));
            $('#grid_host tbody>tr[data-row]').map(function(i,o){
                let virus=o.getAttribute("data-row");
                let data_row_attr='data-row="'+virus+'"';
                if($('td['+data_row_attr+']['+data_col_attr+']').length==0){
                    let cell=$('<td onclick="cell_click(this)" onmouseover="cell_hover(this)" onmouseout="cell_hover(this)" '+data_row_attr+' '+data_col_attr+'>&nbsp;</td>');
                    cell.appendTo($('tr[data-row="'+virus+'"]'));
                }
            });
            mem["host"][pk]=true;
//            do_load_data=false;
        }
        load_host_header(pk);
        if(do_load_data != false){
            load_data(null,pk);
        }
    }else{
        //remove the column
        $("[data-col="+pk+"]").popover('hide').popover('dispose').remove();
        //mem["host"].remove(pk);
        delete mem["host"][pk];
        if(do_load_data != false && $('select[name="host"]').parent().find('input[type="checkbox"]:checked').length==0){
            load_data();
        }else{
            update_get_parameter();
        }
    }
}

function load_host_header(pk){
    $.ajax({
        headers: {
            'X-CSRFToken':getCookie('csrftoken'),
        },
        type: "GET",
        url:get_host_api_url.replace("000000",pk),
        data: {
        },
        success: function (data, textStatus, xhr) {
            let name_span = '<span>'+data["name"]+'</span>';
            if (data["short_name"] != null){
                console.log(data["short_name"]);
                name_span = '<span class="shrinked-name" title="'+data["name"]+'">'+data["short_name"]+'</span>'+name_span;
            }
            identifiers=[];
            if (data["identifier"] != ""){
                identifiers.push(data["identifier"]);
            }
            if (data["tax_id"] != null){
                identifiers.push('<span class="tax">TAX:'+data["tax_id"]+'</span>');
            }
            if (identifiers.length >0){
                name_span='<span class="identifier"> ('+ identifiers.join("; ")+') </span>'+ name_span;
            }
            $('#grid_host thead th[data-col="'+data["id"]+'"]>a').html(name_span);
        },
    });
}

function virus_changed(pk, checked, do_load_data){
    $("#grid_host th[data-col] .ratio").removeClass("computed");
    if (checked){
        //add a new row
        var data_row_attr = 'data-row="'+pk+'"';
        if ($('tr['+data_row_attr+']').length==0){
            var data_init_order = 1 + Math.max(-1, ...$("#grid_host th[data-row][data-init-order]").map(function(i,o){return parseInt(o.getAttribute("data-init-order"))}).get());
            let row_str = [];
            row_str.push('<tr '+data_row_attr+'>'+
                        '<th onmouseover="cell_hover(this)" onmouseout="cell_hover(this)" '+data_row_attr+' data-init-order="'+data_init_order+'">'+
                          '<a target="_blank" href="'+get_virus_url.replace("000000",pk)+'">_</a>'+
                          '<span class="ratio"></span>'+
                        '</th>'/*+
                      '</tr>'*/);
            let hosts = $('#grid_host thead>tr>th[data-col]').map(function(i,o){
                row_str.push('<td onclick="cell_click(this)" onmouseover="cell_hover(this)" onmouseout="cell_hover(this)" '+data_row_attr+' data-col="'+o.getAttribute("data-col")+'">&nbsp;</td>');
            });
            row_str.push('</tr>');
            row=$(row_str.join('')).appendTo($("#grid_host").find("tbody"));
            mem["virus"][pk]=true;
        }
        $.ajax({
            headers: {
                'X-CSRFToken':getCookie('csrftoken'),
            },
            type: "GET",
            url:get_virus_api_url.replace("000000",pk),
            data: {
            },
            success: function (data, textStatus, xhr) {
                let name_span = '<span>'+data["name"]+'</span>';
                if (data["short_name"] != null){
                    console.log(data["short_name"]);
                    name_span = '<span class="shrinked-name" title="'+data["name"]+'">'+data["short_name"]+'</span>'+name_span;
                }
                identifiers=[];
                if (data["identifier"] != ""){
                    identifiers.push(data["identifier"]);
                }
                if (data["tax_id"] != null){
                identifiers.push('<span class="tax">TAX:'+data["tax_id"]+'</span>');
                }
                if (data["her_identifier"] != null){
                    identifiers.push("HER:"+data["her_identifier"]);
                }
                if (identifiers.length >0){
                    name_span='<span class="identifier"> ('+ identifiers.join("; ")+') </span>'+ name_span;
                }
                $('#grid_host tbody th['+data_row_attr+']>a').html(name_span);
            },
        });
        if(do_load_data != false){
            load_data(pk,null);
        }
    }else{
        //remove the row
        $("[data-row="+pk+"]").popover('hide').popover('dispose').remove();
        delete mem["virus"][pk];
        if(do_load_data != false && $('select[name="virus"]').parent().find('input[type="checkbox"]:checked').length==0){
            load_data();
        }else{
            update_get_parameter();
        }
    }
}

function update_get_parameter(){
    var virus_history="",host_history="",ds_history="",other_history="";
    $.map($("#form_filter").serializeArray(), function(n, i){
        if (n['name'] == "ds"){
            if (ds_history==""){
                ds_history="&ds=";
            }else{
                ds_history+=",";
            }
            ds_history+=+n['value'];
        }else  if (n['name'] == "virus"){
            if (virus_history==""){
                virus_history="&virus=";
            }else{
                virus_history+=",";
            }
            virus_history+=+n['value'];
        }else  if (n['name'] == "host"){
            if (host_history==""){
                host_history="&host=";
            }else{
                host_history+=",";
            }
            host_history+=+n['value'];
        }else if  (n['name'] == "csrfmiddlewaretoken"){
            //nothing
        }else {
            other_history+="&"+n['name']+"="+(n['value'] == "on" ? "true" :n['value']);
        }
    });
    // we actually used a not standard variant of get parameter to reduce the number of char used :
    // we normally should write &virus=1&virus=2&virus=3 ... but we can hit easily the limit of 2048 char
    // https://stackoverflow.com/a/417184/2144569
    window.history.replaceState('Browse', 'Browse', "?"+ds_history+virus_history+host_history+other_history);
}

function load_data_wrapper(e){
    if (load_data_wrapper.disabled){
        return;
    }
    load_data();
}

var t0;
function load_data(single_row_pk,single_col_pk){
    t0 = performance.now();
    update_get_parameter();
    var data_to_send="format=json",
        data_virus=null,
        data_host=null,
        data_list_virus="",
        data_list_host="",
        data_ds="";
    form=$("#form_filter");
    if(single_row_pk != null){
        data_virus="&virus="+single_row_pk;
        data_row_attr='[data-row="'+single_row_pk+'"]';
    }else{
        data_row_attr ="";
    }
    if(single_col_pk != null){
        data_host="&host="+single_col_pk;
        data_col_attr='[data-col="'+single_col_pk+'"]';
    }else{
        data_col_attr ="";
    }
    $.map(form.serializeArray(), function(n, i){
        if(n['name'] == "ds"){
            data_ds+="&"+n['name']+"="+n['value'];
        }else if(n['name'] == "virus"){
            data_list_virus+="&"+n['name']+"="+n['value'];
        }else if(n['name'] == "host"){
            data_list_host+="&"+n['name']+"="+n['value'];
        }else {
            data_to_send+="&"+n['name']+"="+n['value'];
        }
    });
    data_virus=data_virus||data_list_virus;
    data_host=data_host||data_list_host;
    let search_candidate_for_deletion_in_row=single_col_pk == null,
        search_candidate_for_deletion_in_col=single_row_pk == null;
    [
        [get_virus_list_api_url, 'virus', data_to_send+data_list_host+data_ds],
        [get_host_list_api_url, 'host', data_to_send+data_list_virus+data_ds],
        [get_data_source_list_api_url, 'ds', data_to_send+data_list_virus+data_list_host],
    ].forEach(function(tuple) {
        $.ajax({
            headers: {
                'X-CSRFToken':getCookie('csrftoken'),
            },
            type: "GET",
            url:tuple[0],
            data: tuple[2]+"&fields=id",
            success: function (data, textStatus, xhr) {
                let inputs = $('select[name="'+tuple[1]+'"]').parent().find('label[title]>input[type="checkbox"]');
                inputs.closest("li").addClass("out-of-scope");
                for(let row in data){
                    inputs.filter("[value="+data[row]['id']+"]").closest("li").removeClass("out-of-scope");
                }
            },
        });
    });
    $.ajax({
        headers: {
            'X-CSRFToken':getCookie('csrftoken'),
        },
        type: "GET",
        //data:form.serialize(),
        data:data_to_send+data_ds+data_virus+data_host,
        url:form.attr("data-action"),
        success: function (data, textStatus, xhr) {
            //store how we should render responses : by range or with average value
            let render_actual_aggregated_responses_checked = $("#id_render_actual_aggregated_responses:checked").length;
            //remove all disagreements
            $("#grid_host tbody td"+data_row_attr+data_col_attr).html("&nbsp;").removeClass("disagree");
            let candidate_row_for_deletion=null,
                candidate_col_for_deletion=null;
            if(search_candidate_for_deletion_in_row){
                //get already present row/virus
                candidate_row_for_deletion = new Set($('#grid_host tbody>tr[data-row]').map(
                    function(i,o){return o.getAttribute("data-row");}
                ));
                //and remove the one that are selected in the select
                $('select[name="virus"]').parent().find('input[type="checkbox"]:checked').map(function(i,o){
                    candidate_row_for_deletion.delete(o.getAttribute("value"));
                });
            }
            if(search_candidate_for_deletion_in_col){
                //get already present row/virus
                candidate_col_for_deletion = new Set($('#grid_host thead>tr>th[data-col]').map(
                    function(i,o){return o.getAttribute("data-col");}
                ));
                //and remove the one that are selected in the select
                $('select[name="host"]').parent().find('input[type="checkbox"]:checked').map(function(i,o){
                    candidate_col_for_deletion.delete(o.getAttribute("value"));
                });
            }
            let first_row=true;
            for(let row in data){
                if(candidate_row_for_deletion){
                    candidate_row_for_deletion.delete(row.toString());
                }
                let data_row = data[row];

                if (first_row){
                    first_row=false;
                    host_changed_multiple(Object.keys(data_row), true, false);
                }
                let tr_row=$('#grid_host tbody>tr[data-row="'+row+'"]');
                //create the row if it does not exist yet, i.e it is a relevant virus for the data source and hosts
                if (tr_row.length==0){
                    virus_changed(row, true, false);
                    tr_row=$('#grid_host tbody>tr[data-row="'+row+'"]');
                }
                for(let col in data_row){
                    if(candidate_col_for_deletion){
                        candidate_col_for_deletion.delete(col.toString());
                    }
                    let val_n_diff = data_row[col]
                    let td=tr_row.children('td[data-col="'+col+'"]');
                    //create the col if it does not exist yet, i.e it is a relevant virus for the data source and virus
                    if (td.length==0){
                        host_changed(col, true, false);
                        td=tr_row.children('td[data-col="'+col+'"]');
                    }
                    // update the value
                    let schematics_value = true;
                    if (render_actual_aggregated_responses_checked || Math.floor(val_n_diff["val"])==val_n_diff["val"]){
                        td.text(val_n_diff["val"]);
                    }else{
                        //td.text("<"+Math.ceil(val_n_diff["val"]));
                        td.html(Math.floor(val_n_diff["val"])+"&#8209;"+Math.ceil(val_n_diff["val"]));
                    }
                    //keep in mind if it was hovered
                    let hover=td.hasClass("hover");
                    //remove all class, easier than removing all schema-* classes
                    td.removeClass();
                    //mark as a disagreements if needed
                    if (val_n_diff["diff"]>1){
                        td.addClass("disagree");
                    }
                    //restore hover status
                    if (hover){
                        td.addClass("hover");
                    }
                    //generate custom css key ...
                    let css_key=("schema-"+val_n_diff["val"]).replace(".","-");
                    td.addClass(css_key);
                    // ... and ask to load it
                    addCSS(custom_css+val_n_diff["val"], css_key);
                }
            }
            if(candidate_row_for_deletion){
                //remove all virus that were not updated and were not explicitly asked
                candidate_row_for_deletion.forEach(function (e){virus_changed(e,false,false)});
            }
            if(candidate_col_for_deletion){
                //remove all hosts that were not updated and were not explicitly asked
                candidate_col_for_deletion.forEach(function (e){host_changed(e,false,false)});
            }
            // now let's work on infection ratio
            refresh_needed_infection_ratio();
            // check if table is empty by looking at the form
            let table_is_empty=true;
            $.map($("#form_filter").serializeArray(), function(n, i){
                if (n['name'] == "ds" || n['name'] == "virus" || n['name'] == "host"){
                    table_is_empty=false
                }
            });
            // if it is empty, adding css class so the placeholder can be displayed
            if(table_is_empty){
                $("#grid_container").addClass("empty");
            }else{
                $("#grid_container").removeClass("empty");
            }
            t1 = performance.now();
            console.log("Call to load_data took " + (t1 - t0) + " milliseconds.");
        },
    });
}

// Include CSS file
function addCSS(filename, css_key){
    if (typeof css_key != "undefined" && $("#"+css_key).length>0){
        return;
    }
    var head = document.getElementsByTagName('head')[0];

    var style = document.createElement('link');
    style.href = filename;
    style.type = 'text/css';
    style.id = css_key;
    style.rel = 'stylesheet';
    head.append(style);
}

function cell_hover(source) {
    let pos=$(source).index()+1;
    $(source).closest('table').find('tr>*:nth-child('+pos+')').toggleClass('hover');
}

function cell_click(source) {
    var elt=$(source);
    if( typeof elt.attr("aria-describedby") == "undefined"){
        title = gettext('Virus')+' <code>'+
                    $('#grid_host th[data-row='+$(elt).attr('data-row')+']').html()+
                '</code>'+
                ', '+gettext('Host')+' <code>'+
                $('#grid_host th[data-col='+$(elt).attr('data-col')+']').html()+
                '</code>';
        elt.popover({
            content: '<i class="fa fa-spinner fa-spin"></i>',
            html: true,
            title : '<span>'+title+'</span>'+
                    '&nbsp;<a role="button" class="close_cmd text-danger float-right">'+
                    '<i class="fa fa-times"></i></a>',
            container: 'body'
        }).on('shown.bs.popover', cell_popover).popover('show');
    }else{
        elt.popover('hide').popover('dispose').attr("aria-describedby",null);
    }
}

function cell_popover(e){
    var elt=$(this);
    var popover=$("#"+$(this).attr("aria-describedby"));
    popover.find("a.close_cmd").on('click', function(e){
        elt.popover('hide').popover('dispose').attr("aria-describedby",null);
    });
    popover.find(".popover-header").on('click', function(e){
        put_popover_in_front(popover);
    });
    popover.find(".popover-body").on('click', function(e){
        put_popover_in_front(popover);
    });

    content = $('<span>'+
        '<b>'+gettext("Virus")+'</b><br/>'+
        $('#grid_host th[data-row='+$(elt).attr('data-row')+']').html()+'<br/>'+
        '<b>'+gettext("Host")+'</b><br/>'+
        $('#grid_host th[data-col='+$(elt).attr('data-col')+']').html()+'<br/></span>'
    );
    content.find("a>*:last-child").each(function(i,e){
        let $p=$(e).parent();
        $(e).detach().addClass("toto").prependTo($p);
    });
    content.appendTo($(popover).find(".popover-body").empty());

    let data="format=json";
    $.map(form.serializeArray(), function(n, i){
        if(n['name'] != "virus" && n['name'] != "host"){
            data+="&"+n['name']+"="+n['value'];
        }
    });
    data+="&host="+$(elt).attr("data-col");
    data+="&virus="+$(elt).attr("data-row");
    $.ajax({
        headers: {
            'X-CSRFToken':getCookie('csrftoken'),
        },
        type: "GET",
        //data:form.serialize(),
        data:data,
        url:get_responses,
        success: function (data, textStatus, xhr) {
            let tbody = $('<tbody></tbody>')
                .appendTo($('<table class="table table-sm table-bordered"><thead>'+
                    '<tr>'+
                        '<th>'+gettext('Data source name')+'</th>'+
                        '<th>'+gettext('Response')+'</th>'+
                    '</tr>'+
                '</thead></table>')
                .appendTo($(popover).find(".popover-body")));
            data=(data[$(elt).attr("data-row")]||{})[$(elt).attr("data-col")]||{};
            var more_info_url=get_response_url.replace("111111",$(elt).attr("data-row"))
                                              .replace("222222",$(elt).attr("data-col"));
            var ds_get_param="";
            for(let ds_pk in data){
                ds_get_param+=(""==ds_get_param?"ds=":",")+ds_pk;
                let val=data[ds_pk];
                let css_key=("schema-"+val).replace(".","-");
                addCSS(custom_css+val, css_key);
                $('<tr class="'+css_key+'">'+
                  '<th data-ds="'+ds_pk+'"><i class="text-muted">'+
                  gettext('Data source')+' '+ds_pk+' <i class="fa fa-spinner fa-spin"></i>'+
                  '</i></th>'+
                  '<td class="text-center">'+val+
                  '</td></tr>')
                .appendTo(tbody);
                //set_data_source_name(ds_pk,tbody);
                $(tbody).find('th[data-ds="'+ds_pk+'"]').html(
                    '<a href="'+data_source_detail.replace("000000",ds_pk)+'">'
                    +$('select[name="ds"]>option[value='+ds_pk+']').text()
                    +'</a>'
                );
            }
            $('<div class="text-right">'+
              '<a href="'+more_info_url+'?'+ds_get_param+'">'+
              '<i><b>'+gettext('See more details...')+'</b></i>'+
              '</a></div>')
                .appendTo($(popover).find(".popover-body"));
        },
    });
}

function put_popover_in_front(popover){
    var elements = $(".popover");
    var vals = [];
    for(var i=0;typeof(elements[i])!='undefined';vals.push(parseInt($(elements[i++]).css('z-index'))));
    popover.css("z-index", Math.max(...vals)+1);
}

function refresh_all_infection_ratio(){
    if (refresh_all_infection_ratio.disabled){
        return;
    }
    refresh_virus_infection_ratio();
    refresh_host_infection_ratio();
}

function refresh_needed_infection_ratio(){
    refresh_needed_virus_infection_ratio();
    refresh_needed_host_infection_ratio();
}

function refresh_virus_infection_ratio(){
    if (refresh_virus_infection_ratio.disabled){
        return;
    }
    $("#grid_host tbody th .ratio").removeClass("computed");
    refresh_needed_virus_infection_ratio();
}

function refresh_host_infection_ratio(){
    if (refresh_host_infection_ratio.disabled){
        return;
    }
    $("#grid_host thead th .ratio").removeClass("computed");
    refresh_needed_host_infection_ratio();
}

function refresh_needed_virus_infection_ratio(){
    var data_to_send="",
        ratio_data_virus="",
        ratio_data_host="",
        data_host="",
        data_virus="";
    $.map(form.serializeArray(), function(n, i){
        if(n['name'] == "virus"){
            data_virus+=","+n['value'];
        }else if(n['name'] == "host"){
            data_host+=","+n['value'];
        }else{
            data_to_send+="&"+n['name']+"="+n['value'];
        }
    });
    data_host = (data_host.length==0?"":"&host="+data_host.substring(1));
    data_virus = (data_virus.length==0?"":"&virus="+data_virus.substring(1));

    if ($('[name="virus_infection_ratio"]:checked').length == 0){
        $('#grid_host tr[data-row] th .ratio').text("").attr("data-sort","");
        $('#grid_host .splitter .virus .sorter').hide();
        let hide_rows_with_no_infection = $("[name='hide_rows_with_no_infection']:checked");
        $("[name='hide_rows_with_no_infection']").prop("disabled",true);
         if ($("[name='hide_rows_with_no_infection']:checked").length>0){
            $("[name='hide_rows_with_no_infection']:checked")
            .prop("checked",false)
            .change()
                .attr("data-toggle-css-class")
                .split(" ")
                .map(function(c){
                    $("#grid_container").toggleClass(c);
                });
         }
    }else{
        $('#grid_host .splitter .virus .sorter').show();
        $("[name='hide_rows_with_no_infection']").prop("disabled",false);
        $('#grid_host tr[data-row] th .ratio:not(.computed)').map(function(i,o){
            $(o).text("--%");
            ratio_data_virus+="&virus="+$(o).closest("[data-row]").attr("data-row");
        })
        if (ratio_data_virus != ""){
            $.ajax({
                headers: {
                    'X-CSRFToken':getCookie('csrftoken'),
                },
                type: "GET",
                data:data_to_send+data_host+data_virus+ratio_data_virus,
                url:infection_ratio_virus,
                success: function (data, textStatus, xhr) {
                    let actual_total=$('#grid_host th[data-col]').length;
                    $('#grid_host tbody tr').removeClass("zero-infection");
                    for(let row in data){
                        var ratio=data[row]['ratio'];
                        var total=data[row]['total'];
                        ratio=ratio*total/actual_total;
                        var title=gettext("Infection ratio:")+(ratio*100).toFixed(3)+"%";
                        var tr = $('#grid_host tr[data-row="'+row+'"]');
                        if (ratio==0){
                            tr.addClass("zero-infection");
                        }
                        ratio_str=(ratio<0.1?"&nbsp;":"")+(ratio*100).toFixed(0)+"%";
                        tr.find('th .ratio')
                            .html(ratio_str)
                            .attr('title',title)
                            .attr('data-sort',ratio)
                            .addClass("computed");
                    }
                    toggle_sort_rows(true);
                },
            });
        }
    }
}

function refresh_needed_host_infection_ratio(){
    var data_to_send="",
        ratio_data_virus="",
        ratio_data_host="",
        data_host="",
        data_virus="";
    $.map(form.serializeArray(), function(n, i){
        if(n['name'] == "virus"){
            data_virus+=","+n['value'];
        }else if(n['name'] == "host"){
            data_host+=","+n['value'];
        }else{
            data_to_send+="&"+n['name']+"="+n['value'];
        }
    });
    data_host = (data_host.length==0?"":"&host="+data_host.substring(1));
    data_virus = (data_virus.length==0?"":"&virus="+data_virus.substring(1));

    if ($('[name="host_infection_ratio"]:checked').length == 0){
        $('#grid_host thead th[data-col] .ratio').text("").attr("data-sort","");
        $('#grid_host .splitter .host .sorter').hide();
        $("[name='hide_cols_with_no_infection']").prop("disabled",true);
        if ($("[name='hide_cols_with_no_infection']:checked").length>0){
            $("[name='hide_cols_with_no_infection']:checked")
                .prop("checked",false)
                .change()
                .attr("data-toggle-css-class")
                .split(" ")
                .map(function(c){
                    $("#grid_container").toggleClass(c);
                });
        }
    }else{
        $('#grid_host .splitter .host .sorter').show();
        $("[name='hide_cols_with_no_infection']").prop("disabled",false);
        $('#grid_host thead th[data-col] .ratio:not(.computed)').map(function(i,o){
            $(o).html("&#8209;&#8209;%");
            ratio_data_host+="&host="+$(o).closest("[data-col]").attr("data-col");
        })
        if (ratio_data_host != ""){
            $.ajax({
                headers: {
                    'X-CSRFToken':getCookie('csrftoken'),
                },
                type: "GET",
                data:data_to_send+data_host+data_virus+ratio_data_host,
                url:infection_ratio_host,
                success: function (data, textStatus, xhr) {
                    let actual_total=$('#grid_host tbody tr[data-row]').length;
                    $('#grid_host th').add($('#grid_host td')).removeClass("zero-infection");
                    for(let col in data){
                        let ratio=data[col]['ratio'];
                        let total=data[col]['total'];
                        ratio=ratio*total/actual_total;
                        title=gettext("Infection ratio:")+(ratio*100).toFixed(3)+"%";
                        let tc = $('#grid_host thead th[data-col="'+col+'"]');
                        if (ratio==0){
                            let pos=$(tc).index()+1;
                            $(tc).closest('table').find('tr>*:nth-child('+pos+')').addClass('zero-infection');
                        }
                        ratio_str=(ratio<0.1?"&nbsp;":"")+(ratio*100).toFixed(0)+"%";
                        tc.find('.ratio')
                            .html(ratio_str)
                            .attr('title',title)
                            .attr('data-sort',ratio)
                            .addClass("computed");
                    }
                    toggle_sort_cols(true);
                },
            });
        }
    }
}


function toggle_pin_advanced_holder() {
    $("#advanced_holder").toggleClass("pinned");
}

function trigger_download(){
    var no_selected_virus = false,
        no_selected_host = false,
        virus_select=$('#form_filter select[name="virus"]'),
        host_select=$('#form_filter select[name="host"]');

    if($('select[name="virus"]').parent().find('input[type="checkbox"]:checked').length==0){
        no_selected_virus=true;
    }
    if($('select[name="host"]').parent().find('input[type="checkbox"]:checked').length==0){
        no_selected_host=true;
    }
    $("#grid_host tbody th[data-row]").map(function(i,n ){
        let o=virus_select.find('option[value="'+$(n).attr("data-row")+'"]');
        if(no_selected_virus){
            o.prop("selected",true);
        }
        o.appendTo(o.parent())
    })
    $("#grid_host thead [data-col]").map(function(i,n ){
        let o=host_select.find('option[value="'+$(n).attr("data-col")+'"]');
        if(no_selected_host){
            o.prop("selected",true);
        }
        o.appendTo(o.parent())
    })
    $('#form_filter').submit();
    if (no_selected_virus){
        $("#grid_host tbody th[data-row]").map(function(i,n ){
            let o=virus_select.find('option[value="'+$(n).attr("data-row")+'"]');
            o.prop("selected",false);
        })
    }
    if (no_selected_host){
        $("#grid_host thead [data-col]").map(function(i,n ){
            let o=host_select.find('option[value="'+$(n).attr("data-col")+'"]');
            o.prop("selected",false);
        })
    }
}

function add_badge(id, text){
    if (typeof text == 'undefined'){
        text = document.getElementById(id).parentNode.querySelectorAll("label")[0].textContent ;
    }
    $('<span for="'+id+'" class="badge badge-pill bg-primary">'+
        '<label for="'+id+'" class="fa fa-times text-primary bg-white rounded-circle"'+
        '></label>'+text+
    '</span>').appendTo($("#badge-container"));
}