function toggle_sort_rows(just_refresh){
    var sorter = $("#grid_host>thead .splitter .rows .sorter");
    if( ! sorter.get()[0].hasAttribute("data-sort-order")){
        sorter.attr("data-sort-order",$('input[name="sort_rows"]').val());
    }
    if( !just_refresh && (sorter.attr("data-sort-order") == "" || typeof sorter.attr("data-sort-order") == "undefined") ||
        just_refresh && sorter.attr("data-sort-order") == "desc" ){
        sortRows("#grid_host>tbody", "desc");
        sorter.attr("data-sort-order","desc");
        $('input[name="sort_rows"]').val("desc");
        sorter.find(".asc").removeClass("text-muted").hide();
        sorter.find(".desc").removeClass("text-muted").show();
    }else if(!just_refresh && sorter.attr("data-sort-order") == "desc"
            || just_refresh && sorter.attr("data-sort-order") == "asc"){
        sortRows("#grid_host>tbody", "asc");
        sorter.attr("data-sort-order","asc");
        $('input[name="sort_rows"]').val("asc");
        sorter.find(".asc").removeClass("text-muted").show();
        sorter.find(".desc").removeClass("text-muted").hide();
    }else if(!just_refresh && sorter.attr("data-sort-order") == "asc"
            || just_refresh && sorter.attr("data-sort-order") == ""){
        sortRows("#grid_host>tbody", "asc", "data-init-order");
        sorter.attr("data-sort-order","");
        $('input[name="sort_rows"]').val("");
    }
    update_get_parameter();
    if ($('[name="row_order"]').length>0){
        $('[name="row_order"]').val(
            $("th[data-row]").map(function(i,o){return o.getAttribute("data-row")}).get().join( "," )
        );
    }
}
function sortRows(sel, order, attr) {
    attr = (typeof attr == "undefined" ?"data-sort":attr);
    var $selector = $(sel),
    $element = $selector.children();
    $element.sort(function(a, b) {
        var an = parseFloat($(a).find("["+attr+"]").attr(attr)),
        bn = parseFloat($(b).find("["+attr+"]").attr(attr));
        if (order == "asc") {
            if (an > bn)
            return 1;
            if (an < bn)
            return -1;
        } else if (order == "desc") {
            if (an < bn)
            return 1;
            if (an > bn)
            return -1;
        }
        var aid = $(a).find(".identifier").length,
            bid = $(b).find(".identifier").length;
        if (aid < bid)
            return 1;
        if (aid > bid)
            return -1;
        return 0;
    });
    $element.detach().appendTo($selector);
}

function toggle_sort_cols(just_refresh){
    var sorter = $("#grid_host>thead .splitter .cols .sorter");
    if( ! sorter.get()[0].hasAttribute("data-sort-order")){
        sorter.attr("data-sort-order",$('input[name="sort_cols"]').val());
    }
    if( !just_refresh && (sorter.attr("data-sort-order") == "" || typeof sorter.attr("data-sort-order") == "undefined") ||
        just_refresh && sorter.attr("data-sort-order") == "desc"){
        sorter.attr("data-sort-order","desc");
        $('input[name="sort_cols"]').val("desc");
        sorter.find(".asc").removeClass("text-muted").hide();
        sorter.find(".desc").removeClass("text-muted").show();
        sortColumns("#grid_host", "desc");
    }else if(!just_refresh && sorter.attr("data-sort-order") == "desc"
            || just_refresh && sorter.attr("data-sort-order") == "asc"){
        sorter.attr("data-sort-order","asc");
        $('input[name="sort_cols"]').val("asc");
        sorter.find(".asc").removeClass("text-muted").show();
        sorter.find(".desc").removeClass("text-muted").hide();
        sortColumns("#grid_host", "asc");
    }else if(!just_refresh && sorter.attr("data-sort-order") == "asc"
            || just_refresh && sorter.attr("data-sort-order") == ""){
        sorter.attr("data-sort-order","");
        $('input[name="sort_cols"]').val("");
        sortColumns("#grid_host", "asc", "data-init-order");
    }
    update_get_parameter();
    if ($('[name="col_order"]').length>0){
        $('[name="col_order"]').val(
            $("thead th[data-col]").map(function(i,o){return o.getAttribute("data-col")}).get().join( ", " )
        );
    }
}
function sortColumns(sel, order, attr) {
    attr = (typeof attr == "undefined" ?"data-sort":attr);
    var $selector = $(sel + " thead>tr");
    $element = $selector.children("th:not(.splitter)");
    $element.sort(function(a, b) {
        var an = parseFloat($(a).find("["+attr+"]").attr(attr)),
        bn = parseFloat($(b).find("["+attr+"]").attr(attr));
        if (order == "asc") {
                if (an > bn)
                return 1;
                if (an < bn)
                return -1;
        } else if (order == "desc") {
                if (an < bn)
                return 1;
                if (an > bn)
                return -1;
        }
        var aid = $(a).find(".identifier").length,
            bid = $(b).find(".identifier").length;
        if (aid < bid)
            return 1;
        if (aid > bid)
            return -1;
        return 0;
    });
    $element.detach().appendTo($selector);
    var $tbody=$(sel + " tbody>tr");
    for(var i=0; i<$element.length;i++){
        let data_col =  $($element[i]).attr("data-col");
        $tbody.find("td[data-col="+data_col+"]").each(function(i,e){
            let $p=$(e).parent();
            $(e).detach().appendTo($p);
        });
    }
}
