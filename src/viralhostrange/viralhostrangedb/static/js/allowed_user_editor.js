var allowed_user_editor_cpt=0;

$(document).ready(function(){
    $(".allowed-users-editor-serialized").addClass("d-none");
    var entries = $(".allowed-users-editor-serialized").val().split("\n");
    for (let i=0; i<entries.length;i++){
        let entry = entries[i].split(';');
        if (entry[0].length>0){
            add_allowed_user_editor(entry[0],entry[1],entry[2].toUpperCase() == "TRUE");
        }
    }
    add_allowed_user_editor();
});

function add_allowed_user_editor(user, pk, can_write){
    let my_allowed_user_editor_cpt = allowed_user_editor_cpt;
    allowed_user_editor_cpt+=1;
    $('<div class="input-group mb-1 allowed-user-editor" data-row="'+allowed_user_editor_cpt+'"> \
        <input type="hidden" \
               class="pk" \
               '+(pk?'value="'+pk+'"':'')+'> \
        <input type="text" \
               class="form-control user" \
               '+(user?'value="'+user+'"':'')+' \
               onchange="update_allowed_users_editor();" \
               onkeyup="update_allowed_users_editor();" \
               placeholder="First name and last name, or email" \
               aria-label="First name and last name, or email"> \
        <div class="input-group-append"> \
            <div class="input-group-text"> \
                <div class="custom-control custom-checkbox"> \
                    <input type="checkbox" \
                           onclick="update_allowed_users_editor();" \
                           class="custom-control-input can-see" \
                           checked="checked" \
                           id="ViewerCheck-'+allowed_user_editor_cpt+'"> \
                    <label class="custom-control-label" \
                           for="ViewerCheck-'+allowed_user_editor_cpt+'" \
                           title="'+gettext('Allow this user to see the data source and its associated data')+'" \
                           >View</label> \
                </div> \
            </div> \
        </div> \
        <div class="input-group-append"> \
            <div class="input-group-text"> \
                <div class="custom-control custom-checkbox"> \
                    <input type="checkbox" \
                           onclick="update_allowed_users_editor();" \
                           class="custom-control-input can-write" \
                           '+(can_write?'checked=checked':'')+' \
                           id="EditorCheck-'+allowed_user_editor_cpt+'"> \
                    <label class="custom-control-label" \
                           for="EditorCheck-'+allowed_user_editor_cpt+'" \
                           title="'+gettext('Allow this user to edit the data source and its associated data')+'" \
                           >Edit</label> \
                </div> \
            </div> \
        </div> \
    </div>').insertBefore($(".allowed-users-editor-serialized"));
}

function update_allowed_users_editor(){
    var content=new Array($(".allowed-user-editor").length);
    $(".allowed-user-editor").each(function (i,elt) {
        let user=$(elt).find("input.user").val();
        let can_see = $(elt).find(".can-see").prop("checked");
        let can_edit = $(elt).find(".can-write").prop("checked");
        if (!can_see){
            $(elt).addClass("cannot-see")
                .find(".input-group-text").add($(elt).find("input"))
                .addClass("bg-light text-secondary");
            $(elt).find(".can-write").prop("checked",false).prop("disabled",true)
        }else{
            $(elt).removeClass("cannot-see")
                .find(".input-group-text").add($(elt).find("input"))
                .removeClass("bg-light text-secondary");
            $(elt).find(".can-write").prop("disabled",false)
            if (user.length>0){
                content[i]=$(elt).find("input.user").val()+";"+$(elt).find("input.pk").val()+";"+can_edit;
                if (i+1==content.length){
                    add_allowed_user_editor();
                }
            }else{
                content[i]="";
            }
        }
    });
    $(".allowed-users-editor-serialized ").val(content.join('\n'));
}