$(document).ready(function(){
    let parts = location.search.substring(1).split('&');
    let searched_str='';
    for (var i = 0; i < parts.length; i++) {
        var nv = parts[i].split('=');
        if (nv[0]=="search")
            searched_str= nv[1];
    }
    let table = $('#main-table').show().DataTable({
        "oSearch": {"sSearch":  searched_str },
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "order": [],
        "iDisplayLength":10,
        initComplete: function () {
            this.api().columns(".dt-combobox").every( function () {
                var column = this;
                let title = $(column.footer()).text();
                var select = $('<select class="form-control"><option value="">All '+title+'</option></select>')
                    .appendTo( $("<div></div>").appendTo($(column.footer()).empty()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
            this.api().columns(".dt-multi-combobox").every( function () {
                var column = this;
                let title = $(column.footer()).text();
                var select = $('<select class="form-control"><option value="">All '+title+'</option></select>')
                    .appendTo( $("<div></div>").appendTo($(column.footer()).empty()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ?  val  : '', true, false )
                            .draw();
                    } );

                let elements = [];
                column.data().unique().sort().each( function ( d, j ) {
                    $(d).filter(":not(text)").each( function ( pos, element ) {
                        elements.push($(element).text());
                    });
                });
                elements=elements.unique().sort(function (a, b) {
                    return a.toUpperCase().localeCompare(b.toUpperCase());
                });
                $(elements).each(function ( pos, element ) {
                    select.append( '<option value="'+element+'">'+element+'</option>' )
                });
            } );
            this.api().columns(".dt-column-filter").every( function () {
                var column = this;
                let title = $(column.footer()).text();
                var select = $('<input class="form-control" type="text" placeholder="Search '+title+'" />')
                    .appendTo( $("<div></div>").appendTo($(column.footer()).empty()) )
                    .on( 'keyup change', function () {
                    if ( column.search() !== this.value ) {
                        column.search( this.value ).draw();
                    }
                } );
            } );
        }
    });
});