$(document).ready(function(){
    $.ajax({
        url : get_statistics_url,
        dataType : 'json',
        data : {public:true},
        success : function(data){
            let $stat = $(".statistics");
            for (k in data){
                $stat.find("."+k).text(data[k]);
            }
        },
    });
});