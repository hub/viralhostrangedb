import csv
import errno
import itertools
import json
import logging
import os
import re
from collections import namedtuple
from enum import Enum
from random import shuffle
from sqlite3 import DataError as sqlite3_DataError
from typing import Union, List

import ezodf
import pandas as pd
from Bio import Entrez
from basetheme_bootstrap.user_preferences_utils import get_user_preferences_for_user
from django.conf import settings
from django.contrib import messages
from django.contrib.admin import models as admin_models
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.core.mail import EmailMultiAlternatives
from django.db import transaction
from django.db.models import Q, Max, Exists, OuterRef
from django.db.models.functions import Upper
from django.urls import reverse
from django.utils import timezone
from django.utils.html import escape, strip_tags
from django.utils.safestring import mark_safe
from django.utils.translation import gettext
from psycopg2._psycopg import DataError as psycopg2_DataError

from live_settings import live_settings
from viralhostrangedb import models
from viralhostrangedb.templatetags.viralhostrange_tags import get_change_message_with_action

logger = logging.getLogger('django')

ViralHostResponse = namedtuple(
    "ViralHostResponse",
    [
        "virus",
        "virus_identifiers",
        "host",
        "host_identifiers",
        "response",
        "parsed_response",
        "row_id",
        "col_id",
    ])

NameAndIdentifierRegexp = re.compile("([^(]*)([(]([^(]+)[)])?")

NCBI_IDENTIFIER = "ncbi"

HER_IDENTIFIER = "her"

TAX_IDENTIFIER = "tax"

MIN_EMPTY_ROWS = 3  # should not be changed as multiple test data were made with tree blank line


def default_identifiers():
    return {NCBI_IDENTIFIER: '', HER_IDENTIFIER: None, TAX_IDENTIFIER: None}


class ImportationObserver:
    class Meta:
        abstract = True

    DUPLICATED = 1
    EMPTY_NAME = 2

    def notify_response_error(self, virus, host, response_str):
        pass

    def notify_host_error(self, host, column_id, reason=None, reason_id=None):
        pass

    def notify_virus_error(self, virus, row_id, reason=None, reason_id=None):
        pass

    def notify_import_ended(self):
        pass

    @staticmethod
    def id_to_excel_index(n):
        # https://stackoverflow.com/a/23862195/2144569
        string = ""
        while n > 0:
            n, remainder = divmod(n - 1, 26)
            string = chr(65 + remainder) + string
        return string


class MessageImportationObserver(ImportationObserver):

    def __init__(self, request):
        self.request = request
        self.host_warned = set()
        self.virus_warned = set()

    def add_message(self, request, level, error_code, message):
        messages.add_message(request=request, level=level, message=f'[{error_code}] {message}')

    def notify_response_error(self, virus, host, response_str):
        self.add_message(
            self.request,
            messages.WARNING,
            "ImportErr2",
            gettext(
                "Could not import response \"%(response)s\" for virus \"%(virus)s\", host\"%(host)s\"") % dict(
                response=str(response_str),
                virus=str(virus),
                host=str(host),
            )
        )

    @staticmethod
    def reason_to_str(msg, reason, reason_id):
        if reason_id == ImportationObserver.DUPLICATED:
            return msg + gettext(": ") + gettext("Duplicated")
        if reason_id == ImportationObserver.EMPTY_NAME:
            return msg + gettext(": ") + gettext("Empty name")
        msg = [msg, "<br/>"]
        if type(reason) == ValidationError:
            reason = reason.error_dict
        if type(reason) == dict:
            msg.append("<ul>")
            for k, v in reason.items():
                msg.append("<li><b>")
                msg.append(escape(k))
                msg.append("</b>: ")
                msg.append(escape(str(v)))
                msg.append("</li>")
            msg.append("</ul>")
        else:
            msg.append(str(reason))
        return "".join(msg)

    def notify_host_error(self, host, column_id, reason=None, reason_id=None):
        if column_id in self.host_warned:
            return
        self.host_warned.add(column_id)
        msg = gettext("Issue with host \"%(host)s\" at column \"%(column_id)s\" "
                                        "(i.e column \"%(column_index)s\")") % dict(
            host=str(host),
            column_id=str(column_id),
            column_index=str(self.id_to_excel_index(column_id)),
        )
        if reason or reason_id:
            msg = self.reason_to_str(msg, reason, reason_id)
        self.add_message(
            self.request,
            messages.WARNING,
            "ImportErr1",
            mark_safe(msg),
        )

    def notify_virus_error(self, virus, row_id, reason=None, reason_id=None):
        if row_id in self.virus_warned:
            return
        self.virus_warned.add(row_id)
        msg = gettext("Issue with virus \"%(virus)s\" at row \"%(row_id)s\"") % dict(
            virus=str(virus),
            row_id=str(row_id),
        )
        if reason or reason_id:
            msg = self.reason_to_str(msg, reason, reason_id)
        self.add_message(
            self.request,
            messages.WARNING,
            "ImportErr3",
            mark_safe(msg),
        )

    def notify_import_ended(self):
        # testing that error are concentrated at the end, and probably the legend
        previous = None
        row_len = 1
        for v_id in list(sorted(self.virus_warned, key=lambda x: -x)):
            if previous is None:
                previous = v_id
            elif previous + 1 != v_id:
                row_len+=1
            else:
                row_len=0
                break
        if row_len>2:
            msg=gettext("Maybe not enough empty line at the end")
            self.add_message(
                self.request,
                messages.INFO,
                "ImportWrn4",
                mark_safe(msg),
            )




class StackErrorImportationObserver(MessageImportationObserver):
    def __init__(self):
        super().__init__(request=None)
        self.errors = []

    def add_message(self, request, level, error_code, message):
        self.errors.append(f'[{error_code}] {message}')


def panda_color_mapping(v):
    key = 'html_color_%s' % str(v)
    color = cache.get(key)
    if color is not None:
        return color
    if v is not None and (isinstance(v, float) or isinstance(v, int)):
        try:
            color = 'background-color:' + models.GlobalViralHostResponseValue.get_html_color_for(v)
            cache.set(key, color, 60)
            return color
        except Exception:
            pass
    cache.set(key, '', 60)
    return ''


def panda_legend_color_mapping(v):
    # try:
    return panda_color_mapping(models.GlobalViralHostResponseValue.objects.get(name=v).value)
    # except ValueError: # No ValueError can be raised
    #     return ""


def export_data_source_to_file(data_source, file_path):
    mapping = dict([(raw, response.value) for raw, response in data_source.get_mapping(only_pk=False)])

    # define once and for all how we order the entry, to avoid bug in future refactoring
    def order_hv(qs, prefixes=None):
        prefixes = prefixes or [""]
        return qs.order_by(*[Upper(p + field) for p, field in itertools.product(prefixes, ['name', 'identifier', ])])

    # get virus and host, ordered by pk (i.e order of import, thus order liked by file owner)
    col_header = [o.explicit_name for o in order_hv(data_source.host_set)]
    row_header = [o.explicit_name for o in order_hv(data_source.virus_set)]
    # get at which position each host is, useful when data are sparse
    col_pos = dict([(pk, i) for i, pk in enumerate(order_hv(data_source.host_set).values_list('pk', flat=True))])
    row_pos = dict([(pk, i) for i, pk in enumerate(order_hv(data_source.virus_set).values_list('pk', flat=True))])

    old_virus_id = None
    row_raw_content = None
    row_mapped_content = None
    data_raw = []
    data_mapped = []
    # get all responses, ordered by virus_id and then host_id to fill the matrix ideally from left to right, and then
    # top to bottom.
    for virus_id, host_id, raw_response in order_hv(data_source.responseindatasource, prefixes=["virus__", "host__"]) \
            .values_list('virus_id', 'host_id', 'raw_response'):
        if old_virus_id != virus_id:
            # new virus, thus new line in the matrix
            old_virus_id = virus_id
            for i in range(len(data_raw), row_pos[virus_id] + 1):
                # if some row are empty, creating empty array to reflect it
                row_raw_content = [""] * len(col_header)
                data_raw.append(row_raw_content)
                row_mapped_content = [""] * len(col_header)
                data_mapped.append(row_mapped_content)

        col_id = col_pos[host_id]
        # set the response in the right column (useful when data are sparse)
        row_raw_content[col_id] = raw_response
        row_mapped_content[col_id] = mapping.get(raw_response, "")

    # write in a temp file
    with pd.ExcelWriter(file_path) as writer:
        pd.DataFrame(data_raw,
                     index=row_header,
                     columns=col_header) \
            .to_excel(writer, sheet_name='raw_responses')

        df_data = pd.DataFrame(data_mapped, columns=col_header, index=row_header)
        df_data = df_data.style.applymap(func=panda_color_mapping)
        df_data.to_excel(writer, sheet_name='mapped_responses')


def ods_to_csv(file):
    doc = ezodf.opendoc(file)
    sheet = doc.sheets[0]
    for row in sheet.rows():
        csv_row = []
        for cell in row:
            value = cell.value
            csv_row.append('' if value is None else str(value))
        yield csv_row


def parse_mapping(file, importation_observer: ImportationObserver = None):
    data_as_dict = {}
    mapping = {}
    for vhr in __parse_file(file, importation_observer):
        host = explicit_item(
            name=vhr.host,
            ncbi=vhr.host_identifiers[NCBI_IDENTIFIER],
            her_identifier=None,
            tax_id=vhr.host_identifiers[TAX_IDENTIFIER],
        )
        virus = explicit_item(
            name=vhr.virus,
            ncbi=vhr.virus_identifiers[NCBI_IDENTIFIER],
            her_identifier=vhr.virus_identifiers[HER_IDENTIFIER],
            tax_id=vhr.virus_identifiers[TAX_IDENTIFIER],
        )
        host_dict = data_as_dict.setdefault(host, dict())
        host_dict[virus] = vhr.response
    for vhr in __parse_file(file, importation_observer, sheet_name="mapped_responses"):
        host = explicit_item(
            name=vhr.host,
            ncbi=vhr.host_identifiers[NCBI_IDENTIFIER],
            her_identifier=None,
            tax_id=vhr.host_identifiers[TAX_IDENTIFIER],
        )
        virus = explicit_item(
            name=vhr.virus,
            ncbi=vhr.virus_identifiers[NCBI_IDENTIFIER],
            her_identifier=vhr.virus_identifiers[HER_IDENTIFIER],
            tax_id=vhr.virus_identifiers[TAX_IDENTIFIER],
        )
        mapping_for_response = mapping.setdefault(vhr.response, set())
        mapping_for_response.add(data_as_dict[host][virus])
    return mapping


def parse_file(file, importation_observer: ImportationObserver = None):
    return __parse_file(file, importation_observer)


def __parse_file(file, importation_observer: ImportationObserver = None, sheet_name: Union[str, int] = 0):
    input_file = None
    if type(file) is str:
        filename = file
    else:
        filename = file.name
    if filename.endswith("csv"):
        input_file = open(file, 'r')
        my_reader = csv.reader(input_file, delimiter=";", quotechar="\"")
    elif filename.endswith("ods"):
        my_reader = ods_to_csv(file)
    else:
        content = pd.read_excel(file, index_col=0, header=None, sheet_name=sheet_name, engine='openpyxl')
        my_reader = csv.reader(content.to_csv(sep=';').split('\n'), delimiter=';')
        next(my_reader)
    header = None
    start_at = 0
    has_seen_data = False
    row_id = 0
    blank_line = 0
    virus_set = set()
    header_error_dicts = None
    for row in my_reader:
        row_id += 1
        sub_row = row[start_at:]
        virus = None
        virus_identifiers = None
        if (
                (any(c != '' for c in row)
                 # Reverse compatibility patching, template prior to 2023-08 were generated with only two lines
                 and sub_row[0:3] != ['', '', "Recommended responses scheme"])
        ):
            blank_line = 0
        else:
            blank_line += 1
            if header and blank_line >= MIN_EMPTY_ROWS:
                break
            continue
        for id_col, cell in enumerate(sub_row):
            cell = cell.strip()
            if header is None or not has_seen_data and id_col == 1 and sub_row[0] == "":
                # building header, or re-building if a previous one have been seen
                if cell != "" and not cell.startswith("Unnamed: "):
                    header = [h[:-2] if h.endswith(".0") else h for h in row]
                    start_at = id_col - 1 + start_at
                    header_set = set(header[start_at + 1:])
                    if importation_observer:
                        header_error_dicts = []
                        for header_col, h in enumerate(header[start_at + 1:]):
                            try:
                                header_set.remove(h)
                            except KeyError:
                                header_error_dicts.append(dict(
                                    host=h,
                                    column_id=header_col + start_at + 2,
                                    reason_id=ImportationObserver.DUPLICATED,
                                ))
                    break
            elif id_col > 0 and sub_row[0] != "":
                # row with data and virus with a name
                cell = cell.strip()
                try:
                    dot_pos = cell.index('.')
                    cell = cell[0:dot_pos] + (cell[dot_pos:].rstrip('.0'))
                except ValueError:
                    pass
                if cell == "":
                    continue
                if virus is None:
                    virus, virus_identifiers = extract_name_and_identifiers(sub_row[0])
                    former_len = len(virus_set)
                    virus_set.add((virus, str(virus_identifiers)))
                    if importation_observer and former_len == len(virus_set):
                        importation_observer.notify_virus_error(
                            sub_row[0],
                            row_id,
                            reason_id=ImportationObserver.DUPLICATED,
                        )
                has_seen_data = True
                h = header[id_col + start_at]
                if h == "" or h.startswith("Unnamed: "):
                    if importation_observer:
                        importation_observer.notify_host_error(
                            h,
                            id_col + start_at + 1,
                            reason_id=ImportationObserver.EMPTY_NAME,
                        )
                    continue
                host, host_identifiers = extract_name_and_identifiers(h)
                try:
                    parsed_response = float(cell)
                except ValueError:
                    parsed_response = -1000
                    if importation_observer:
                        importation_observer.notify_response_error(virus, host, cell)
                yield ViralHostResponse(
                    virus=virus,
                    virus_identifiers=virus_identifiers,
                    host=host,
                    host_identifiers=host_identifiers,
                    response=cell,
                    parsed_response=parsed_response,
                    row_id=row_id,
                    col_id=id_col + start_at,
                )
            elif id_col > 0 and importation_observer:
                importation_observer.notify_virus_error(
                    sub_row[0],
                    row_id,
                    reason_id=ImportationObserver.EMPTY_NAME,
                )
    if header_error_dicts is not None:
        for d in header_error_dicts:
            importation_observer.notify_host_error(**d)
    if input_file is not None:
        input_file.close()
    if importation_observer:
        importation_observer.notify_import_ended()


def extract_name_and_identifiers(text):
    match = NameAndIdentifierRegexp.search(text)
    name = match.groups()[0].strip()
    identifiers_raw = match.groups()[-1]
    identifiers = default_identifiers()
    if identifiers_raw:
        for identifier_candidate in identifiers_raw.strip().split(';'):
            identifier_candidate = identifier_candidate.strip()
            if identifier_candidate.upper().startswith("HER:"):
                identifiers[HER_IDENTIFIER] = int(identifier_candidate[4:])
            elif identifier_candidate.upper().startswith("TAX:"):
                identifiers[TAX_IDENTIFIER] = identifier_candidate[4:]
            elif len(identifier_candidate) > 0:
                identifiers[NCBI_IDENTIFIER] = identifier_candidate
    return name, identifiers


def to_dict(viral_host_responses, transposed=False):
    data_as_dict = {}
    for vhr in viral_host_responses:
        host = explicit_item(
            name=vhr.host,
            ncbi=vhr.host_identifiers[NCBI_IDENTIFIER],
            her_identifier=None,
            tax_id=vhr.host_identifiers[TAX_IDENTIFIER],
        )
        virus = explicit_item(
            name=vhr.virus,
            ncbi=vhr.virus_identifiers[NCBI_IDENTIFIER],
            her_identifier=vhr.virus_identifiers[HER_IDENTIFIER],
            tax_id=vhr.virus_identifiers[TAX_IDENTIFIER],
        )
        host_dict = data_as_dict.setdefault(virus if transposed else host, dict())
        host_dict[host if transposed else virus] = vhr.response
    return data_as_dict


def to_stat(viral_host_responses, transposed=False):
    hosts = set()
    viruses = set()
    responses = set()
    count = 0
    for vhr in viral_host_responses:
        hosts.add(explicit_item(vhr.host, **vhr.host_identifiers))
        viruses.add(explicit_item(vhr.virus, **vhr.virus_identifiers))
        responses.add(vhr.response)
        count += 1
    return dict(
        hosts=viruses if transposed else hosts,
        viruses=hosts if transposed else viruses,
        response_count=count,
        responses=responses,
    )


def explicit_item(name, identifier=None, her_identifier=None, tax_id=None, ncbi=None, her=None, tax=None, html=False):
    assert identifier is None or ncbi is None
    assert her_identifier is None or her is None
    assert tax_id is None or tax is None
    identifiers = []
    if identifier is None:
        identifier = ncbi
    if her_identifier is None:
        her_identifier = her
    if tax_id is None:
        tax_id = tax
    if identifier is not None and identifier != '':
        identifiers.append(identifier)
    if tax_id is not None:
        identifiers.append("TAX:%s" % tax_id)
    if her_identifier is not None:
        identifiers.append("HER:%i" % her_identifier)
    if len(identifiers) > 0:
        if html:
            return "%s<small>(%s)</small>" % (name, "; ".join(identifiers))
        return "%s (%s)" % (name, "; ".join(identifiers))
    return name


@transaction.atomic
def restore_backup(*, data_source, log_entry, importation_observer: ImportationObserver = None):
    assert str(data_source.pk) == log_entry.object_id \
           and ContentType.objects.get_for_model(models.DataSource).id == log_entry.content_type_id

    backup_file_path = get_backup_file_path(log_entry=log_entry)

    return import_file(
        data_source=data_source,
        file=backup_file_path,
        importation_observer=importation_observer,
    )


def import_file_later(*, file):
    observer = StackErrorImportationObserver()
    data = list(__parse_file(file, importation_observer=observer))

    def actually_import_file(data_source, importation_observer):
        return __import_file(data_source=data_source, parsed_file=data, importation_observer=importation_observer)

    return observer.errors, actually_import_file


def import_file(*, data_source, file, importation_observer: ImportationObserver = None):
    parsed_file = parse_file(file, importation_observer)
    return __import_file(data_source=data_source, parsed_file=parsed_file, importation_observer=importation_observer)


@transaction.atomic
def __import_file(*, data_source, parsed_file, importation_observer: ImportationObserver = None):
    """
    Import the file and associate responses to the data source provided. If responses are already present there are
    overwritten with the one from the file. Updated response are automatically map following the mapping observed in db

    :param data_source: data source that will be updated/enriched (can be a fresh data source)
    :param file: file or path to the file to import for the passed data source
    :param importation_observer: an observer allowing to notify of problem during the importation without crashing
    :return: the data source, updated with the response gathered from the file
    """
    not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()
    # virus already associated with this data source
    old_virus_pk = set(data_source.virus_set.values_list('pk', flat=True))
    # host already associated with this data source
    old_host_pk = set(data_source.host_set.values_list('pk', flat=True))
    # cached virus
    virus_dict = dict()
    # cached host
    host_dict = dict()
    # response to bulk create
    responses_to_create = []
    # former mapping, if present empty dict otherwise
    former_mapping = dict(data_source.get_mapping(only_pk=True))
    # purge old response, not an issue if process fails as we are in a transaction
    models.ViralHostResponseValueInDataSource.objects.filter(data_source=data_source).delete()
    for vhr in parsed_file:
        # if vhr.virus == "" or vhr.host == "" or vhr.response == "":
        #     continue
        explicit_virus = explicit_item(
            name=vhr.virus,
            identifier=vhr.virus_identifiers[NCBI_IDENTIFIER],
            her_identifier=vhr.virus_identifiers[HER_IDENTIFIER],
            tax_id=vhr.virus_identifiers[TAX_IDENTIFIER],
        )
        try:
            # search virus in dict
            virus = virus_dict[explicit_virus]
        except KeyError:
            try:
                virus = models.Virus.objects.filter(
                    name=vhr.virus,
                    identifier=vhr.virus_identifiers[NCBI_IDENTIFIER],
                    her_identifier=vhr.virus_identifiers[HER_IDENTIFIER],
                    # tax_id=vhr.virus_identifiers[TAX_IDENTIFIER], # do not consider tax_id as it will be fetched later
                )[0]
            except IndexError:
                # create virus
                virus = models.Virus(
                    name=vhr.virus,
                    identifier=vhr.virus_identifiers[NCBI_IDENTIFIER],
                    her_identifier=vhr.virus_identifiers[HER_IDENTIFIER],
                    is_ncbi_identifier_value=False if vhr.virus_identifiers[NCBI_IDENTIFIER] == '' else None,
                    # tax_id=vhr.virus_identifiers[TAX_IDENTIFIER], # we do not import tax_id, will be fetched later
                )
                try:
                    # check is validators are ok
                    virus.full_clean()
                    # save in db
                    virus.save()
                except (sqlite3_DataError, psycopg2_DataError, ValidationError) as e:
                    if importation_observer:
                        importation_observer.notify_virus_error(
                            str(vhr.virus),
                            vhr.row_id,
                            reason=e,
                        )
                    continue

            virus.data_source.add(data_source)
            # set virus in dict
            virus_dict[explicit_virus] = virus
        explicit_host = explicit_item(
            name=vhr.host,
            identifier=vhr.host_identifiers[NCBI_IDENTIFIER],
            her_identifier=None,
            tax_id=vhr.host_identifiers[TAX_IDENTIFIER],
        )
        try:
            # search host in dict
            host = host_dict[explicit_host]
        except KeyError:
            try:
                # search host in db
                host = models.Host.objects.filter(
                    name=vhr.host,
                    identifier=vhr.host_identifiers[NCBI_IDENTIFIER],
                    # tax_id=vhr.host_identifiers[TAX_IDENTIFIER], # do not consider tax_id as it will be fetched later
                )[0]
            except IndexError:
                # create host
                host = models.Host(
                    name=vhr.host,
                    identifier=vhr.host_identifiers[NCBI_IDENTIFIER],
                    is_ncbi_identifier_value=False if vhr.host_identifiers[NCBI_IDENTIFIER] == '' else None,
                    # tax_id=vhr.host_identifiers[TAX_IDENTIFIER], # we do not import tax_id, will be fetched later
                )
                try:
                    # check is validators are ok
                    host.full_clean()
                    # save in db
                    host.save()
                except (sqlite3_DataError, psycopg2_DataError, ValidationError) as e:
                    if importation_observer:
                        importation_observer.notify_host_error(
                            str(vhr.host),
                            vhr.col_id,
                            reason=e,
                        )
                    continue
            host.data_source.add(data_source)
            # set host in dict
            host_dict[explicit_host] = host

        try:
            float(vhr.response)
        except ValueError as e:
            if importation_observer:
                importation_observer.notify_response_error(vhr.virus, vhr.host, vhr.response)
            else:
                raise e
        # update or create response in db
        responses_to_create.append(
            models.ViralHostResponseValueInDataSource(
                data_source=data_source,
                virus=virus,
                host=host,
                raw_response=vhr.parsed_response,
                response_id=former_mapping.get(vhr.parsed_response, not_mapped_yet.pk),
            )
        )
        old_virus_pk.discard(virus.pk)
        old_host_pk.discard(host.pk)

    # insert all responses, if duplicate, django will ignore them
    models.ViralHostResponseValueInDataSource.objects.bulk_create(responses_to_create)
    # remove duplicated response, i.e same data source virus and host in this data source
    models.ViralHostResponseValueInDataSource.objects.filter(data_source=data_source).annotate(
        is_dup=Exists(
            models.ViralHostResponseValueInDataSource.objects.filter(
                data_source_id=OuterRef('data_source_id'),
                virus_id=OuterRef('virus_id'),
                host_id=OuterRef('host_id'),
                id__lt=OuterRef('id'),
            )
        )
    ).filter(is_dup=True).delete()
    # remove responses associated to virus that we did not see
    models.ViralHostResponseValueInDataSource.objects.filter(
        data_source=data_source,
        virus_id__in=old_virus_pk,
    ).delete()
    # de-associate virus not seen from the data source
    for o in models.Virus.objects.filter(pk__in=old_virus_pk):
        if o.data_source.count() <= 1:
            o.delete()
        else:
            o.data_source.remove(data_source)

    # remove responses associated to host that we did not see
    models.ViralHostResponseValueInDataSource.objects.filter(
        data_source=data_source,
        host_id__in=old_host_pk,
    ).delete()
    # de-associate host not seen from the data source
    for o in models.Host.objects.filter(pk__in=old_host_pk):
        if o.data_source.count() <= 1:
            o.delete()
        else:
            o.data_source.remove(data_source)

    # update lat edition time
    data_source.last_edition_date = timezone.now()
    data_source.save()
    return data_source


def reset_mapping(queryset):
    not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()
    for o in queryset:
        models.ViralHostResponseValueInDataSource.objects.filter(data_source=o).update(response=not_mapped_yet)


def get_ncbi_ids(identifier):
    with Entrez.esearch(db="nuccore", retmax=1, term="%s[ACCESSION]" % identifier) as handle:
        response = Entrez.read(handle)
        if int(response["Count"]) == 0:
            return None, None
        nc_id = response["IdList"][0]
    try:
        with Entrez.efetch(db="nuccore", id=nc_id, rettype="fasta", retmode="xml") as handle:
            tax_id = Entrez.read(handle)[0]['TSeq_taxid']
            if tax_id == "0":
                return nc_id, None
    except IndexError:
        with Entrez.efetch(db="nuccore", id=nc_id, rettype="gb", retmode="xml") as handle:
            for gbq in Entrez.read(handle)[0]['GBSeq_feature-table'][0]['GBFeature_quals']:
                if gbq['GBQualifier_name'] == 'db_xref':
                    tax_id = str(gbq['GBQualifier_value']).replace("taxon:", "")
            if tax_id == "0":
                return nc_id, None
    return nc_id, tax_id


def reset_identifier_status(queryset):
    queryset.update(is_ncbi_identifier_value=None)


def set_identifier_status(queryset, status):
    queryset.update(is_ncbi_identifier_value=status)


def get_identifier_status_unknown(queryset):
    return queryset.filter(is_ncbi_identifier_value__isnull=True)


def fetch_identifier_status(queryset, progress=False):
    ids = list(queryset.values_list('id', flat=True))
    shuffle(ids)
    if progress:
        logger.warning("Working with " + str(queryset.count()) + " of " + str(queryset.model))
        try:
            from tqdm import tqdm
            ids = tqdm(ids)
        except ModuleNotFoundError as e:
            if getattr(fetch_identifier_status, "first_time_warned", True):
                logger.warning("Asked for a progress bar while tqdm is not available")
                fetch_identifier_status.first_time_warned = False
    for o_id in ids:
        o = queryset.filter(id=o_id).first()  # in case of multi parallel call, get can raise DoesNotExist
        assert o is None or o.is_ncbi_identifier is not None, "Failed with pk " + str(o.id)


def is_curator(user):
    if not user.is_authenticated:
        return False
    return user.groups.filter(name="Moderator").exists()


def get_curators():
    moderator, _ = Group.objects.get_or_create(name="Moderator")
    return moderator.user_set.all()


def set_curator(user, status):
    moderator, _ = Group.objects.get_or_create(name="Moderator")
    if status:
        user.groups.add(moderator)
    else:
        user.groups.remove(moderator)


def get_backup_file_path(log_entry, test_if_exists=False):
    assert_backup_dir_exists()
    backup_file_path = os.path.join(settings.BACKUP_LOCATION, 'ds-%s-v-%i.xlsx' % (log_entry.object_id, log_entry.id))
    if test_if_exists and not os.path.isfile(backup_file_path):
        return None
    return backup_file_path


def assert_backup_dir_exists():
    try:
        os.mkdir(settings.BACKUP_LOCATION)
    except OSError as e:
        assert e.errno == errno.EEXIST, e


def get_user_media_root(user):
    if user is None or not user.is_authenticated:
        file_path = os.path.join(settings.MEDIA_ROOT, "anonymous")
    else:
        file_path = os.path.join(settings.MEDIA_ROOT, str(user.pk))
    try:
        os.makedirs(file_path)
    except OSError as e:
        assert e.errno == errno.EEXIST, e
    return file_path


class DataSourceAlteredData(Enum):
    UNKNOWN = 0
    ALL = 0
    RESPONSES = 1
    VIRUS = 2
    HOST = 3
    INNER_INFORMATION = 4
    MAPPING = 5


def is_change_should_trigger_a_backup(altered_data: Union[DataSourceAlteredData, List[DataSourceAlteredData]]):
    if type(altered_data) is list:
        for a in altered_data:
            if is_change_should_trigger_a_backup(a):
                return True
    return altered_data in [
        DataSourceAlteredData.UNKNOWN,
        DataSourceAlteredData.ALL,
        DataSourceAlteredData.RESPONSES,
        DataSourceAlteredData.VIRUS,
        DataSourceAlteredData.HOST,
        # DataSourceAlteredData.INNER_INFORMATION, # NOT YET
        # DataSourceAlteredData.MAPPING, # NOT YET
    ]


def backup_data_source(
        user,
        data_source,
        altered_data: Union[DataSourceAlteredData, List[DataSourceAlteredData]],
        is_addition=False,
        is_change=False,
        is_deletion=False,
        change_fields=None,
        action_name=None,
):
    assert not is_change or change_fields is not None, "change_fields is mandatory for a change"
    if is_deletion:
        action_flag = admin_models.DELETION
        change_message_dict = {"deleted": {"object": ", ".join(change_fields), "fields": change_fields, "name": ""}}
    elif is_change:
        action_flag = admin_models.CHANGE
        change_fields = ["???", ] if change_fields is None else change_fields
        change_message_dict = {"changed": {"fields": change_fields}}
    elif is_addition:
        change_fields = change_fields or []
        action_flag = admin_models.ADDITION
        change_message_dict = {"added": {"name": '', 'object': action_name or '', "fields": change_fields}}
    else:
        raise AssertionError("Must either be a add, change, or deletion")

    has_backup = is_change_should_trigger_a_backup(altered_data)

    change_message_dict["has_backup"] = has_backup

    if action_name is not None:
        list(change_message_dict.values())[0]["action"] = action_name

    le = admin_models.LogEntry.objects.log_action(
        user_id=user.pk,
        content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
        object_id=data_source.pk,
        object_repr=data_source.__str__(),
        action_flag=action_flag,
        change_message=json.dumps([change_message_dict, ]),
    )
    if has_backup:
        export_data_source_to_file(data_source=data_source, file_path=get_backup_file_path(le))


def message_to_user(message, request=None, modeladmin=None, level=messages.SUCCESS):
    if not request:
        logger.warning(message)
        return
    if modeladmin:
        modeladmin.message_user(request=request, message=message, level=level)
    else:
        messages.add_message(request=request, level=level, message=message, )


def create_backup_when_missing(queryset, request=None, modeladmin=None):
    for data_source in queryset:
        le = None
        for candidate_le in admin_models.LogEntry.objects.filter(
                object_id=data_source.pk,
                content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
                change_message__contains="\"has_backup\": true",
        ):
            if get_backup_file_path(candidate_le, test_if_exists=True) is not None:
                le = candidate_le
        if le is not None:
            continue

        try:
            backup_data_source(
                user=data_source.owner,
                data_source=data_source,
                is_addition=True,
                change_fields=[],
                action_name="Created backup.",
                altered_data=DataSourceAlteredData.ALL,
            )
            message_to_user(
                message="Created a backup for data source %i \"%s\"" % (data_source.pk, data_source.__str__()),
                modeladmin=modeladmin, request=request
            )
        except Exception as e:
            message_to_user(
                "Failed to backup data source %i \"%s\"\n%s" % (data_source.pk, str(data_source), str(e)),
                level=messages.ERROR,
                modeladmin=modeladmin, request=request,
            )


def get_log_entries(data_source):
    return admin_models.LogEntry.objects.filter(
        object_id=data_source.pk,
        content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
    ).order_by("-action_time", '-pk')


def get_orphan_log_entries(queryset):
    return queryset \
        .filter(~Q(object_id__in=[str(i) for i in models.DataSource.objects.values_list('id', flat=True)])) \
        .filter(content_type_id=ContentType.objects.get_for_model(models.DataSource).id)


def _delete_backup_file(les, modeladmin, request):
    removed = 0
    for le in les:
        backup_file_path = get_backup_file_path(le, test_if_exists=True)
        if backup_file_path is None:
            continue
        try:
            os.unlink(backup_file_path)
            removed += 1
        except OSError as e:
            message_to_user(
                "Failed to backup data source %i \"%s\"\n%s" % (le.object_id, le.object_repr, str(e)),
                level=messages.ERROR,
                modeladmin=modeladmin, request=request,
            )
    return removed


def remove_old_orphan_backup_files(queryset, request=None, modeladmin=None):
    old_orphan = get_orphan_log_entries(queryset).filter(
        action_time__lt=
        timezone.now() - timezone.timedelta(weeks=settings.BACKUP_MIN_WEEKS_TO_KEEP)
    )
    removed = _delete_backup_file(les=old_orphan, modeladmin=modeladmin, request=request)
    if removed > 0:
        message_to_user(
            "%i orphan backup files were removed" % (
                removed,
            ), modeladmin=modeladmin, request=request,
        )
    else:
        message_to_user(
            "No orphan backup files were removed, all are  younger than %i weeks (~%i months)." % (
                settings.BACKUP_MIN_WEEKS_TO_KEEP,
                int(settings.BACKUP_MIN_WEEKS_TO_KEEP / 52 * 12),
            ), modeladmin=modeladmin, request=request,
        )


def remove_old_backup_files(queryset, request=None, modeladmin=None):
    removed_a_backup = False
    for data_source in queryset:
        les = get_log_entries(data_source)
        if les.count() <= settings.BACKUP_MIN_OCCURRENCES_TO_KEEP:
            continue

        older_les = les.filter(
            action_time__lt=
            timezone.now() - timezone.timedelta(weeks=settings.BACKUP_MIN_WEEKS_TO_KEEP)
        ).filter(~Q(pk__in=les.values_list('pk', flat=True)[: settings.BACKUP_MIN_OCCURRENCES_TO_KEEP]))

        removed = _delete_backup_file(les=older_les, modeladmin=modeladmin, request=request)

        if removed > 0:
            removed_a_backup = True
            message_to_user(
                "%i backup files were removed for data source %i \"%s\"" % (
                    removed,
                    data_source.pk,
                    str(data_source),
                ), modeladmin=modeladmin, request=request,
            )
    if not removed_a_backup:
        message_to_user(
            "No backup files were removed, all are either in the top %i for there data source,"
            " or younger than %i weeks (~%i months)." % (
                settings.BACKUP_MIN_OCCURRENCES_TO_KEEP,
                settings.BACKUP_MIN_WEEKS_TO_KEEP,
                int(settings.BACKUP_MIN_WEEKS_TO_KEEP / 52 * 12),
            ), modeladmin=modeladmin, request=request,
        )


def send_latest_edition_notification(starting_with: int = None):
    if starting_with is None:
        starting_with = live_settings.log_entry_up_to or 0

    max_pk = list(admin_models.LogEntry.objects.aggregate(Max('pk')).values())[0]
    les = admin_models.LogEntry.objects.filter(pk__gt=starting_with, pk__lte=max_pk)
    logger.info("reviewing event such as %i<pk<=%i" % (starting_with, max_pk,))
    print(logger.level)
    send_edition_notification(queryset=les)
    live_settings.log_entry_up_to = max_pk


def send_edition_notification(queryset, request=None, modeladmin=None):
    from viralhostrangedb.models import UserPreferences as UPref
    # filter for only DataSource LogEntry...
    queryset = queryset.filter(content_type_id=ContentType.objects.get_for_model(models.DataSource).id)

    # put all LogEntry in a dict Owner->Data source->Log Entries
    all_le = dict()
    notify_to_curator = set()
    for le in queryset.order_by("action_time"):
        data_source = models.DataSource.objects.filter(pk=int(le.object_id)).first()
        if data_source is None:
            continue

        change_message = json.loads(le.change_message)
        if data_source.public and (
                'public' in change_message[0].get('changed', {}).get('fields', [])
                or 'added' in change_message[0]
        ):
            notify_to_curator.add(data_source)
            logger.info("curators will be notified for data source %i (event %i)" % (data_source.pk, le.pk,))

        pref = get_user_preferences_for_user(data_source.owner)
        if pref.edition_notification == UPref.NOTIFY_FOR_NO_ONE:
            logger.info("%s does not want notification, skipping notification for %i" %
                        (data_source.owner, le.pk,))
            continue
        if pref.edition_notification < UPref.NOTIFY_EVEN_FOR_ME and \
                data_source.owner == le.user:
            logger.info("%s is the owner, skipping notification for %i" %
                        (data_source.owner, le.pk,))
            continue
        if pref.edition_notification < UPref.NOTIFY_FOR_EDITOR_AND_CURATOR and \
                data_source.granteduser_set.filter(user=le.user, can_write=True).exists():
            logger.info("%s is an allowed editor of data source %s, skipping notification for %i" %
                        (le.user, le.object_id, le.pk,))
            continue
        if pref.edition_notification < UPref.NOTIFY_FOR_CURATOR and \
                is_curator(le.user):
            logger.info("%s is a curator, skipping notification for %i" %
                        (le.user, le.object_id))
            continue

        owner_le = all_le.setdefault(data_source.owner, dict())
        owner_le.setdefault(le.object_id, list()).append(le)

    # For each owner that accept some kind of notification
    for owner, owner_le in all_le.items():
        content_array = [
            gettext("change_notification_email__body__start%(recipient_first_name)s%(recipient_last_name)s") %
            dict(
                recipient_first_name=escape(owner.first_name),
                recipient_last_name=escape(owner.last_name),
            ),
            "<ul>",
        ]
        # for each object that generate some notifications
        for object_id, les in owner_le.items():
            content_array.append("<li>")
            data_source = models.DataSource.objects.get(pk=object_id)
            content_array.append(
                # . Translators: This text is for data source that are still in the db
                gettext("change_notification_email__object_entry%(object_repr)s%(link_to_object)s") % dict(
                    object_repr=data_source.name,
                    link_to_object=settings.DEFAULT_DOMAIN +
                                   reverse("viralhostrangedb:data-source-detail", args=[data_source.pk]),
                )
            )
            content_array.append("<ul>")
            # for each log entry
            for le in les:
                # what is the role, might be wrong as we use current state of the data source, not the one at the time
                # of the change
                role = "UNKNOWN"
                if owner == le.user:
                    role = gettext("change_notification_email__role_owner")
                elif data_source.granteduser_set.filter(user=le.user, can_write=True).exists():
                    role = gettext("change_notification_email__role_editor")
                elif is_curator(le.user):
                    role = gettext("change_notification_email__role_curator")
                    # what was done, use custom version in place of le.get_change_message
                msg = get_change_message_with_action(le)
                # Add an entry in the email
                content_array.append(
                    gettext("change_notification_email__le_entry%(action_time)s%(role)s%(user)s%(change)s") % dict(
                        action_time=le.action_time.strftime('%Y-%m-%d %H:%M:%S'),
                        user=le.user.last_name.upper() + " " + le.user.first_name.title(),
                        change=msg[0].lower() + msg[1:],
                        role=role,
                    )
                )
            content_array.append("</ul>")
            content_array.append("</li>")
        content_array.append("</ul>")
        content_array.append(
            gettext("change_notification_email__body__end%(settings_url)s") %
            dict(settings_url=
                 settings.DEFAULT_DOMAIN + reverse("basetheme_bootstrap:account") + "#tab-Applicationsettings",
                 )
        )

        # send the email
        body = "".join(content_array)
        email = EmailMultiAlternatives(
            subject=gettext("change_notification_email__subject"),
            body=strip_tags(body),
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[
                owner.email,
            ],

        )
        email.attach_alternative(body, "text/html")
        email.send()

        message_to_user(
            "Notification summary sent to %s" % (owner.last_name.upper() + " " + owner.first_name.title()),
            level=messages.INFO,
            modeladmin=modeladmin, request=request,
        )

    if len(notify_to_curator) > 0:
        content_array = [
            gettext("notify_visibility_to_curator_email__body__start"),
            "<ul>",
        ]
        for data_source in notify_to_curator:
            content_array.append("<li>")
            content_array.append(
                # . Translators: This text is for data source that are still in the db
                gettext("change_notification_email__object_entry%(object_repr)s%(link_to_object)s") % dict(
                    object_repr=data_source.name,
                    link_to_object=settings.DEFAULT_DOMAIN +
                                   reverse("viralhostrangedb:data-source-detail", args=[data_source.pk]),
                )
            )
            content_array.append("</li>")
        content_array.append("</ul>")
        content_array.append(gettext("notify_visibility_to_curator_email__body__end"))
        body = "".join(content_array)
        email = EmailMultiAlternatives(
            subject=gettext("notify_visibility_to_curator_email__subject"),
            body=strip_tags(body),
            from_email=settings.DEFAULT_FROM_EMAIL,
            bcc=[curator.email for curator in get_curators()],
        )
        email.attach_alternative(body, "text/html")
        logger.info("notifying curators " + (", ".join(str(curator) for curator in get_curators())))
        email.send()
