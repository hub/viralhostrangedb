from django.contrib.auth.models import AnonymousUser
from django.test import RequestFactory

from viralhostrangedb import mixins, models, business_process
from viralhostrangedb.tests import test_models_methods


class DataSourceDownloadTestCase(test_models_methods.ModelsTestCaseMixin):

    # def test_visibility_only_public_or_owned_or_staff_queryset_filter(self):
    #     my_set = set([o.pk for o in mixins.only_public_or_owned_or_staff_queryset_filter(
    #         self=None,
    #         request=None,
    #         queryset=models.DataSource.objects.all(),
    #         user=self.toto
    #     )])
    #     self.assertNotIn(self.private_data_source_of_user.pk, my_set)
    #     self.assertIn(self.public_data_source_of_user.pk, my_set)
    #     self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
    #     self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    # def test_visibility_only_public_or_owned_or_staff_queryset_filter_req(self):
    #     request = RequestFactory().get('/blabla')
    #     request.user = AnonymousUser()
    #     my_set = set([o.pk for o in mixins.only_public_or_owned_or_staff_queryset_filter(
    #         self=None,
    #         request=request,
    #         queryset=models.DataSource.objects.all(),
    #         user=None
    #     )])
    #     self.assertNotIn(self.private_data_source_of_user.pk, my_set)
    #     self.assertIn(self.public_data_source_of_user.pk, my_set)
    #     self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
    #     self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    # def test_visibility_only_public_or_owned_or_staff_queryset_filter_admin_user(self):
    #     self.toto.is_staff = True
    #     self.toto.save()
    #     my_set = set([o.pk for o in mixins.only_public_or_owned_or_staff_queryset_filter(
    #         self=None,
    #         request=None,
    #         queryset=models.DataSource.objects.all(),
    #         user=self.toto
    #     )])
    #     self.assertIn(self.private_data_source_of_user.pk, my_set)
    #     self.assertIn(self.public_data_source_of_user.pk, my_set)
    #     self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
    #     self.assertIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    # def test_unicity_ok_only_public_or_owned_or_staff_queryset_filter(self):
    #     my_set = set()
    #     for ds in mixins.only_public_or_owned_or_staff_queryset_filter(
    #             self=None,
    #             request=None,
    #             queryset=models.DataSource.objects.all(),
    #             user=self.titi
    #     ):
    #         self.assertNotIn(ds.pk, my_set)
    #         my_set.add(ds.pk)

    # def test_unicity_ok_only_public_or_owned_or_staff_queryset_filter_2(self):
    #     my_set = set()
    #     for ds in mixins.only_public_or_owned_or_staff_queryset_filter(
    #             self=None,
    #             request=None,
    #             queryset=models.DataSource.objects.all(),
    #             user=self.toto
    #     ):
    #         self.assertNotIn(ds.pk, my_set)
    #         my_set.add(ds.pk)

    def test_visibility_only_public_or_granted_or_owned_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_public_or_granted_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_unicity_ok_only_public_or_granted_or_owned_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_public_or_granted_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_public_or_granted_or_owned_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_public_or_granted_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.toto
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_visibility_only_owned_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_visibility_only_owned_queryset_filter_from_virus(self):
        my_set = set([o.pk for o in mixins.only_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.toto,
            path_to_data_source="data_source__",
        )])
        self.assertSetEqual(set(), set(models.Virus.objects.filter(data_source__in=[
            self.private_data_source_of_user,
            self.public_data_source_of_user,
            self.public_data_source_of_user_mapped,
            self.private_data_source_but_granted_of_titi,
        ]).values_list('pk', flat=True)).intersection(my_set))

    def test_unicity_ok_only_owned_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_owned_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.user
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_visibility_only_public_or_owned_queryset_filter(self):
        my_set = set([o.pk for o in mixins.only_public_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_visibility_only_public_or_owned_queryset_filter_req(self):
        request = RequestFactory().get('/blabla')
        request.user = AnonymousUser()
        my_set = set([o.pk for o in mixins.only_public_or_owned_queryset_filter(
            self=None,
            request=request,
            queryset=models.DataSource.objects.all(),
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)
        my_set = set([o.pk for o in mixins.only_public_or_owned_queryset_filter(
            self=None,
            request=None,
            user=None,
            queryset=models.DataSource.objects.all(),
        )])
        self.assertNotIn(self.private_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.private_data_source_but_granted_of_titi.pk, my_set)

    def test_unicity_ok_only_public_or_owned_queryset_filter(self):
        my_set = set()
        for ds in mixins.only_public_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.titi
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_unicity_ok_only_public_or_owned_queryset_filter_2(self):
        my_set = set()
        for ds in mixins.only_public_or_owned_queryset_filter(
                self=None,
                request=None,
                queryset=models.DataSource.objects.all(),
                user=self.toto
        ):
            self.assertNotIn(ds.pk, my_set)
            my_set.add(ds.pk)

    def test_visibility_only_editor_or_owned_queryset_filter(self):

        class User_to_request:
            def __init__(self, user):
                self.request = RequestFactory().get('/blabla')
                self.request.user = user

        class QS_DataSource(User_to_request):

            def get_queryset(self):
                return models.DataSource.objects.all()

        class QS_Virus(User_to_request):

            def get_queryset(self):
                return models.Virus.objects.all()

        class MockOnlyEditorOrOwnedMixin(mixins.OnlyEditorOrOwnedMixin, QS_DataSource):
            pass

        class MockOnlyEditorOrOwnedRelatedMixin(mixins.OnlyEditorOrOwnedRelatedMixin, QS_Virus):
            pass

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.user
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedMixin(self.user).get_queryset()]))
        self.assertIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedMixin(self.toto).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with allowed")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set, "Public are not to be found here")
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "Public are not to be found here")

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.tata_is_staff
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedMixin(self.tata_is_staff).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with is_staff")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "Public are not to be found here")

        # test the mixin with AnonymousUser

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=AnonymousUser()
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedMixin(AnonymousUser()).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with AnonymousUser()")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set, "err with AnonymousUser()")
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "err with AnonymousUser()")

        # test the mixin from Virus model

        virus_not_shared_pk = self.no_virus_host_shared.virus_set.first().pk

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.user,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.user).get_queryset()]))
        self.assertNotIn(virus_not_shared_pk, my_set)

        self.no_virus_host_shared.allowed_users.add(self.user)

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.user,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.user).get_queryset()]))
        self.assertNotIn(virus_not_shared_pk, my_set)

        models.GrantedUser.objects.update_or_create(
            data_source=self.no_virus_host_shared,
            user=self.user,
            defaults=dict(can_write=True)
        )

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.user,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.user).get_queryset()]))
        self.assertIn(virus_not_shared_pk, my_set)

        business_process.set_curator(self.toto, False)

        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedMixin(self.toto).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with allowed")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set, "Public are not to be found here")
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "Public are not to be found here")

        self.no_virus_host_shared.public = True
        self.no_virus_host_shared.owner = self.user
        self.no_virus_host_shared.save()
        my_set = set([o.pk for o in mixins.only_editor_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.toto,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.toto).get_queryset()]))
        self.assertNotIn(virus_not_shared_pk, my_set)

    def test_visibility_only_editor_or_curator_or_owned_queryset_filter(self):

        class User_to_request:
            def __init__(self, user):
                self.request = RequestFactory().get('/blabla')
                self.request.user = user

        class QS_DataSource(User_to_request):

            def get_queryset(self):
                return models.DataSource.objects.all()

        class QS_Virus(User_to_request):

            def get_queryset(self):
                return models.Virus.objects.all()

        class MockOnlyEditorOrCuratorOrOwnedMixin(mixins.OnlyEditorOrCuratorOrOwnedMixin, QS_DataSource):
            pass

        class MockOnlyEditorOrOwnedRelatedMixin(mixins.OnlyEditorOrCuratorOrOwnedRelatedMixin, QS_Virus):
            pass

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.user
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrCuratorOrOwnedMixin(self.user).get_queryset()]))
        self.assertIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set)
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertIn(self.public_data_source_of_user.pk, my_set)

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrCuratorOrOwnedMixin(self.toto).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with allowed")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set, "Public are not to be found here")
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "Public are not to be found here")

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.tata_is_staff
        )])
        self.assertSetEqual(my_set,
                            set([o.pk for o in MockOnlyEditorOrCuratorOrOwnedMixin(self.tata_is_staff).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with is_staff")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set)
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "Public are not to be found here")

        # test the mixin with AnonymousUser

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=AnonymousUser()
        )])
        self.assertSetEqual(my_set,
                            set([o.pk for o in MockOnlyEditorOrCuratorOrOwnedMixin(AnonymousUser()).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with AnonymousUser()")
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, my_set, "err with AnonymousUser()")
        self.assertNotIn(self.public_data_source_of_user.pk, my_set, "err with AnonymousUser()")

        # test the mixin from Virus model

        virus_not_shared_pk = self.no_virus_host_shared.virus_set.first().pk

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.user,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.user).get_queryset()]))
        self.assertNotIn(virus_not_shared_pk, my_set)

        self.no_virus_host_shared.allowed_users.add(self.user)

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.user,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.user).get_queryset()]))
        self.assertNotIn(virus_not_shared_pk, my_set)

        models.GrantedUser.objects.update_or_create(
            data_source=self.no_virus_host_shared,
            user=self.user,
            defaults=dict(can_write=True)
        )

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.user,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.user).get_queryset()]))
        self.assertIn(virus_not_shared_pk, my_set)

        business_process.set_curator(self.toto, True)

        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.DataSource.objects.all(),
            user=self.toto
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrCuratorOrOwnedMixin(self.toto).get_queryset()]))
        self.assertNotIn(self.private_data_source_but_write_read_granted_of_titi.pk, my_set, "err with allowed")
        self.assertIn(self.public_data_source_of_user_mapped.pk, my_set, "Public are not to be found here")
        self.assertIn(self.public_data_source_of_user.pk, my_set, "Public are not to be found here")

        self.no_virus_host_shared.public = True
        self.no_virus_host_shared.owner = self.user
        self.no_virus_host_shared.save()
        my_set = set([o.pk for o in mixins.only_editor_or_curator_or_owned_queryset_filter(
            self=None,
            request=None,
            queryset=models.Virus.objects.all(),
            user=self.toto,
            path_to_data_source="data_source__"
        )])
        self.assertSetEqual(my_set, set([o.pk for o in MockOnlyEditorOrOwnedRelatedMixin(self.toto).get_queryset()]))
        self.assertIn(virus_not_shared_pk, my_set)
