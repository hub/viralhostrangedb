from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.test import override_settings
from django.utils import timezone

from viralhostrangedb import models, business_process
from viralhostrangedb.templatetags.viralhostrange_tags import can_edit, can_see, can_delete, is_editor_or_owner_of_ds
from viralhostrangedb.tests.test_views_others import TooledTestCase


class TemplateTagsTests(TooledTestCase):
    def setUp(self) -> None:
        super().setUp()
        self.user = get_user_model().objects.create(
            username="user",
            email="b@a.a",
        )
        self.toto = get_user_model().objects.create(
            username="toto",
            email="toto@a.a",
        )
        self.titi = get_user_model().objects.create(
            username="titi",
            email="titi@a.a",
        )
        self.d_user = models.DataSource.objects.create(
            owner=self.user,
            name="d_user",
            raw_name="d_user",
            creation_date=timezone.now(),
        )
        self.d_toto = models.DataSource.objects.create(
            owner=self.toto,
            name="d_toto",
            raw_name="d_toto",
            creation_date=timezone.now(),
        )
        self.d_toto_grant_titi = models.DataSource.objects.create(
            owner=self.toto,
            name="d_toto_grant_titi",
            raw_name="d_toto_grant_titi",
            creation_date=timezone.now(),
        )
        self.d_toto_grant_titi.allowed_users.add(self.titi, through_defaults=dict(can_write=True))
        self.d_toto_grant_titi.allowed_users.add(self.user, through_defaults=dict(can_write=False))

    @override_settings(DEBUG=True)
    def test_can_edit_wrong_param_debug(self):
        self.assertRaises(Exception, can_edit, self.user, self.user)

        self.assertFalse(can_edit(AnonymousUser(), self.d_toto_grant_titi))

        # not allowed to curator
        business_process.set_curator(self.user, True)
        self.assertFalse(can_edit(self.user, self.d_toto))
        # allowed to curator
        self.d_toto.public = True
        self.d_toto.save()
        business_process.set_curator(self.user, True)
        self.assertTrue(can_edit(self.user, self.d_toto))

    @override_settings(DEBUG=False)
    def test_can_edit_wrong_param(self):
        self.assertFalse(can_edit(self.user, self.user))

    @override_settings(DEBUG=False)
    def test_can_edit(self):
        self.assertFalse(can_edit(self.user, self.d_toto))
        self.assertTrue(can_edit(self.toto, self.d_toto))

        self.assertFalse(can_edit(self.toto, self.d_user))
        self.assertTrue(can_edit(self.user, self.d_user))

        self.assertTrue(can_edit(self.toto, self.d_toto_grant_titi))
        self.assertTrue(can_edit(self.titi, self.d_toto_grant_titi))
        self.assertFalse(can_edit(self.user, self.d_toto_grant_titi))

        self.assertFalse(can_edit(AnonymousUser(), self.d_toto_grant_titi))

        # not allowed to curator
        business_process.set_curator(self.user, True)
        self.assertFalse(can_edit(self.user, self.d_toto))
        # allowed to curator
        self.d_toto.public = True
        self.d_toto.save()
        business_process.set_curator(self.user, True)
        self.assertTrue(can_edit(self.user, self.d_toto))

    @override_settings(DEBUG=False)
    def test_is_editor_or_owner_of_ds(self):
        self.assertFalse(is_editor_or_owner_of_ds(self.user, self.d_toto))
        self.assertTrue(is_editor_or_owner_of_ds(self.toto, self.d_toto))

        self.assertTrue(is_editor_or_owner_of_ds(self.titi, self.d_toto_grant_titi))
        business_process.set_curator(self.titi, True)
        self.assertTrue(is_editor_or_owner_of_ds(self.titi, self.d_toto_grant_titi), "should still be allowed")

        self.assertFalse(is_editor_or_owner_of_ds(self.user, self.d_toto_grant_titi))
        self.assertFalse(is_editor_or_owner_of_ds(self.user, self.d_toto_grant_titi.pk))
        business_process.set_curator(self.user, True)
        self.assertFalse(is_editor_or_owner_of_ds(self.user, self.d_toto_grant_titi), "still not allowed")
        self.assertFalse(is_editor_or_owner_of_ds(self.user, self.d_toto_grant_titi.pk), "still not allowed")

    @override_settings(DEBUG=False)
    def test_can_delete(self):
        self.assertFalse(can_delete(self.user, self.d_toto))
        self.assertTrue(can_delete(self.toto, self.d_toto))

        self.assertFalse(can_delete(self.toto, self.d_user))
        self.assertTrue(can_delete(self.user, self.d_user))

        self.assertTrue(can_delete(self.toto, self.d_toto_grant_titi), "Only owner can delete")
        self.assertFalse(can_delete(self.titi, self.d_toto_grant_titi))
        self.assertFalse(can_delete(self.user, self.d_toto_grant_titi))

        self.assertFalse(can_delete(AnonymousUser(), self.d_toto_grant_titi))

        # not allowed to curator
        business_process.set_curator(self.user, True)
        self.assertFalse(can_edit(self.user, self.d_toto))
        # allowed to curator
        self.d_toto.public = True
        self.d_toto.save()
        business_process.set_curator(self.user, True)
        self.assertTrue(can_edit(self.user, self.d_toto))

    @override_settings(DEBUG=True)
    def test_can_see_wrong_param_debug(self):
        self.assertRaises(Exception, can_see, self.user, self.user)
        self.assertRaises(NotImplementedError, can_see, self.user, get_user_model().objects.all())

        self.assertFalse(can_see(AnonymousUser(), self.d_toto_grant_titi))

    @override_settings(DEBUG=False)
    def test_can_see_wrong_param(self):
        self.assertFalse(can_see(self.user, self.user))

        self.assertFalse(can_see(AnonymousUser(), self.d_toto_grant_titi))

    @override_settings(DEBUG=True)
    def test_can_delete_wrong_param_debug(self):
        self.assertRaises(Exception, can_delete, self.user, self.user)
        self.assertRaises(Exception, can_delete, self.user, get_user_model().objects.all())

        self.assertFalse(can_delete(AnonymousUser(), self.d_toto_grant_titi))

        # not allowed to curator
        business_process.set_curator(self.user, True)
        self.assertFalse(can_edit(self.user, self.d_toto))
        # allowed to curator
        self.d_toto.public = True
        self.d_toto.save()
        business_process.set_curator(self.user, True)
        self.assertTrue(can_edit(self.user, self.d_toto))

    @override_settings(DEBUG=False)
    def test_can_delete_wrong_param(self):
        self.assertFalse(can_delete(self.user, self.user))

    @override_settings(DEBUG=False)
    def test_can_see(self):
        self.assertFalse(can_see(self.user, self.d_toto))
        self.assertFalse(can_see(self.toto, self.d_user))
        self.assertTrue(can_see(self.toto, self.d_toto))
        self.assertTrue(can_see(self.user, self.d_user))
