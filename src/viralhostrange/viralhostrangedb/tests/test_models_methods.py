import logging
import os

from django.contrib.auth import get_user_model
from django.urls import reverse

from viralhostrangedb import models, business_process
from viralhostrangedb.tests.test_views_others import TooledTestCase

logger = logging.getLogger(__name__)


class ModelsTestCaseMixin(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        self.user_pwd = "eil2guj4cuSho2Vai3hu"

        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            email="b@a.a",
            is_staff=True,
        )
        self.user.set_password(self.user_pwd)
        self.user.save()

        ################################################################################
        self.toto = get_user_model().objects.create(
            username="toto",
            email="a@a.a",
        )
        self.toto.set_password(self.user_pwd)
        self.toto.save()

        ################################################################################
        self.titi = get_user_model().objects.create(
            username="titi",
            email="i@i.i",
        )
        self.titi.set_password(self.user_pwd)
        self.titi.save()

        ################################################################################
        self.tata_is_staff = get_user_model().objects.create(
            username="tata",
            email="a@a.a",
            is_staff=True,
        )
        self.tata_is_staff.set_password(self.user_pwd)
        self.tata_is_staff.save()

        ################################################################################
        self.private_data_source_of_user = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_of_user,
            file=filename,
        )

        ################################################################################
        self.private_data_source_but_granted_of_titi = models.DataSource.objects.create(
            owner=self.titi,
            public=False,
            name="private titi",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_but_granted_of_titi,
            file=filename,
        )
        self.private_data_source_but_granted_of_titi.allowed_users.add(self.toto)
        self.private_data_source_but_granted_of_titi.allowed_users.add(self.user)
        self.private_data_source_but_granted_of_titi.save()

        ################################################################################
        self.private_data_source_but_write_read_granted_of_titi = models.DataSource.objects.create(
            owner=self.titi,
            public=False,
            name="private titi",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.private_data_source_but_granted_of_titi,
            file=filename,
        )
        self.private_data_source_but_write_read_granted_of_titi.allowed_users.add(self.toto)
        self.private_data_source_but_write_read_granted_of_titi.allowed_users \
            .add(self.user, through_defaults=dict(can_write=True), )
        self.private_data_source_but_write_read_granted_of_titi.save()

        ################################################################################
        self.public_data_source_of_user = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="public user",
            raw_name="ee",
            kind="FILE",
        )
        self.public_data_source_of_user.allowed_users.add(self.user)
        self.public_data_source_of_user.save()
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        business_process.import_file(
            data_source=self.public_data_source_of_user,
            file=filename,
        )

        ################################################################################
        self.public_data_source_of_user_mapped = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="public user mapped",
            raw_name="ee",
            kind="FILE",
        )
        self.public_data_source_of_user_mapped.allowed_users.add(self.user)
        self.public_data_source_of_user_mapped.save()
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        business_process.import_file(
            data_source=self.public_data_source_of_user_mapped,
            file=filename,
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit',
                      args=[self.public_data_source_of_user_mapped.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.public_data_source_of_user_mapped.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": 3,
            "form-1-raw_response": 1.0,
            "form-1-mapping": 3,
            "form-2-raw_response": 2.0,
            "form-2-mapping": 2
        }

        self.client.post(url, form_data)
        self.client.logout()

        ################################################################################
        self.no_virus_host_shared = models.DataSource.objects.create(
            owner=self.toto,
            public=False,
            name="private toto no_virus_or_host_shared",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "rxCx.xlsx")
        business_process.import_file(
            data_source=self.no_virus_host_shared,
            file=filename,
        )

        ################################################################################


class ModelsTestCase(ModelsTestCaseMixin, TooledTestCase):

    def test_all(self):
        str(models.DataSource.objects.all())
        str(models.Virus.objects.all())
        str(models.Host.objects.all())
        str(models.GlobalViralHostResponseValue.objects.all())
        str(models.ViralHostResponseValueInDataSource.objects.all())

        pks = set(models.DataSource.get_not_mapped_data_sources_pk(owner=self.user))
        self.assertIn(self.private_data_source_of_user.pk, pks)
        self.assertIn(self.public_data_source_of_user.pk, pks)
        self.assertNotIn(self.public_data_source_of_user_mapped.pk, pks)
        for ds in models.DataSource.get_not_mapped_data_sources(owner=self.user):
            pks.remove(ds.pk)
        self.assertEqual(len(pks), 0)
