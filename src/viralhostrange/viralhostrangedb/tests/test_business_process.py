import json
import logging
import os
from tempfile import NamedTemporaryFile
import time

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.db.models import Q
from django.utils import timezone

from viralhostrangedb import business_process, models
from viralhostrangedb.business_process import extract_name_and_identifiers, ImportationObserver, DataSourceAlteredData
from viralhostrangedb.tests.test_views_others import ViewTestCase, TooledTestCase

logger = logging.getLogger(__name__)

class ListErrorImportationObserver(ImportationObserver):
    _errors=None

    def __init__(self):
        self.reset()

    def reset(self):
        self._errors = dict()

    @property
    def errors(self):
        return list(self._errors.values())

    @staticmethod
    def reason_id_to_str(reason):
        if reason == ImportationObserver.DUPLICATED:
            return "DUPL"
        if reason == ImportationObserver.EMPTY_NAME:
            return "EMPTY"
        return str(reason)

    def append_error(self, error_dict):
        self._errors[str(error_dict)]=error_dict

    def notify_response_error(self, virus, host, response_str):
        self.append_error(dict(virus=virus, host=host, response_str=response_str))

    def notify_host_error(self, host, column_id, reason=None, reason_id=None):
        self.append_error(
            dict(host=host, column_id=column_id, reason=reason, reason_id=self.reason_id_to_str(reason_id)))

    def notify_virus_error(self, virus, row_id, reason=None, reason_id=None):
        self.append_error(dict(virus=virus, row_id=row_id, reason=reason, reason_id=self.reason_id_to_str(reason_id)))


class DataFileReaderTestCase(TooledTestCase):
    test_data = "./test_data"
    example_public_data = "./viralhostrangedb/static/media/"

    def setUp(self):
        super().setUp()
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_get_media_root(self):
        user = get_user_model().objects.create(
            username="user",
        )
        paths = set()
        users = [self.owner, user, AnonymousUser()]
        for u in users:
            paths.add(business_process.get_user_media_root(u))
        self.assertEqual(len(paths), len(users))
        paths.add(business_process.get_user_media_root(None))
        self.assertEqual(len(paths), len(users))

    def listdir(self):
        for f in os.listdir(self.test_data):
            if f.endswith(".xlsx") and not f.endswith(".err.xlsx"):
                yield f
            if f.endswith(".ods") and not f.endswith(".err.ods"):
                yield f
            if f.endswith(".csv") and not f.endswith(".err.csv"):
                yield f

    def test_read_work_fine(self):
        obs = ListErrorImportationObserver()
        for f in self.listdir():
            obs.reset()
            vhrs = business_process.parse_file(os.path.join(self.test_data, f), importation_observer=obs)
            vhrs_dict = business_process.to_dict(vhrs)
            candidate = os.path.join(self.test_data, f + ".json.candidate")
            with open(candidate, "w") as f_candidate:
                f_candidate.write(json.dumps(vhrs_dict, indent=2))
            error_candidate = os.path.join(self.test_data, f + ".errors.json.candidate")
            with open(error_candidate, "w") as f_error_candidate:
                f_error_candidate.write(json.dumps(obs.errors, indent=2))
            try:
                with open(os.path.join(self.test_data, f + ".errors.json")) as stream:
                    expected_parse_errors = json.load(stream)
            except FileNotFoundError as e:
                expected_parse_errors = []
            try:
                with open(os.path.join(self.test_data, f + ".json")) as stream:
                    data = json.load(stream)
                    self.assertEqual(data, vhrs_dict, "with file %s" % f)
                # self.assertEqual(obs.count, 0, "with file %s" % f)
                # if len(obs.errors) > 0:
                #     logger.error(f'{str(f)};{len(obs.errors)};{str(obs.errors)}')
                self.assertEqual(obs.errors, expected_parse_errors, "with file %s" % f)
                os.remove(candidate)
                os.remove(error_candidate)
            except FileNotFoundError as e:
                if os.path.exists(os.path.join(self.test_data, f + ".json.not_needed")):
                    os.remove(candidate)
                    os.remove(error_candidate)
                    continue
                logger.exception("json associated file is missing, reducing robustness of tests. "
                                 "We created it, check if it is correct")
                logger.exception(json.dumps(vhrs_dict, indent=2))
                open(os.path.join(self.test_data, f + ".json.not_needed.candidate"), "w")

    def test_read_work_with_stream(self):
        with open(os.path.join(self.test_data, "simple.xlsx"), "rb") as input_file:
            business_process.parse_file(input_file)

    def test_examples_works(self):
        for f in [f for f in os.listdir(self.example_public_data) if
                  f.endswith(".xlsx") and not f.endswith(".err.xlsx")]:
            with open(os.path.join(self.example_public_data, f), "rb") as input_file:
                business_process.parse_file(input_file)

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "simple.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.owner, self.owner)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C4"),
            virus=models.Virus.objects.get(data_source=data_source, name="r1"),
        ).raw_response, 4)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C2"),
            virus=models.Virus.objects.get(data_source=data_source, name="r2"),
        ).raw_response, 6)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C3"),
            virus=models.Virus.objects.get(data_source=data_source, name="r3"),
        ).raw_response, 11)
        self.assertEqual(models.ViralHostResponseValueInDataSource.objects.get(
            data_source=data_source,
            host=models.Host.objects.get(data_source=data_source, name="C1"),
            virus=models.Virus.objects.get(data_source=data_source, name="r1"),
        ).raw_response, 1)

    def test_import_file_remove_deleted_virus(self):
        models.Virus.objects.all().delete()
        models.Host.objects.all().delete()
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4)

        filename = os.path.join(self.test_data, "empty_row.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3 - 1)
        self.assertEqual(data_source.host_set.count(), 4)
        self.assertEqual(models.Virus.objects.count(), 3 - 1)
        self.assertEqual(models.Host.objects.count(), 4)

    def test_import_file_remove_deleted_host(self):
        models.Virus.objects.all().delete()
        models.Host.objects.all().delete()
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4)

        filename = os.path.join(self.test_data, "empty_col.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4 - 1)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4 - 1)

    def test_import_file_remove_deleted_response_but_do_not_delete_host_or_virus(self):
        # shift pk for hosts (easier to debug)
        models.Host.objects.create(name="won't last", pk=100).delete()
        # clean up the db
        models.ViralHostResponseValueInDataSource.objects.all().delete()
        models.Virus.objects.all().delete()
        models.Host.objects.all().delete()

        # create a first file, so every Virus/Host is used for sure
        filename = os.path.join(self.test_data, "simple.xlsx")
        stable, _ = models.DataSource.objects.get_or_create(
            raw_name="stable",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=stable,
            file=filename,
        )

        # create the fluctuating object
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        # check everything is as expected
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4)

        # re-import without a column
        filename = os.path.join(self.test_data, "empty_col.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        # one column is missing, but the rest remains
        self.assertEqual(data_source.virus_set.count(), 3)
        self.assertEqual(data_source.host_set.count(), 4 - 1)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4)

        # re-import without a row
        filename = os.path.join(self.test_data, "empty_row.xlsx")
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        # on row is missing, but the rest remains
        self.assertEqual(data_source.virus_set.count(), 3 - 1)
        self.assertEqual(data_source.host_set.count(), 4)
        self.assertEqual(models.Virus.objects.count(), 3)
        self.assertEqual(models.Host.objects.count(), 4)

    def test_import_in_db_failure(self):
        filename = os.path.join(self.test_data, "fails.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        self.assertRaises(ValueError, business_process.import_file, data_source=data_source, file=filename)
        business_process.import_file(data_source=data_source, file=filename, importation_observer=ImportationObserver())

    def test_import_in_db_later(self):
        filename = os.path.join(self.test_data, "duplicated_empty_invalid.xlsx")
        errors, _ = business_process.import_file_later(file=filename)
        self.assertEqual(
            [s[:s.index("]") + 1] for s in errors],
            ['[ImportErr1]', '[ImportErr3]', '[ImportErr3]', '[ImportErr2]', '[ImportErr1]', ],
        )

    def test_no_duplication_in_data(self):
        x = 5
        delimiter = ';'
        with NamedTemporaryFile(prefix="0__vhrdb__", suffix='.csv') as file:
            with open(file.name, 'w') as f:
                f.write(delimiter)
                for h in range(4 * x):
                    f.write(f'h{h}{delimiter}')
                f.write('\n')
                for v in range(x):
                    f.write(f'v{1}{delimiter}')
                    for h in range(4 * x):
                        f.write(f'{(h + v) % 3}{delimiter}')
                    f.write('\n')
            data_source = models.DataSource.objects.create(
                owner=get_user_model().objects.create(
                    username="titi",
                ),
                public=False,
                name="private user",
                raw_name="ee",
                kind="FILE",
            )
            start = time.time()
            business_process.import_file(
                data_source=data_source,
                file=file.name,
            )
            duration = time.time() - start
            self.assertLessEqual(duration, 10)
            self.assertEqual(models.ViralHostResponseValueInDataSource.objects.count(), 4 * x * 1)
            business_process.import_file(
                data_source=data_source,
                file=file.name,
            )
            self.assertEqual(models.ViralHostResponseValueInDataSource.objects.count(), 4 * x * 1)


class DataFileReaderWithIdentifierTestCase(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C1").identifier, "HG738867.1")
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C2").identifier, "HGC2738867.3")
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C3").identifier, 'blablablabla')
        self.assertEqual(models.Host.objects.get(data_source=data_source, name="C4").identifier, "tralala")
        self.assertEqual(models.Virus.objects.get(data_source=data_source, name="r1").identifier, "NC_001416")
        self.assertEqual(models.Virus.objects.get(data_source=data_source, name="r3").identifier, 'blabla')
        self.assertRaises(
            models.Virus.MultipleObjectsReturned,
            models.Virus.objects.get
            , data_source=data_source, name="r2")
        va = models.Virus.objects.get(data_source=data_source, name="r2", identifier="NC_00141454566")
        vb = models.Virus.objects.get(data_source=data_source, name="r2", identifier="NC_00141454566 variant T")
        self.assertNotEqual(va, vb)
        self.assertEqual(models.Host.objects.filter(data_source=data_source).count(), 4)
        self.assertEqual(models.Virus.objects.filter(data_source=data_source).count(), 4)

    def test_do_not_import_tax_id_but_load_it(self):
        filename = os.path.join(self.test_data, "simple-with-her-id-and-ncbi.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )
        h_c1 = models.Host.objects.get(data_source=data_source, name="C1")
        self.assertIsNone(h_c1.is_ncbi_identifier_value, "my_id")
        self.assertEqual(h_c1.identifier, "my_id")
        self.assertIsNone(h_c1.tax_id_value)
        self.assertIsNone(
            h_c1.tax_id,
            "tax_id is None as nuccore id is 'my_id'"
        )
        v_r1 = models.Virus.objects.get(data_source=data_source, name="r1")
        self.assertEqual(v_r1.identifier, "NC_001416")
        self.assertIsNone(v_r1.tax_id_value)
        self.assertEqual(
            v_r1.tax_id,
            "2681611",
            "tax_id is not None as nuccore id is valid"
        )


class AutomatedMergeOnImportTestCase(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple,
            file=filename,
        )
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple_2, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple_2,
            file=filename,
        )

        self.assertEqual(models.Virus.objects.filter(name="r1").count(), 1)
        self.assertEqual(models.Virus.objects.filter(name="r2").count(), 1)
        self.assertEqual(models.Virus.objects.filter(name="r3").count(), 1)
        self.assertEqual(models.Virus.objects.filter().count(), 3)
        self.assertEqual(models.Host.objects.filter(name="C1").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C2").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C3").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C4").count(), 1)
        self.assertEqual(models.Host.objects.filter().count(), 4)

    def test_merge_considers_identifier(self):
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple,
            file=filename,
        )
        filename = os.path.join(self.test_data, "simple-with-some-id.xlsx")
        simple_with_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_id,
            file=filename,
        )

        self.assertEqual(models.Virus.objects.filter(name="r1").count(), 2)
        self.assertEqual(models.Virus.objects.filter(name="r2").count(), 3)
        self.assertEqual(models.Virus.objects.filter(name="r3").count(), 1)
        self.assertEqual(models.Virus.objects.filter().count(), 6)
        self.assertEqual(models.Virus.objects.filter(name="r3", data_source=three_reponse_simple).count(), 1)
        self.assertEqual(models.Virus.objects.filter(name="r3", data_source=simple_with_id).count(), 1)
        self.assertEqual(models.Virus.objects.get(data_source=three_reponse_simple, name="r3").identifier, '')
        self.assertEqual(models.Virus.objects.get(data_source=simple_with_id, name="r3").identifier, '')
        self.assertEqual(models.Virus.objects.get(data_source=simple_with_id, name="r1").identifier, "NC_001416")
        self.assertEqual(models.Virus.objects.get(data_source=three_reponse_simple, name="r1").identifier, '')

        self.assertEqual(models.Host.objects.filter(name="C1").count(), 2)
        self.assertEqual(models.Host.objects.filter(name="C2").count(), 2)
        self.assertEqual(models.Host.objects.filter(name="C3").count(), 1)
        self.assertEqual(models.Host.objects.filter(name="C4").count(), 2)
        self.assertEqual(models.Host.objects.filter().count(), 7)

    def test_creation_handle_duplicate_in_db(self):
        models.Virus.objects.create(name="r1", identifier="")
        models.Virus.objects.create(name="r1", identifier="")
        filename = os.path.join(self.test_data, "three_reponse_simple.xlsx")
        three_reponse_simple, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=three_reponse_simple,
            file=filename,
        )
        self.assertEqual(models.Virus.objects.filter(name="r1").count(), 2)

    # def test_creation_fetch_identifier(self):
    #     filename = os.path.join(self.test_data, "simple-with-id.xlsx")
    #     simple_with_id, created = models.DataSource.objects.get_or_create(
    #         raw_name=filename,
    #         kind="FILE",
    #         owner=self.owner,
    #         creation_date=timezone.now(),
    #         last_edition_date=timezone.now(),
    #     )
    #     business_process.import_file(
    #         data_source=simple_with_id,
    #         file=filename,
    #     )
    #     r1 = models.Virus.objects.get(name='r1')
    #     r1.her_identifier = 2
    #     r1.save()
    #     self.assertEqual(1, models.Virus.objects.filter(name='r3').count())
    #     models.Virus.objects.filter(name='r3').update(her_identifier=3)
    #     filename = os.path.join(self.test_data, "simple-with-her-id.xlsx")
    #     simple_with_her_id, created = models.DataSource.objects.get_or_create(
    #         raw_name=filename,
    #         kind="FILE",
    #         owner=self.owner,
    #         creation_date=timezone.now(),
    #         last_edition_date=timezone.now(),
    #     )
    #     business_process.import_file(
    #         data_source=simple_with_her_id,
    #         file=filename,
    #     )
    #     self.assertEqual(simple_with_her_id.virus_set.get(her_identifier=2).identifier, r1.identifier)
    #     self.assertEqual(2, models.Virus.objects.filter(name='r3').count())

    def test_creation_do_not_overlap_with_more_complete_instance(self):
        filename = os.path.join(self.test_data, "simple-with-her-id-and-ncbi.xlsx")
        simple_with_both_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_both_id,
            file=filename,
        )
        filename = os.path.join(self.test_data, "simple-with-her-id.xlsx")
        simple_with_her_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_her_id,
            file=filename,
        )
        self.assertEqual(2, models.Virus.objects.filter(name='r3').count())
        self.assertEqual(2, models.Virus.objects.filter(name='r2').count())
        self.assertEqual(2, models.Virus.objects.filter(name='r1').count())
        self.assertEqual(2, models.Virus.objects.filter(name='r4').count())

    def test_creation_do_not_overlap_with_less_complete_instance(self):
        filename = os.path.join(self.test_data, "simple-with-her-id.xlsx")
        simple_with_her_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_her_id,
            file=filename,
        )
        filename = os.path.join(self.test_data, "simple-with-her-id-and-ncbi.xlsx")
        simple_with_both_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_both_id,
            file=filename,
        )
        self.assertEqual(2, models.Virus.objects.filter(name='r3').count())
        self.assertEqual(2, models.Virus.objects.filter(name='r2').count())
        self.assertEqual(2, models.Virus.objects.filter(name='r1').count())
        self.assertEqual(2, models.Virus.objects.filter(name='r4').count())

    def test_creation_do_not_ignore_her_id(self):
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        simple_with_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_id,
            file=filename,
        )
        for v in simple_with_id.virus_set.all():
            v.her_identifier = v.id
            v.save()
        ids_of_simple_with_id = set(simple_with_id.virus_set.values_list('id', flat=True))
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        simple_with_id_2, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_id_2,
            file=filename,
        )
        ids_of_simple_with_id_2 = set(simple_with_id_2.virus_set.values_list('id', flat=True))
        self.assertTrue(ids_of_simple_with_id_2.isdisjoint(ids_of_simple_with_id))


class ResetMappingTestCase(ViewTestCase):
    def test_reset_mapping_ok(self):
        self.assertTrue(self.four_reponse_simple.is_mapping_done)
        business_process.reset_mapping(models.DataSource.objects.filter(pk=self.four_reponse_simple.pk))
        self.assertFalse(self.four_reponse_simple.is_mapping_done)
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple) \
                         .filter(~Q(response=self.not_mapped_yet)).exists())


class TestOthers(TooledTestCase):
    test_data = "./test_data"

    def test_to_stat(self):
        filename = os.path.join(self.test_data, "simple.xlsx")

        data_source, created = models.DataSource.objects.get_or_create(
            raw_name="blabla",
            kind="FILE",
            owner=get_user_model().objects.create(username="user", ),
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
        )

        self.assertEqual(
            business_process.to_stat(business_process.parse_file(filename)),
            dict(
                hosts=set(o.explicit_name for o in data_source.host_set.all()),
                viruses=set(o.explicit_name for o in data_source.virus_set.all()),
                response_count=data_source.responseindatasource.count(),
                responses=set(str(int(r.raw_response)) for r in data_source.responseindatasource.all()),
            )
        )

        pass

    def test_extract_name_and_identifiers(self):
        expected_name = "A"
        expected_identifier = "i"
        expected_her_identifier = 42
        expected_identifier_dict = business_process.default_identifiers()
        expected_identifier_dict[business_process.NCBI_IDENTIFIER] = expected_identifier
        expected_her_identifier_dict = business_process.default_identifiers()
        expected_her_identifier_dict[business_process.HER_IDENTIFIER] = expected_her_identifier
        expected_both_identifier_dict = business_process.default_identifiers()
        expected_both_identifier_dict[business_process.NCBI_IDENTIFIER] = expected_identifier
        expected_both_identifier_dict[business_process.HER_IDENTIFIER] = expected_her_identifier
        expected_no_identifier_dict = business_process.default_identifiers()
        for p in [
            "{} ({})",
            "{}({})",
            "   {} ({})",
            "   {}({})",
            "   {}({})   ",
            "   {}({})   ",
            "{} (  {})",
            "{}(  {})",
            "   {} (  {})",
            "   {}(  {})",
            "   {}(  {})   ",
            "   {}(  {})   ",
            "{} ({}  )",
            "{}({}  )",
            "   {} ({}  )",
            "   {}({}  )",
            "   {}({}  )   ",
            "   {}({}  )   ",
        ]:
            name, identifiers = extract_name_and_identifiers(p.format(expected_name, expected_identifier))
            self.assertEqual(name, expected_name)
            self.assertDictEqual(identifiers, expected_identifier_dict)
            name, identifiers = extract_name_and_identifiers(
                p.format(expected_name, "her:%i" % expected_her_identifier))
            self.assertEqual(name, expected_name)
            self.assertDictEqual(identifiers, expected_her_identifier_dict)
        for p in [
            "{}",
            "{}    ",
            "    {}    ",
            "    {}",
        ]:
            name, identifiers = extract_name_and_identifiers(p.format(expected_name))
            self.assertEqual(name, expected_name)
            self.assertDictEqual(identifiers, expected_no_identifier_dict)
        for p in [
            "({})",
            "({})    ",
            "    ({})    ",
            "    ({})",
        ]:
            name, identifiers = extract_name_and_identifiers(p.format(expected_identifier))
            self.assertEqual(name, "")
            self.assertDictEqual(identifiers, expected_identifier_dict)
        for p in [
            "%(name)s (%(id)s;her:%(her)s)",
            "%(name)s (her:%(her)s;%(id)s)",
            "%(name)s (her:%(her)s;%(id)s;)",
            "%(name)s (;her:%(her)s;%(id)s;)",
            "%(name)s (;%(id)s;her:%(her)s;)",

            "%(name)s (%(id)s;  her:%(her)s)",
            "%(name)s (  her:%(her)s  ;%(id)s)",
            "%(name)s (her:%(her)s  ;  %(id)s;  )",
            "%(name)s (;her: %(her)s;  %(id)s;)",
            "%(name)s (;%(id)s; her: %(her)s;)",
        ]:
            name, identifiers = extract_name_and_identifiers(p % dict(
                name=expected_name,
                id=expected_identifier,
                her=expected_her_identifier,
            ))
            self.assertEqual(name, expected_name)
            self.assertDictEqual(identifiers, expected_both_identifier_dict)

    def test_backup_data_source(self):
        self.assertRaises(
            AssertionError,
            business_process.backup_data_source,
            user=None,
            data_source=None,
            altered_data=DataSourceAlteredData.ALL,
        )


class TestNCBIIdentifierStatus(TooledTestCase):
    def test_biopython_ok(self):
        try:
            from Bio import Entrez
        except ImportError:
            self.assertFalse(True, "BioPython not imported")

    def test_ws(self):
        f = business_process.get_ncbi_ids
        self.assertEqual(f("NZ_JRWU00000000.1"), ('769915460', '1513468'))
        self.assertEqual(f("NC_000913.3"), ('556503834', '511145'))
        self.assertEqual(f("AE005174.2"), ('56384585', '155864'))
        self.assertEqual(f("NC_000866.4"), ('29366675', '2681598'))
        self.assertEqual(f("NC_001604.1"), ('9627425', '10760'))
        self.assertEqual(f("NC_001604-1"), ('9627425', '10760'))

        self.assertEqual(f("NC_0016041"), (None, None))
        self.assertEqual(f("APEC O25"), (None, None))
        self.assertEqual(f("tralala"), (None, None))


class TestCuratorStatus(TooledTestCase):
    def test_ok(self):
        user = get_user_model().objects.create(
            username="u_read",
            first_name="alice",
            last_name="u_readL",
            email="u_read@a.b",
        )
        toto = get_user_model().objects.create(
            username="toto",
            first_name="totoF",
            last_name="totoL",
            email="toto@a.b",
        )
        self.assertFalse(business_process.get_curators().exists())
        self.assertFalse(business_process.is_curator(user))
        business_process.set_curator(user, True)
        self.assertTrue(business_process.is_curator(user))
        business_process.set_curator(user, True)
        self.assertSetEqual(
            set(business_process.get_curators().values_list('pk', flat=True)),
            {user.pk}
        )
        business_process.set_curator(toto, True)
        self.assertSetEqual(
            set(business_process.get_curators().values_list('pk', flat=True)),
            {toto.pk, user.pk}
        )
        self.assertTrue(business_process.is_curator(user))
        business_process.set_curator(user, False)
        self.assertSetEqual(
            set(business_process.get_curators().values_list('pk', flat=True)),
            {toto.pk}
        )
        self.assertFalse(business_process.is_curator(user))
        business_process.set_curator(user, False)
        self.assertSetEqual(
            set(business_process.get_curators().values_list('pk', flat=True)),
            {toto.pk}
        )
        self.assertFalse(business_process.is_curator(user))


class TestImportBench(TooledTestCase):
    def test_it(self):
        x = 100
        delimiter = ';'
        with NamedTemporaryFile(prefix="0__vhrdb__", suffix='.csv') as file:
            with open(file.name, 'w') as f:
                f.write(delimiter)
                for h in range(4 * x):
                    f.write(f'h{h}{delimiter}')
                f.write('\n')
                for v in range(x):
                    f.write(f'v{v}{delimiter}')
                    for h in range(4 * x):
                        f.write(f'{(h + v) % 3}{delimiter}')
                    f.write('\n')
            data_source = models.DataSource.objects.create(
                owner=get_user_model().objects.create(
                    username="titi",
                ),
                public=False,
                name="private user",
                raw_name="ee",
                kind="FILE",
            )
            start = time.time()
            business_process.import_file(
                data_source=data_source,
                file=file.name,
            )
            duration = time.time() - start
            self.assertLessEqual(duration, 30, "If import start to be too long, 504 error will appear.")
            self.assertEqual(models.ViralHostResponseValueInDataSource.objects.count(), 4 * x * x)
            business_process.import_file(
                data_source=data_source,
                file=file.name,
            )
            self.assertEqual(models.ViralHostResponseValueInDataSource.objects.count(), 4 * x * x)
