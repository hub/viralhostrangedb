import errno
import logging
import time
import os
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse

from viralhostrangedb import models, business_process

logger = logging.getLogger(__name__)

class TimeMeasuredTestCase(TestCase):
    __duration = 0
    __f_duration = 0
    time_log_location = os.path.join(settings.PERSISTENT_VOLUME, 'TimeMeasuredTestCase/')

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        try:
            os.mkdir(cls.time_log_location)
        except OSError as e:
            assert e.errno == errno.EEXIST, e
        with open(f'{cls.time_log_location[:-1]}Headers.txt', 'w') as f:
            f.write(f'ClassOrFunction,Identifier,Duration\n')

        cls.__duration -= time.time()

    @classmethod
    def tearDownClass(cls) -> None:
        super().tearDownClass()
        cls.__duration += time.time()
        identifier = cls.__name__
        with open(f'{cls.time_log_location}{identifier}.txt', 'w') as f:
            f.write(f'C,{identifier},{cls.__duration}\n')

    def setUp(self) -> None:
        super().setUp()
        self.__f_duration = -time.time()

    def tearDown(self) -> None:
        super().tearDown()
        self.__f_duration += time.time()
        identifier=f'{self.__class__.__name__}.{self._testMethodName}'
        with open(f'{self.time_log_location}{identifier}.txt', 'w') as f:
            f.write(f'F,{identifier},{self.__f_duration}\n')


class TooledTestCase(TimeMeasuredTestCase):
    test_data = "./test_data"

    # __public_big_data_source_of_user_mapped = None

    def write_in_tmp_file(self, response):
        with NamedTemporaryFile(delete=False, suffix=".html") as f:
            f.write(response.content)
            print(response.content)
            print(f.name)

class OneDataSourceTestCase(TooledTestCase):
    test_data = "./test_data"

    def write_in_tmp_file(self, response):
        from tempfile import NamedTemporaryFile
        with NamedTemporaryFile(delete=False, suffix=".html") as f:
            f.write(response.content)
            print(response.content)
            print(f.name)

    def setUp(self):
        super().setUp()
        ################################################################################
        self.u_read = get_user_model().objects.create(
            username="u_read",
            first_name="alice",
            last_name="u_readL",
            email="u_read@a.b",
        )

        ################################################################################
        self.u_write = get_user_model().objects.create(
            username="u_write",
            first_name="bob",
            last_name="u_writeL",
            email="u_write@a.b",
        )

        ################################################################################
        self.u_none = get_user_model().objects.create(
            username="u_none",
            first_name="charlie",
            last_name="u_noneL",
            email="u_none@a.b",
        )

        ################################################################################
        self.superuser = get_user_model().objects.create(
            username="u_superuser",
            first_name="dave",
            last_name="u_superuserL",
            email="u_superuser@a.b",
            is_superuser=True,
        )

        ################################################################################
        self.staff_superuser = get_user_model().objects.create(
            username="u_staff_superuser",
            first_name="emy",
            last_name="staff_superuserL",
            email="staff_superuser@a.b",
            is_staff=True,
            is_superuser=True,
        )

        ################################################################################
        self.staff = get_user_model().objects.create(
            username="u_staff",
            first_name="eve",
            last_name="u_staffL",
            email="u_staff@a.b",
            is_staff=True,
        )

        ################################################################################
        self.staff_admin = get_user_model().objects.create(
            username="u_staff_admin",
            first_name="fiona",
            last_name="u_staff_adminL",
            email="u_staff_admin@a.b",
            is_staff=True,
        )
        Group.objects.get(name="Admin").user_set.add(self.staff_admin)

        ################################################################################
        self.u_curator = get_user_model().objects.create(
            username="u_curator",
            first_name="curatorF",
            last_name="curatorL",
            email="u_curator@a.b",
        )
        business_process.set_curator(self.u_curator, True)

        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            first_name="Rajesh",
            last_name="Koothrappali",
            email="user@a.b",
        )

        ################################################################################
        self.user_b = get_user_model().objects.create(
            username="user_b",
            first_name="Sheldon",
            last_name="Cooper",
            email="user_b@a.b",
        )

        ################################################################################
        self.ds = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        self.ds.allowed_users.add(self.u_read)
        self.ds.allowed_users.add(self.u_write, through_defaults=dict(can_write=True))
        self.ds.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds,
            file=filename,
        )

    def data_source_data_upload(self, *, filename, data_source, user=None):
        # upload a new version, thus create lof entry and backup
        f = open(os.path.join(self.test_data, filename), "rb")
        form_data = dict(
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        self.client.force_login(user if user else data_source.owner)
        url = reverse('viralhostrangedb:data-source-data-update', args=[data_source.pk])
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)

    def delete_data_source(self, data_source, user=None):
        url = reverse('viralhostrangedb:data-source-delete', args=[data_source.pk])
        self.client.force_login(user if user else data_source.owner)
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 302)

    def data_source_data_update(
            self,
            data_source,
            user=None,
            name=None,
            life_domain=None,
            description=None,
            public=None,
    ):
        url = reverse('viralhostrangedb:data-source-update', args=[data_source.pk])
        self.client.force_login(user if user else data_source.owner)

        response = self.client.post(
            url, dict(
                name=name if name is not None else data_source.name,
                life_domain=life_domain if life_domain is not None else data_source.life_domain,
                description=description if description is not None else data_source.description,
                public=public if public is not None else data_source.public,
                allowed_users_editor=
                "\n".join(["%s %s;%i;%s" % (gu.user.last_name, gu.user.first_name, gu.user.pk, gu.can_write)
                           for gu in data_source.granteduser_set.all()]),
            )
        )
        self.assertEqual(response.status_code, 302)
        self.ds.refresh_from_db()

    def data_source_virus_update(self, data_source, user=None, changes: dict = None):
        return self.__actual_data_source_vh_update(data_source=data_source, user=user, is_virus=True, changes=changes)

    def data_source_host_update(self, data_source, user=None, changes: dict = None):
        return self.__actual_data_source_vh_update(data_source=data_source, user=user, is_virus=False, changes=changes)

    def __actual_data_source_vh_update(self, data_source, is_virus, user=None, changes: dict = None):
        if is_virus:
            entry_set = 'virus_set'
            url = reverse('viralhostrangedb:data-source-virus-update', args=[data_source.pk])
        else:
            entry_set = 'host_set'
            url = reverse('viralhostrangedb:data-source-host-update', args=[data_source.pk])
        self.client.force_login(user if user else data_source.owner)

        count = getattr(data_source, entry_set).order_by("id").count()
        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }

        changes = dict() if changes is None else changes
        for i, v in enumerate(getattr(data_source, entry_set).order_by("id")):
            its_changes = changes.get(v.id, {})
            form_data["form-%i-name" % i] = its_changes.get("name", v.name)
            form_data["form-%i-id" % i] = v.id
            form_data["form-%i-identifier" % i] = its_changes.get("identifier", v.identifier)
            if is_virus:
                form_data["form-%i-her_identifier" % i] = its_changes.get("her_identifier", v.her_identifier or '')

        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)

    def data_source_virus_delete(self, data_source, user=None, changes: set = None):
        return self.__actual_data_source_vh_delete(data_source=data_source, user=user, is_virus=True, changes=changes)

    def data_source_host_delete(self, data_source, user=None, changes: set = None):
        return self.__actual_data_source_vh_delete(data_source=data_source, user=user, is_virus=False, changes=changes)

    def __actual_data_source_vh_delete(self, data_source, is_virus, user=None, changes: set = None):
        if is_virus:
            entry_set = 'virus_set'
            url = reverse('viralhostrangedb:data-source-virus-delete', args=[data_source.pk])
        else:
            entry_set = 'host_set'
            url = reverse('viralhostrangedb:data-source-host-delete', args=[data_source.pk])
        self.client.force_login(user if user else data_source.owner)

        count = getattr(data_source, entry_set).order_by("id").count()
        form_data = {
            "form-TOTAL_FORMS": count,
            "form-INITIAL_FORMS": count,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }

        changes = set() if changes is None else changes
        for i, v in enumerate(getattr(data_source, entry_set).order_by("id")):
            form_data["form-%i-name" % i] = v.name
            form_data["form-%i-id" % i] = v.id
            form_data["form-%i-identifier" % i] = v.identifier
            if is_virus:
                form_data["form-%i-her_identifier" % i] = v.her_identifier or ''
            if v.id in changes:
                form_data["form-%i-DELETE" % i] = "on"
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)

    def data_source_file_import(
            self,
            user,
            filename,
            name=None,
            life_domain=None,
            description=None,
            public=None,
    ):
        already_there = list(models.DataSource.objects.values_list('id', flat=True))
        url = reverse('viralhostrangedb:file-import-view')
        self.client.force_login(user)
        f = open(os.path.join(self.test_data, filename), "rb")
        form_data = dict(
            name=name if name is not None else 'imported',
            life_domain=life_domain if life_domain is not None else "bacteria",
            description=description if description is not None else "Nop",
            public=public if public is not None else True,
            file=SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
        )
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)
        return models.DataSource.objects.exclude(id__in=already_there).get()

    def response_update(self, data_source, virus, host, new_raw_response=666, user=None):
        self.client.force_login(user if user else data_source.owner)
        url = reverse(
            'viralhostrangedb:response-update',
            kwargs=dict(
                ds_pk=data_source.pk,
                virus_pk=virus.pk,
                host_pk=host.pk,
            ),
        )
        form_data = dict(raw_response=new_raw_response, )
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)

    def data_source_mapping_range_edit(self, data_source, user=None, weak_starts_with=1, lysis_starts_with=2):
        lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection")
        weak = models.GlobalViralHostResponseValue.objects_mappable().get(name="Intermediate")
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[data_source.pk])
        # no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection")
        # not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()
        self.client.force_login(user if user else data_source.owner)
        form_data = {
            "mapping_for_%i_starts_with" % weak.pk: weak_starts_with,
            "mapping_for_%i_starts_with" % lysis.pk: lysis_starts_with,
        }
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)

    def data_source_mapping_label_edit(self, data_source, mapping: dict, user=None):
        form_data = {
            "form-TOTAL_FORMS": len(mapping),
            "form-INITIAL_FORMS": len(mapping),
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
        }
        for i, value in enumerate(sorted(mapping.items(), key=lambda o: o[0])):
            form_data["form-%i-raw_response" % i] = value[0]
            form_data["form-%i-mapping" % i] = value[1]

        self.client.force_login(user if user else data_source.owner)
        url = reverse('viralhostrangedb:data-source-mapping-label-edit', args=[data_source.pk])
        response = self.client.post(url, form_data)
        self.assertEqual(response.status_code, 302)

    def data_source_wizard_contribution(
            self,
            *,
            filename,
            user,
            name="test",
            life_domain="bacteria",
            description="",
            experiment_date="",
            publication_url="https://www.pasteur.fr/fr",
            # public=None,
    ):
        self.client.force_login(user)
        already_there = list(models.DataSource.objects.values_list('id', flat=True))

        url = reverse('viralhostrangedb:data-source-create')
        response_home = self.client.get(url, follow=True)
        target_home = response_home.redirect_chain[0][0]
        response_0 = self.client.post(
            target_home,
            {"data_source_wizard-current_step": target_home.split("/")[-2]},
            follow=True
        )

        target_1 = response_0.redirect_chain[0][0]
        step_name_1 = target_1.split("/")[-2]
        form_data_1 = {
            step_name_1 + "-name": name,
            step_name_1 + "-life_domain": life_domain,
            # step_name_1 + "-public": False, # comment when false to be sure that it is considered as False
            "data_source_wizard-current_step": step_name_1,
        }
        response_1 = self.client.post(target_1, form_data_1)
        self.assertEqual(response_1.status_code, 302)
        response_1 = self.client.post(target_1, form_data_1, follow=True)

        target_1bis = response_1.redirect_chain[0][0]
        step_name_1bis = target_1bis.split("/")[-2]
        form_data_1bis = {
            step_name_1bis + "-allowed_users_editor": "",
            "data_source_wizard-current_step": step_name_1bis,
        }
        response_1bis = self.client.post(target_1bis, form_data_1bis)
        self.assertEqual(response_1bis.status_code, 302)
        response_1bis = self.client.post(target_1bis, form_data_1bis, follow=True)

        target_2 = response_1bis.redirect_chain[0][0]
        step_name_2 = target_2.split("/")[-2]
        form_data_2 = {
            step_name_2 + "-description": description,
            step_name_2 + "-experiment_date": experiment_date,
            step_name_2 + "-publication_url": publication_url,
            "data_source_wizard-current_step": step_name_2,
        }
        response_2 = self.client.post(target_2, form_data_2)
        self.assertEqual(response_2.status_code, 302)
        response_2 = self.client.post(target_2, form_data_2, follow=True)

        target_3 = response_2.redirect_chain[0][0]
        step_name_3 = target_3.split("/")[-2]
        form_data_3 = {
            step_name_3 + "-upload_or_live_input": "upload",
            "data_source_wizard-current_step": step_name_3,
        }
        response_3 = self.client.post(target_3, form_data_3)
        self.assertEqual(response_3.status_code, 302)
        response_3 = self.client.post(target_3, form_data_3, follow=True)

        f = open(os.path.join(self.test_data, filename), "rb")
        target_4 = response_3.redirect_chain[0][0]
        step_name_4 = target_4.split("/")[-2]
        form_data_4 = {
            step_name_4 + "-file": SimpleUploadedFile(f.name, f.read(), content_type="application/vnd.ms-excel"),
            "data_source_wizard-current_step": step_name_4,
        }
        response_4 = self.client.post(target_4, form_data_4, follow=True)

        data_source = models.DataSource.objects.exclude(id__in=already_there).get()
        self.assertRedirects(
            response_4,
            expected_url=reverse('viralhostrangedb:data-source-mapping-edit', args=[data_source.pk])
        )
        return data_source

    def data_source_history_restoration(self, data_source, log_entry, user=None):
        url = reverse('viralhostrangedb:data-source-history-restoration', args=[data_source.pk, log_entry.pk])
        self.client.force_login(user if user else data_source.owner)
        response = self.client.post(url, dict(agree="True"))
        self.assertEqual(response.status_code, 302)


class WriteInFileTestCase(OneDataSourceTestCase):

    def test_write_in_tmp_file(self):
        self.write_in_tmp_file(self.client.get(reverse('viralhostrangedb:home')))
