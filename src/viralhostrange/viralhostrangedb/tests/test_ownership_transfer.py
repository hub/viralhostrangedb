import json
import logging

from django.core import mail
from django.core.signing import JSONSerializer, b64_encode
from django.test import override_settings
from django.urls import reverse

from viralhostrangedb import models
from viralhostrangedb.templatetags.viralhostrange_tags import can_edit
from viralhostrangedb.tests import base_test_case

logger = logging.getLogger(__name__)


class OwnerShipTestCase(base_test_case.OneDataSourceTestCase):
    def get_post_users_ret_status_code(self, url, read=True, write=True, none=True, logged_out=True,
                                       data=None, status_code=404):
        if read:
            self.client.force_login(self.u_read)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status_code)
            response = self.client.post(url, data=data or {})
            self.assertEqual(response.status_code, status_code)
        if write:
            self.client.force_login(self.u_write)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status_code)
            response = self.client.post(url, data=data or {})
            self.assertEqual(response.status_code, status_code)
        if none:
            self.client.force_login(self.u_none)
            response = self.client.get(url)
            self.assertEqual(response.status_code, status_code)
            response = self.client.post(url, data=data or {})
            self.assertEqual(response.status_code, status_code)
        if logged_out:
            self.client.logout()
            response = self.client.get(url)
            self.assertEqual(response.status_code, 302)
            response = self.client.post(url, data=data or {})
            self.assertEqual(response.status_code, 302)

    def test_process(self):
        url = reverse('viralhostrangedb:data-source-initiate-transfer', args=[self.ds.pk])
        self.get_post_users_ret_status_code(url)
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)

        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        link = mail.outbox[0].body
        start = link.index("http")
        link = link[start:link.index('\n', start + 1)]

        self.assertIn(
            reverse('viralhostrangedb:data-source-verified-transfer', args=link.split("/")[-4:-1]),
            link
        )

        self.get_post_users_ret_status_code(link)
        self.client.force_login(self.user)
        response = self.client.get(link)
        self.assertEqual(response.status_code, 200)

        for u in [self.u_none, self.u_none, self.u_none]:
            data = dict(
                ownership_transfer_recipient="%s %s" % (u.first_name, u.last_name)
            )
            self.get_post_users_ret_status_code(link, data=data)
            self.client.force_login(self.user)
            response = self.client.post(link, data=data)
            self.assertRedirects(response,
                                 expected_url=reverse('viralhostrangedb:data-source-detail', args=[self.ds.pk]))
            self.ds = models.DataSource.objects.get(pk=self.ds.pk)
            self.assertEqual(self.ds.owner, u)
            self.assertTrue(can_edit(self.user, self.ds))
            self.ds.owner = self.user
            self.ds.save()

    @override_settings(MAX_MINUTES_CHECK_EMAIL=0)
    def test_too_late(self):
        self.client.force_login(self.user)
        url = reverse('viralhostrangedb:data-source-initiate-transfer', args=[self.ds.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        link = mail.outbox[0].body
        start = link.index("http")
        link = link[start:link.index('\n', start + 1)]

        response = self.client.get(link)
        self.assertRedirects(response, expected_url=url)

    def test_sig_wrong(self):
        self.client.force_login(self.user)
        url = reverse('viralhostrangedb:data-source-initiate-transfer', args=[self.ds.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 1)
        link_val = mail.outbox[0].body
        start = link_val.index("http")
        link_val = link_val[start:link_val.index('\n', start + 1)]

        link_val = link_val.split("/")[-4:-1]
        link = reverse('viralhostrangedb:data-source-verified-transfer', args=[link_val[0], link_val[1], "terter"])

        # when user is good and data source owner, just redirect to initiate, it is no big deal
        response = self.client.get(link)
        self.assertRedirects(response, expected_url=url)

        # data source not owned, maybe trying to steal the ds, log critical and 403
        self.get_post_users_ret_status_code(link, status_code=403)

        # json not readable, maybe not the good user and trying to steal the ds, log critical and 403
        link = reverse('viralhostrangedb:data-source-verified-transfer', args=["terter", link_val[1], link_val[2]])
        self.client.force_login(self.user)
        response = self.client.get(link)
        self.assertEqual(response.status_code, 403)

        # forge the url with another user pk
        from django.core.signing import b64_decode
        data = json.loads(b64_decode(link_val[0].encode()))
        data["user"] = self.u_none.pk
        base64d = b64_encode(JSONSerializer().dumps(data)).decode()
        link = reverse('viralhostrangedb:data-source-verified-transfer', args=[base64d, link_val[1], link_val[2]])

        # log in with this user and try use the link
        self.client.force_login(self.u_none)
        response = self.client.get(link)
        # failing signature
        self.assertEqual(response.status_code, 403)
