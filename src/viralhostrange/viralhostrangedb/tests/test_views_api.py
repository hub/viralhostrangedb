import itertools
import json
import os
import random
import string
import urllib
from unittest import skipIf

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.db import connection
from django.db.models import Q
from django.urls import reverse

from viralhostrangedb import models, mixins, business_process
from viralhostrangedb.tests.test_views_others import ViewTestCase, TooledTestCase
from viralhostrangedb.views_api import MyAPIView, to_int_array, remplace_greek_letters_name_by_themselves


class GlobalResponseTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:globalviralhostresponsevalue-list') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        grs = json.loads(str(response.content, encoding='utf8'))
        grs = sorted(grs, key=lambda gr: gr['value'])
        grs = json.dumps(grs)
        self.assertJSONEqual(
            grs,
            [
                {"name": "NOT MAPPED YET", "description": "", "value": -1000000.0, "color": "#0000FF"},
                {"name": "No infection", "description": "", "value": 0.0, "color": "#FF0000"},
                {"name": "Intermediate", "description": "", "value": 1.0, "color": "#ff8000"},
                {"name": "Infection", "description": "", "value": 2.0, "color": "#00FF00"},
            ],
        )


class ResponseAggregatedTestCase(ViewTestCase):
    def test_works_no_auth(self):
        url = reverse('viralhostrangedb-api:aggregated-responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:aggregated-responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_auth(self):
        self.client.force_login(self.toto)
        url = reverse('viralhostrangedb-api:aggregated-responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:aggregated-responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        viruses = ",".join(str(i) for i in self.private_data_source_of_toto.virus_set.values_list('pk', flat=True))
        hosts = ",".join(str(i) for i in self.private_data_source_of_toto.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:aggregated-responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.private_data_source_of_toto.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_with_mapped_data_source(self):
        url = reverse('viralhostrangedb-api:aggregated-responses') \
              + "?format=json&ds=%i" % self.public_data_source_of_user_mapped.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection").value
        no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection").value
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True))
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:aggregated-responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.public_data_source_of_user_mapped.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis},
                str(self.C3.pk): {'diff': 1, 'val': lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': lysis},
                str(self.C3.pk): {'diff': 1, 'val': no_lysis},
                str(self.C4.pk): {'diff': 1, 'val': no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True)[1:])
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True)[:2])
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:aggregated-responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.public_data_source_of_user_mapped.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r2.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {'diff': 1, 'val': no_lysis},
                str(self.C2.pk): {'diff': 1, 'val': lysis}}
        })

    def test_works_when_filtering_on_only_published_data(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&only_published_data=on&host=%s" % \
              (pk_ds, pk_host)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
        })

        self.public_data_source_of_user_mapped.publication_url = "http://a.a"
        self.public_data_source_of_user_mapped.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {pk_host: {pk_ds: self.no_lysis.value}},
            str(self.r2.pk): {pk_host: {pk_ds: self.no_lysis.value}},
            str(self.r3.pk): {pk_host: {pk_ds: self.no_lysis.value}},
        })

    def test_works_when_filtering_on_only_virus_ncbi_id(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&only_virus_ncbi_id=on&host=%s" % \
              (pk_ds, pk_host)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
        })

        self.r1.identifier = "NC_001604.1"
        self.r1.save()
        self.r2.identifier = "azeaze"
        self.r2.save()
        self.r3.identifier = "NC_001416"
        self.r3.save()

        business_process.reset_identifier_status(self.public_data_source_of_user_mapped.virus_set.all())
        business_process.fetch_identifier_status(self.public_data_source_of_user_mapped.virus_set.all())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {pk_host: {pk_ds: self.no_lysis.value}},
            str(self.r3.pk): {pk_host: {pk_ds: self.no_lysis.value}},
        })

class ResponseAggregatedTestCase2(ViewTestCase):
    def test_works_when_filtering_on_only_host_ncbi_id(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&only_host_ncbi_id=on&virus=%i" % \
              (pk_ds, self.r1.pk)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
        })

        self.C1.identifier = "NC_000913.3"
        self.C1.save()
        self.C3.identifier = "azeaze"
        self.C3.save()
        self.C4.identifier = "AE005174.2"
        self.C4.save()

        business_process.reset_identifier_status(self.public_data_source_of_user_mapped.host_set.all())
        business_process.fetch_identifier_status(self.public_data_source_of_user_mapped.host_set.all())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {pk_ds: self.no_lysis.value},
                str(self.C4.pk): {pk_ds: self.lysis.value},
            },
        })

class ResponseAggregatedTestCase3(ViewTestCase):
    def test_works_when_filtering_life_domain(self):
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        virus_count = self.public_data_source_of_user_mapped.virus_set.count()
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&host=%s" % \
              (pk_ds, pk_host)
        response = self.client.get(url + "&life_domain=bacteria")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(str(response.content, encoding='utf8'))), virus_count)

        self.public_data_source_of_user_mapped.life_domain = "archaea"
        self.public_data_source_of_user_mapped.save()

        response = self.client.get(url + "&life_domain=bacteria")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(str(response.content, encoding='utf8'))), 0)

        response = self.client.get(url + "&life_domain=")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(json.loads(str(response.content, encoding='utf8'))), virus_count)


class CompleteResponseTestCase(ViewTestCase):
    def test_works_no_auth(self):
        url = reverse('viralhostrangedb-api:responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_auth(self):
        self.client.force_login(self.toto)
        url = reverse('viralhostrangedb-api:responses') + "?format=json"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        url = reverse('viralhostrangedb-api:responses') \
              + "?format=json&ds=%i" % self.private_data_source_of_toto.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

        viruses = ",".join(str(i) for i in self.private_data_source_of_toto.virus_set.values_list('pk', flat=True))
        hosts = ",".join(str(i) for i in self.private_data_source_of_toto.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:responses'),
            viruses=viruses,
            hosts=hosts,
            ds=str(self.private_data_source_of_toto.pk),
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {})

    def test_works_with_mapped_data_source(self):
        pk = str(self.public_data_source_of_user_mapped.pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s" % pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection").value
        no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection").value
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: lysis},
                str(self.C4.pk): {pk: no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True))
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True))
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:responses'),
            viruses=viruses,
            hosts=hosts,
            ds=pk,
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # print(json.dumps(json.loads(str(response.content, encoding='utf8')), indent=4))
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: lysis}},
            str(self.r2.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis},
                str(self.C3.pk): {pk: lysis},
                str(self.C4.pk): {pk: no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: lysis},
                str(self.C3.pk): {pk: no_lysis},
                str(self.C4.pk): {pk: no_lysis}}
        })

        viruses = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.virus_set.values_list('pk', flat=True)[1:])
        hosts = ",".join(
            str(i) for i in self.public_data_source_of_user_mapped.host_set.values_list('pk', flat=True)[:2])
        url = "%(url)s?format=json&ds=%(ds)s&host=%(hosts)s&virus=%(viruses)s" % dict(
            url=reverse('viralhostrangedb-api:responses'),
            viruses=viruses,
            hosts=hosts,
            ds=pk,
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        # print(json.dumps(json.loads(str(response.content, encoding='utf8')), indent=4))
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r2.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: no_lysis}},
            str(self.r3.pk): {
                str(self.C1.pk): {pk: no_lysis},
                str(self.C2.pk): {pk: lysis}}
        })

    def test_works_when_filtering_on_host(self):
        # lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection").value
        no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection").value
        pk_ds = str(self.public_data_source_of_user_mapped.pk)
        pk_host = str(self.public_data_source_of_user_mapped.host_set.first().pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s&host=%s" % (pk_ds, pk_host)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(str(response.content, encoding='utf8'), {
            str(self.r1.pk): {pk_host: {pk_ds: no_lysis}},
            str(self.r2.pk): {pk_host: {pk_ds: no_lysis}},
            str(self.r3.pk): {pk_host: {pk_ds: no_lysis}},
        })

    def test_staff_cannot_see_private(self):
        pk = str(self.private_data_source_of_toto_with_no_virus_or_host_shared.pk)
        url = reverse('viralhostrangedb-api:responses') + "?format=json&ds=%s" % pk
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(len(ret), 0)


class DataSourceListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?pks=1,2")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?pk=1,2")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?ids=1,2")
        self.assertEqual(response.status_code, 200)
        response = self.client.get(url + "?id=1,2")
        self.assertEqual(response.status_code, 200)

    def test_filter_only_published_data(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.user)

        self.public_data_source_of_user.publication_url = "http://a.com"
        self.public_data_source_of_user.save()
        self.public_data_source_of_user_mapped.publication_url = "http://a.com"
        self.public_data_source_of_user_mapped.save()
        self.private_data_source_of_user.publication_url = "http://a.com"
        self.private_data_source_of_user.save()

        response = self.client.get(url + "?only_published_data=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

    def test_filter_life_domain(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.user)

        response = self.client.get(url + "?life_domain=bacteria")
        ret = json.loads(str(response.content, encoding='utf8'))
        bacteria_count = len(ret)

        self.public_data_source_of_user.life_domain = "eukaryote"
        self.public_data_source_of_user.save()
        self.public_data_source_of_user_mapped.life_domain = "eukaryote"
        self.public_data_source_of_user_mapped.save()
        self.private_data_source_of_user.life_domain = "eukaryote"
        self.private_data_source_of_user.save()

        response = self.client.get(url + "?life_domain=eukaryote")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

        response = self.client.get(url + "?life_domain=bacteria")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, bacteria_count - len(ret))

    def test_filter_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.toto)

        models.Virus.objects.filter(~Q(identifier='')).update(is_ncbi_identifier_value=False)

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v.is_ncbi_identifier_value = True
        v.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_filter_only_host_ncbi_id(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.toto)

        models.Host.objects.filter(~Q(identifier='')).update(is_ncbi_identifier_value=False)

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v.is_ncbi_identifier_value = True
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_filter_only_host_ncbi_id_and_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:datasource-list')
        self.client.force_login(self.toto)

        models.Virus.objects.filter(~Q(identifier='')).update(is_ncbi_identifier_value=False)
        models.Host.objects.filter(~Q(identifier='')).update(is_ncbi_identifier_value=False)

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        v.identifier = "ee"
        v.is_ncbi_identifier_value = True
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.identifier = "ee"
        v.is_ncbi_identifier_value = True
        v.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_owner(self):
        url = reverse('viralhostrangedb-api:datasource-list') + "?sample_size=1000&owner=%i" % self.user.pk
        qs_data_source = models.DataSource.objects.filter(owner__pk=self.user.pk)
        self.client.logout()

        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=AnonymousUser(),
            queryset=qs_data_source,
        ).count(), len(ret))

        self.client.force_login(self.toto)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=self.toto,
            queryset=qs_data_source,
        ).count(), len(ret))


class VirusListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:virus-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_sublist(self):
        pk1 = str(self.public_data_source_of_user_mapped.pk)
        pk2 = str(self.public_data_source_of_user.pk)
        virus_pks = set(models.Virus.objects.filter(data_source__pk__in=[pk1, pk2]).values_list("id", flat=True))
        url = reverse('viralhostrangedb-api:virus-list') + "?ds=" + pk1 + "&ds=" + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        virus_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(virus_pks, virus_pks_api)
        url = reverse('viralhostrangedb-api:virus-list') + "?ds=" + pk1 + "," + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        virus_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(virus_pks, virus_pks_api)

    def test_default_fields(self):
        url = reverse('viralhostrangedb-api:virus-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {
            "id",
            "identifier",
            "her_identifier",
            "is_ncbi_identifier_value",
            "name",
            "short_name",
            "tax_id",
        })

    def test_fields_works(self):
        url = reverse('viralhostrangedb-api:virus-list') + "?fields=id"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id"})
        url = reverse('viralhostrangedb-api:virus-list') + "?fields=id,name"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id", "name"})

    def test_filter_only_published_data(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.user)

        response = self.client.get(url + "?only_published_data=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        self.public_data_source_of_user.publication_url = "http://a.com"
        self.public_data_source_of_user.save()

        response = self.client.get(url + "?only_published_data=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(self.public_data_source_of_user.virus_set.count(), len(ret))

    def test_filter_life_domain(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.user)

        response = self.client.get(url + "?life_domain=eukaryote")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        self.public_data_source_of_user.life_domain = "eukaryote"
        self.public_data_source_of_user.save()

        response = self.client.get(url + "?life_domain=eukaryote")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(3, len(ret))

    def test_filter_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.identifier = "ee"
        v.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        v = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        v.is_ncbi_identifier_value = True
        v.save()

        response = self.client.get(url + "?only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_filter_only_host_ncbi_id_and_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:virus-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        o.identifier = "ee"
        o.is_ncbi_identifier_value = True
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        o.identifier = "ee"
        o.is_ncbi_identifier_value = True
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_owner(self):
        url = reverse('viralhostrangedb-api:virus-list') + "?sample_size=1000&owner=%i" % self.user.pk
        qs_virus = models.Virus.objects.filter(data_source__owner__pk=self.user.pk)
        self.client.logout()

        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=AnonymousUser(),
            queryset=qs_virus,
            path_to_data_source="data_source__",
        ).count(), len(ret))

        self.client.force_login(self.toto)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(mixins.only_public_or_granted_or_owned_queryset_filter(
            None,
            request=None,
            user=self.toto,
            queryset=qs_virus,
            path_to_data_source="data_source__",
        ).count(), len(ret))

    def test_staff_cannot_see_private(self):
        url = reverse('viralhostrangedb-api:virus-list') + "?pk=%i" % self.rx.pk
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(len(ret), 0)


class HostListViewTestCase(ViewTestCase):
    def test_works(self):
        url = reverse('viralhostrangedb-api:host-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.client.login(username="user", email="a@a.a", password=self.user_pwd))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_sublist(self):
        pk1 = str(self.public_data_source_of_user_mapped.pk)
        pk2 = str(self.public_data_source_of_user.pk)
        host_pks = set(models.Host.objects.filter(data_source__pk__in=[pk1, pk2]).values_list("id", flat=True))
        url = reverse('viralhostrangedb-api:host-list') + "?ds=" + pk1 + "&ds=" + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        host_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(host_pks, host_pks_api)
        url = reverse('viralhostrangedb-api:host-list') + "?ds=" + pk1 + "," + pk2
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        host_pks_api = set([d["id"] for d in ret])
        self.assertSetEqual(host_pks, host_pks_api)

    def test_default_fields(self):
        url = reverse('viralhostrangedb-api:host-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {
            "id",
            "identifier",
            "is_ncbi_identifier_value",
            "name",
            "short_name",
            "tax_id",
        })

    def test_fields_works(self):
        url = reverse('viralhostrangedb-api:host-list') + "?fields=id"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id"})
        url = reverse('viralhostrangedb-api:host-list') + "?fields=id,name"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        ret = json.loads(str(response.content, encoding='utf8'))
        keys = set(itertools.chain(*[list(d.keys()) for d in ret]))
        self.assertSetEqual(keys, {"id", "name"})

    def test_filter_only_host_ncbi_id(self):
        url = reverse('viralhostrangedb-api:host-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        o.identifier = "ee"
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        o.is_ncbi_identifier_value = True
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_filter_only_host_ncbi_id_and_only_virus_ncbi_id(self):
        url = reverse('viralhostrangedb-api:host-list')
        self.client.force_login(self.toto)
        self.public_data_source_with_ids_of_toto.owner = self.user
        self.public_data_source_with_ids_of_toto.public = False
        self.public_data_source_with_ids_of_toto.save()

        models.Virus.objects.filter(~Q(identifier='')).update(is_ncbi_identifier_value=False)
        models.Host.objects.filter(~Q(identifier='')).update(is_ncbi_identifier_value=False)

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.host_set.first()
        o.identifier = "ee"
        o.is_ncbi_identifier_value = True
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(0, len(ret))

        o = self.private_data_source_of_toto_with_no_virus_or_host_shared.virus_set.first()
        o.identifier = "ee"
        o.is_ncbi_identifier_value = True
        o.save()

        response = self.client.get(url + "?only_host_ncbi_id=on&only_virus_ncbi_id=on")
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(1, len(ret))

    def test_staff_cannot_see_private(self):
        url = reverse('viralhostrangedb-api:host-list') + "?pk=%i" % self.Cx.pk
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        self.assertEqual(len(ret), 0)


class DetailViewTestCase(ViewTestCase):
    def test_host_works_no_auth(self):
        o = models.Host.objects.first()
        url = reverse('viralhostrangedb-api:host-detail', args=[o.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {
                'id': o.pk,
                'identifier': o.identifier,
                'name': o.name,
                'short_name': None,
                'is_ncbi_identifier_value': o.is_ncbi_identifier_value,
                'tax_id': None,
            },
        )

    def test_virus_works_no_auth(self):
        o = models.Virus.objects.first()
        url = reverse('viralhostrangedb-api:virus-detail', args=[o.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(
            str(response.content, encoding='utf8'),
            {
                'id': o.pk,
                'identifier': o.identifier,
                'her_identifier': o.her_identifier,
                'name': o.name,
                'short_name': None,
                'is_ncbi_identifier_value': o.is_ncbi_identifier_value,
                'tax_id': None,
            },
        )
        long_name = "This is a very long name and it should be truncated"
        o.name = long_name
        o.save()
        short_name = json.loads(self.client.get(url).content)["short_name"]
        self.assertLess(len(short_name), len(long_name))
        self.assertEqual(short_name[:5], long_name[:5])
        self.assertEqual(short_name[-5:], long_name[-5:])

    def test_data_source_works_no_auth(self):
        url = reverse('viralhostrangedb-api:datasource-detail', args=[self.public_data_source_with_ids_of_toto.pk])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        resp = json.loads(str(response.content, encoding='utf8'))
        del resp['creation_date']
        del resp['last_edition_date']
        self.assertJSONEqual(
            json.dumps(resp),
            dict(
                description=self.public_data_source_with_ids_of_toto.description,
                id=self.public_data_source_with_ids_of_toto.id,
                name=self.public_data_source_with_ids_of_toto.name,
                public=self.public_data_source_with_ids_of_toto.public,
            ),
        )

    def test_staff_cannot_see_private(self):
        url = reverse('viralhostrangedb-api:host-detail', args=[self.Cx.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        del ret["detail"]
        self.assertDictEqual(ret, {})

        url = reverse('viralhostrangedb-api:virus-detail', args=[self.rx.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        del ret["detail"]
        self.assertDictEqual(ret, {})

        url = reverse('viralhostrangedb-api:datasource-detail',
                      args=[self.private_data_source_of_toto_with_no_virus_or_host_shared.pk])
        self.client.force_login(self.staff_user)
        response = self.client.get(url)
        ret = json.loads(str(response.content, encoding='utf8'))
        del ret["detail"]
        self.assertDictEqual(ret, {})


class DummyTest(TooledTestCase):
    def test_dummy(self):
        self.assertRaises(NotImplementedError, MyAPIView().get, None)


class InfectionRatioTestCase(ViewTestCase):
    def test_wrong_slug(self):
        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-list', args=["toto"]) + "?virus=%i" % self.r1.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

    def test_works__and_focus_list_or_detail_idempotent(self):
        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            self.r1.pk
        ])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        resp_1 = json.loads(str(response.content, encoding='utf8'))

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-list', args=["virus"]) + "?virus=%i" % self.r1.pk
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        resp_2 = json.loads(str(response.content, encoding='utf8'))
        self.assertJSONEqual(
            json.dumps(resp_1),
            json.dumps(resp_2),
            msg="The ratio when focusing on a virus (resp_1) should be the same as "
                "the ratios when filtering on this virus (resp_2)"
        )

    def test_ratio_r1(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.r1

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.75, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.5, 'total': 4}}),
        )

    def test_ratio_r2(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.r2

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.5, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )

    def test_ratio_r3(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.r3

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "virus",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.5, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0.25, 'total': 4}}),
        )

    def test_ratio_C1(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])
        x = self.C1

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-detail', args=[
            "host",
            x.pk
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 3}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 1.0 / 3, 'total': 3}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 3}}),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a + w).content, encoding='utf8'))),
            json.dumps({x.pk: {'ratio': 0, 'total': 3}}),
        )

    def test_ratio_C1_to_C4(self):
        a = "&agreed_infection"
        w = "&weak_infection"
        ds = "?ds=" + ",".join(str(i) for i in [
            self.three_reponse_simple.pk,
            self.three_reponse_simple_2.pk,
            self.four_reponse_simple.pk,
        ])

        url = reverse('viralhostrangedb-api:aggregated-infection-ratio-list', args=[
            "host"
        ]) + ds
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 0.0,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                }
            }),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 1.0,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 2.0 / 3,
                    "total": 3
                }
            }),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + w + a).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 0.0,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 2.0 / 3,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                }
            }),
        )
        self.assertJSONEqual(
            json.dumps(json.loads(str(self.client.get(url + a).content, encoding='utf8'))),
            json.dumps({
                self.C1.pk: {
                    "ratio": 0.0,
                    "total": 3
                },
                self.C2.pk: {
                    "ratio": 1.0 / 3,
                    "total": 3
                },
                self.C3.pk: {
                    "ratio": 0,
                    "total": 3
                },
                self.C4.pk: {
                    "ratio": 0,
                    "total": 3
                }
            }),
        )


class SearchTestCaseAPI1(ViewTestCase):
    def test_ko(self):
        url = reverse('viralhostrangedb-api:search')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertIn("err", json.loads(response.content))

    def test_works_no_auth(self):
        url = reverse('viralhostrangedb-api:search') + "?search=privateprivateprivate"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)
        self.assertEqual(response["data_source"]["count"], 0)

    def test_works_auth(self):
        url = reverse('viralhostrangedb-api:search') + "?search=\"private%20user\""
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertGreater(response["data_source"]["count"], 0)

    def test_works_ui_help(self):
        url = reverse('viralhostrangedb-api:search') + "?search=\"private%20user\"&ui_help=True"
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertGreater(response["data_source"]["count"], 0)

    def test_privacy_cross_auth(self):
        url = reverse('viralhostrangedb-api:search') + "?search=\"private%20user\""
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response["data_source"]["count"], 0)

class SearchTestCaseAPI2(ViewTestCase):
    def test_advanced(self):
        ds = self.private_data_source_of_toto_with_no_virus_or_host_shared
        url = reverse('viralhostrangedb-api:search') + "?search=\"" + ds.name + "\""
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response["data_source"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)
        self.client.logout()
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)

        url = reverse('viralhostrangedb-api:search') + "?search=" + ds.virus_set.first().name
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 1)
        self.assertEqual(response["host"]["count"], 0)
        self.client.logout()
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["virus"]["count"], 0)

        url = reverse('viralhostrangedb-api:search') + "?search=" + ds.host_set.first().name
        self.client.force_login(self.toto)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        response = json.loads(response.content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 1)
        self.client.logout()
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["host"]["count"], 0)

    def test_only_identifier_virus(self):
        search = "test_only_identifier"
        self.r1.name = search
        self.r1.save()

        url = reverse('viralhostrangedb-api:search') + "?search=" + search
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 1)
        self.assertEqual(response["host"]["count"], 0)

        url = reverse('viralhostrangedb-api:search') + "?search=" + search + "&only_virus_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.r1.identifier = "blabla"
        self.r1.save()

        url = reverse('viralhostrangedb-api:search') + "?search=" + search + "&only_virus_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.r1.is_ncbi_identifier_value = "True"
        self.r1.save()

        url = reverse('viralhostrangedb-api:search') + "?search=" + search + "&only_virus_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 1)
        self.assertEqual(response["host"]["count"], 0)

    def test_only_identifier_host(self):
        search = "test_only_identifier"
        self.C1.name = search
        self.C1.save()

        url = reverse('viralhostrangedb-api:search') + "?search=" + search
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 1)

        url = reverse('viralhostrangedb-api:search') + "?search=" + search + "&only_host_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.C1.identifier = "blabla"
        self.C1.save()

        url = reverse('viralhostrangedb-api:search') + "?search=" + search + "&only_host_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.C1.is_ncbi_identifier_value = "True"
        self.C1.save()

        url = reverse('viralhostrangedb-api:search') + "?search=" + search + "&only_host_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 1)

class SearchTestCaseAPI3(ViewTestCase):
    def test_only_identifier_XXX_for_data_source(self):
        self.client.force_login(self.toto)
        base_url = reverse('viralhostrangedb-api:search') + "?search=\"" + \
                   self.private_data_source_of_toto_with_no_virus_or_host_shared.name + "\""

        url = base_url
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        url = base_url + "&only_host_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.Cx.identifier = "blabla"
        self.Cx.save()

        url = base_url + "&only_host_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.Cx.is_ncbi_identifier_value = "True"
        self.Cx.save()

        url = base_url + "&only_host_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        url = base_url + "&only_host_ncbi_id=on" + "&only_virus_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.rx.identifier = "trertertertert"
        self.rx.save()

        url = base_url + "&only_host_ncbi_id=on" + "&only_virus_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

        self.rx.is_ncbi_identifier_value = "True"
        self.rx.save()

        url = base_url + "&only_host_ncbi_id=on" + "&only_virus_ncbi_id=on"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

    def test_her_works(self):
        her = 25
        self.assertLess(her, 100)
        self.r1.name = str(her)
        self.r1.save()
        self.r2.her_identifier = her
        self.r2.save()
        url = reverse('viralhostrangedb-api:search') + "?search=%i" % her
        response = json.loads(self.client.get(url).content)
        self.assertSetEqual(set([o["id"] for o in response["virus"]["sample"]]), {self.r1.id, self.r2.id})
        url = reverse('viralhostrangedb-api:search') + "?search=her:%i" % her
        response = json.loads(self.client.get(url).content)
        self.assertSetEqual(set([o["id"] for o in response["virus"]["sample"]]), {self.r2.id})
        url = reverse('viralhostrangedb-api:search') + "?search=her:ZZ"
        response = json.loads(self.client.get(url).content)
        self.assertEqual(response["data_source"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        self.assertEqual(response["host"]["count"], 0)

    @skipIf(connection.vendor == "sqlite", "Sqlite3 cannot perform accent insensitive search")
    def test_accent_insensitive(self):
        word = "Hérelle"
        text = "lorem ipsum %s dolor sit amet" % word
        # I could have use unidecode but I did not
        url = reverse('viralhostrangedb-api:search') + "?search=%s" % "Herelle"

        self.assertEqual(json.loads(self.client.get(url).content)["data_source"]["count"], 0)

        models.DataSource.objects.filter(pk=self.public_data_source_of_user.pk).update(description=text)
        self.assertEqual(json.loads(self.client.get(url).content)["data_source"]["count"], 1)

        models.DataSource.objects.filter(pk=self.public_data_source_of_user.pk).update(name=text)
        self.assertEqual(json.loads(self.client.get(url).content)["data_source"]["count"], 1)

        models.DataSource.objects.filter(pk=self.public_data_source_with_ids_of_toto.pk).update(description=text)
        self.assertEqual(json.loads(self.client.get(url).content)["data_source"]["count"], 2)

class SearchTestCaseAPI4(ViewTestCase):
    def test_data_source_searched(self):
        base_url = reverse('viralhostrangedb-api:search') + "?search="

        # search by owner should work, also with a substring of the first/last name
        for searched in [
            self.public_data_source_of_user.owner.first_name,
            self.public_data_source_of_user.owner.last_name[3:-3],
            self.public_data_source_of_user.owner.first_name[:-3],
        ]:
            response = json.loads(self.client.get(base_url + searched).content)
            self.assertEqual(
                response['data_source']['sample'][0]['provider'],
                self.user.last_name.upper() + " " + self.user.first_name.title()
            )

        self.public_data_source_of_user.provider_first_name = "amanita"
        self.public_data_source_of_user.provider_last_name = "kaplan"
        self.public_data_source_of_user.save()

        # search by provider should work, also with a substring of the first/last name
        for searched in [
            self.public_data_source_of_user.provider_first_name[2:],
            self.public_data_source_of_user.provider_last_name[2:],
        ]:
            response = json.loads(self.client.get(base_url + searched).content)
            self.assertEqual(
                response['data_source']['sample'][0]['provider'],
                self.public_data_source_of_user.provider_last_name.upper() + " " +
                self.public_data_source_of_user.provider_first_name.title()
            )
            pks = {d['id'] for d in response['data_source']['sample']}
            self.assertIn(self.public_data_source_of_user.pk, pks)

        # search by owner when provider is field should not work,
        for searched in [
            self.public_data_source_of_user.owner.first_name,
            self.public_data_source_of_user.owner.last_name[3:-3],
        ]:
            response = json.loads(self.client.get(base_url + searched).content)
            pks = {d['id'] for d in response['data_source']['sample']}
            self.assertNotIn(self.public_data_source_of_user.pk, pks)

    def test_trimmed(self):
        for d in models.DataSource.objects.filter(public=True):
            d.description = "azerty" + (''.join(random.choice(string.ascii_lowercase) for i in range(120)))
            d.name = (''.join(random.choice(string.ascii_lowercase) for i in range(70)))
            d.save()
        base_url = reverse('viralhostrangedb-api:search') + "?search=azerty&str_max_length=%i"
        for str_max_length in [10, 50, 100, 200]:
            response = json.loads(self.client.get(base_url % str_max_length).content)
            for d in response["data_source"]["sample"]:
                self.assertLessEqual(len(d["description"]), str_max_length)
                self.assertTrue(models.DataSource.objects.filter(name=d["name"], id=d["id"]).exists())


class OtherTests(TooledTestCase):
    def test_functions(self):
        self.assertEqual(list(to_int_array(["e", "1", "2", "1", "1"])), [1, 2, 1, 1])

    def test_greek(self):
        f = remplace_greek_letters_name_by_themselves
        self.assertEqual(f('Alpha Beta Gamma Delta epsilon'), ("α β γ δ ε", True))
        self.assertEqual(f('sigmaa'), ('σa', True))
        self.assertEqual(f('Lambda'), ('λ', True))
        self.assertEqual(f('lambda'), ('λ', True))
        self.assertEqual(f('lambdavir'), ('λvir', True))
        self.assertEqual(f('CCC'), ('CCC', False))
        self.assertEqual(f('lamBda'), ('λ', True))
        self.assertEqual(f('42Alpha42'), ("42α42", True))
        self.assertEqual(f('42Alfa42'), ("42Alfa42", False))
        self.assertEqual(f('Alpha 42'), ("α 42", True))
        self.assertEqual(f('PhiPLS27'), ('φPLS27', True))
        self.assertEqual(f('phiPLS27'), ('φPLS27', True))
        self.assertEqual(f('Lambdavir'), ('λvir', True))
        self.assertEqual(f('lambdavir'), ('λvir', True))


class StatisticsTest(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            first_name="Rajesh",
            last_name="Koothrappali",
            email="user@a.b",
        )

        ################################################################################
        self.ds = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        self.ds.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds,
            file=filename,
        )

        ################################################################################
        self.ds2 = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="private user2",
            raw_name="ee",
            kind="FILE",
        )
        self.ds.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds2,
            file=filename,
        )

    def test_works(self):
        self.assertJSONEqual(
            str(self.client.get(reverse('viralhostrangedb-api:get_statistics')).content, encoding='utf8'),
            {
                'data_source_count': 2,
                'host_count': 4,
                'response_count': 24,
                'virus_count': 3
            }
        )

    def test_ds_private(self):
        self.ds.public = False
        self.ds.save()
        self.assertJSONEqual(
            str(self.client.get(reverse('viralhostrangedb-api:get_statistics')).content, encoding='utf8'),
            {
                'data_source_count': 1,
                'host_count': 4,
                'response_count': 12,
                'virus_count': 3
            }
        )
        self.client.force_login(self.user)
        self.assertJSONEqual(
            str(self.client.get(reverse('viralhostrangedb-api:get_statistics')).content, encoding='utf8'),
            {
                'data_source_count': 2,
                'host_count': 4,
                'response_count': 24,
                'virus_count': 3,
            }
        )


class GreekLettersTest(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            first_name="Rajesh",
            last_name="Koothrappali",
            email="user@a.b",
        )

        ################################################################################
        self.ds = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        self.ds.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds,
            file=filename,
        )
        o_s = list(models.Virus.objects.all()[:3])
        o_s[0].name = "λ"
        o_s[0].save()
        o_s[1].name = "Λ"
        o_s[1].save()
        o_s[2].name = "ΦPLS27"
        o_s[2].save()

    def test_works(self):
        url = reverse('viralhostrangedb-api:search') + "?"
        for s, cpt in [
            ("\"λ\"", 1 if connection.vendor == "sqlite" else 2),
            ("\"Λ\"", 1 if connection.vendor == "sqlite" else 2),
            ("phi", 0 if connection.vendor == "sqlite" else 1),
            ("Lambda", 1 if connection.vendor == "sqlite" else 2),
            ("LAMbda", 1 if connection.vendor == "sqlite" else 2),
            ("LAMBDA", 1 if connection.vendor == "sqlite" else 2),
        ]:
            response = self.client.get(url + urllib.parse.urlencode(dict(search=s)))
            self.assertEqual(response.status_code, 200)
            response = json.loads(response.content)
            self.assertEqual(response["virus"]["count"], cpt, "For %s expected %i" % (s, cpt))
            self.assertEqual(response["host"]["count"], 0)
            self.assertEqual(response["data_source"]["count"], 0)

        for letter, name in [
            ("λ", "lambda"),
            ("γ", "gamma"),
            ("θ", "theta"),
            ("φ", "phi"),
            ("ξ", "xi"),
        ]:
            response = self.client.get(url + "&search=" + name)
            response = json.loads(response.content)
            self.assertIn(letter, response["query"]["alternative_searched_texts"])


class SearchSpaceSplitTest(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            first_name="Rajesh",
            last_name="Koothrappali",
            email="user@a.b",
        )

        ################################################################################
        self.ds = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="foo bar loo γray",
            raw_name="ee",
            kind="FILE",
        )
        self.ds.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds,
            file=filename,
        )
        o_s = list(models.Virus.objects.all()[:3])
        o_s[0].name = "λvir"
        o_s[0].save()
        o_s[1].name = "Λ"
        o_s[1].save()
        o_s[2].name = "ΦPLS27"
        o_s[2].save()

    def test_for_none_greek(self):
        url = reverse('viralhostrangedb-api:search') + "?"
        for s, cpt in [
            ("foo", 1),
            ("foo bar", 1),
            ("loo foo bar", 1),
            ("foo loo bar", 1),
            ("bar foo", 1),
            ("\"foo bar\"", 1),
            ("\"bar foo\"", 0),
            ("\"foo bar\" γ ray", 1),
        ]:
            response = self.client.get(url + "&search=" + s)
            self.assertEqual(response.status_code, 200)
            response = json.loads(response.content)
            self.assertEqual(response["virus"]["count"], 0)
            self.assertEqual(response["host"]["count"], 0)
            self.assertEqual(response["data_source"]["count"], cpt, "For <i>%s</i> expected %i" % (s, cpt))

    def test_for_greek(self):
        url = reverse('viralhostrangedb-api:search') + "?"
        for s, cpt in [
            ("lambdavir", 1),
            ("lambda vir", 1 if connection.vendor == "sqlite" else 2),
            ("vir lambda", 1 if connection.vendor == "sqlite" else 2),
            ("\"lambdavir\"", 1),
            ("\"lambda vir\"", 0),
            ("\"vir lambda\"", 0),
        ]:
            response = self.client.get(url + urllib.parse.urlencode(dict(search=s)))
            self.assertEqual(response.status_code, 200)
            response = json.loads(response.content)
            self.assertEqual(response["virus"]["count"], cpt, "For <i>%s</i> expected %i" % (s, cpt))
            self.assertEqual(response["host"]["count"], 0)
            self.assertEqual(response["data_source"]["count"], 0)


class SearchMatchCountTest(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        ################################################################################
        self.berni = get_user_model().objects.create(
            username="superuser",
            first_name="Bernadette",
            last_name="Rostenkowski",
            email="superuser@a.b",
        )
        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            first_name="Rajesh",
            last_name="Koothrappali",
            email="user@a.b",
        )

        ################################################################################
        self.ds0 = models.DataSource.objects.create(
            owner=self.berni,
            public=True,
            name="hello home",
            description="FOO bar ",
            raw_name="ee",
            kind="FILE",
        )
        self.ds0.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds0,
            file=filename,
        )

        ################################################################################
        self.ds1 = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="FOö bar x",
            description="FOö bar lol γray fool x",
            raw_name="ee",
            kind="FILE",
        )
        self.ds1.save()
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        business_process.import_file(
            data_source=self.ds1,
            file=filename,
        )

        ################################################################################
        self.ds2 = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="hello wworld",
            description="football stuff",
            publication_url="https://lorem.ipsum.dolor",
            raw_name="ee",
            kind="FILE",
        )
        self.ds2.save()
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.ds2,
            file=filename,
        )
        self.url = reverse('viralhostrangedb-api:search') + "?"

    def search(self, s, kind=None, sorting=None):
        d = dict(search=s)
        if sorting:
            d["sorting"] = sorting
        if kind:
            d["kind"] = kind
        return json.loads(self.client.get(self.url + urllib.parse.urlencode(d)).content)

    def test_sorting_pk(self):
        # don't care of the number of hit, just return them, by pk probably
        self.assertEqual(
            [o["id"] for o in self.search("\"l\" \"x\"", sorting="PK", kind='data_source')["data_source"]["sample"]],
            [self.ds2.pk, self.ds0.pk, self.ds1.pk],
        )

    def test_sorting_tf(self):
        # more l and x in ds2 than the other
        self.assertEqual(
            [o["id"] for o in self.search("\"l\" \"x\"", sorting="TF", kind='data_source')["data_source"]["sample"]],
            [self.ds2.pk, self.ds1.pk, self.ds0.pk],
        )

    def test_sorting_tfidf(self):
        # more l and x in ds2, but x only in ds2 so ds2 have a bonus with tdidf
        self.assertEqual(
            [o["id"] for o in self.search("\"l\" \"x\"", sorting="TFIDF", kind='data_source')["data_source"]["sample"]],
            [self.ds2.pk, self.ds1.pk, self.ds0.pk],
        )

    def test_sorting_tfidf_identifier_and_publi_first(self):
        r1_a = models.Virus.objects.exclude(identifier="").get(name="r1")
        r1_b = models.Virus.objects.get(name="r1", identifier="")
        r1_a.is_ncbi_identifier_value = True
        r1_a.save()
        self.assertEqual(
            [o["id"] for o in self.search("r1", sorting="TFIDF", kind='virus')["virus"]["sample"]],
            [r1_a.pk, r1_b.pk, ],
            "r1_a has an identifier and should be first",
        )
        r1_a.identifier = ""
        r1_a.is_ncbi_identifier_value = False
        r1_a.save()
        r1_b.identifier = "azeaze"
        r1_b.is_ncbi_identifier_value = True
        r1_b.save()
        self.assertEqual(
            [o["id"] for o in self.search("r1", sorting="TFIDF", kind='virus')["virus"]["sample"]],
            [r1_b.pk, r1_a.pk, ],
            "we removed id of r1_a, and add one to r1_b. So r1_b should be first",
        )
        r1_b.identifier = ""
        r1_b.her_identifier = 666
        r1_b.is_ncbi_identifier_value = False
        r1_b.save()
        self.assertEqual(
            [o["id"] for o in self.search("r1", sorting="TFIDF", kind='virus')["virus"]["sample"]],
            [r1_b.pk, r1_a.pk, ],
            "we removed id of r1_a, and add one to r1_b. So r1_b should be first",
        )
        r1_b.identifier = "zeaze"
        r1_b.tax_id_value = "666"
        r1_b.is_ncbi_identifier_value = False
        r1_b.save()
        self.assertEqual(
            [o["id"] for o in self.search("r1", sorting="TFIDF", kind='virus')["virus"]["sample"]],
            [r1_b.pk, r1_a.pk, ],
            "we removed id of r1_a, and add one to r1_b. So r1_b should be first",
        )
        r1_b.identifier = ""
        r1_b.her_identifier = None
        r1_b.tax_id_value = None
        r1_b.is_ncbi_identifier_value = False
        r1_b.save()

        c1_a = models.Host.objects.exclude(identifier="").get(name="C1")
        c1_b = models.Host.objects.get(name="C1", identifier="")
        c1_a.is_ncbi_identifier_value = True
        c1_a.save()
        self.assertEqual(
            [o["id"] for o in self.search("C1", sorting="TFIDF", kind='host')["host"]["sample"]],
            [c1_a.pk, c1_b.pk, ],
            "c1_a has an identifier and should be first",
        )
        c1_a.identifier = ""
        c1_a.is_ncbi_identifier_value = False
        c1_a.save()
        c1_b.identifier = "azeaze"
        c1_b.is_ncbi_identifier_value = True
        c1_b.save()
        self.assertEqual(
            [o["id"] for o in self.search("C1", sorting="TFIDF", kind='host')["host"]["sample"]],
            [c1_b.pk, c1_a.pk, ],
            "we removed id of c1_a, and add one to c1_b. So c1_b should be first",
        )

        self.assertEqual(
            [o["id"] for o in self.search("hell", sorting="TFIDF", kind='data_source')["data_source"]["sample"]],
            [self.ds2.pk, self.ds0.pk, ],
            "ds2 has a publi and should be first",
        )
        self.ds0.publication_url, self.ds2.publication_url = self.ds2.publication_url, self.ds0.publication_url
        self.ds0.save()
        self.ds2.save()
        self.assertEqual(
            [o["id"] for o in self.search("hell", sorting="TFIDF", kind='data_source')["data_source"]["sample"]],
            [self.ds0.pk, self.ds2.pk, ],
            "we swap publi, ds0 should be first",
        )

    def test_ds(self):
        for sorting, search_and_results in [
            (None, [
                ("foo", [self.ds0.pk, self.ds2.pk, self.ds1.pk, ]
                if connection.vendor == "sqlite" else
                [self.ds1.pk, self.ds0.pk, self.ds2.pk]),
                ("foo bar", [self.ds1.pk, self.ds0.pk, self.ds2.pk]),
                ("foo bar +ll", [self.ds0.pk, self.ds2.pk, ]),
                ("foo bar -ll", [self.ds1.pk, ]),
                ("foo bar -LL", [self.ds1.pk, ]),
                ("\"bar lol\"", [self.ds1.pk, ]),
                ("foo bar +\"bar lol\"", [self.ds1.pk, ]),
                ("foo bar -\"bar lol\"", [self.ds0.pk, self.ds2.pk]),
                ("\"foo bar\"", [self.ds0.pk, ] if connection.vendor == "sqlite" else [self.ds1.pk, self.ds0.pk]),
                ("\"l\"", [self.ds2.pk, self.ds1.pk, self.ds0.pk]),
                ("\"w\"", [self.ds2.pk, self.ds0.pk]),
                ("\"ö\"", [self.ds1.pk]
                if connection.vendor == "sqlite" else
                [self.ds1.pk, self.ds2.pk, self.ds0.pk]),
                ("\"l\" +lorem", [self.ds2.pk, ]),
                ("\"l\" -lorem", [self.ds1.pk, self.ds0.pk, ]),
                ("-lorem", []),
                ("+lorem", [self.ds2.pk, ]),
            ]),
            ("TFIDF", [
                ("foo", [self.ds2.pk, self.ds0.pk, self.ds1.pk, ]
                if connection.vendor == "sqlite" else
                [self.ds1.pk, self.ds2.pk, self.ds0.pk]),
                ("\"ö\"", [self.ds1.pk]
                if connection.vendor == "sqlite" else
                [self.ds2.pk, self.ds1.pk, self.ds0.pk]),
            ]),
        ]:
            for s, ids_expected in search_and_results:
                d = dict(search=s, kind='data_source')
                if sorting is not None:
                    d["sorting"] = sorting
                response = self.client.get(self.url + urllib.parse.urlencode(d))
                self.assertEqual(response.status_code, 200)
                response = json.loads(response.content)
                if 'app_error_code' in response:
                    ids = []
                else:
                    ids = [o["id"] for o in response["data_source"]["sample"]]
                self.assertEqual(ids, ids_expected, "failed with " + str(s) + " " + json.dumps(d, indent=4))

    def test_app_error_code(self):
        self.assertEqual(
            self.search("r"),
            {
                'err': 'Single letter query are not permitted, put it between double quote to override',
                'app_error_code': 2,
                'searched_text': 'r'
            },
        )
        self.assertEqual(
            self.search("name:"),
            {
                'err': 'Invalid query',
                'app_error_code': 1,
                'searched_text': ''
            },
        )

    def test_virus(self):
        # for o in models.Virus.objects.all():
        #     print(o.pk, o.name, o.identifier)
        self.assertIn(models.Virus.objects.get(identifier="NC_00141454566 variant T").pk,
                      set(x["id"] for x in self.search("\"r\"", kind='virus')["virus"]["sample"]))
        self.assertIn(models.Virus.objects.get(identifier="NC_00141454566 variant T").pk,
                      set(x["id"] for x in self.search("\"r\" \"t\"", kind='virus')["virus"]["sample"]))
        self.assertIn(models.Virus.objects.get(identifier="blabla").pk,
                      set(x["id"] for x in self.search("\"r\" \"b\"", kind='virus')["virus"]["sample"]))
        viruses = self.search("\"r\" \"1\"", kind='virus')["virus"]["sample"]
        self.assertEqual(
            [
                {viruses[0]["id"]},
                {
                    viruses[1]["id"],
                    viruses[2]["id"]
                },
                {viruses[3]["id"]},
            ],
            [
                {models.Virus.objects.get(identifier="NC_001416").pk},
                {
                    models.Virus.objects.get(identifier="NC_00141454566").pk,
                    models.Virus.objects.get(identifier="NC_00141454566 variant T").pk,
                },
                {models.Virus.objects.get(name="r1", identifier="").pk},
            ]
        )

        self.assertEqual([o["id"] for o in self.search("r1 -NC", kind='virus')["virus"]["sample"]],
                         [models.Virus.objects.get(name="r1", identifier="").pk, ], )
        self.assertEqual([o["id"] for o in self.search("r1 -nc", kind='virus')["virus"]["sample"]],
                         [models.Virus.objects.get(name="r1", identifier="").pk, ], )
        self.assertEqual([o["id"] for o in self.search("r1 -Nc", kind='virus')["virus"]["sample"]],
                         [models.Virus.objects.get(name="r1", identifier="").pk, ], )
        self.assertEqual(
            [o["id"] for o in self.search("+\"NC_00141454566 variant T\"", kind='virus')["virus"]["sample"]],
            [models.Virus.objects.get(identifier="NC_00141454566 variant T").pk, ], )

        v = models.Virus.objects.first()
        v.her_identifier = 424666
        v.save()
        self.assertEqual([o["id"] for o in self.search("424666", kind='virus')["virus"]["sample"]],
                         [v.pk, ], )
        self.assertIn(v.pk, [o["id"] for o in self.search("424666 " + v.name, kind='virus')["virus"]["sample"]])
        self.assertEqual([o["id"] for o in self.search("+424666 " + v.name, kind='virus')["virus"]["sample"]],
                         [v.pk, ], )
        self.assertEqual([o["id"] for o in self.search("+424666", kind='virus')["virus"]["sample"]],
                         [v.pk, ], )
        self.assertEqual([o["id"] for o in self.search("+42466", kind='virus')["virus"]["sample"]],
                         [v.pk, ], )
        self.assertEqual([o["id"] for o in self.search("+24666", kind='virus')["virus"]["sample"]],
                         [v.pk, ], )
        self.assertEqual([o["id"] for o in self.search("+24 +66", kind='virus')["virus"]["sample"]],
                         [v.pk, ], )

    def test_host(self):
        # for o in models.Host.objects.all():
        #     print(o.pk, o.name, o.identifier)
        response = self.search("\"c\"", kind='host')
        self.assertIn(models.Host.objects.get(identifier="HGC2738867.3").pk,
                      set(x["id"] for x in response["host"]["sample"]))

        response = self.search("hg 27", kind='host')
        self.assertEqual(set([o["id"] for o in response["host"]["sample"][:2]]), {
            models.Host.objects.get(identifier="HGC2738867.3").pk,
            models.Host.objects.get(identifier="HG738867.1").pk,
        }, )

        self.assertEqual([o["id"] for o in self.search("+C hg -hgc", kind='host')["host"]["sample"]],
                         [models.Host.objects.get(name="C1", identifier="HG738867.1").pk, ], )
        self.assertEqual([o["id"] for o in self.search("+c Hg -hGc", kind='host')["host"]["sample"]],
                         [models.Host.objects.get(name="C1", identifier="HG738867.1").pk, ], )

    def test_tax_id(self):
        response = self.search("10665")
        self.assertEqual(response["host"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 0)
        o = models.Host.objects.get(identifier="HGC2738867.3")
        o.is_ncbi_identifier_value = True
        o.tax_id_value = "10655"
        o.save()

        response = self.search("10655")
        self.assertEqual(response["host"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 0)

        o = models.Virus.objects.get(identifier="NC_00141454566")
        o.is_ncbi_identifier_value = True
        o.tax_id_value = "10666"
        o.save()
        response = self.search("10666")
        self.assertEqual(response["host"]["count"], 0)
        self.assertEqual(response["virus"]["count"], 1)

        response = self.search("106")
        self.assertEqual(response["host"]["count"], 1)
        self.assertEqual(response["virus"]["count"], 1)
