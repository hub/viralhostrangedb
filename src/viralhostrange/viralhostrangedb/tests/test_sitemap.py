import logging
import os

from django.contrib.auth import get_user_model

from viralhostrangedb import models, business_process
from viralhostrangedb.tests.test_views_others import TooledTestCase

logger = logging.getLogger(__name__)


class SiteMapTestCase(TooledTestCase):
    test_data = "./test_data"

    def setUp(self) -> None:
        super().setUp()
        ################################################################################
        self.user = get_user_model().objects.create(
            username="user",
            email="b@a.a",
        )
        self.user.set_password("qdfjbqmoefbqzmeofibzeq90812çààç")
        self.user.save()

        ################################################################################
        self.private_data_source = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.private_data_source,
            file=filename,
        )

        ################################################################################
        self.public_data_source = models.DataSource.objects.create(
            owner=self.user,
            public=True,
            name="private user",
            raw_name="ee",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "simple.xlsx")
        business_process.import_file(
            data_source=self.public_data_source,
            file=filename,
        )

    def test_works(self):
        response = self.client.get("/sitemap.xml")
        self.assertEqual(response.status_code, 200)
        resp = str(response.content)

        self.assertIn(self.public_data_source.get_absolute_url(), resp)
        self.assertNotIn(self.private_data_source.get_absolute_url(), resp)
        self.assertIn("about", resp)
