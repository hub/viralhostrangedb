import logging
import os

from django.contrib.auth import get_user_model
from django.core import management
from django.utils import timezone

from viralhostrangedb import business_process, models
from viralhostrangedb.tests.base_test_case import TooledTestCase

logger = logging.getLogger(__name__)


class ResponseException(Exception):
    pass


class VirusException(Exception):
    pass


class HostException(Exception):
    pass


class NoErrorImportationObserver(business_process.MessageImportationObserver):
    class Meta:
        abstract = True

    def notify_response_error(self, virus, host, response_str):
        raise ResponseException(
            "[ImportErr2] " +
            "Could not import response \"%(response)s\" for virus \"%(virus)s\", host\"%(host)s\"" % dict(
                response=str(response_str),
                virus=str(virus),
                host=str(host),
            )
        )

    def notify_host_error(self, host, column_id, reason=None, reason_id=None):
        msg = "[ImportErr1] " + "Could not parse host \"%(host)s\" at column \"%(column_id)s\" (i.e column \"%(column_index)s\")" % dict(
            host=str(host),
            column_id=str(column_id),
            column_index=str(self.id_to_excel_index(column_id)),
        )
        if reason:
            msg = self.reason_to_str(msg, reason)
        raise HostException(msg)

    def notify_virus_error(self, virus, row_id, reason=None, reason_id=None):
        msg = "[ImportErr3] " + "Could not parse virus \"%(virus)s\" at row \"%(row_id)s\"" % dict(
            virus=str(virus),
            row_id=str(row_id),
        )
        if reason:
            msg = self.reason_to_str(msg, reason, reason_id)
        raise VirusException(msg)


class NoErrorImportationObserverTestCase(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        self.owner = get_user_model().objects.create(
            username="root",
        )
        self.data_source = models.DataSource.objects.create(
            name="data_source",
            raw_name="e",
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )

    def test_wrong_response(self):
        self.assertRaises(
            ResponseException,
            business_process.import_file,
            data_source=self.data_source,
            file=os.path.join(self.test_data, "fails.xlsx"),
            importation_observer=NoErrorImportationObserver(request=None),
        )
        self.assertRaises(
            VirusException,
            business_process.import_file,
            data_source=self.data_source,
            file=os.path.join(self.test_data, "too_long_virus_name.xlsx"),
            importation_observer=NoErrorImportationObserver(request=None),
        )
        self.assertRaises(
            HostException,
            business_process.import_file,
            data_source=self.data_source,
            file=os.path.join(self.test_data, "missing_host.xlsx"),
            importation_observer=NoErrorImportationObserver(request=None),
        )


class DataFileReaderWithIdentifierTestCase(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        self.owner = get_user_model().objects.create(
            username="root",
        )

    def test_import_in_db(self):
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        data_source, created = models.DataSource.objects.get_or_create(
            name="data_source",
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source,
            file=filename,
            importation_observer=NoErrorImportationObserver(request=None),
        )
        for o in list(data_source.virus_set.all())[2:]:
            o.responseindatasource.all().delete()
            o.delete()
        for o in list(data_source.host_set.all())[2:]:
            o.responseindatasource.all().delete()
            o.delete()
        filename = os.path.join(self.test_data, "simple.xlsx")
        data_source_simple, created = models.DataSource.objects.get_or_create(
            name="data_source_simple",
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=data_source_simple,
            file=filename,
            importation_observer=NoErrorImportationObserver(request=None),
        )

        self.assertEqual(models.Virus.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source.virus_set.count())
        self.assertEqual(models.Host.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source.host_set.count())

        management.call_command('fetch_identifier_status', virus=True, progress=True)
        self.assertEqual(models.Virus.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         0)
        self.assertEqual(models.Host.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source.host_set.count())

        management.call_command('fetch_identifier_status', virus=True, reset=True)
        self.assertEqual(models.Virus.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         models.Virus.objects.count())
        self.assertEqual(models.Host.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source.host_set.count())
        self.assertEqual(data_source_simple.virus_set.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source_simple.virus_set.count())
        self.assertEqual(data_source_simple.host_set.filter(is_ncbi_identifier_value__isnull=True).count(), 0)

        management.call_command('fetch_identifier_status', host=True, progress=True)
        self.assertEqual(models.Virus.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         models.Virus.objects.count())
        self.assertEqual(models.Host.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         0)

        management.call_command('fetch_identifier_status', host=True, reset=True)
        self.assertEqual(models.Virus.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         models.Virus.objects.count())
        self.assertEqual(models.Host.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         models.Host.objects.count())
        self.assertEqual(data_source_simple.virus_set.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source_simple.virus_set.count())
        self.assertEqual(data_source_simple.host_set.filter(is_ncbi_identifier_value__isnull=True).count(),
                         data_source_simple.host_set.count())

        management.call_command('fetch_identifier_status', all=True)
        self.assertEqual(models.Virus.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         0)
        self.assertEqual(models.Host.objects.filter(is_ncbi_identifier_value__isnull=True).count(),
                         0)
        self.assertEqual(data_source_simple.virus_set.filter(is_ncbi_identifier_value__isnull=True).count(), 0)
        self.assertEqual(data_source_simple.host_set.filter(is_ncbi_identifier_value__isnull=True).count(), 0)
        self.assertEqual(data_source_simple.virus_set.filter(is_ncbi_identifier_value=True).count(), 0)
        self.assertEqual(data_source_simple.host_set.filter(is_ncbi_identifier_value=True).count(), 0)


class DummyCommandTestCase(TooledTestCase):
    def test_works(self):
        management.call_command('trim_backups_history')
        self.assertTrue(True)
