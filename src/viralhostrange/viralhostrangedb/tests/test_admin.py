import os

from basetheme_bootstrap.user_preferences_utils import get_user_preference_class, get_user_preferences_for_user
from django.contrib.admin import AdminSite
from django.contrib.admin import models as admin_models
from django.contrib.auth import get_user_model, get_permission_codename
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.test import  RequestFactory
from django.urls import reverse, NoReverseMatch
from django.utils import timezone

from viralhostrangedb import models, business_process, admin
from viralhostrangedb.tests.base_test_case import OneDataSourceTestCase
from viralhostrangedb.tests.test_views_others import TooledTestCase


class ModelAdminTests(TooledTestCase):
    test_data = "./test_data"

    def setUp(self):
        super().setUp()
        ################################################################################ 
        self.user = get_user_model().objects.create(
            username="root",
        )

        ################################################################################
        self.site = AdminSite()

        ################################################################################
        self.lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="Infection")
        self.weak = models.GlobalViralHostResponseValue.objects_mappable().get(name="Intermediate")
        self.no_lysis = models.GlobalViralHostResponseValue.objects_mappable().get(name="No infection")
        self.not_mapped_yet = models.GlobalViralHostResponseValue.get_not_mapped_yet()

        ################################################################################
        filename = os.path.join(self.test_data, "simple-with-id.xlsx")
        self.data_source, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.user,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=self.data_source,
            file=filename,
        )

        ################################################################################
        self.data_source_mapped = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="private user mapped",
            raw_name="ee",
            kind="FILE",
            publication_url="https://toto.com/tatata",
        )
        self.data_source_mapped.allowed_users.add(self.user)
        self.data_source_mapped.save()
        filename = os.path.join(self.test_data, "three_reponse_simple_2.xlsx")
        business_process.import_file(
            data_source=self.data_source_mapped,
            file=filename,
        )

        url = reverse('viralhostrangedb:data-source-mapping-label-edit',
                      args=[self.data_source_mapped.pk])
        self.client.force_login(self.user)

        self.assertFalse(self.data_source_mapped.is_mapping_done)

        form_data = {
            "form-TOTAL_FORMS": 3,
            "form-INITIAL_FORMS": 3,
            "form-MIN_NUM_FORMS": 0,
            "form-MAX_NUM_FORMS": 1000,
            "form-0-raw_response": 0.0,
            "form-0-mapping": self.no_lysis.pk,
            "form-1-raw_response": 1.0,
            "form-1-mapping": self.weak.pk,
            "form-2-raw_response": 2.0,
            "form-2-mapping": self.lysis.pk,
        }

        self.client.post(url, form_data)
        self.client.logout()

    # def test_has_publication(self):
    #     ma = admin.DataSourceAdmin(self.data_source, self.site)
    #     self.assertFalse(ma.has_publication(self.data_source))
    #     self.assertTrue(ma.has_publication(self.data_source_mapped))

    # def test_is_mapping_done(self):
    #     ma = admin.DataSourceAdmin(self.data_source, self.site)
    #     self.assertEqual(ma.is_mapping_done(self.data_source), self.data_source.is_mapping_done)
    #     self.assertEqual(ma.is_mapping_done(self.data_source_mapped), self.data_source_mapped.is_mapping_done)

    # def test_ViralHostResponseValueInDataSourceAdmin(self):
    #     ma = admin.ViralHostResponseValueInDataSourceAdmin(
    #         models.ViralHostResponseValueInDataSource.objects.first(), self.site)
    #     for o in [
    #         models.Virus.objects.first(),
    #         models.Virus.objects.filter(identifier="").first(),
    #         models.Virus.objects.filter(~Q(identifier="")).first(),
    #     ]:
    #         self.assertEqual(ma.virus_name(o.responseindatasource.first()), o.explicit_name)
    #     for o in [
    #         models.Host.objects.first(),
    #         models.Host.objects.filter(identifier="").first(),
    #         models.Host.objects.filter(~Q(identifier="")).first(),
    #     ]:
    #         self.assertEqual(ma.host_name(o.responseindatasource.first()), o.explicit_name)

    def test_GlobalViralHostResponseValueAdmin(self):
        opts = models.GlobalViralHostResponseValue._meta
        codename = "change_globalviralhostresponsevalue"
        permission_str = "viralhostrangedb." + codename
        self.assertEqual(
            permission_str,
            "%s.%s" % (opts.app_label, get_permission_codename('change', opts)),
        )
        ma = admin.GlobalViralHostResponseValueAdmin(
            models.GlobalViralHostResponseValue.objects.first(), self.site)

        # common user
        request = RequestFactory().get('/blabla')
        request.user = get_user_model().objects.get(pk=self.user.pk)

        self.assertFalse(self.user.has_perm(permission_str))
        self.assertIn('name', ma.get_readonly_fields(request))
        self.assertIn('value', ma.get_readonly_fields(request))
        self.assertFalse(ma.has_change_permission(request))

        # moderator
        moderator = Group.objects.get(name__iexact="moderator")
        moderator.user_set.add(self.user)
        self.user.groups.add(moderator)
        request = RequestFactory().get('/blabla')
        request.user = get_user_model().objects.get(pk=self.user.pk)

        self.assertFalse(self.user.has_perm(permission_str))
        self.assertIn('name', ma.get_readonly_fields(request))
        self.assertIn('value', ma.get_readonly_fields(request))
        self.assertTrue(ma.has_change_permission(request))

        # root
        self.user.is_superuser = True
        self.user.save()
        request = RequestFactory().get('/blabla')
        request.user = get_user_model().objects.get(pk=self.user.pk)

        self.assertTrue(self.user.has_perm(permission_str))
        self.assertNotIn('name', ma.get_readonly_fields(request))
        self.assertIn('value', ma.get_readonly_fields(request))
        self.assertTrue(ma.has_change_permission(request))

    # def test_reset_mapping(self):
    #     admin.reset_mapping(None, None, models.DataSource.objects.filter(pk=self.data_source.pk))
    #     self.assertFalse(self.data_source.is_mapping_done)
    #     self.assertTrue(self.data_source_mapped.is_mapping_done)
    #     admin.reset_mapping(None, None, models.DataSource.objects.filter(pk=self.data_source_mapped.pk))
    #     self.assertFalse(self.data_source.is_mapping_done)
    #     self.assertFalse(self.data_source_mapped.is_mapping_done)
    #
    # def test_purge_without_data_source(self):
    #     virus_cpt = models.Virus.objects.count()
    #     host_cpt = models.Host.objects.count()
    #     admin.purge_without_data_source(None, None, models.Virus.objects.all())
    #     self.assertEqual(virus_cpt, models.Virus.objects.count())
    #     self.assertEqual(host_cpt, models.Host.objects.count())
    #     admin.purge_without_data_source(None, None, models.Host.objects.all())
    #     self.assertEqual(virus_cpt, models.Virus.objects.count())
    #     self.assertEqual(host_cpt, models.Host.objects.count())
    #
    #     models.Host.objects.create(name='ererzer', identifier='rr')
    #     self.assertEqual(host_cpt + 1, models.Host.objects.count())
    #     admin.purge_without_data_source(None, None, models.Virus.objects.all())
    #     self.assertEqual(host_cpt + 1, models.Host.objects.count())
    #     admin.purge_without_data_source(None, None, models.Host.objects.all())
    #     self.assertEqual(host_cpt, models.Host.objects.count())
    #
    #     models.Virus.objects.create(name='ererzer', identifier='rr')
    #     self.assertEqual(virus_cpt + 1, models.Virus.objects.count())
    #     admin.purge_without_data_source(None, None, models.Host.objects.all())
    #     self.assertEqual(virus_cpt + 1, models.Virus.objects.count())
    #     admin.purge_without_data_source(None, None, models.Virus.objects.all())
    #     self.assertEqual(virus_cpt, models.Virus.objects.count())

    def test_curator(self):
        class MockModelAdmin():
            def message_user(self, *args, **kwargs):
                pass

        modeladmin = MockModelAdmin()

        admin.grant_curate_permission(modeladmin, None, type(self.user).objects.filter(pk=self.user.pk))
        self.assertTrue(business_process.is_curator(self.user))
        admin.grant_curate_permission(modeladmin, None, type(self.user).objects.filter(pk=self.user.pk))
        self.assertTrue(business_process.is_curator(self.user))

        admin.revoke_curate_permission(modeladmin, None, type(self.user).objects.filter(pk=self.user.pk))
        self.assertFalse(business_process.is_curator(self.user))
        admin.revoke_curate_permission(modeladmin, None, type(self.user).objects.filter(pk=self.user.pk))
        self.assertFalse(business_process.is_curator(self.user))


class ModelAdminOneDataSourceTests(OneDataSourceTestCase):

    def test_password_locked(self):
        self.client.force_login(self.staff_superuser)
        response = self.client.get(reverse("admin:auth_user_password_change", args=[self.user.pk]))
        self.assertIn(response.status_code, [302, 403, 404])

    def test_actions(self):
        self.client.force_login(self.staff_superuser)
        for model, model_admin in [
            # (models.DataSource, admin.DataSourceAdmin),
            # (models.Host, admin.VirusHostAdmin),
            # (admin_models.LogEntry, admin.DataSourceLogEntryAdmin),
            (get_user_model(), admin.MyUserAdmin),
        ]:
            ct = ContentType.objects.get_for_model(model)
            url = reverse("admin:%s_%s_changelist" % (ct.app_label, ct.model))
            for action in model_admin.actions:
                self.client.post(
                    url,
                    {
                        'action': action.__name__,
                        '_selected_action': model.objects.values_list('pk', flat=True),
                    },
                    follow=True,
                )

    def test_privacy(self):
        self.client.force_login(self.staff_superuser)
        for model in [
            models.DataSource,
            models.Host,
            models.Virus,
            models.ViralHostResponseValueInDataSource,
            Group,
            admin_models.LogEntry
        ]:
            ct = ContentType.objects.get_for_model(model)
            self.assertRaises(NoReverseMatch, reverse, "admin:%s_%s_changelist" % (ct.app_label, ct.model))

    def test_user_view(self):
        url = reverse("admin:auth_user_change", args=[self.user.pk])

        self.client.force_login(self.user)
        self.assertIn(self.client.get(url).status_code, [302, 403, 404])

        self.client.force_login(self.superuser)
        self.assertIn(self.client.get(url).status_code, [302, 403, 404])

        self.client.force_login(self.u_read)
        self.assertIn(self.client.get(url).status_code, [302, 403, 404])

        self.client.force_login(self.staff)
        self.assertIn(self.client.get(url).status_code, [302, 403, 404])

        self.client.force_login(self.staff_superuser)
        self.assertEqual(self.client.get(url).status_code, 200)

        self.client.force_login(self.staff_admin)
        self.assertEqual(self.client.get(url).status_code, 200)

    def test_pref_view(self):
        ct = ContentType.objects.get_for_model(get_user_preference_class())
        view_name = "admin:%s_%s_change" % (ct.app_label, ct.model)
        get_user_preferences_for_user(self.staff_admin)
        for p in get_user_preference_class().objects.all():
            url = reverse(view_name, args=[p.pk])

            self.client.force_login(self.staff)
            self.assertIn(self.client.get(url).status_code, [302, 403, 404])

            self.client.force_login(self.staff_admin)
            self.assertIn(self.client.get(url).status_code, [302, 403, 404])

            self.client.force_login(self.staff_superuser)
            self.assertEqual(self.client.get(url).status_code, 200)
