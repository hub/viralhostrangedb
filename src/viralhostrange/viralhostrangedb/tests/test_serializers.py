import json
import logging

from django.db.models import Value, IntegerField
from rest_framework import serializers

from viralhostrangedb import models
from viralhostrangedb.serializers import HitOrder
from viralhostrangedb.tests.test_views_others import TooledTestCase

logger = logging.getLogger(__name__)


class DemoTestCase(TooledTestCase):
    def test_HitOrderSuperClass(self):
        class MyVirusSerializer(HitOrder, serializers.ModelSerializer):
            class Meta:
                model = models.Virus
                depth = 1
                fields = (
                    "name",
                    "identifier",
                    "is_ncbi_identifier_value",
                    "her_identifier",
                    "tax_id",
                    "id",
                )

        v1, _ = models.Virus.objects.get_or_create(name="v1")
        self.assertFalse(v1.is_ncbi_identifier)
        viruses = models.Virus.objects.all()
        viruses = viruses.annotate(azeaze=Value(555, output_field=IntegerField()))
        self.assertEqual(
            json.loads(json.dumps(MyVirusSerializer(viruses, many=True).data)),
            [
                {
                    "name": "v1",
                    "identifier": "",
                    "is_ncbi_identifier_value": False,
                    "her_identifier": None,
                    "tax_id": None,
                    "id": v1.id,
                    "relevance": -1,
                    "has_identifier": -1,
                }
            ],
        )
        viruses = viruses.annotate(relevance=Value(666, output_field=IntegerField()))
        self.assertEqual(
            json.loads(json.dumps(MyVirusSerializer(viruses, many=True).data)),
            [
                {
                    "name": "v1",
                    "identifier": "",
                    "is_ncbi_identifier_value": False,
                    "her_identifier": None,
                    "tax_id": None,
                    "id": v1.id,
                    "relevance": 666,
                    "has_identifier": -1,
                }
            ],
        )
        viruses = viruses.annotate(
            has_identifier=Value(777, output_field=IntegerField())
        )
        self.assertEqual(
            json.loads(json.dumps(MyVirusSerializer(viruses, many=True).data)),
            [
                {
                    "name": "v1",
                    "identifier": "",
                    "is_ncbi_identifier_value": False,
                    "her_identifier": None,
                    "tax_id": None,
                    "id": v1.id,
                    "relevance": 666,
                    "has_identifier": 777,
                }
            ],
        )
