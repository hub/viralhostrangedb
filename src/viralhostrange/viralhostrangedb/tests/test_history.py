import errno
import json
import logging
import os
import shutil
from tempfile import NamedTemporaryFile

from django.conf import settings
from django.contrib.admin import models as admin_models
from django.contrib.contenttypes.models import ContentType
from django.test import override_settings, RequestFactory
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time

from viralhostrangedb import models, business_process
from viralhostrangedb.tests import base_test_case

logger = logging.getLogger(__name__)


@override_settings(BACKUP_LOCATION=os.path.join(
    settings.BASE_DIR,
    'persistent_volume',
    f'data_source_backup_for_tests_in_{__name__.split(".")[-1]}',
))
class DataSourceHistoryTestCase(base_test_case.OneDataSourceTestCase):
    def tearDown(self) -> None:
        super().tearDown()
        if 'data_source_backup_for_tests' in settings.BACKUP_LOCATION:
            shutil.rmtree(settings.BACKUP_LOCATION)

    def setUp(self) -> None:
        try:
            os.mkdir(settings.BACKUP_LOCATION)
        except OSError as e:
            assert e.errno == errno.EEXIST, e
        super().setUp()

    def test_access(self):
        self.data_source_data_upload(filename="three_reponse_simple.xlsx", data_source=self.ds)

        url_see = reverse('viralhostrangedb:data-source-history', args=[self.ds.pk])
        le = admin_models.LogEntry.objects.filter(object_id=self.ds.pk).first()
        url_get = reverse('viralhostrangedb:data-source-history-download', args=[self.ds.pk, le.pk])
        url_restore = reverse('viralhostrangedb:data-source-history-restoration', args=[self.ds.pk, le.pk])

        self.client.force_login(self.u_none)
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.client.force_login(self.u_read)
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.client.force_login(self.u_write)
        self.assertEqual(self.client.get(url_see).status_code, 200)
        self.assertEqual(self.client.get(url_get).status_code, 200)
        self.assertEqual(self.client.get(url_restore).status_code, 200)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 302)

        self.client.force_login(self.u_curator)
        self.ds.public = True
        self.ds.save()
        self.assertEqual(self.client.get(url_see).status_code, 200)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)
        self.ds.public = False
        self.ds.save()
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.client.force_login(self.user)
        self.assertEqual(self.client.get(url_see).status_code, 200)
        self.assertEqual(self.client.get(url_get).status_code, 200)
        self.assertEqual(self.client.get(url_restore).status_code, 200)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 302)

        self.client.force_login(self.superuser)
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.client.force_login(self.staff)
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.client.force_login(self.staff_superuser)
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.delete_data_source(self.ds)

        for u in [
            self.u_none,
            self.u_read,
            self.u_write,
            self.user,
            self.staff,
        ]:
            self.client.force_login(u)
            self.assertEqual(self.client.get(url_see).status_code, 404, u)
            self.assertEqual(self.client.get(url_get).status_code, 404, u)
            self.assertEqual(self.client.get(url_restore).status_code, 404)
            self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

        self.client.force_login(self.superuser)
        self.assertEqual(self.client.get(url_see).status_code, 404)
        self.assertEqual(
            self.client.get(url_get).status_code,
            200,
            "Yes superuser can see backup of delete data source",
        )
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

    def test_cross_data_source(self):
        self.data_source_data_upload(filename="three_reponse_simple.xlsx", data_source=self.ds)
        data_source = self.data_source_file_import(user=self.user_b, filename="three_reponse_simple_2.xlsx")
        self.client.force_login(self.user_b)
        url_see = reverse('viralhostrangedb:data-source-history', args=[data_source.pk])
        le = admin_models.LogEntry.objects.filter(object_id=self.ds.pk).first()
        url_get = reverse('viralhostrangedb:data-source-history-download', args=[data_source.pk, le.pk])
        url_restore = reverse('viralhostrangedb:data-source-history-restoration', args=[data_source.pk, le.pk])
        self.assertEqual(self.client.get(url_see).status_code, 200)
        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

    def test_when_backup_is_missing(self):
        self.response_update(
            data_source=self.ds,
            virus=self.ds.virus_set.first(),
            host=self.ds.host_set.first(),
            new_raw_response=666,
        )
        self.client.force_login(self.ds.owner)

        le = self.get_log_entries(self.ds).get()
        os.unlink(business_process.get_backup_file_path(le))

        url_get = reverse('viralhostrangedb:data-source-history-download', args=[self.ds.pk, le.pk])
        url_restore = reverse('viralhostrangedb:data-source-history-restoration', args=[self.ds.pk, le.pk])

        self.assertEqual(self.client.get(url_get).status_code, 404)
        self.assertEqual(self.client.get(url_restore).status_code, 404)
        self.assertEqual(self.client.post(url_restore, dict(agree="True")).status_code, 404)

    def test_create_backup_when_missing(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_after:
            self.assertEqual(self.get_log_entries(self.ds).count(), 0)

            # creating a backup
            business_process.create_backup_when_missing(models.DataSource.objects.all())
            self.assertEqual(self.get_log_entries(self.ds).count(), 1)

            # no need to create a new backup there is already one
            business_process.create_backup_when_missing(models.DataSource.objects.all(),
                                                        request=RequestFactory().get('/blabla'))
            self.assertEqual(self.get_log_entries(self.ds).count(), 1)

            # delete backup file, new backup needed
            le = self.get_log_entries(self.ds).first()
            os.unlink(business_process.get_backup_file_path(le))
            business_process.create_backup_when_missing(models.DataSource.objects.all())
            self.assertEqual(self.get_log_entries(self.ds).count(), 2)

            # no need to create a new backup there is already one
            business_process.create_backup_when_missing(models.DataSource.objects.all())
            le_2 = self.get_log_entries(self.ds).first()
            self.assertEqual(self.get_log_entries(self.ds).count(), 2)

            # check the the backup file is here
            self.assertIsNotNone(business_process.get_backup_file_path(
                self.get_log_entries(self.ds).first(),
                test_if_exists=True,
            ))
            # check the the backup file it is sound
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertBackUpIs(self.ds, vhrs_dict_after, le_count=2)

            # new log entry without backup
            self.data_source_data_update(description="az", data_source=self.ds)
            self.assertEqual(self.get_log_entries(self.ds).count(), 3)

            # no need to create a new backup there is already one in the past
            business_process.create_backup_when_missing(models.DataSource.objects.all())
            self.assertEqual(self.get_log_entries(self.ds).count(), 3)

            # delete the first backup in the past
            os.unlink(business_process.get_backup_file_path(le_2))
            business_process.create_backup_when_missing(models.DataSource.objects.all())
            le_4 = self.get_log_entries(self.ds).first()
            self.assertEqual(self.get_log_entries(self.ds).count(), 4)

            # update a response which will trigger a backup
            self.response_update(
                data_source=self.ds,
                virus=self.ds.virus_set.first(),
                host=self.ds.host_set.first(),
                new_raw_response=1000,
            )

            # delete the second backup
            os.unlink(business_process.get_backup_file_path(le_4))
            business_process.create_backup_when_missing(models.DataSource.objects.all())
            self.assertEqual(self.get_log_entries(self.ds).count(), 5)

            # no need to create a new backup there is already one from the response update
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertBackUpIs(self.ds, vhrs_dict_after, le_count=5, fields=["response"])

    def assertBackUpIs(self, data_source, vhrs_dict_after, fields=None, le_count=1, mapping=None, has_backup=True,
                       by=None):
        les = self.get_log_entries(data_source)
        self.assertEqual(les.count(), le_count)
        le = les.first()
        change_message = json.loads(le.change_message)[0]
        self.assertEqual(change_message.get("has_backup", False), has_backup)
        if le.action_flag == admin_models.CHANGE:
            self.assertSetEqual(set(change_message["changed"]["fields"]), set(fields or []))
        self.assertEqual(le.user, data_source.owner if by is None else by)
        file_path = business_process.get_backup_file_path(le, test_if_exists=True)
        self.assertTrue((file_path is not None) or (not has_backup))
        if has_backup:
            vhrs_backup = business_process.parse_file(file_path)
            vhrs_dict_backup = business_process.to_dict(vhrs_backup)
            self.assertEquals(vhrs_dict_after, vhrs_dict_backup)
            if mapping:
                mapping_backup = business_process.parse_mapping(file_path)
                self.assertEquals(mapping, mapping_backup)

        ct = ContentType.objects.get_for_model(admin_models.LogEntry)
        urls_to_test = [
            # (reverse("admin:%s_%s_change" % (ct.app_label, ct.model), args=(le.id,)), self.staff_superuser, True),
            # (reverse("admin:%s_%s_changelist" % (ct.app_label, ct.model)), self.staff_superuser, True),
        ]
        if models.DataSource.objects.filter(id=data_source.id).exists():
            urls_to_test += [
                # (
                #     reverse("admin:%s_%s_changelist" % (ct.app_label, ct.model)) + "?object_id=-1",
                #     self.staff_superuser,
                #     True,
                # ),
                # (
                #     reverse("admin:%s_%s_changelist" % (ct.app_label, ct.model)) + "?object_id=" + le.object_id,
                #     self.staff_superuser,
                #     True,
                # ),
                (
                    reverse("viralhostrangedb:data-source-history", args=(data_source.id,)),
                    data_source.owner,
                    True,
                ),
                (
                    reverse("viralhostrangedb:data-source-history-restoration", args=(le.object_id, le.id)),
                    data_source.owner,
                    has_backup,
                ),
            ]
        for url, user, is_200 in urls_to_test:
            self.client.force_login(user)
            response = self.client.get(url)
            self.assertEqual(response.status_code, 200 if is_200 else 404)

    def get_log_entries(self, data_source):
        return business_process.get_log_entries(data_source=data_source)

    def test_data_source_data_update_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            self.data_source_data_update(self.ds, description="blabla", name="titi")
            self.assertEqual(self.ds.description, "blabla")
            self.assertEqual(self.ds.name, "titi")

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_before)

            self.assertBackUpIs(self.ds, vhrs_dict_after, fields=["description", "name"], has_backup=False)

    def test_data_source_data_upload_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # load reference file
            filename = "three_reponse_simple_2.xlsx"
            vhrs_reference = business_process.parse_file(os.path.join(self.test_data, filename))
            vhrs_dict_reference = business_process.to_dict(vhrs_reference)

            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)
            self.assertNotEquals(vhrs_dict_before, vhrs_dict_reference)

            self.data_source_data_upload(filename=filename, data_source=self.ds)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_reference)

            self.assertBackUpIs(self.ds, vhrs_dict_after, fields=['viruses', 'responses', 'hosts', ])

    def test_data_source_host_update_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            changes = {}

            o_set = list(self.ds.host_set.all())
            o = o_set[0]
            changes[o.id] = dict(name='blabla')
            vhrs_dict_before[business_process.explicit_item('blabla')] = vhrs_dict_before[o.explicit_name]
            del vhrs_dict_before[o.explicit_name]

            o = o_set[1]
            changes[o.id] = dict(identifier='tralala')
            vhrs_dict_before[business_process.explicit_item(o.name, 'tralala')] = vhrs_dict_before[o.explicit_name]
            del vhrs_dict_before[o.explicit_name]

            self.data_source_host_update(data_source=self.ds, changes=changes)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_before)

            self.assertBackUpIs(self.ds, vhrs_dict_after, fields=["2 hosts", ])

    def test_data_source_virus_update_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            changes = {}

            o_set = list(self.ds.virus_set.all())
            o = o_set[0]
            changes[o.id] = dict(name='blabla')
            for v in vhrs_dict_before.values():
                v[business_process.explicit_item('blabla')] = v[o.explicit_name]
                del v[o.explicit_name]

            o = o_set[1]
            changes[o.id] = dict(identifier='tralala')
            for v in vhrs_dict_before.values():
                v[business_process.explicit_item(o.name, 'tralala')] = v[o.explicit_name]
                del v[o.explicit_name]

            o = o_set[2]
            changes[o.id] = dict(her_identifier=666)
            for v in vhrs_dict_before.values():
                v[business_process.explicit_item(o.name, o.identifier, 666)] = v[o.explicit_name]
                del v[o.explicit_name]

            self.data_source_virus_update(data_source=self.ds, changes=changes)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_before)

            self.assertBackUpIs(self.ds, vhrs_dict_after, fields=["3 viruses", ])

    def test_data_source_host_delete_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            o_set = list(self.ds.host_set.all())
            changes = set()
            for i in [0, 2]:
                changes.add(o_set[i].id)
                del vhrs_dict_before[o_set[i].explicit_name]

            self.data_source_host_delete(data_source=self.ds, changes=changes)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_before)

            self.assertBackUpIs(self.ds, vhrs_dict_after)

    def test_data_source_virus_delete_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            o_set = list(self.ds.virus_set.all())
            changes = set()
            for i in [0, 1]:
                changes.add(o_set[i].id)
                for v in vhrs_dict_before.values():
                    del v[o_set[i].explicit_name]

            self.data_source_virus_delete(data_source=self.ds, changes=changes)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_before)

            self.assertBackUpIs(self.ds, vhrs_dict_after)

    def test_file_import_log_entry(self):
        filename = "three_reponse_simple_2.xlsx"
        with  NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            vhrs_reference = business_process.parse_file(os.path.join(self.test_data, filename))
            vhrs_dict_reference = business_process.to_dict(vhrs_reference)

            data_source = self.data_source_file_import(user=self.user, filename=filename)

            business_process.export_data_source_to_file(data_source=data_source, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_reference)

            self.assertBackUpIs(data_source, vhrs_dict_after)

    def test_response_update_log_entry(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            virus = self.ds.virus_set.first()
            host = self.ds.host_set.first()
            new_raw_response = 666

            vhrs_dict_before[host.explicit_name][virus.explicit_name] = str(new_raw_response)

            self.response_update(data_source=self.ds, virus=virus, host=host, new_raw_response=new_raw_response)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)

            self.assertEquals(vhrs_dict_after, vhrs_dict_before)

            self.assertBackUpIs(self.ds, vhrs_dict_after, fields=["response", ])

    def test_data_source_mapping_range_edit_entry(self):
        le_count = self.get_log_entries(self.ds).count()
        with  NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            self.assertEqual(
                business_process.parse_mapping(f_before.name),
                {'-1000000': {'2', '1', '3', '4', '5', '6', '7', '8', '12', '11', '10', '9'}}
            )

            self.data_source_mapping_range_edit(data_source=self.ds, weak_starts_with=4, lysis_starts_with=7)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)

            self.assertBackUpIs(
                self.ds,
                vhrs_dict_after=vhrs_dict_after,
                fields=["mapping", ],
                mapping={'0': {'2', '1', '3'}, '1': {'4', '5', '6'}, '2': {'7', '8', '12', '11', '10', '9'}},
                has_backup=False,
                le_count=le_count + 1
            )

    def test_data_source_mapping_label_edit_entry(self):
        with  NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            self.assertEqual(
                business_process.parse_mapping(f_before.name),
                {'-1000000': {'2', '1', '3', '4', '5', '6', '7', '8', '12', '11', '10', '9'}}
            )

            raw_responses = self.ds.responseindatasource.values_list('raw_response', flat=True) \
                .order_by('raw_response').distinct()
            objects_mappable = [(o.pk, o.value) for o in models.GlobalViralHostResponseValue.objects_mappable()]
            mapping = dict()
            transposed_mapping = dict()
            for i, value in enumerate(raw_responses):
                m = objects_mappable[int(value) % len(objects_mappable)]
                mapping[value] = m[0]
                transposed_mapping.setdefault(str(int(m[1])), set()).add(str(int(value)))

            self.data_source_mapping_label_edit(data_source=self.ds, mapping=mapping)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)

            self.assertBackUpIs(
                self.ds,
                vhrs_dict_after=vhrs_dict_after,
                fields=["mapping by label", ],
                mapping=transposed_mapping,
                has_backup=False,
            )

    def test_data_source_wizard_contribution_log_entry(self):
        filename = "three_reponse_simple_2.xlsx"
        with  NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            vhrs_reference = business_process.parse_file(os.path.join(self.test_data, filename))
            vhrs_dict_reference = business_process.to_dict(vhrs_reference)

            data_source = self.data_source_wizard_contribution(user=self.user, filename=filename)

            business_process.export_data_source_to_file(data_source=data_source, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)
            self.assertEquals(vhrs_dict_after, vhrs_dict_reference)

            self.assertBackUpIs(data_source, vhrs_dict_after)

    def test_delete_data_source(self):
        with  NamedTemporaryFile(suffix=".xlsx") as f_before, NamedTemporaryFile(suffix=".xlsx") as f_after:
            # export data source to temp file
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_before.name)
            vhrs_before = business_process.parse_file(f_before.name)
            vhrs_dict_before = business_process.to_dict(vhrs_before)

            self.delete_data_source(data_source=self.ds)

            self.assertBackUpIs(
                self.ds,
                vhrs_dict_after=vhrs_dict_before,
                fields=[],
            )

    def test_data_source_history_restoration(self):
        with NamedTemporaryFile(suffix=".xlsx") as f_target, \
                NamedTemporaryFile(suffix=".xlsx") as f_final, \
                NamedTemporaryFile(suffix=".xlsx") as f_after:
            first = (self.ds.virus_set.first(), self.ds.host_set.first(), 666)
            last = (self.ds.virus_set.last(), self.ds.host_set.last(), 444)

            self.assertEqual(self.get_log_entries(self.ds).count(), 0)

            self.response_update(data_source=self.ds, virus=first[0], host=first[1], new_raw_response=first[2])

            target_le = self.get_log_entries(self.ds).get()  # == assertEqual(self.get_log_entries(self.ds).count(), 1)
            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_target.name)
            vhrs_target = business_process.parse_file(f_target.name)
            vhrs_dict_target = business_process.to_dict(vhrs_target)

            self.response_update(data_source=self.ds, virus=last[0], host=last[1], new_raw_response=last[2])

            self.assertEqual(self.get_log_entries(self.ds).count(), 2)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_final.name)
            vhrs_final = business_process.parse_file(f_final.name)
            vhrs_dict_final = business_process.to_dict(vhrs_final)

            self.assertNotEquals(vhrs_dict_target, vhrs_dict_final)

            self.data_source_history_restoration(data_source=self.ds, log_entry=target_le)

            business_process.export_data_source_to_file(data_source=self.ds, file_path=f_after.name)
            vhrs_after = business_process.parse_file(f_after.name)
            vhrs_dict_after = business_process.to_dict(vhrs_after)

            self.assertEquals(vhrs_dict_target, vhrs_dict_after)

            self.assertBackUpIs(self.ds, vhrs_dict_after, fields=['hosts', 'responses', 'viruses'], le_count=3)

    def check_backup_setting_are_sounds(self):
        self.assertGreater(settings.BACKUP_MIN_OCCURRENCES_TO_KEEP, 5)
        self.assertGreater(settings.BACKUP_MIN_WEEKS_TO_KEEP, 6 * 4)

    @override_settings(BACKUP_MIN_OCCURRENCES_TO_KEEP=5)
    def test_remove_old_backup_files(self):
        one_month = timezone.now() - timezone.timedelta(weeks=4)
        after_min_month = timezone.now() - timezone.timedelta(weeks=settings.BACKUP_MIN_WEEKS_TO_KEEP - 1)
        before_min_month = timezone.now() - timezone.timedelta(weeks=settings.BACKUP_MIN_WEEKS_TO_KEEP + 1)

        self.assertLess(after_min_month, one_month)
        self.assertLess(before_min_month, after_min_month)

        new_raw_response = 666
        for d in [before_min_month, before_min_month, before_min_month, after_min_month, one_month]:
            with freeze_time(d.strftime("%Y-%m-%d")):
                self.response_update(
                    data_source=self.ds,
                    virus=self.ds.virus_set.first(),
                    host=self.ds.host_set.first(),
                    new_raw_response=new_raw_response,
                )
                new_raw_response += 1

        self.assertEqual(5, self.get_log_entries(self.ds).count())

        business_process.remove_old_backup_files(models.DataSource.objects.all())

        for le in self.get_log_entries(self.ds):
            self.assertIsNotNone(business_process.get_backup_file_path(le, True))

        self.response_update(
            data_source=self.ds,
            virus=self.ds.virus_set.first(),
            host=self.ds.host_set.first(),
            new_raw_response=new_raw_response,
        )

        business_process.remove_old_backup_files(models.DataSource.objects.all())

        self.assertEqual(6, self.get_log_entries(self.ds).count())
        for i, le in enumerate(self.get_log_entries(self.ds)):
            if i < settings.BACKUP_MIN_OCCURRENCES_TO_KEEP:
                self.assertIsNotNone(business_process.get_backup_file_path(le, True),
                                     "file of %s should still be here" % str(le.action_time))
            else:
                self.assertIsNone(business_process.get_backup_file_path(le, True),
                                  "file of %s should have been here" % str(le.action_time))

        self.response_update(
            data_source=self.ds,
            virus=self.ds.virus_set.first(),
            host=self.ds.host_set.first(),
            new_raw_response=new_raw_response + 1,
        )
        business_process.remove_old_backup_files(models.DataSource.objects.all())

        self.assertEqual(7, self.get_log_entries(self.ds).count())
        for i, le in enumerate(self.get_log_entries(self.ds)):
            if i < settings.BACKUP_MIN_OCCURRENCES_TO_KEEP:
                self.assertIsNotNone(business_process.get_backup_file_path(le, True))
            else:
                self.assertIsNone(business_process.get_backup_file_path(le, True))

    @override_settings(BACKUP_MIN_OCCURRENCES_TO_KEEP=5)
    def test_remove_old_orphan_backup_files(self):
        one_month = timezone.now() - timezone.timedelta(weeks=4)
        after_min_month = timezone.now() - timezone.timedelta(weeks=settings.BACKUP_MIN_WEEKS_TO_KEEP - 1)
        before_min_month = timezone.now() - timezone.timedelta(weeks=settings.BACKUP_MIN_WEEKS_TO_KEEP + 1)

        self.assertLess(after_min_month, one_month)
        self.assertLess(before_min_month, after_min_month)

        new_raw_response = 666
        for d in [before_min_month, before_min_month, before_min_month, after_min_month, one_month]:
            with freeze_time(d.strftime("%Y-%m-%d")):
                self.response_update(
                    data_source=self.ds,
                    virus=self.ds.virus_set.first(),
                    host=self.ds.host_set.first(),
                    new_raw_response=new_raw_response,
                )
                new_raw_response += 1
                self.data_source_file_import(user=self.user, filename="three_reponse_simple_2.xlsx", )

        self.assertEqual(10, admin_models.LogEntry.objects.count())

        for i, le in enumerate(self.get_log_entries(self.ds)):
            if i < settings.BACKUP_MIN_OCCURRENCES_TO_KEEP:
                self.assertIsNotNone(business_process.get_backup_file_path(le, True))
            else:
                self.assertIsNone(business_process.get_backup_file_path(le, True))

        self.delete_data_source(self.ds)

        self.assertEqual(11, admin_models.LogEntry.objects.count())

        business_process.remove_old_orphan_backup_files(admin_models.LogEntry.objects.all())

        for i, le in enumerate(self.get_log_entries(self.ds)):
            if i < 3:
                self.assertIsNotNone(business_process.get_backup_file_path(le, True))
            else:
                self.assertIsNone(business_process.get_backup_file_path(le, True))

    def test_empty_change_message_does_not_break_something(self):
        admin_models.LogEntry.objects.log_action(
            user_id=self.ds.owner.pk,
            content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
            object_id=self.ds.pk,
            object_repr=self.ds.__str__(),
            action_flag=admin_models.ADDITION,
            change_message='',
        )
        admin_models.LogEntry.objects.log_action(
            user_id=self.ds.owner.pk,
            content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
            object_id=self.ds.pk,
            object_repr=self.ds.__str__(),
            action_flag=admin_models.CHANGE,
            change_message='',
        )
        admin_models.LogEntry.objects.log_action(
            user_id=self.ds.owner.pk,
            content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
            object_id=self.ds.pk,
            object_repr=self.ds.__str__(),
            action_flag=admin_models.CHANGE,
            change_message='[]',
        )
        admin_models.LogEntry.objects.log_action(
            user_id=self.ds.owner.pk,
            content_type_id=ContentType.objects.get_for_model(models.DataSource).id,
            object_id=self.ds.pk,
            object_repr=self.ds.__str__(),
            action_flag=admin_models.DELETION,
            change_message='',
        )
        self.test_data_source_mapping_range_edit_entry()

    def test_log_entry_user_is_correct(self):
        filename = "three_reponse_simple_2.xlsx"
        user = self.u_write
        self.ds.allowed_users.add(self.user_b, through_defaults=dict(can_write=True))

        ##################################################
        business_process.create_backup_when_missing(models.DataSource.objects.all())
        self.assertEqual(self.get_log_entries(self.ds).first().user, self.ds.owner)

        ##################################################
        self.data_source_data_upload(filename="three_reponse_simple.xlsx", data_source=self.ds, user=user)
        self.assertEqual(self.get_log_entries(self.ds).first().user, user)

        ##################################################
        raw_responses = self.ds.responseindatasource.values_list('raw_response', flat=True) \
            .order_by('raw_response').distinct()
        objects_mappable = [(o.pk, o.value) for o in models.GlobalViralHostResponseValue.objects_mappable()]
        mapping = dict()
        for i, value in enumerate(raw_responses):
            mapping[value] = objects_mappable[int(value) % len(objects_mappable)][0]
        self.data_source_mapping_label_edit(data_source=self.ds, mapping=mapping, user=user)
        self.assertEqual(self.get_log_entries(self.ds).first().user, user)

        ##################################################
        self.data_source_history_restoration(self.ds, self.get_log_entries(self.ds).last(), user=self.user_b)
        self.assertEqual(self.get_log_entries(self.ds).first().user, self.user_b,
                         "log entry was done by %s but restored by %s" % (str(user), str(self.user_b)))

        ##################################################
        self.data_source_mapping_range_edit(data_source=self.ds, weak_starts_with=4, lysis_starts_with=7, user=user)
        self.assertEqual(self.get_log_entries(self.ds).first().user, user)

        ##################################################
        self.data_source_data_update(description="az", data_source=self.ds, user=user)
        self.assertEqual(self.get_log_entries(self.ds).first().user, user)

        ##################################################
        changes = dict([(o.id, dict(name='blabla%i' % o.id)) for o in self.ds.virus_set.all()[:2]])
        self.data_source_virus_update(data_source=self.ds, changes=changes, user=user)
        self.assertEqual(self.get_log_entries(self.ds).first().user, user)

        ##################################################
        data_source = self.data_source_wizard_contribution(user=user, filename=filename)
        self.assertEqual(self.get_log_entries(data_source).first().user, user)

        ##################################################
        data_source = self.data_source_file_import(user=user, filename=filename)
        self.assertEqual(self.get_log_entries(data_source).first().user, user)

        ##################################################
        changes = set([o.id for o in self.ds.virus_set.all()[:2]])
        self.data_source_virus_delete(data_source=self.ds, changes=changes, user=user)
        self.assertEqual(self.get_log_entries(self.ds).first().user, user)
