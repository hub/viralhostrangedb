import logging

from django.contrib.auth import get_user_model
from django.core import management

from viralhostrangedb import models, business_process
from viralhostrangedb.tests.test_views_others import TooledTestCase

logger = logging.getLogger(__name__)


class DemoTestCase(TooledTestCase):

    def test_get_value(self):
        management.call_command("setup_demo")

        alice = get_user_model().objects.get(username="alice")
        bob = get_user_model().objects.get(username="bob")
        curator = get_user_model().objects.get(username="curator")
        root = get_user_model().objects.get(username="root")

        self.assertIsNotNone(alice)

        self.assertIsNotNone(bob)

        self.assertIsNotNone(curator)
        self.assertTrue(business_process.is_curator(curator))

        self.assertIsNotNone(root)
        self.assertTrue(root.is_staff)
        self.assertTrue(root.is_superuser)

        for d in models.DataSource.objects.all():
            self.assertTrue(d.is_mapping_done)
