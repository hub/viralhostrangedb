import logging

from django import forms as django_forms
from django.utils import timezone

logger = logging.getLogger(__name__)

import os

from basetheme_bootstrap.user_preferences_utils import get_user_preferences_for_user
from django.contrib.auth import get_user_model
from django.db.models import Min, Max
from django.db.models import Q
from django.http import QueryDict
from django.urls import reverse

from viralhostrangedb import forms
from viralhostrangedb import models, business_process
from viralhostrangedb.tests.test_views_others import ViewTestCase, TooledTestCase


class _RangeMappingFormTestCase(ViewTestCase):

    def setUp(self):
        super().setUp()

        ################################################################################
        self.two_response_simple = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="two_response_simple user",
            raw_name="two_response_simple.xlsx",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "two_response_simple.xlsx")
        business_process.import_file(
            data_source=self.two_response_simple,
            file=filename,
        )

        ################################################################################
        self.one_response = models.DataSource.objects.create(
            owner=self.user,
            public=False,
            name="one_response user",
            raw_name="one_response.xlsx",
            kind="FILE",
        )
        filename = os.path.join(self.test_data, "one_response.xlsx")
        business_process.import_file(
            data_source=self.one_response,
            file=filename,
        )

    def build_form(self, min_for_weak, min_for_lysis, ds):
        data = {
            'mapping_for_%i_starts_with' % self.weak.pk: min_for_weak,
            'mapping_for_%i_starts_with' % self.lysis.pk: min_for_lysis
        }
        form = forms.RangeMappingForm(
            data_source=ds,
            data=data
        )
        return form

class RangeMappingFormTestCase1(_RangeMappingFormTestCase):
    def test_find_thresholds(self):
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple)
        self.assertNotIn('mapping_for_%i_starts_with', form.fields)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, self.no_lysis.value)
        self.assertLess(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, self.lysis.value)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, self.weak.value)

class RangeMappingFormTestCase2(_RangeMappingFormTestCase):
    def test_works_when_empty(self):
        self.four_reponse_simple.responseindatasource.all().delete()
        forms.RangeMappingForm(data_source=self.four_reponse_simple)

class RangeMappingFormTestCase3(_RangeMappingFormTestCase):
    def test_ok_when_only_no_lysis(self):
        models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .update(response=self.no_lysis)
        min_raw, max_raw = models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .aggregate(Min('raw_response'), Max('raw_response')).values()
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, max_raw)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, max_raw)

        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple_not_mapped) \
                         .filter(~Q(response=self.no_lysis)).exists())

class RangeMappingFormTestCase4(_RangeMappingFormTestCase):
    def test_ok_when_only_weak(self):
        models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .update(response=self.weak)
        min_raw, max_raw = models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .aggregate(Min('raw_response'), Max('raw_response')).values()
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped)
        self.assertLessEqual(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, min_raw)
        self.assertGreater(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, max_raw)

        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple_not_mapped) \
                         .filter(~Q(response=self.weak)).exists())

class RangeMappingFormTestCase5(_RangeMappingFormTestCase):
    def test_ok_when_only_lysis(self):
        models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .update(response=self.lysis)
        min_raw, max_raw = models.ViralHostResponseValueInDataSource.objects \
            .filter(data_source=self.four_reponse_simple_not_mapped) \
            .aggregate(Min('raw_response'), Max('raw_response')).values()
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped)
        self.assertLessEqual(form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial, min_raw)
        self.assertLessEqual(form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial, min_raw)

        data = {key: form.fields[key].initial for key in form.fields.keys()}
        form = forms.RangeMappingForm(data_source=self.four_reponse_simple_not_mapped, data=data)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertFalse(models.ViralHostResponseValueInDataSource.objects \
                         .filter(data_source=self.four_reponse_simple_not_mapped) \
                         .filter(~Q(response=self.lysis)).exists())

class RangeMappingFormTestCase6(_RangeMappingFormTestCase):
    def test_range_is_respected(self):
        self.actual_test_range_is_respected(3, 8, self.private_data_source_of_user)
        self.actual_test_range_is_respected(1, 3.1, self.four_reponse_simple_not_mapped)

    def actual_test_range_is_respected(self, min_for_weak, min_for_lysis, ds):
        form = self.build_form(min_for_weak, min_for_lysis, ds)
        self.assertTrue(form.is_valid())
        form.save()
        self.assertSetEqual(
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(raw_response__lt=min_for_weak, data_source=ds) \
                .values_list('pk', flat=True))
            ,
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(response=self.no_lysis, data_source=ds) \
                .values_list('pk', flat=True))
        )
        self.assertSetEqual(
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(raw_response__gte=min_for_weak, raw_response__lt=min_for_lysis, data_source=ds) \
                .values_list('pk', flat=True))
            ,
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(response=self.weak, data_source=ds) \
                .values_list('pk', flat=True))
        )
        self.assertSetEqual(
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(raw_response__gte=min_for_lysis, data_source=ds) \
                .values_list('pk', flat=True))
            ,
            set(models.ViralHostResponseValueInDataSource.objects \
                .filter(response=self.lysis, data_source=ds) \
                .values_list('pk', flat=True))
        )

class RangeMappingFormTestCase7(_RangeMappingFormTestCase):
    def test_ko_with_missing_threshold(self):
        for a, b in [('', '1'), ('1', ''), ('', '')]:
            self.assertFalse(self.build_form(a, b, self.four_reponse_simple_not_mapped).is_valid())

class RangeMappingFormTestCase8(_RangeMappingFormTestCase):
    def test_ok_with_equal(self):
        for a, b in [('1', '1'), ('1', '1.0'), ('1.0', '1'), ('1.0', '1.0')]:
            self.assertTrue(self.build_form(a, b, self.four_reponse_simple_not_mapped).is_valid())

class RangeMappingFormTestCase9(_RangeMappingFormTestCase):
    def test_ok_with_int_and_float(self):
        for a, b in [('1', '2'), ('1.0', '2'), ('1', '2.0'), ('1.0', '2.0')]:
            self.assertTrue(self.build_form(a, b, self.four_reponse_simple_not_mapped).is_valid())

class RangeMappingFormTestCase10(_RangeMappingFormTestCase):
    def test_ok_when_new_value_inserted_in_existing_range(self):
        for v in [4, 1]:
            changed = models.ViralHostResponseValueInDataSource.objects \
                .filter(data_source=self.four_reponse_simple) \
                .filter(raw_response=v).first()
            changed.raw_response = v - 0.1
            changed.response = models.GlobalViralHostResponseValue.get_not_mapped_yet()
            changed.save()
            self.four_reponse_simple.refresh_from_db()
            self.assertFalse(self.four_reponse_simple.is_mapping_done)
            self.four_reponse_simple.refresh_from_db()
            form = forms.RangeMappingForm(data_source=self.four_reponse_simple)
            data = {key: form.fields[key].initial for key in form.fields.keys()}
            form = forms.RangeMappingForm(data_source=self.four_reponse_simple, data=data)
            self.assertTrue(form.is_valid())
            form.save()
            self.four_reponse_simple.refresh_from_db()
            self.assertTrue(self.four_reponse_simple.is_mapping_done)

class RangeMappingFormTestCase11(_RangeMappingFormTestCase):
    def test_two_response_simple(self):
        ds = self.two_response_simple
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 2,
            "mapping_for_%i_starts_with" % self.lysis.pk: 2,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertGreater(weak_start, 0)
        self.assertGreater(lysis_start, 0)

class RangeMappingFormTestCase12(_RangeMappingFormTestCase):
    def test_two_response_simple_2(self):
        ds = self.two_response_simple
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 2,
            "mapping_for_%i_starts_with" % self.lysis.pk: 3,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertGreater(weak_start, 0, msg="0s are not associated with weak")
        self.assertLessEqual(weak_start, 2, msg="2s are not associated with weak")
        self.assertGreater(lysis_start, 2, msg="2s are not associated with weak")

class RangeMappingFormTestCase12(_RangeMappingFormTestCase):
    def test_one_response_simple_to_no_lysis(self):
        ds = self.one_response
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 1,
            "mapping_for_%i_starts_with" % self.lysis.pk: 1,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertGreater(weak_start, 0)
        self.assertGreater(lysis_start, 0)

class RangeMappingFormTestCase13(_RangeMappingFormTestCase):
    def test_one_response_simple_to_weak(self):
        ds = self.one_response
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 0,
            "mapping_for_%i_starts_with" % self.lysis.pk: 1,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertLessEqual(weak_start, 0)
        self.assertGreater(lysis_start, 0)

class RangeMappingFormTestCase14(_RangeMappingFormTestCase):
    def test_one_response_simple_to_lysis(self):
        ds = self.one_response
        url = reverse('viralhostrangedb:data-source-mapping-edit', args=[ds.pk])
        self.client.force_login(self.user)
        form_data = {
            "mapping_for_%i_starts_with" % self.weak.pk: 0,
            "mapping_for_%i_starts_with" % self.lysis.pk: 0,
        }
        self.client.post(url, form_data)
        self.assertTrue(ds.is_mapping_done)

        form = forms.RangeMappingForm(data_source=ds)
        lysis_start = form.fields['mapping_for_%i_starts_with' % self.lysis.pk].initial
        weak_start = form.fields['mapping_for_%i_starts_with' % self.weak.pk].initial

        self.assertLessEqual(weak_start, 0)
        self.assertLessEqual(lysis_start, 0)


class BrowseFormTestCase(TooledTestCase):

    def setUp(self):
        super().setUp()
        self.user = get_user_model().objects.create(
            username="user",
            email="user@a.a",
        )
        self.user.save()
        p = get_user_preferences_for_user(self.user)
        p.virus_infection_ratio = True
        p.save()
        p = get_user_preferences_for_user(None)
        p.host_infection_ratio = True
        p.save()

    def test_get_pref(self):
        form = forms.BrowseForm(
            user=None,
        )
        self.assertTrue(form.initial["host_infection_ratio"])
        self.assertFalse(form.initial["virus_infection_ratio"])

    def test_avoid_pref(self):
        form = forms.BrowseForm(
            user=None,
            data=QueryDict(
                "&use_pref=False"
            )
        )
        self.assertTrue(form.is_valid())
        self.assertFalse(form.cleaned_data["host_infection_ratio"])
        self.assertFalse(form.cleaned_data["virus_infection_ratio"])

    def test_get_pref_for_user(self):
        form = forms.BrowseForm(
            user=self.user,
        )
        self.assertFalse(form.initial["host_infection_ratio"])
        self.assertTrue(form.initial["virus_infection_ratio"])

    def test_avoid_pref_for_user(self):
        form = forms.BrowseForm(
            user=self.user,
            data=QueryDict(
                "&use_pref=False"
            )
        )
        self.assertTrue(form.is_valid())
        self.assertFalse(form.cleaned_data["host_infection_ratio"])
        self.assertFalse(form.cleaned_data["virus_infection_ratio"])


class LiveInputVirusHostFormTestCase(TooledTestCase):
    test_data = "./test_data"

    def setUp(self) -> None:
        super().setUp()
        self.owner = get_user_model().objects.create(
            username="root",
        )
        filename = os.path.join(self.test_data, "simple-with-her-id-and-ncbi.xlsx")
        simple_with_both_id, created = models.DataSource.objects.get_or_create(
            raw_name=filename,
            kind="FILE",
            owner=self.owner,
            creation_date=timezone.now(),
            last_edition_date=timezone.now(),
        )
        business_process.import_file(
            data_source=simple_with_both_id,
            file=filename,
        )

    def test_guess_simple_sep(self):
        for sep in [";", ",", "\t"]:
            form = forms.LiveInputVirusHostForm(
                data=dict(
                    host="e",
                    virus=sep.join(models.Virus.objects.all().values_list("name", flat=True)),
                )
            )
            form.is_valid()
            self.assertEqual(form.get_guessed_delimiter("virus"), sep)

    def test_guess_two_col_sep(self):
        for sep in [";", ",", "\t"]:
            viruses = []
            for v in models.Virus.objects.all():
                viruses.append(sep.join([v.name, v.identifier]))
            form = forms.LiveInputVirusHostForm(
                data=dict(
                    host="e",
                    virus="\n".join(viruses),
                )
            )
            form.is_valid()
            # test if sep is good
            self.assertEqual(form.get_guessed_delimiter("virus"), "two_col" + sep)
            # test if get_as_list works fine:
            # first we erase all her_id to have an explicit_name without it later
            models.Virus.objects.all().update(her_identifier=None)
            # then we compare what is extracted and in the db
            self.assertEqual(
                list(form.get_as_list("virus")),
                [business_process.extract_name_and_identifiers(o.explicit_name)
                 for o in models.Virus.objects.all()]
            )

    def test_guess_tri_col_sep(self):
        for sep in [";", ",", "\t"]:
            lines = []
            for o in models.Virus.objects.all():
                lines.append(sep.join([o.name, o.identifier, str(o.her_identifier)]))
            form = forms.LiveInputVirusHostForm(
                data=dict(
                    host="e",
                    virus="\n".join(lines),
                )
            )
            form.is_valid()
            # test if sep is good
            self.assertEqual(form.get_guessed_delimiter("virus"), "tri_col" + sep)
            # test if get_as_list works fine
            self.assertEqual(
                list(form.get_as_list("virus")),
                [business_process.extract_name_and_identifiers(o.explicit_name)
                 for o in models.Virus.objects.all()]
            )

    def test_not_guess_tri_col_sep_for_host(self):
        for sep in [";", ",", "\t"]:
            lines = []
            for o in models.Host.objects.all():
                lines.append(sep.join([o.name, o.identifier, "42"]))
            form = forms.LiveInputVirusHostForm(
                data=dict(
                    virus="e",
                    host="\n".join(lines),
                )
            )
            form.is_valid()
            self.assertNotEqual(form.get_guessed_delimiter("virus")[:7], "tri_col")


class TestFindUser(TooledTestCase):
    class TestForm(django_forms.Form):
        toto = django_forms.CharField(max_length=255, required=False)

    def setUp(self) -> None:
        super().setUp()
        self.form = self.TestForm(data={"toto": "e"})
        self.form.is_valid()

        self.user = get_user_model().objects.create(
            username="user",
            first_name="userF",
            last_name="userL",
            email="user@a.b",
        )
        self.userHom1 = get_user_model().objects.create(
            username="userHom1",
            first_name="john",
            last_name="jack",
            email="john@a.b",
        )
        self.userHom2 = get_user_model().objects.create(
            username="userHom2",
            first_name="jack",
            last_name="john",
            email="jack@a.b",
        )
        self.bob_double_email1 = get_user_model().objects.create(
            username="bob_double_email1",
            first_name="bobF",
            last_name="bobL",
            email="bob@a.b",
        )
        self.bob_double_email2 = get_user_model().objects.create(
            username="bob_double_email2",
            first_name="bobF",
            last_name="bobL",
            email="bob@a.b",
        )
        self.lilou = get_user_model().objects.create(
            username="lilou",
            first_name="lilou",
            last_name="universievna dallas",
            email="lilou@a.b",
        )

    def test_work(self):
        # test find users
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.first_name + " " + self.user.last_name + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";;qd"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";;False"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";;true"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertTrue(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + "       " + self.user.first_name + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + "\t" + self.user.first_name + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.email + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.email + "\t" + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str="\t" + self.user.email + ";;"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertFalse(rw)

        # test can_write works WITHOUT pk
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str="\t" + self.user.email + ";;True"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertTrue(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";;True"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertTrue(rw)

        # test can_write works WITH pk
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str="\t" + self.user.email + ";" + str(self.user.pk) + ";True"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertTrue(rw)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";" + str(self.user.pk) + ";True"
        )
        self.assertEqual(u.pk, self.user.pk)
        self.assertTrue(rw)

        # test malicious user does not succeed
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str="\t" + self.user.email + ";" + str(self.lilou.pk) + ";True"
        )
        self.assertIsNone(u)

        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.user.last_name + " " + self.user.first_name + ";" + str(self.lilou.pk) + ";True"
        )
        self.assertIsNone(u)

        # test fails with homonyms
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.userHom1.first_name + " " + self.userHom1.last_name + ";;"
        )
        self.assertIsNone(u)
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.userHom2.first_name + " " + self.userHom2.last_name + ";;"
        )
        self.assertIsNone(u)

        # test fails with more than one space
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.lilou.first_name + " " + self.lilou.last_name + ";;"
        )
        self.assertIsNone(u)

        # test fails with email used multiple time
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.bob_double_email1.email + ";;"
        )
        self.assertIsNone(u)

        # test ok if comma is used
        u, rw = forms.find_user(
            form=self.form,
            field_name="toto",
            user_str=self.lilou.first_name + "," + self.lilou.last_name + ";;"
        )
        self.assertEqual(u.pk, self.lilou.pk)
