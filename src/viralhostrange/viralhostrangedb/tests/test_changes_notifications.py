import errno
import logging
import os
import random
import shutil
import string

from basetheme_bootstrap.user_preferences_utils import get_user_preferences_for_user
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core import mail, management
from django.test import override_settings

from viralhostrangedb import business_process
from viralhostrangedb.tests import base_test_case

logger = logging.getLogger(__name__)


def random_str(l):
    return ''.join(random.choice(string.ascii_letters) for _ in range(l))


@override_settings(BACKUP_LOCATION=os.path.join(
    settings.BASE_DIR,
    'persistent_volume',
    f'data_source_backup_for_tests_in_{__name__.split(".")[-1]}',
))
class DataSourceChangeNotificationTestCase(base_test_case.OneDataSourceTestCase):
    def tearDown(self) -> None:
        super().tearDown()
        if 'data_source_backup_for_tests' in settings.BACKUP_LOCATION:
            shutil.rmtree(settings.BACKUP_LOCATION)

    def setUp(self) -> None:
        try:
            os.mkdir(settings.BACKUP_LOCATION)
        except OSError as e:
            assert e.errno == errno.EEXIST, e
        super().setUp()
        ################################################################################
        # self.u_curator = get_user_model().objects.create(
        #     username="u_curator",
        #     first_name="Dany",
        #     last_name="u_curatorL",
        #     email="u_curator@a.b",
        # )
        # business_process.set_curator(self.u_curator, True)
        self.ds.public = True
        self.ds.save()

    def test_notification_to_owner(self):
        from viralhostrangedb.models import UserPreferences as UPref

        pref = get_user_preferences_for_user(self.ds.owner)

        for level, for_owner, for_editor, for_curator in [
            (UPref.NOTIFY_EVEN_FOR_ME, True, True, True),
            (UPref.NOTIFY_FOR_EDITOR_AND_CURATOR, False, True, True),
            (UPref.NOTIFY_FOR_CURATOR, False, False, True),
            # (UPref.NOTIFY_FOR_NO_ONE, False, False, False),
        ]:
            pref.edition_notification = level
            pref.save()

            outbox_len = len(mail.outbox)
            self.data_source_data_update(self.ds, description=random_str(10))
            management.call_command("send_edition_notification")
            if for_owner:
                self.assertEqual(len(mail.outbox), outbox_len + 1)
                self.assertEqual(mail.outbox[-1].recipients(), [self.ds.owner.email, ])
                management.call_command("send_edition_notification")
                self.assertEqual(len(mail.outbox), outbox_len + 1)
            else:
                self.assertEqual(len(mail.outbox), outbox_len)

            outbox_len = len(mail.outbox)
            self.data_source_data_update(self.ds, description=random_str(10), user=self.u_write)
            management.call_command("send_edition_notification")
            if for_editor:
                self.assertEqual(len(mail.outbox), outbox_len + 1)
                self.assertEqual(mail.outbox[-1].recipients(), [self.ds.owner.email, ])
                management.call_command("send_edition_notification")
                self.assertEqual(len(mail.outbox), outbox_len + 1)
            else:
                self.assertEqual(len(mail.outbox), outbox_len)

            outbox_len = len(mail.outbox)
            self.data_source_data_update(self.ds, description=random_str(10), user=self.u_curator)
            management.call_command("send_edition_notification")
            if for_curator:
                self.assertEqual(len(mail.outbox), outbox_len + 1)
                self.assertEqual(mail.outbox[-1].recipients(), [self.ds.owner.email, ])
                management.call_command("send_edition_notification")
                self.assertEqual(len(mail.outbox), outbox_len + 1)
            else:
                self.assertEqual(len(mail.outbox), outbox_len)

        for level, for_owner, for_editor, for_curator in [
            (UPref.NOTIFY_EVEN_FOR_ME, True, True, True),
            (UPref.NOTIFY_FOR_EDITOR_AND_CURATOR, False, True, True),
            (UPref.NOTIFY_FOR_CURATOR, False, False, True),
        ]:
            pref.edition_notification = level
            pref.save()

            self.data_source_data_update(self.ds, description=random_str(10))
            self.data_source_data_update(self.ds, description=random_str(10), user=self.u_write)
            self.data_source_data_update(self.ds, description=random_str(10), user=self.u_curator)
            management.call_command("send_edition_notification")
            content = mail.outbox[-1].alternatives[0][0]
            content = content[content.index("<li>"):]
            for changer, is_in_mail in [
                (self.user.last_name.upper() + " " + self.user.first_name.title(), for_owner),
                (self.u_write.last_name.upper() + " " + self.u_write.first_name.title(), for_editor),
                (self.u_curator.last_name.upper() + " " + self.u_curator.first_name.title(), for_curator),
            ]:
                if is_in_mail:
                    self.assertTrue(changer in content, changer + " should be here for settings " + str(level))
                else:
                    self.assertFalse(changer in content, changer + " should not be here for settings " + str(level))

        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification", starting_with=0)
        self.assertEqual(len(mail.outbox), outbox_len + 1)
        management.call_command("send_edition_notification", starting_with=0)
        self.assertEqual(len(mail.outbox), outbox_len + 2)

    def test_notification_to_curator(self):
        ################################################################################
        # create a second curator
        self.u_curator_2 = get_user_model().objects.create(
            username="u_curator_2",
            first_name="Dany",
            last_name="u_curator_2L",
            email="u_curator_2@a.b",
        )
        business_process.set_curator(self.u_curator_2, True)
        # make the ds private
        self.ds.public = False
        self.ds.save()

        # modification of private data source, no notification to curator
        self.data_source_data_update(self.ds, description=random_str(10))
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len)

        # modification to make it public, notification to curator
        self.data_source_data_update(self.ds, public=True)
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len + 1)

        # modification of public data source, no notification to curator
        self.data_source_data_update(self.ds, description=random_str(10))
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len)

        # modification to make it private, no notification to curator
        self.data_source_data_update(self.ds, public=False)
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len)

        # modification to make it public and alter description, notification to curator
        self.data_source_data_update(self.ds, public=True, description=random_str(10))
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len + 1)

        # new data source but private
        self.data_source_file_import(user=self.user_b, filename="three_reponse_simple_2.xlsx", public=False)
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len, "no notification")

        # new data source public
        self.data_source_file_import(user=self.user_b, filename="three_reponse_simple_2.xlsx", public=True,
                                     description=random_str(10), name=random_str(10))
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len + 1, "notification should be sent")

        # new data source but private, and then public
        ds_temp = self.data_source_file_import(user=self.user_b, filename="three_reponse_simple_2.xlsx", public=False)
        self.data_source_data_update(ds_temp, public=True, description=random_str(10), name="ds_temp")
        outbox_len = len(mail.outbox)
        management.call_command("send_edition_notification")
        self.assertEqual(len(mail.outbox), outbox_len + 1, "notification should be sent")
