import logging

from viralhostrangedb import business_process
from viralhostrangedb.tests.test_views_others import TooledTestCase

logger = logging.getLogger(__name__)


class OnlyForCoverage(TooledTestCase):

    def test_ImportationObserver(self):
        class MyImportationObserver(business_process.ImportationObserver):
            def notify_host_error(self, *args, **kwargs):
                super().notify_host_error(*args, **kwargs)

            def notify_virus_error(self, *args, **kwargs):
                super().notify_virus_error(*args, **kwargs)

        MyImportationObserver().notify_host_error(None, None)
        MyImportationObserver().notify_virus_error(None, None)
