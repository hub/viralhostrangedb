from basetheme_bootstrap.admin import ViewOnSiteModelAdmin, UserPreferencesAdmin
from basetheme_bootstrap.user_preferences_utils import get_user_preference_class
from django.contrib import admin
from django.contrib import messages
from django.contrib.auth import get_permission_codename
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _

from viralhostrangedb import models, business_process


@admin.register(models.GlobalViralHostResponseValue)
class GlobalViralHostResponseValueAdmin(ViewOnSiteModelAdmin):
    list_display = (
        'name',
        'value',
        'color',
    )
    readonly_fields = ('value',)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = self.readonly_fields
        if not request.user.has_perm("%s.%s" % (self.opts.app_label, get_permission_codename('change', self.opts))):
            readonly_fields += ('name', 'value')
        return readonly_fields

    def has_change_permission(self, request, obj=None):
        if super().has_change_permission(request=request, obj=obj):
            return True
        opts = self.opts
        codename = get_permission_codename('change', opts) + "_color"
        return request.user.has_perm("%s.%s" % (opts.app_label, codename))


admin.site.site_header = _('ViralHostRangeDB website administration')

admin.site.site_title = admin.site.site_header

admin.site.index_header = _('ViralHostRangeDB administration')

admin.site.index_title = admin.site.index_header


def grant_curate_permission(modeladmin, request, queryset, revoke=False):
    for o in queryset:
        business_process.set_curator(o, not revoke)

    count = queryset.count()
    msg = "Permission %s for %i user(s). "

    if revoke:
        msg = msg % ("revoked", count)
    else:
        msg = msg % ("granted", count)

    modeladmin.message_user(request, msg, messages.SUCCESS)


grant_curate_permission.short_description = "Grant permission to curate data"
grant_curate_permission.allowed_permissions = ["change", ]


def revoke_curate_permission(modeladmin, request, queryset):
    grant_curate_permission(modeladmin, request, queryset, True)


revoke_curate_permission.short_description = "Revoke permission to curate data"
revoke_curate_permission.allowed_permissions = ["change", ]


class MyUserAdmin(UserAdmin):
    actions = [grant_curate_permission, revoke_curate_permission]
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_admin",
        "is_curator",
    )
    readonly_fields = (
        'last_login',
        'date_joined',
        'user_permissions',
        'password',
    )

    def get_readonly_fields(self, request, obj=None):
        readonly = super().get_readonly_fields(request=request, obj=obj)
        if not request.user.is_superuser:
            readonly += (
                'is_superuser',
                'groups',
            )
        if obj is not None and obj.pk is not None:
            readonly += (
                'username',
                'email',
            )
        return readonly

    def is_curator(self, object):
        return business_process.is_curator(object)

    is_curator.boolean = True

    def is_admin(self, object):
        return object.groups.filter(name="Moderator").exists()

    is_admin.boolean = True

    def user_change_password(self, *args, **kwargs):
        raise PermissionDenied


admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), MyUserAdmin)

admin.site.unregister(Group)


class MyUserPreferencesAdmin(UserPreferencesAdmin):
    def has_change_permission(self, request, obj=None):
        if obj is None:
            return super().has_change_permission(request=request, obj=obj)
        return obj.user is None and super().has_change_permission(request=request, obj=None)


admin.site.unregister(get_user_preference_class())
admin.site.register(get_user_preference_class(), MyUserPreferencesAdmin)
