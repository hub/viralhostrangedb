from django.urls import path, include, re_path
from rest_framework import routers

from viralhostrangedb import views_api

app_name = 'viralhostrangedb-api'
router = routers.DefaultRouter()
router.register(r'virus', views_api.VirusViewSet)
router.register(r'host', views_api.HostViewSet)
router.register(r'data-source', views_api.DataSourceViewSet)
router.register('global-response', views_api.GlobalViralHostResponseValueViewSet)

urlpatterns = [
    path('aggregated-responses/', views_api.AggregatedResponseViewSet.as_view(), name='aggregated-responses'),
    path('responses/', views_api.CompleteResponseViewSet.as_view(), name='responses'),
    path('', include(router.urls)),
    re_path(r'^aggregated-infection-ratio/(?P<slug>[^/]*)/$',
        views_api.VirusInfectionRatioViewSet.as_view(),
        name='aggregated-infection-ratio-list'),
    re_path(r'^aggregated-infection-ratio/(?P<slug>[^/]*)/(?P<slug_pk>\d+)/$',
        views_api.VirusInfectionRatioViewSet.as_view(),
        name='aggregated-infection-ratio-detail'),
    re_path(r'^infection-ratio/(?P<slug>[^/]*)/(?P<slug_pk>\d+)/$',
        views_api.VirusInfectionRatioViewSet.as_view(data_source_aggregated=False),
        name='infection-ratio-detail'),
    path('search/', views_api.search, name='search'),
    path('fetch-identifier-status/', views_api.fetch_identifier_status, name='fetch-identifier-status'),
    path('statistics/', views_api.get_statistics, name='get_statistics'),

]
