from django.contrib import sitemaps
from django.urls import reverse

from viralhostrangedb import models
from viralhostrangedb.mixins import only_public_or_owned_queryset_filter


class DataSourceSitemap(sitemaps.Sitemap):
    changefreq = "weekly"
    priority = 1

    def items(self):
        return only_public_or_owned_queryset_filter(
            None,
            request=None,
            queryset=models.DataSource.objects,
            user=None
        )

    def lastmod(self, obj):
        return obj.last_edition_date


class StaticViewSitemap(sitemaps.Sitemap):
    priority = 0.5
    changefreq = 'weekly'

    def items(self):
        return [
            'basetheme_bootstrap:about_page',
            'viralhostrangedb:data-source-list',
            'viralhostrangedb:host-list',
            'viralhostrangedb:virus-list',
            'viralhostrangedb:home',
        ]

    def location(self, item):
        return reverse(item)


my_sitemaps = {
    'static': StaticViewSitemap,
    "data source": DataSourceSitemap(),
}
