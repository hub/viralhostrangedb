# Generated by Django 2.2.5 on 2020-04-09 11:44

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('viralhostrangedb', '0038_pg_unaccent_20200330_1533'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='datasource',
            managers=[
            ],
        ),
    ]
