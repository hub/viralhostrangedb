from django.contrib.postgres.operations import UnaccentExtension
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0037_auto_20200325_1332'),
    ]

    operations = [
        UnaccentExtension(),
    ]
