# Generated by Django 2.1.7 on 2019-03-15 14:02

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('viralhostrangedb', '0005_auto_20190306_1418'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='datasource',
            managers=[
                ('objects_annotated_with_pending', django.db.models.manager.Manager()),
            ],
        ),
    ]
