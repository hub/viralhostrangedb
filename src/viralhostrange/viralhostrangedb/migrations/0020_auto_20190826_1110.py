# Generated by Django 2.2.3 on 2019-08-26 09:10

from django.db import migrations, models
import django.db.models.functions.text


class Migration(migrations.Migration):

    dependencies = [
        ('viralhostrangedb', '0019_auto_20190812_1222'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='datasource',
            options={'ordering': [django.db.models.functions.text.Upper('name')], 'verbose_name': 'Data Source', 'verbose_name_plural': 'Data Sources'},
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='agreed_infection',
            field=models.BooleanField(default=False, help_text='By default we consider that there is an infection when in <i>at least one</i> data source an infection was observed.UserPreference:option_in_browse_view', verbose_name='Consider that there is an infection only when <i>all data sources</i> documenting the interaction observed an infection'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='display_all_at_once',
            field=models.BooleanField(default=False, help_text='Otherwise we fit the table to the screen and use scroll bar when needed.UserPreference:option_in_browse_view', verbose_name='Display all the table at once, even if it does not fit on screen'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='focus_disagreements_toggle',
            field=models.BooleanField(default=False, help_text='Responses where data sources disagree are highlighted, others are faded.UserPreference:option_in_browse_view', verbose_name='Render table focusing on disagreements'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='hide_cols_with_no_infection',
            field=models.BooleanField(default=False, help_text='hide_cols_with_no_infection_help_textUserPreference:option_in_browse_view', verbose_name='hide_cols_with_no_infection_label'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='hide_rows_with_no_infection',
            field=models.BooleanField(default=False, help_text='hide_rows_with_no_infection_help_textUserPreference:option_in_browse_view', verbose_name='hide_rows_with_no_infection_label'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='host_infection_ratio',
            field=models.BooleanField(default=False, help_text='Note that it can deteriorate the overall response time.UserPreference:option_in_browse_view', verbose_name='Show the infection ratio for the hosts'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='virus_infection_ratio',
            field=models.BooleanField(default=False, help_text='Note that it can deteriorate the overall response time.UserPreference:option_in_browse_view', verbose_name='Show the infection ratio for the viruses'),
        ),
        migrations.AlterField(
            model_name='userpreferences',
            name='weak_infection',
            field=models.BooleanField(default=False, help_text='By default we consider that there is an infection only when the highest response is observed.UserPreference:option_in_browse_view', verbose_name='Consider all positive response as a infection'),
        ),
    ]
