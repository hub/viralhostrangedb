# Generated by Django 2.2.5 on 2020-03-25 12:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('viralhostrangedb', '0036_auto_20200316_1824'),
    ]

    operations = [
        migrations.AddField(
            model_name='userpreferences',
            name='horizontal_column_header',
            field=models.BooleanField(default=False, help_text='preference.horizontal_column_header.help_text', verbose_name='preference.horizontal_column_header.label'),
        ),
        migrations.AddField(
            model_name='userpreferences',
            name='render_actual_aggregated_responses',
            field=models.BooleanField(default=False, help_text='preference.render_actual_aggregated_responses.help_text', verbose_name='preference.render_actual_aggregated_responses.label'),
        ),
    ]
