# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import unicode_literals

from django.contrib.auth import get_permission_codename
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


def migration_code(apps, schema_editor):
    # remove deprecated permission
    Permission.objects.filter(codename__icontains='Strain').delete()

    # inspired by django.contrib.auth.management.create_permissions
    moderator, _ = Group.objects.get_or_create(name="Moderator")

    for klass in [
        apps.get_model("viralhostrangedb", "ViralHostResponseValueInDataSource"),
        apps.get_model("viralhostrangedb", "DataSource"),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            if action == "add":
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            moderator.permissions.remove(perm)

    for klass in [
        apps.get_model("viralhostrangedb", "Host"),
        apps.get_model("viralhostrangedb", "Virus"),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            if action != "view":
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            moderator.permissions.remove(perm)


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0043_userpreferences_edition_notification'),
    ]

    operations = [
        # reverse_code is 0023_create_missing_permissions_then_moderator_group.migration_code
        migrations.RunPython(migration_code, reverse_code=migrations.RunPython.noop),
    ]
