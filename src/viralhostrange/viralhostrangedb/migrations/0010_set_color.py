# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def migration_code(apps, schema_editor):
    GlobalViralStrainResponseValue = apps.get_model("viralhostrangedb", "GlobalViralStrainResponseValue")

    for name, value in [("Lysis", "#00FF00"), ("Weak", "#ff8000"), ("No lysis", "#FF0000")]:
        GlobalViralStrainResponseValue.objects.update_or_create(
            name=name,
            defaults=dict(
                color=value,
            )
        )


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0009_globalviralstrainresponsevalue_color'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=migrations.RunPython.noop),
    ]
