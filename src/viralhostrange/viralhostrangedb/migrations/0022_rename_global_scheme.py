# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def migration_code(apps, schema_editor):
    GlobalViralStrainResponseValue = apps.get_model("viralhostrangedb", "GlobalViralHostResponseValue")

    GlobalViralStrainResponseValue.objects.filter(name="No lysis").update(name="No infection")
    GlobalViralStrainResponseValue.objects.filter(name="Weak").update(name="Intermediate")
    GlobalViralStrainResponseValue.objects.filter(name="Lysis").update(name="Infection")


def reverse_code(apps, schema_editor):
    GlobalViralStrainResponseValue = apps.get_model("viralhostrangedb", "GlobalViralHostResponseValue")

    GlobalViralStrainResponseValue.objects.filter(name="No infection").update(name="No lysis")
    GlobalViralStrainResponseValue.objects.filter(name="Intermediate").update(name="Weak")
    GlobalViralStrainResponseValue.objects.filter(name="Infection").update(name="Lysis")


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0021_auto_20190926_2307'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=reverse_code),
    ]
