# Generated by Django 2.2.5 on 2020-03-13 16:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('viralhostrangedb', '0031_virus_her_identifier'),
    ]

    operations = [
        migrations.AddField(
            model_name='host',
            name='is_ncbi_identifier_value',
            field=models.BooleanField(default=False, help_text='This attribute is computed', verbose_name='Is identifier an NCBI identifier'),
        ),
        migrations.AddField(
            model_name='virus',
            name='is_ncbi_identifier_value',
            field=models.BooleanField(default=False, help_text='This attribute is computed', verbose_name='Is identifier an NCBI identifier'),
        ),
        migrations.CreateModel(
            name='VirusIdentifierHasToBeStudied',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='viralhostrangedb.Virus')),
            ],
            options={
                'index_together': {('id', 'target')},
            },
        ),
        migrations.CreateModel(
            name='HostIdentifierHasToBeStudied',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('target', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='viralhostrangedb.Host')),
            ],
            options={
                'index_together': {('id', 'target')},
            },
        ),
    ]
