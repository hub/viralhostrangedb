# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_permission_codename
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


def migration_code(apps, schema_editor):
    # remove deprecated permission
    Permission.objects.filter(codename__icontains='Strain').delete()

    # inspired by django.contrib.auth.management.create_permissions
    moderator, _ = Group.objects.get_or_create(name="Moderator")

    for klass in [
        apps.get_model("viralhostrangedb", "Host"),
        apps.get_model("viralhostrangedb", "Virus"),
    ]:
        ct = ContentType.objects.get_for_model(klass.data_source.through)
        opts = klass.data_source.through._meta

        for action in klass.data_source.through._meta.default_permissions:
            if action != "delete":
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            moderator.permissions.add(perm)

    for klass in [
        apps.get_model("viralhostrangedb", "ViralHostResponseValueInDataSource"),
        apps.get_model("viralhostrangedb", "DataSource"),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            if action == "add":
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            moderator.permissions.add(perm)

    for klass in [
        apps.get_model("viralhostrangedb", "Host"),
        apps.get_model("viralhostrangedb", "Virus"),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            if action != "view":
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            moderator.permissions.add(perm)


def reverse_code(apps, schema_editor):
    for klass in [
        apps.get_model("viralhostrangedb", "Host"),
        apps.get_model("viralhostrangedb", "Virus"),
    ]:
        ct = ContentType.objects.get_for_model(klass.data_source.through)
        Permission.objects.filter(content_type=ct).delete()


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0022_rename_global_scheme'),
    ]

    operations = [
        migrations.RunPython(migration_code, reverse_code=reverse_code),
    ]
