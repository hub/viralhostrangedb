# Generated by Django 2.1.7 on 2019-04-21 20:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('viralhostrangedb', '0010_set_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='viralstrainresponsevalueindatasource',
            name='strain',
            field=models.ForeignKey(help_text='strain__help_text', on_delete=django.db.models.deletion.PROTECT, related_name='responseindatasource', to='viralhostrangedb.Strain', verbose_name='strain__verbose_name'),
        ),
        migrations.AlterField(
            model_name='viralstrainresponsevalueindatasource',
            name='virus',
            field=models.ForeignKey(help_text='virus__help_text', on_delete=django.db.models.deletion.PROTECT, related_name='responseindatasource', to='viralhostrangedb.Virus', verbose_name='virus__verbose_name'),
        ),
    ]
