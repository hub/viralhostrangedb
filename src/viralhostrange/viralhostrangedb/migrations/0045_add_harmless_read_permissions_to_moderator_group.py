# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import unicode_literals

from django.contrib.admin.models import LogEntry
from django.contrib.auth import get_permission_codename, get_user_model
from django.contrib.auth.models import Permission, Group
from django.contrib.contenttypes.models import ContentType
from django.db import migrations


def migration_code_moderator(apps, schema_editor):
    moderator, _ = Group.objects.get_or_create(name="Moderator")
    for klass, allowed_action in [
        (apps.get_model("viralhostrangedb", "DataSource"), {'view'}),
        (apps.get_model("viralhostrangedb", "Host"), {'view'}),
        (apps.get_model("viralhostrangedb", "Virus"), {'view'}),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            if action not in allowed_action:
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            moderator.permissions.add(perm)


def migration_code_admin(apps, schema_editor):
    admin, _ = Group.objects.get_or_create(name="Admin")
    for klass, allowed_action in [
        (LogEntry, {'view'}),
        (get_user_model(), {'view', 'add', 'change'}),
    ]:
        ct = ContentType.objects.get_for_model(klass)
        opts = klass._meta

        for action in klass._meta.default_permissions:
            if action not in allowed_action:
                continue
            perm, _ = Permission.objects.get_or_create(
                codename=get_permission_codename(action, opts),
                name='Can %s %s' % (action, opts.verbose_name_raw),
                content_type=ct)
            admin.permissions.add(perm)


def migration_code_color_to_admin(apps, schema_editor):
    moderator, _ = Group.objects.get_or_create(name="Admin")
    GlobalViralHostResponseValue = apps.get_model("viralhostrangedb", "GlobalViralHostResponseValue")
    ct = ContentType.objects.get_for_model(GlobalViralHostResponseValue)
    opts = GlobalViralHostResponseValue._meta
    for action, codename, suffix in [
        ('change', get_permission_codename('change', opts) + "_color", "color"),
        ('view', get_permission_codename('view', opts), ""),
    ]:
        for perm in Permission.objects.filter(codename=codename, content_type=ct):
            moderator.permissions.add(perm)


class Migration(migrations.Migration):
    dependencies = [
        ('viralhostrangedb', '0044_remove_permissions_to_moderator_group'),
    ]

    operations = [
        # reverse_code is 0023_create_missing_permissions_then_moderator_group.migration_code
        migrations.RunPython(migration_code_admin, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(migration_code_moderator, reverse_code=migrations.RunPython.noop),
        migrations.RunPython(migration_code_color_to_admin, reverse_code=migrations.RunPython.noop),
    ]
