"""
Django settings for viralhostrange project.

Generated by 'django-admin startproject' using Django 2.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import logging
import os
from socket import gethostname, gethostbyname

from Bio import Entrez
from basetheme_bootstrap import db_finder
from decouple import config
from django.utils.translation import gettext_lazy

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PERSISTENT_VOLUME = os.path.join(BASE_DIR, 'persistent_volume')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DEBUG', default=False, cast=bool)

ALLOWED_HOSTS = [s.strip() for s in config('ALLOWED_HOSTS').split(',')] \
                + [gethostbyname(gethostname()), ]  # for k8s probe compatibility

DEFAULT_DOMAIN = 'https://{}'.format(ALLOWED_HOSTS[0])

CSRF_TRUSTED_ORIGINS = [f'https://{h}' for h in ALLOWED_HOSTS]

# Application definition

INSTALLED_APPS = [
    'live_settings',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admindocs',
    'rest_framework',
    'crispy_forms',
    'crispy_bootstrap4',
    'formtools',
    'basetheme_bootstrap',
    'django.contrib.admin',
    'colorfield',
    # 'huey.contrib.djhuey',
    'viralhostrangedb',
    'django_kubernetes_probes',
    'django.contrib.sitemaps',
]
# if config('PREVENT_HUEY', default=False, cast=bool):
#     INSTALLED_APPS.remove('huey.contrib.djhuey')

# INSTALLED_APPS.append('django_extensions')

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'viralhostrange.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'basetheme_bootstrap.context_processors.processors',
                "live_settings.context_processors.processors",
            ],
        },
    },
]

WSGI_APPLICATION = 'viralhostrange.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

if config('USE_SQLITE_AS_DB', default=False, cast=bool):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(PERSISTENT_VOLUME, 'db.sqlite3'),
        }
    }
else:
    CONN_MAX_AGE = config('CONN_MAX_AGE', 0, cast=int)
    if os.environ.get('POSTGRES_PASSWORD', '') == '':
        DATABASES_HOST = db_finder.get_db_ip()
    else:
        DATABASES_HOST = config('POSTGRES_HOST')
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': config('POSTGRES_DB', 'viralhostrangedb'),
            'USER': config('POSTGRES_USER', 'postgres'),
            'PASSWORD': config('POSTGRES_PASSWORD'),
            'HOST': DATABASES_HOST,
            'PORT': 5432,
        }
    }

    INSTALLED_APPS += ['django.contrib.postgres', ]  # needed for __unaccent

print(DATABASES['default']['ENGINE'], DATABASES['default'].get('HOST', None))
# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'
LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale/'),
]
LANGUAGES = [
    ('en', gettext_lazy('English')),
    ('fr', gettext_lazy('French')),
]

TIME_ZONE = 'CET'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/


STATIC_ROOT = os.path.join(BASE_DIR, '.static')
STATIC_ROOT_SHARED = f'{STATIC_ROOT}.shared'
STATIC_URL = config('STATIC_URL', default='/static') + '/'
MEDIA_ROOT = os.path.join(PERSISTENT_VOLUME, '.media')
MEDIA_URL = config('MEDIA_URL', default='/media') + '/'

################################################################################
# LOGGING
################################################################################
ADMINS = [
    ('EMAIL_ADMIN', config('EMAIL_ADMIN', default='bryan.brancotte+viralhostrangedb-logs@pasteur.fr')),
]
MAIL_ADMINS_ON_ERROR = config('MAIL_ADMINS_ON_ERROR', default="auto") in ["True", "true", "1"] \
                       or config('MAIL_ADMINS_ON_ERROR', default="auto") == "auto" \
                       and not config('USE_SQLITE_AS_DB', default=False, cast=bool)


def skip_probe(record):
    if record and record.request and record.request.path == "/probe_ready/":
        return False
    return True


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'simple': {
            'format': '[{asctime}] {levelname} {message}',
            'style': '{',
        },
    },
    'filters': {
        'skip_probes': {
            '()': 'django.utils.log.CallbackFilter',
            'callback': skip_probe,
        }
    },
    'handlers': {
        'console': {
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            'class': 'logging.StreamHandler',
            'formatter': 'simple',
        },
        'mail_admins': {
            'level': 'ERROR',
            'formatter': 'simple',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'filters': ['skip_probes'],
        }
    },
    'loggers': {
        'django': {
            'handlers':
                ['console', 'mail_admins', ] if MAIL_ADMINS_ON_ERROR else ['console', ],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            'propagate': True,
        },
    },
}

################################################################################
# DRF
################################################################################
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )
}

################################################################################
# django-crispy-forms
################################################################################
CRISPY_TEMPLATE_PACK = 'bootstrap4'
CRISPY_ALLOWED_TEMPLATE_PACKS = "bootstrap4"

################################################################################
# basetheme_bootstrap
################################################################################
BASETHEME_BOOTSTRAP_TEMPLATE_LOCATION_PROJECT = "viralhostrangedb"
BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_ENABLED = True
BASETHEME_BOOTSTRAP_USER_PREFERENCE_MODEL_LOCATION_APP = "viralhostrangedb"
BASETHEME_BOOTSTRAP_USERNAME_IS_EMAIL = True
BASETHEME_BOOTSTRAP_FIRST_LAST_NAME_REQUIRED = True
BASETHEME_BOOTSTRAP_VALIDATE_EMAIL_BEFORE_ACTIVATION = True

################################################################################
# EMAIL
################################################################################
EMAIL_HOST = config('EMAIL_HOST', default='smtp.pasteur.fr')
EMAIL_PORT = config('EMAIL_PORT', default='25')
EMAIL_USE_TLS = config('EMAIL_USE_TLS', default='True')
DEFAULT_FROM_EMAIL = config('DEFAULT_FROM_EMAIL', default='viralhostrangedb@pasteur.fr')
EMAIL_SUBJECT_PREFIX = config('DEFAULT_FROM_EMAIL', default='[ViralHostRangeDB]')
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

################################################################################
# Ownership transfer
################################################################################
MAX_MINUTES_CHECK_EMAIL = config('MAX_MINUTES_CHECK_EMAIL', default=5, cast=int)

################################################################################
# Uploading files
################################################################################
FILE_UPLOAD_HANDLERS = [
    "django.core.files.uploadhandler.MemoryFileUploadHandler",
    "django.core.files.uploadhandler.TemporaryFileUploadHandler",
]

################################################################################
# BioPython
################################################################################
Entrez.email = os.environ.get("NCBI_EMAIL", None)
Entrez.api_key = os.environ.get("NCBI_API_KEY", None)

################################################################################
# Backups
################################################################################
BACKUP_LOCATION = os.path.join(
    BASE_DIR,
    'persistent_volume',
    'data_source_backup',
)

BACKUP_MIN_OCCURRENCES_TO_KEEP = 20
BACKUP_MIN_WEEKS_TO_KEEP = int(18 * 52 / 12)

################################################################################
# Django tunning for this project
################################################################################
DATA_UPLOAD_MAX_NUMBER_FIELDS = 10000
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

################################################################################
# HUEY
################################################################################
# HUEY = {
#     'name': str(DATABASES['default']['NAME']),
#     'huey_class': 'huey.SqliteHuey',
#     'immediate': config('HUEY_IMMEDIATE', default=DEBUG, cast=bool),
#     'consumer': {
#         'blocking': True,
#         'loglevel': logging.DEBUG,
#         'workers': 1,
#         'scheduler_interval': 1,
#         'simple_log': True,
#     },
#     'filename': os.path.join(PERSISTENT_VOLUME, 'huey.sqlite3'),
# }

################################################################################
