"""viralhostrange URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.urls import path, include, re_path
from django.views.static import serve

from viralhostrange import settings
from viralhostrangedb import views
from viralhostrangedb.sitemap import my_sitemaps

urlpatterns = [
    path('', include('django_kubernetes_probes.urls')),
    path('', include('basetheme_bootstrap.urls')),
    path('', include('viralhostrangedb.urls')),
    path('api/', include('viralhostrangedb.urls_api')),
    # path('api/', include('webapi.urls')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('settings/', include("live_settings.urls")),
    path('', views.index, name='home'),
    path('sitemap.xml', sitemap, {'sitemaps': my_sitemaps},
         name='django.contrib.sitemaps.views.sitemap')
]

if not settings.DEBUG:
    urlpatterns.append(
        re_path(r'^static/(?P<path>.*)$', serve, {'document_root': settings.STATIC_ROOT})
    )
urlpatterns.append(
    re_path(r'^media/(?P<path>.*)$', views.xsendfile_serve)
)
