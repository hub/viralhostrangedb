```
docker build . -t test_vhrdb

docker run -v $(pwd)/persistent_volume:/code/persistent_volume -e "USE_SQLITE_AS_DB=True" -e "ALLOWED_HOSTS=localhost" -e "SECRET_KEY=*" -e "DEBUG=True" -e "STATIC_URL=/static" -e "MEDIA_URL=/media" -e "PROJECT_NAME=viralhostrange" -e "HUEY_IMMEDIATE=False" -p 8086:8086 test_vhrdb
```

To run tests:
```
docker run -v $(pwd)/persistent_volume:/code/persistent_volume -v ./test_data:/code/test_data -e "USE_SQLITE_AS_DB=True" -e "ALLOWED_HOSTS=localhost" -e "SECRET_KEY=*" -e "DEBUG=True" -e "STATIC_URL=/static" -e "MEDIA_URL=/media" -e "PROJECT_NAME=viralhostrange" -e "HUEY_IMMEDIATE=False" -p 8086:8086 test_vhrdb test
```

To start the/a db
```
docker run --name vhr-pg_db -e POSTGRES_PASSWORD=eee -e POSTGRES_DB=viralhostrangedb -e POSTGRES_USER=postgres -e LC_COLLATE=POSIX -p 5432:5432 -d postgres:16.2
```

## How to load a specific dump in PostgreSQL

Useful when upgrading PostgreSQL, or restoring the DB after cluster migration.

### get a dump
Get it from the CI as it is an artifact

### clean up
```
export CI_COMMIT_REF_SLUG="split-deploy"

# Stop web to prevent manage.py migrate
kubectl delete deployments -l branch=branch-$CI_COMMIT_REF_SLUG

# Stop the db, and delete its storage
kubectl delete statefulsets,pvc -l app.kubernetes.io/instance=db-$CI_COMMIT_REF_SLUG
```
Do keep the storage of the web container

# load the data from the dump
```
CI_COMMIT_REF_SLUG=$(git branch --show)
REL_NAME=$CI_COMMIT_REF_SLUG
dev_db=$(kubectl --namespace=$NAMESPACE get po -l "app.kubernetes.io/instance=${REL_NAME},app.kubernetes.io/name=postgresql" --output jsonpath='{.items[0].metadata.name}' || echo "")
echo $dev_db
POSTGRES_PASSWORD="$(kubectl get secret ${REL_NAME}-postgresql --output jsonpath='{.data.postgres-password}' | base64 -d)"
# echo $POSTGRES_PASSWORD
kubectl --namespace=$NAMESPACE exec -i $dev_db -- bash -c "PGPASSWORD=$POSTGRES_PASSWORD psql -U postgres vhr-pg-db" < prod.sql
```

# Restart

Re-run CI job deploy_web_*

# misc
echo "PGPASSWORD=\"$(kubectl get secret ${REL_NAME}-postgresql --output jsonpath='{.data.postgres-password}' | base64 -d)\" psql -Upostgres"