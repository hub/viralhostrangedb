#!/usr/bin/env bash

cd /code

. /home/kiwi/env.sh

echo "Starting dump at $(date)"

export DUMPS_DIR=/code/persistent_volume/dumps

if [ $(date +"%H") == "00" ]; then
  if [ $(date +"%d") == "01" ]; then
      SUFFIX=monthly
  else
      if [ $(date +"%u") == "5" ]; then
          SUFFIX=weekly
      else
          SUFFIX=daily
      fi
  fi
else
  SUFFIX=daily
fi

echo "DUMPS_DIR is $DUMPS_DIR"

mkdir -p $DUMPS_DIR

/usr/local/bin/python /code/manage.py dumpdata | gzip > ${DUMPS_DIR}/dump-db-$(date +"%Y-%m-%d--%H-%M").$SUFFIX.json.gz

echo "Dump done"

# Delete daily dump older than a month
find $DUMPS_DIR/ -type f -name 'dump-db-*.daily.json.gz' -mtime +30 -exec rm {} \;

# Delete weekly dump older than one year
find $DUMPS_DIR/ -type f -name 'dump-db-*.weekly.json.gz' -mtime +360 -exec rm {} \;

# Delete monthly dump older than two year
find $DUMPS_DIR/ -type f -name 'dump-db-*.monthly.json.gz' -mtime +720 -exec rm {} \;

echo "Flush completed"
