---
title: 'Viral Host Range database, a resource for virus-host interactions studies'
tags:
  - python
  - django
  - kubernetes
  - biology
  - virology
  - phage
  - bacteria
authors:
  - name: Bryan Brancotte
    orcid: 0000-0001-8669-5525
    affiliation: "1" # (Multiple affiliations must be quoted)
  - name: Quentin Lamy-Besnier
    orcid: 0000-0002-7141-6340
    affiliation: 2
  - name: Laurent Debarbieux
    orcid: 0000-0001-6875-5758
    affiliation: 2
  - name: Hervé Ménager
    orcid: 0000-0002-7552-1009
    affiliation: 1
affiliations:
 - name: Hub de Bioinformatique et Biostatistique - Département Biologie Computationnelle, Institut Pasteur, USR 3756 CNRS, Paris, France
   index: 1
 - name: Institut Pasteur, groupe interactions bactériophages-bactéries chez l'animal, département de microbiologie, 25, rue du Docteur Roux, 75015 Paris, France
   index: 2
date: 01 February 2020
bibliography: paper.bib
---

# Summary

How do I search for a virus infecting a given host, or the other way around. If answering this question is trivial for some human pathogenic viruses, it is more puzzling for viruses in general. The Viral Host Range database (VHRdb) gives access to the experimental data to answer this question. The aim of the VHRdb is to provide to the community the data generated by scientists documenting the host range of their favorite virus through an easy (copy/paste) contribution process including benefits. VHRdb also integrates analysis tools to compare and download tailored experimental datasets.

## What is stored

The database contains **responses**, a **response** being the status of the infection of an **host** by a **virus**. Responses are to be found in a three state scheme: **No infection**, **Intermediate** (if the infection was not conclusive), and **Infection**. A **data source** regroup a set of hosts and viruses and the responses for each pair of host and virus in this data source. A data source comes with meta data such as the life domain, the condition of the experiment, and who/where the experiment was conducted.

## Exploring the database


The interface to explore (see Fig. 1, or open [here](https://viralhostrangedb.pasteur.cloud/explore/?&ds=108&host=920,1467,591,981,1468,1469,1083,1216,1225,1226,1227,1228,1229,1230,1231,1232,1233,1234,1217,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1218,1245,1246,1247,1248,1249,1250,1251,1252,1253,1254,1219,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1220,1265,1266,1267,1268,1269,1270,1271,1272,1273,1274,1221,1275,1276,1277,1278,1279,1280,1281,1282,1283,1284,1222,1285,1223,1224,978,977,975,976,914,913,1470,1293,1471,1472,1294,1473,1474,1475,1476,1477,1478,1479,1480,1481&sort_rows=&sort_cols=desc&virus_infection_ratio=true&host_infection_ratio=true&only_published_data=true&life_domain=)) allows to select in (1) the data sources, hosts and viruses. The response are loaded on the fly in the 2D array that can be seen in the lower part. In "Advanced options" (3) multiple filtering, rendering, and analysis tool can be enabled. For example you can filter to keep only the data source associated to a publication (2). You also can show the infection ratio (2, 5) which helps users to rapidly identify a virus that have a large range of infection by computing the percentage of displayed hosts that have been infected. In Fig. 1 we chose to render infection ratio for both hosts and viruses (2), the infection ratios are displayed in (5), and we sorted the hosts by their infection ratio with (4) while the viruses order remains unchanged (6). 

![VHRdb explore interface](explore.png)

VHRdb is meant to encourage the diffusion of knowledge on host responses to virus. To this end we propose this "Explore" interface that can help scientist finding a virus infecting a given host, but also to share by a [link](https://viralhostrangedb.pasteur.cloud/explore/?&ds=108&host=920,1467,591,981,1468,1469,1083,1216,1225,1226,1227,1228,1229,1230,1231,1232,1233,1234,1217,1235,1236,1237,1238,1239,1240,1241,1242,1243,1244,1218,1245,1246,1247,1248,1249,1250,1251,1252,1253,1254,1219,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1220,1265,1266,1267,1268,1269,1270,1271,1272,1273,1274,1221,1275,1276,1277,1278,1279,1280,1281,1282,1283,1284,1222,1285,1223,1224,978,977,975,976,914,913,1470,1293,1471,1472,1294,1473,1474,1475,1476,1477,1478,1479,1480,1481&sort_rows=&sort_cols=desc&virus_infection_ratio=true&host_infection_ratio=true&only_published_data=true&life_domain=) (7) the combination of selection and analysis tool use she/he just used. A file with all this information can also be downloaded. 

Data source can be public, but also private with the ability to share it with named users. The respect of data sources privacy is of a paramount importance. To this end both the explore interface and the download functionality use the REST API to access to the responses. Thanks to this architecture and a well tested REST API, we are very confident in the respect of privacy of the data sources.

## Sharing data with the community

Easing the process of sharing a data source is crucial for the adoption of VHRdb by the virology community. We decomposed the submission of a new data source into multiple step (i.e a *wizard form* pattern). First, the user provide general information and indicate if the data source is public, if not the next step let him/her specify the list of user granted to see the data source. Then, the user is encourage to given an extensive description of the experiment (lab, experimenter, scoring method, ...). To import the data, they have to be stored in spreadsheet file (Excel, LibreOffice) following a specific, yet simple, format documented [here](http://hub.pages.pasteur.fr/viralhostrangedb/compatible_file.html). The user can either indicate that she/he has a file following the format, or can generate a tailored template file. To generate the template he/she provides the list of hosts and viruses for which she/he has responses. At the last step the template can be downloaded, filled by the user and re-uploaded.

Each data source can have different standard in recording the response of the infection of an host by a virus. For example one can use a two-state scheme (0/1), while the other one would use a continuous scale from 0 to 4 (as in the following example). In order to compare a data source to the others, the data source owner have to map the dedicated scheme of its data source to the global scheme.

![Mapping example with continuous responses](../doc/_static/mapping-float.png)


## Architecture, installation and integration

Viral Host Range database is a django based application. The continuous integration (CI) covering 99% of the Python code is run at each commit. The application is automatically deployed (CD) on a Kubernetes cluster managed by the *Institut Pasteur*.

## Related work

https://www.phage.ulaval.ca/fr/accueil/

[@10.1093/nar/gkx1124]

# Acknowledgements

We acknowledge Thomas Menard and the Direction des Service de l'Information of the Institut Pasteur in hosting the Kubernetes cluster, and their training in using this cluster.

# References
