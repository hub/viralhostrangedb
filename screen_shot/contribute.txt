https://viralhostrangedb.pasteur.cloud/data-source-contribution/Description/

Aspect:
#Zoom 175% en pleine écran
Zoom 400% en pleine écran

Ecran :
# 3840, échelle 200%
3440, échelle 100%

Modification page :
$($(".container")[0]).addClass("container-fluid").removeClass("container");
$("#id_Information-public").prop("checked",true);
$("#id_Information-name").val("My_new_data_source");
$("#div_id_Information-life_domain").closest("form").parent().removeClass().addClass("col-8");
$('<div id="div_id_Description-description" class="form-group"> <label for="id_Description-description" class="">Description</label> <div class=""> <textarea name="Description-description" cols="40" rows="2" class="textarea form-control" id="id_Description-description">Host range test performed on the d/m/y by X.Y (spotting serial dilution on plates).</textarea> <small id="hint_id_Description-description" class="form-text text-muted">Description of the data source</small> </div> </div>').insertAfter($("#div_id_Information-life_domain"));
